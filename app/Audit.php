<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model{
    public function auditUserName(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}