<?php
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Mykj\ListPegawai2;
use App\Models\Mykj\Peribadi;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsCatatan;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Mail;


class CommonController extends Controller{
    public function listing(){

    }

    public static function checkUserStat(){
        if(Auth::user()->id != 37){
            if(Auth::user()->user_profile->urussetia_active == 0 && Auth::user()->user_profile->kpa_active == 0){
                return Redirect::to('/disabled')->send();
            }
        }
    }

    public function pengguna_carian(Request $request){

        $data = [];
        $search_term = $request->input('q');
        $peribadi = ListPegawai2::where('nokp', 'ilike', '%'.$search_term.'%')
            ->orWhereRaw("LOWER(nama) ilike '%".$search_term."%'")->limit(10)->get();


        if(count($peribadi) != 0){
            foreach($peribadi as $p){
                $data[] = array(
                    'id' => $p->nokp,
                    'text' => html_entity_decode($p->nama, ENT_QUOTES | ENT_HTML5).' - '.$p->nokp
                );
            }
        }


        return response()->json([
            'data' => $data
        ]);
    }

    public function pengguna_maklumat(Request $request) : \Illuminate\Http\JsonResponse
    {
        $no_ic = $request->input('no_ic');
        try {
            $getMaklumat = ListPegawai2::getMaklumatPegawai($no_ic);
            return response()->json([
                'success' => 1,
                'data' => $getMaklumat
            ]);
        } catch(Exception $e) {
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public static function upload_image($photo, $folder){
        $filename = $photo->getClientOriginalName();
        $extension = $photo->getClientOriginalExtension();
        $image = str_replace(' ', '+', $photo);
        $imagename = str_random(10).'.'. $extension;
        $photo->move($folder, $imagename);

        return $imagename;
    }

    public static function cutAfterDot($number, $afterDot = 2){
        $a = $number * pow(10, $afterDot);
        $b = floor($a);
        $c = pow(10, $afterDot);
        return $b/$c ;
    }

    public static function mail_test(){
        $email = [
            'akiyamasensei555@gmail.com', 
            'Meor Nazrin bin M Meor Ahmad',
        ];

        Mail::send('segment/mail/newKPA', [], function($message) use ($email){
            $message->to($email[0], $email[1])
            ->subject('Permohonan Pertukaran Peranan Sistem 3PS');
            $message->from($email[0],'JKR 3PS');
        });
    }

    public function delete_duplicate_kpa_soalan(){
        $model = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor::where('projeks_tahuns_penilais_id', 201)->where('projeks_sub_penilaians_perkaras_id', '>=', 15059);

        foreach($model->get() as $m){
            ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsCatatan::where('projeks_tahuns_penilaians_sub_perkara_penilais_skor_id', $m->id)->delete();

            ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsUpload::where('projeks_tahuns_penilaians_sub_perkara_penilais_skors_id', $m->id)->delete();
        }

        $model->delete();
    }
}
