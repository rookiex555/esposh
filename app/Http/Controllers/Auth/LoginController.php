<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Mykj\ListPegawai2;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout', 'disabledUser');
    }

    public function authenticate(Request $request)
    {
		
        $validator = Validator::make($request->all(), array(
            'email' => 'required|numeric',
            'password' => 'required|string',
        ));
		
        //if ($validator->fails()) {
           // return redirect('login')
                //->withErrors($validator);
        //}
		//echo '</pre>';
		//print_r($request->all());
		//echo '</pre>';
		//die();
		
        $credentials = $request->only('email', 'password');

        $checkNokp = User::where('nokp', $credentials['email'])->first();

        /* if($checkNokp){
            $credentials['email'] = $checkNokp->email;
        }else{ */
		$getMaklumat = ListPegawai2::getMaklumatPegawai($credentials['email']);
		
		if(empty($getMaklumat)){ 
			//return view('loginerror');
			//return view('loginerror'); 
			//return redirect()
            //->route('wrong-user');
            //->withErrors(['Wrong Credentials']);
			echo "<script>";
			echo "alert('Harap Maaf, Sistem EASPOSH Adalah Hanya Untuk Kakitangan JKR Sahaja');";
			echo "window.location = 'https://easposh.jkr.gov.my/AGLogout';";
			echo "</script>";
			die();
		}
		
		
		$user = User::createOrUpdate($getMaklumat, true);
		$getPerson = User::where('nokp', $credentials['email'])->first();
		$credentials['email'] = $getPerson->email;
        //}

        if (Auth::attempt($credentials)) {
            if(Auth::user()->id != 37){
                if(Auth::user()->user_profile->urussetia_active == 1 || Auth::user()->user_profile->kpa_active == 1){
                    return redirect()->intended('/dashboard');
                }else{
                    return redirect()->intended('/disabled');
                }
            }
        }
    }

    public function disabledUser(){
        Auth::logout();
        return view('segment.errorpage.disabled');
    }

    public function logout(Request $request) {
		//Auth::guard('user')->logout();
        //Auth::logout();
		$this->guard()->logout();
		$request->session()->invalidate();
		
		echo "<script>";
		echo "window.location = 'https://easposh.jkr.gov.my/AGLogout';";
		echo "</script>";
		die();
        return redirect('/');
    }
	
	public function login_sso(){
        return redirect('/login');
    }
	
	public function landing(){
        return view('landing');
    }
	
	public function redirectToerror(){
		return view('loginerror');
	}
}
