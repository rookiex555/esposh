<?php
namespace App\Http\Controllers\Segment\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Projeks\Projek;
use Illuminate\Support\Facades\Auth;
use Entrust;
use App\Http\Controllers\Main\CommonController;

class DashboardController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index(){
        if(Entrust::hasRole('KPA')){
            return redirect('/kpa/penilaian');
        }else if(Entrust::hasRole('Urussetia')){
            return redirect('/urussetia/projek');
        }else if(Entrust::hasRole('Pentadbir')){
            return redirect('/admin/pentadbir/tetapan/projek');
        }

        return view('segment.dashboard.index');
    }

    public function get_chart_data_selesai(){
        $data = [];
        if(Entrust::hasRole('Urussetia')){
            $data = self::getUrussetiaData();
        }

//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        die();
        return response()->json([
            'data' => $data
        ]);
    }

    public static function getUrussetiaData(){
        $data = [];
        $countable = [];
        $projek = Projek::where('profiles_id', Auth::user()->user_profile->id)->get();
        $countable['s'] = 0;
        $countable['t_s'] = 0;
        $countable['d'] = 0;

        if(count($projek) > 0){
            foreach($projek as $p){
                $projek_tahun = $p->projek_Projek_tahun;
                if($projek_tahun){
                    foreach($projek_tahun as $pt){
                        if($pt->status == 0){
                            $countable['d'] += 1;
                        }else if($pt->status == 1){
                            $countable['t_s'] += 1;
                        }else if($pt->status == 2){
                            $countable['s'] += 1;
                        }
                    }
                }
            }
        }

        $data = [
            ['Draf', $countable['d']]  ,
            ['Tidak Selesai', $countable['t_s']]  ,
            ['Selesai', $countable['s']]  ,
        ];
        return $data;
    }
}
