<?php
namespace App\Http\Controllers\Segment\Kpa\Projek;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Segment\Urussetia\Projek\ProjekPenilaianController;
use App\Models\Projeks\ProjeksTahun;
use App\Models\Projeks\ProjeksTahunPenilai;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaian;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaiansPerkara;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsUpload;
use Illuminate\Http\Request;
use Illuminate\Queue\RedisQueue;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use App;
use App\Http\Controllers\Main\CommonController;
use PDF;

class ProjectKpaController extends Controller{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index(){
        return view('segment.kpa.projekkpa.index');
    }

    public function penilaian_list(){
        $model = ProjeksTahunPenilai::where(function ($query){
            $query->where('nokp', Auth::user()->nokp)->where('status', 1);
        })->orWhere(function($query){
            $query->where('nokp', Auth::user()->nokp)->where('status', 2);
        })->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-penilai-id' => function($data) {
                    return $data->id;
                },
                'data-projek-tahun-id' => function($data){
                    return $data->projek_tahun_penilai_Projek_tahun->id;
                }
            ])
            ->addColumn('projek_name', function($data){
                return strtoupper($data->projek_tahun_penilai_Projek_tahun->projek_tahun_projek->nama.' <b style="color: red">|</b> '.'<b>('.$data->projek_tahun_penilai_Projek_tahun->no_klausa.')</b>');
            })
            ->addColumn('tahun', function($data){
                return strtoupper($data->projek_tahun_penilai_Projek_tahun->projek_tahun_Tahun->tahun);
            })
            ->addColumn('tarikh_penilaian', function($data){
                return date("d-m-Y", strtotime($data->projek_tahun_penilai_Projek_tahun->tkh_penilaian));
            })
            ->addColumn('status_penilaian', function($data){
                $stat_text = '';
                $status = $data->projek_tahun_penilai_Projek_tahun->status;
                if($status == 1){
                    $stat_text = 'Dalam Proses';
                }else if($status == 2){
                    $stat_text = 'Selesai';
                }
                return strtoupper($stat_text);
            })
            ->rawColumns(['active', 'action', 'projek_name'])
            ->make(true);
    }

    public function penilaian_get_soalan($projek_tahun_id, $penilai_id){
        $data = self::getProgress($projek_tahun_id, $penilai_id);
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        die();
        return view('segment.kpa.projekkpa.projekkpasoalan.index', [
            'data' => $data,
            'penilai_id' => $penilai_id,
            'projek_tahun_id' => $projek_tahun_id,
        ]);
    }

    public static function getSubPen($penilaian_id, $penilai_id) : array{
        $data = [];

        $model = ProjeksTahunsPenilaiansSubPenilaian::where('projeks_tahuns_penilaians_id', $penilaian_id)->where('delete_id', 0)->orderBy('ref_id', 'asc')->get();

        if(count($model) > 0){
            foreach($model as $sub){
                $data[] = [
                    'id' => $sub->id,
                    'bil' => $sub->bil,
                    'tajuk_perkara' => $sub->tajuk_perkara,
                    'no_klausa' => $sub->no_klausa,
                    'perkara' => self::getSubPenPer($sub->id, $penilai_id)
                ];
            }
        }
        return $data;
    }

    public static function getSubPenPer($sub_id, $penilai_id) : array{
        $data = [];

        $model = ProjeksTahunsPenilaiansSubPenilaiansPerkara::where('projeks_tahuns_penilaians_sub_penilaians_id', $sub_id)->where('delete_id', 0)->orderBy('bil', 'asc')->get();

        if(count($model) > 0){
            foreach($model as $per){
                $data[] = [
                    'id' => $per->id,
                    'bil' => $per->bil,
                    'tajuk_perkara' => $per->tajuk_perkara,
                    'no_klausa' => $per->no_klausa,
                    'jawapan' => self::getSubPenPerAns($per->id, $penilai_id)
                ];
            }
        }

        return $data;
    }

    public static function getSubPenPerAns($perkara_id, $penilai_id){
        $model = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor::where('projeks_tahuns_penilais_id', $penilai_id)->where('projeks_sub_penilaians_perkaras_id', $perkara_id)->first();
        $data = [];

         if($model){
             $data = [
                 'skor' => $model->skor,
                  'catatan' => $model->projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_perkara_penilais_skors_catatan ? $model->projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_perkara_penilais_skors_catatan->catatan : '',
                 'uploads' => self::getSubPenPerAnsUploads($model)
             ];
         }

        return $data;
    }

    public static function getSubPenPerAnsUploads($skorModel){
        $model = $skorModel->projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_perkara_penilais_skors_uploads;
        $data = [];
        if(count($model) > 0){
            $ref_id = 1;
            foreach($model as $img){
                $data['img'.$ref_id] = [
                    url('/').$img->uploads_location.'/'.$img->uploads_name,
                    $img->uploads_name,
                    $img->id
                ];
                $ref_id++;
            }
        }

        return $data;
    }

    public function penilaian_jawab(Request $request){
        $model = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor::save_jawapan($request);
        return response()->json([
            'success' => 1,
            'data' => [
                'save_trigger' => $model
            ]
        ]);
    }

    public function penilaian_print($projeks_tahuns_id){
        $data = ProjekPenilaianController::penilaian_review_data($projeks_tahuns_id);

        $pdf = PDF::loadView('segment.urussetia.projek_laporan.keseluruhan_pdf', compact('data'), [], [
            'orientation' => 'L',
			'debug' => true,
			'showImageErrors' => true,
            'pdfaauto' => false
        ]);
        return $pdf->stream();
    }

    public function penilaian_padam_preview(Request $request){
        $upload_id = $request->input('upload_id');
        $model = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsUpload::find($upload_id);
        if($model){
            $model->uploads_name = null;
            $model->uploads_location = null;
            if($model->save()){
                return response()->json([
                    'success' => 1,
                    'data' => [
                        'id' => $upload_id
                    ]
                ]);
            }
        }
    }

    public function getProgress($projek_tahun_id, $penilai_id){
        $data = [];

        $projek_tahun = ProjeksTahun::find($projek_tahun_id);
        $data['projek_tahun'] = [
            'id' => $projek_tahun->id,
            'no_klausa' => $projek_tahun->no_klausa
        ];

        $penilaian = $projek_tahun->projek_tahun_Projek_tahun_penilaian;
        if($penilaian){
            $total_score = 0;
            foreach($penilaian as $pen){
                $data['pen_score'][$pen->id] = [
                    'id' => $pen->id,
                    'score' => 0,
                    'full_score' => 0
                ];

                $data['penilaian'][$pen->id] = [
                    'id' => $pen->id,
                    'bil' => $pen->bil,
                    'nama' => $pen->nama,
                    'peratus' => $pen->peratus_penilaian,
                    'sub_pen' => self::getSubPen($pen->id, $penilai_id),
                ];
            }

            foreach($data['penilaian'] as $getscore){
                $p_id = $getscore['id'];
                $peratus = $getscore['peratus'];
                $data['pen_score'][$p_id]['full_score'] = $peratus;
                $totalZero = 0;
                $totalHalf = 0;
                $totalFull = 0;
                $totalTb = 0;
                $totalQues = 0;

                foreach($getscore['sub_pen'] as $sp){
                    foreach($sp['perkara'] as $spp){
                        $totalQues++;
                        if(!empty($spp['jawapan'])){
                            switch ($spp['jawapan']['skor']) {
                                case '0':
                                    $totalZero += 1;
                                    break;
                                case '1':
                                    $totalHalf += 1;
                                    break;
                                case '2':
                                    $totalFull += 1;
                                    break;
                                case 'tb':
                                    $totalTb += 1;
                                    break;
                            }
                        }
                    }
                }

                $upper = ($totalZero * 0) + ($totalHalf * 0.5) + ($totalFull * 1);
                $below = $totalQues - $totalTb;
                $calculate = $upper == 0 && $below == 0 ? 0 : CommonController::cutAfterDot(($upper / $below) * $peratus);
                $data['pen_score'][$p_id]['score'] = $calculate;
            }
            $data['totalOverall'] = 0;
            foreach($data['pen_score'] as $ps){
                $data['totalOverall'] += $ps['score'];
            }
            $data['totalOverall'] = CommonController::cutAfterDot($data['totalOverall']);
        }else{
            $data['penilaian'] = [];
        }

        return $data;
    }

    public function penilaian_jawab_get(Request $request){
        $projek_tahun_id = $request->input('projek_tahun_id');
        $penilai_id = $request->input('penilai_id');

        $data = self::getProgress($projek_tahun_id, $penilai_id);
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function ubah_tkh(Request $request){
        $projek_tahun_id = $request->input('projek_tahun_id');
        $tkh_penilaian = $request->input('tkh_penilaian');

        $model = ProjeksTahun::find($projek_tahun_id);
        $model->tkh_penilaian = date("Y-m-d", strtotime($tkh_penilaian));

        if($model->save()){
            return response()->json([
               'success' => 1
            ]);
        }
    }
}
