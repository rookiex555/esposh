<?php
namespace App\Http\Controllers\Segment\Urussetia\Projek;

use App\Http\Controllers\Controller;
use App\Models\Projeks\Projek;
use App\Models\Regular\Tahun;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Main\CommonController;

class ProjekTahunanController extends Controller{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index($projek_id){
        $projek_name = Projek::select('nama')->where('id', $projek_id)->first();
        return view('segment.urussetia.projek_tahunan.index', [
            'projek_id' => $projek_id,
            'projek_nama' => $projek_name->nama
        ]);
    }

    public function projek_tahunan_list(){
        $model = Tahun::where('flag', 1)->where('delete_id', 0)->get();
        return DataTables::of($model)
            ->setRowAttr([
                'data-tahun-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('tahun', function($data){
                return $data->tahun;
            })
            ->addColumn('jumlah_penilaian', function($data){
                return 0;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }
}
