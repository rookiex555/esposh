<?php
namespace App\Http\Controllers\Segment\Urussetia\Projek;

use App\Http\Controllers\Controller;
use App\Models\Jawatan\JawatanPejabat;
use App\Models\Jawatan\JawatanPejabatsAlamat;
use App\Models\Jawatan\JawatansWakil;
use App\Models\Jawatan\JawatansWakilsCawangan;
use App\Models\Jawatan\JawatansWakilsCawangansPejabat;
use App\Models\Profiles\Jawatan;
use App\Models\Projeks\Projek;
use App\Models\Projeks\ProjeksTahun;
use App\Models\Projeks\ProjeksTahunPenilai;
use App\Models\Projeks\ProjeksTahunsPenilaian;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor;
use App\Models\Regular\Tahun;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Main\CommonController;
use DateTime;
use Mail;

class ProjekPenilaianController extends Controller{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index($projek_id, $tahun_id){
        $projek_name = Projek::select('nama')->where('id', $projek_id)->first();
        $tahun_name = Tahun::select('tahun')->where('id', $tahun_id)->first();
        $pengarah = JawatanPejabat::where('delete_id', 0)->where('flag', 1)->get();
        $pPejabat = JawatanPejabatsAlamat::where('delete_id', 0)->where('flag', 1)->get();
        $wakil = JawatansWakil::where('delete_id', 0)->where('flag', 1)->get();
        $wPejabat = JawatansWakilsCawangansPejabat::where('delete_id', 0)->where('flag', 1)->get();

        if($tahun_name->tahun < date('Y')){
            $tahun_pass = 1;
        }else{
            $tahun_pass = 0;
        }

        return view('segment.urussetia.projek_penilaian.index', [
            'projek_name' => $projek_name->nama,
            'tahun_name' => $tahun_name->tahun,
            'projek_id' => $projek_id,
            'tahun_id' => $tahun_id,
            'pengarah' => $pengarah,
            'pPejabat' => $pPejabat,
            'wakil' => $wakil,
            'wPejabat' => $wPejabat,
            'passYear' => $tahun_pass
        ]);
    }

    public function projek_penilaian_list($projek_id, $tahun_id){
        $model = ProjeksTahun::where('projeks_id', $projek_id)->where('tahuns_id', $tahun_id)->where('delete_id', 0)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-projek-penilaian-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('no_klausa', function($data){
                return $data->no_klausa;
            })
            ->addColumn('penilai', function($data){
                return $data->projek_tahun_Projek_tahun_penilai->nama;
            })
            ->addColumn('tkh_penilaian', function($data){
                return date('d-m-Y', strtotime($data->tkh_penilaian));
            })
            ->addColumn('status_penilaian', function($data){
                $status = '';
                $color = '';
                switch ($data->status){
                    case 0:
                        $status = 'Draf';
                        $color = 'blue';
                        break;
                    case 1:
                        $status = 'Dalam Proses Penilaian';
                        $color = 'orange';
                        break;
                    case 2:
                        $status = 'Penilaian Selesai';
                        $color = 'green';
                        break;
                }

                return '<b style="color: '.$color.'">'.strtoupper($status).'</b>';
            })
            ->rawColumns(['status_penilaian','active', 'action'])
            ->make(true);
    }

    public function projek_penilaian_tambah(Request $request){
        try {
            $projek = ProjeksTahun::createOrUpdate($request);

            return response()->json($projek);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function get_projek_penilaian(Request $request){
        $projek_penilaian_id = $request->input('projek_penilaian_id');
        $model = ProjeksTahun::getProjek($projek_penilaian_id);

        return response()->json([
            'success' => 1,
            'data' => $model
        ]);
    }

    public function projek_penilaian_aktif(Request $request){
        $projek_penilaian_id = $request->input('projek_penilaian_id');
        $process = $this->aktif_delete($projek_penilaian_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'projek_penilaian_id' => $process['projek_penilaian_id'],
                'flag' => $process['flag']
            ]
        ]);
    }


    public function projek_penilaian_delete(Request $request){
        $projek_penilaian_id = $request->input('projek_penilaian_id');
        $process = $this->aktif_delete($projek_penilaian_id, 2);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'projek_penilaian_id' => $process['projek_penilaian_id'],
            ]
        ]);
    }

    public function aktif_delete($projek_penilaian_id, $trigger) : array{
        $model = ProjeksTahun::find($projek_penilaian_id);
        if($trigger == 1){
            $model->flag = $model->flag == 1 ? 0 : 1;
        }else{
            $model->delete_id = 1;
        }

        return [
            'success' => $model->save() ? 1 : 0,
            'projek_penilaian_id' => $model->id,
            'flag' => $model->flag,
        ];
    }

    public function projek_penilaian_hantar(Request $request){
        $penilaian_id = $request->input('penilaian_id');
        $model = ProjeksTahun::find($penilaian_id);
        $soalan = self::checkPenilaianAvailableSend($model);

        if(!$soalan){
            return response()->json([
                'success' => 2,
            ]);
        }
        $model->status = 1;
        if($model->save()){
            $penilai = ProjeksTahunPenilai::where('projeks_tahuns_id', $model->id)->orderBy('id', 'desc')->first();
            $penilai->status = 1;
            $penilai->save();

            $email = [
                'meor.nazrin1994@gmail.com', 
                $penilai->nama,
            ];

            $data = [
                'penilai' => $penilai->nama,
                'nama' => $penilai->projek_tahun_penilai_Projek_tahun->projek_tahun_projek->nama,
                'tkh_penilaian' => $penilai->projek_tahun_penilai_Projek_tahun->tkh_penilaian,
                'no_klausa' => $penilai->projek_tahun_penilai_Projek_tahun->no_klausa
            ];
    
            Mail::send('segment/mail/newKPAproject', $data, function($message) use ($email){
                $message->from('kualiti@jkr.gov.my', 'EASPOSH JKR');
                $message->to($email[0], $email[1])
                ->subject('Pelantikan '.$email[1].' Sebagai Ketua Penilai Audit');
                $message->from($email[0],'EASPOSH');
            });

            return response()->json([
                'success' => 1,
            ]);
        }
    }

    public static function checkPenilaianAvailableSend(ProjeksTahun $projeksTahun) : bool{
        $pass = 0;
        $data = true;
        $penilaian = $projeksTahun->projek_tahun_Projek_tahun_penilaian;
        if(count($penilaian) > 0){
           foreach($penilaian as $p){
               $sub = $p->projek_tahun_penilaian_Projek_tahuns_penilaians_sub_penilaians;
               if(count($sub) > 0){
                   foreach($sub as $s){
                       $perkara = $s->projek_tahuns_penilaians_sub_penilaians_Projek_tahuns_penilaians_sub_penilaians_perkara;
                       if(count($perkara) > 0){
                           $data = true;
                       }else{
                           $pass++;
                       }
                   }
               }else{
                   $pass++;
               }
           }
        }else{

            $pass++;
        }
        return $pass > 0 ? 0 : 1;
    }

    public function projek_penilaian_preview($projek_tahun_id){
        
        $data = self::penilaian_review_data($projek_tahun_id);
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        die();
        return view('segment.urussetia.projek_penilaian.review',[
            'data' => $data
        ]);
    }

    public static function penilaian_review_data($projek_tahun_id){
        $data = [];
        $projek_tahun = ProjeksTahun::find($projek_tahun_id);
        $projek = $projek_tahun->projek_tahun_projek;
        $projek_penilai = $projek_tahun->projek_tahun_Projek_tahun_penilai->id;

        $data['projek'] = [
            'nama' => $projek->nama,
            'tahun' => $projek_tahun->projek_tahun_Tahun->tahun
        ];

        $d1 = new DateTime($projek_tahun->tkh_milik_tpk);
        $d2 = new DateTime($projek_tahun->tkh_milik_tpk);
        $d3 = new DateTime($projek_tahun->tkh_siap_asal);
        $d4 = new DateTime($projek_tahun->tkh_siap_semasa);
        $d5 = new DateTime($projek_tahun->tkh_penilaian);

        $data['projek_tahun'] = [
            'pengarah' => $projek_tahun->projek_tahun_Projek_tahun_pengarah->projek_tahun_pengarah_jawatan->name,
            'pengarah_alamat' => $projek_tahun->projek_tahun_Projek_tahun_pengarah->projek_tahun_pengarah_jawatan_pejabat->name,
            'wakil' => $projek_tahun->projek_tahun_Projek_tahun_wakil->projek_tahun_wakil_jawatan->name,
            'wakil_alamat' => $projek_tahun->projek_tahun_Projek_tahun_wakil->projek_tahun_wakil_jawatan_alamat->jawatanAlamat->name.', '.$projek_tahun->projek_tahun_Projek_tahun_wakil->projek_tahun_wakil_jawatan_alamat->name,
            'tkh_milik' => $d1->format('d-m-Y'),
             // $projek_tahun->tkh_milik_tpk,
            'tkh_milik_tpk' => $d2->format('d-m-Y'),
            //$projek_tahun->tkh_milik_tpk,
            'tkh_siap_asal' => $d3->format('d-m-Y'),
            //$projek_tahun->tkh_siap_asal,
            'tkh_siap_semasa' => $d4->format('d-m-Y'),
            //$projek_tahun->tkh_siap_semasa,
            'tkh_penilaian' => $d5->format('d-m-Y'),
            //$projek_tahun->tkh_penilaian,
            'no_klausa' => $projek_tahun->no_klausa,
            'kemajuan_fizikal' => $projek_tahun->kemajuan_penilaian.'%',
            'penilai' => [
                'nama' => $projek_tahun->projek_tahun_Projek_tahun_penilai->nama,
                'nokp' => $projek_tahun->projek_tahun_Projek_tahun_penilai->nokp
            ],
            'status' => $projek_tahun->status
        ];

        $projek_penilaian = $projek_tahun->projek_tahun_Projek_tahun_penilaian;
        if(count($projek_penilaian) > 0){
            foreach($projek_penilaian as $pen){
                $data['soalan_jawapan'][$pen->id] = [
                    'nama' => $pen->nama,
                    'peratus' => $pen->peratus_penilaian,
                    'bil' => $pen->bil,
                    'sub' => []
                ];
                $projek_sub = $pen->projek_tahun_penilaian_Projek_tahuns_penilaians_sub_penilaians;
                foreach ($projek_sub as $subpen){
                    $data['soalan_jawapan'][$pen->id]['sub'][$subpen->id] = [
                        'bil' => $subpen->bil,
                        'no_klausa' => $subpen->no_klausa,
                        'nama' => $subpen->tajuk_perkara,
                        'jawapan_skor' => []
                    ];
                    $projek_perkara = $subpen->projek_tahuns_penilaians_sub_penilaians_Projek_tahuns_penilaians_sub_penilaians_perkara;
                    foreach($projek_perkara as $subpenper){
                        $perkara_id = $subpenper->id;
                        $skor = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor::where('projeks_tahuns_penilais_id', $projek_penilai)->where('projeks_sub_penilaians_perkaras_id', $perkara_id)->first();

                        $data['soalan_jawapan'][$pen->id]['sub'][$subpen->id]['jawapan_skor'][$subpenper->id] = [
                            'bil' => $subpenper->bil,
                            'nama' => $subpenper->tajuk_perkara,
                            'no_klausa' => $subpenper->no_klausa,
                            'skor' => $skor ? $skor->skor : 99,
                            'catatan' => $skor ? $skor->projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_perkara_penilais_skors_catatan->catatan : '',
                            'uploads' => []
                        ];

                        if($skor){
                            foreach($skor->projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_perkara_penilais_skors_uploads as $uploads){
                                if($uploads->uploads_location && $uploads->uploads_name){
                                    $data['soalan_jawapan'][$pen->id]['sub'][$subpen->id]['jawapan_skor'][$subpenper->id]['uploads'][] = [
                                        'name' => $uploads->uploads_name,
                                        'link' => $uploads->uploads_location.'/'.$uploads->uploads_name
                                    ];
                                }
                            }
                        }else{
                            $data['soalan_jawapan'][$pen->id]['sub'][$subpen->id]['jawapan_skor'][$subpenper->id]['uploads'];
                        }
                    }
                }

                $withScore = self::getSectorScore($data);
                $data = self::getAllSectorScore($withScore);
//                echo '<pre>';
//                print_r($data);
//                echo '</pre>';
//                die();
            }
        }
        return $data;
    }

    public static function getSectorScore($data){
        if(count($data['soalan_jawapan']) > 0){
            foreach($data['soalan_jawapan'] as $penId => $pen){
                if(count($pen['sub']) > 0){
                    $data['soalan_jawapan'][$penId]['totalZero'] = 0;
                    $data['soalan_jawapan'][$penId]['totalHalf'] = 0;
                    $data['soalan_jawapan'][$penId]['totalFull'] = 0;
                    $data['soalan_jawapan'][$penId]['totalTB'] = 0;
                    $data['soalan_jawapan'][$penId]['totalQuestion'] = 0;
                    $data['soalan_jawapan'][$penId]['totalOverall'] = 0;

                    foreach($pen['sub'] as $subpen){
                        if(count($subpen['jawapan_skor']) > 0){
                            foreach($subpen['jawapan_skor'] as $subpenper){
                                $data['soalan_jawapan'][$penId]['totalQuestion'] += 1;
                                $skor = $subpenper['skor'];

                                if($skor != '' || $skor != null){
                                    switch ($skor){
                                        case '0':
                                            $data['soalan_jawapan'][$penId]['totalZero'] += 1;
                                            break;
                                        case '1':
                                            $data['soalan_jawapan'][$penId]['totalHalf'] += 1;
                                            break;
                                        case '2':
                                            $data['soalan_jawapan'][$penId]['totalFull'] += 1;
                                            break;
                                        case 'tb':
                                            $data['soalan_jawapan'][$penId]['totalTB'] += 1;
                                            break;
                                    }
                                }
                            }
                        }else{
                            $data['soalan_jawapan'][$penId]['totalZero'] += 0;
                            $data['soalan_jawapan'][$penId]['totalHalf'] += 0;
                            $data['soalan_jawapan'][$penId]['totalFull'] += 0;
                            $data['soalan_jawapan'][$penId]['totalTB'] += 0;
                        }
                    }

                    $upper = ($data['soalan_jawapan'][$penId]['totalZero'] * 0) + ($data['soalan_jawapan'][$penId]['totalHalf'] * 0.5) + ($data['soalan_jawapan'][$penId]['totalFull'] * 1);
                    $below = $data['soalan_jawapan'][$penId]['totalQuestion'] - $data['soalan_jawapan'][$penId]['totalTB'];
                    $calculate = $upper == 0 && $below == 0 ? 0 : CommonController::cutAfterDot(($upper / $below) * str_replace('%', '', $pen['peratus']));
                    $data['soalan_jawapan'][$penId]['totalOverall'] = $calculate;
                }
            }
        }

        return $data;
    }

    public static function getAllSectorScore($data){
        $total = 0;
        $data['projek_tahun']['overallSector'] = 0;

        if(isset($data['soalan_jawapan'])){
            if(count($data['soalan_jawapan']) > 0){
                foreach($data['soalan_jawapan'] as $pen){
                    $total += isset($pen['totalOverall']) ? $pen['totalOverall'] : 0;
                }
                $data['projek_tahun']['overallSector'] = $total;
                
                if($total != 0){
                    switch ($total){
                        case $total >=90 && $total <=100:
                            $data['projek_tahun']['grade'] = 'A';
                            break;
                        case $total >=70 && $total <=90:
                            $data['projek_tahun']['grade'] = 'B';
                            break;
                        case $total >=50 && $total <=70:
                            $data['projek_tahun']['grade'] = 'C';
                            break;
                        case $total >=30 && $total <=50:
                            $data['projek_tahun']['grade'] = 'D';
                            break;
                        case $total >=0 && $total <=30:
                            $data['projek_tahun']['grade'] = 'E';
                            break;
                        case $total == 0:
                            $data['projek_tahun']['grade'] = '-';
                            break;
                    }
                }else{
                    $data['projek_tahun']['grade'] = '-';
                }
            }
        }

        return $data;
    }
}
