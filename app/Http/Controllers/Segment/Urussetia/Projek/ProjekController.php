<?php
namespace App\Http\Controllers\Segment\Urussetia\Projek;

use App\Http\Controllers\Controller;
use App\Models\Jawatan\ProjekList;
use App\Models\Projeks\Projek;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Main\CommonController;

class ProjekController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index(){
        $projek_list = ProjekList::where('flag', 1)->where('delete_id', 0)->get();
        return view('segment.urussetia.projek.index', [
            'projek_list' => $projek_list
        ]);
    }

    public function projek_list(){
        $model = Projek::where('delete_id', 0)->where('profiles_id', Auth::user()->user_profile->id)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-projek-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('projek_name', function($data){
                return strtoupper($data->nama);
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function projek_tambah(Request $request){
        try {
            $projek = Projek::createOrUpdate($request);

            return response()->json($projek);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function get_projek(Request $request){
        $projek_id = $request->input('projek_id');
        $model = Projek::getProjek($projek_id);

        return response()->json([
            'success' => 1,
            'data' => $model
        ]);
    }

    // public function projek_duplicate(Request $request){
       // $projek_id = $request->input('projek_id');
        // $model = Projek::duplicate($projek_id);

        // return response()->json([
           //  'success' => 1,
           //  'data' => $model
       // ]);
    //}

    public function projek_aktif(Request $request){
        $projek_id = $request->input('projek_id');
        $process = $this->aktif_delete($projek_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'projek_id' => $process['projek_id'],
                'flag' => $process['flag']
            ]
        ]);
    }

    public function projek_delete(Request $request){
        $projek_id = $request->input('projek_id');
        $process = $this->aktif_delete($projek_id, 2);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'projek_id' => $process['projek_id'],
            ]
        ]);
    }

    public function aktif_delete($projek_id, $trigger) : array{
        $model = Projek::find($projek_id);
        if($trigger == 1){
            $model->flag = $model->flag == 1 ? 0 : 1;
        }else{
            $model->delete_id = 1;
        }

        return [
            'success' => $model->save() ? 1 : 0,
            'projek_id' => $model->id,
            'flag' => $model->flag,
        ];
    }
}
