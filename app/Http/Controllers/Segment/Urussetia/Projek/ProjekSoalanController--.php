<?php
namespace App\Http\Controllers\Segment\Urussetia\Projek;

use App\Http\Controllers\Controller;
use App\Models\Projeks\ProjeksTahun;
use App\Models\Projeks\ProjeksTahunsPenilaian;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaian;
use App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaiansPerkara;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Main\CommonController;

class ProjekSoalanController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index($projek_tahun_id){
        $projek_tahun = ProjeksTahun::find($projek_tahun_id);
        $projek_name = $projek_tahun->projek_tahun_projek->nama;
        $tahun = $projek_tahun->projek_tahun_Tahun->tahun;
        $tahun_id = $tahun = $projek_tahun->projek_tahun_Tahun->id;
        $getPenilaianData = ProjeksTahunsPenilaian::getAllData($projek_tahun_id);

        $getProjeksList = DB::select('
        Select p.id as projek_id, p.nama as projek, t.tahun, ptp2.nama, pt.no_klausa, pt.id as projek_tahun_id from projeks p
            left join projeks_tahuns pt on p.id = pt.projeks_id
            left join tahuns t on t.id = pt.tahuns_id
            left join projeks_tahun_penilais ptp2 on pt.id = ptp2.projeks_tahuns_id
        where pt.id != '.$projek_tahun_id.'
        AND pt.status != 0
        AND (p.flag = 1 and p.delete_id = 0)
        AND (pt.flag = 1 and pt.delete_id = 0)
        ');


        return view('segment.urussetia.projek_soalan.index', [
            'projek_tahun_id' => $projek_tahun_id,
            'no_klausa' => $projek_tahun->no_klausa,
            'tahun_name' => $tahun,
            'tahun_id' => $tahun_id,
            'projek_name' => $projek_name,
            'data' => $getPenilaianData,
            'projekdropdown' => $getProjeksList,
        ]);
    }

    public function projek_penilaian_soalan_main_tambah(Request $request){

        try {
            $process = ProjeksTahunsPenilaian::createOrUpdate($request);

            return response()->json($process);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function projek_penilaian_soalan_main_get_soalan(Request $request){
        $projek_penilaian_id = $request->input('projek_penilaian_soalan_main_id');
        $model = ProjeksTahunsPenilaian::find($projek_penilaian_id);
        return response()->json([
            'success' => 1,
            'data' => [
                'nama' => $model->nama,
                'bil' => $model->bil,
                'percentage' => $model->peratus_penilaian
            ]
        ]);
    }

    public function projek_penilaian_soalan_main_delete(Request $request) : array{
        $model = ProjeksTahunsPenilaian::find($request->input('projek_penilaian_id'));
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
            'data' => [
                'projek_penilaian_main_id' => $model->id,
            ]
        ];
    }

    public function projek_penilaian_soalan_sub_list($projek_penilaian_soalan_main_id){
        $model = ProjeksTahunsPenilaiansSubPenilaian::where('projeks_tahuns_penilaians_id', $projek_penilaian_soalan_main_id)->where('delete_id', 0)->orderBy('ref_id', 'asc')->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-projek-penilaian-soalan-main-sub-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('tajuk_perkara', function($data){
                return $data->tajuk_perkara;
            })
            ->addColumn('no_klausa', function($data){
                return $data->bil;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function projek_penilaian_soalan_main_sub_tambah(Request $request){

        try {
            $process = ProjeksTahunsPenilaiansSubPenilaian::createOrUpdate($request);

            return response()->json($process);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function projek_penilaian_soalan_main_sub_get_soalan(Request $request){
        $projek_penilaian_soalan_main_sub_id = $request->input('projek_penilaian_soalan_main_sub_id');
        $model = ProjeksTahunsPenilaiansSubPenilaian::find($projek_penilaian_soalan_main_sub_id);
        return response()->json([
            'success' => 1,
            'data' => [
                'bil' => $model->bil,
                'no_klausa' => $model->no_klausa,
                'tajuk_perkara' => $model->tajuk_perkara
            ]
        ]);
    }

    public function projek_penilaian_soalan_main_sub_delete(Request $request){
        $model = ProjeksTahunsPenilaiansSubPenilaian::find($request->input('projek_penilaian_soalan_main_sub_id'));
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
            'data' => [
                'projek_penilaian_soalan_main_sub_id' => $model->id,
                'projeks_tahuns_penilaians_id' => $model->projeks_tahuns_penilaians_id,
            ]
        ];
    }

    public function projek_penilaian_soalan_main_sub_perkara_tambah(Request $request){

        try {
            $process = ProjeksTahunsPenilaiansSubPenilaiansPerkara::createOrUpdate($request);

            return response()->json($process);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function projek_penilaian_soalan_main_sub_perkara_get_soalan(Request $request){
        $projek_penilaian_soalan_main_sub_perkara_id = $request->input('projek_penilaian_soalan_main_sub_perkara_id');
        $model = ProjeksTahunsPenilaiansSubPenilaiansPerkara::find($projek_penilaian_soalan_main_sub_perkara_id);

        return response()->json([
            'success' => 1,
            'data' => [
                'id' => $model->id,
                'bil' => $model->bil,
                'tajuk_perkara' => $model->tajuk_perkara,
                'no_klausa' => $model->no_klausa,
                'ref_id' => $model->ref_id,
                'sub_id' => $model->projeks_tahuns_penilaians_sub_penilaians_id,
                'main_id' => $model->projek_tahuns_penilaians_sub_penilaians_perkara_Projek_tahuns_penilaians_sub_penilaians->projek_tahuns_penilaians_sub_penilaians_Projek_tahun_penilaian->id
            ]
        ]);
    }

    public function projek_penilaian_soalan_main_sub_perkara_delete(Request $request){
        $model = ProjeksTahunsPenilaiansSubPenilaiansPerkara::find($request->input('projek_penilaian_soalan_main_sub_perkara_id'));
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
            'data' => [
                'id' => $model->id,
                'sub_id' => $model->projeks_tahuns_penilaians_sub_penilaians_id,
            ]
        ];
    }

    public function projek_penilaian_soalan_duplicate(Request $request){
        $projek_projek_tahun = $request->input('pilihan');
        $selected_projek_tahun_id = $request->input('selected_projek_tahun_id');
        $split_id = explode('/', $projek_projek_tahun);

        $projek_id = $split_id[0];
        $projek_tahun_id = $split_id[1];

        $projek_tahun = ProjeksTahun::find($projek_tahun_id);

        if($projek_tahun->projek_tahun_Projek_tahun_penilaian){
            foreach($projek_tahun->projek_tahun_Projek_tahun_penilaian as $first_level){
                $projek_tahun_penilaian_main = new ProjeksTahunsPenilaian;
                $projek_tahun_penilaian_main->flag = 1;
                $projek_tahun_penilaian_main->delete_id = 0;
                $projek_tahun_penilaian_main->projeks_tahuns_id = $selected_projek_tahun_id;
                $projek_tahun_penilaian_main->ref_id = ProjeksTahunsPenilaian::getNewRef($selected_projek_tahun_id);
                $projek_tahun_penilaian_main->nama = $first_level->nama;
                $projek_tahun_penilaian_main->peratus_penilaian = $first_level->peratus_penilaian;
                $projek_tahun_penilaian_main->save();

                $projek_sub = ProjeksTahunsPenilaiansSubPenilaian::where('projeks_tahuns_penilaians_id', $first_level->id)->where('delete_id', 0)->get();
                if(count($projek_sub) > 0){
                    foreach($projek_sub as $second_level){
                        $projek_tahun_penilaian_main_sub = new ProjeksTahunsPenilaiansSubPenilaian();
                        $projek_tahun_penilaian_main_sub->flag = 1;
                        $projek_tahun_penilaian_main_sub->delete_id = 0;
                        $projek_tahun_penilaian_main_sub->projeks_tahuns_penilaians_id = $projek_tahun_penilaian_main->id;
                        $projek_tahun_penilaian_main_sub->ref_id = ProjeksTahunsPenilaiansSubPenilaian::getNewRef($projek_tahun_penilaian_main->id);
                        $projek_tahun_penilaian_main_sub->bil = $second_level->bil;
                        $projek_tahun_penilaian_main_sub->tajuk_perkara = $second_level->tajuk_perkara;
                        $projek_tahun_penilaian_main_sub->no_klausa = $second_level->no_klausa;
                        $projek_tahun_penilaian_main_sub->save();

                        $projek_sub_perkara = ProjeksTahunsPenilaiansSubPenilaiansPerkara::where('projeks_tahuns_penilaians_sub_penilaians_id', $second_level->id)->where('delete_id', 0)->get();

                        if(count($projek_sub_perkara) > 0){
                            foreach($projek_sub_perkara as $third_level){
                                $projek_tahun_penilaian_main_sub_perkara = new ProjeksTahunsPenilaiansSubPenilaiansPerkara;
                                $projek_tahun_penilaian_main_sub_perkara->flag = 1;
                                $projek_tahun_penilaian_main_sub_perkara->delete_id = 0;
                                $projek_tahun_penilaian_main_sub_perkara->projeks_tahuns_penilaians_sub_penilaians_id = $projek_tahun_penilaian_main_sub->id;
                                $projek_tahun_penilaian_main_sub_perkara->ref_id = ProjeksTahunsPenilaiansSubPenilaiansPerkara::getNewRef($projek_tahun_penilaian_main_sub->id);
                                $projek_tahun_penilaian_main_sub_perkara->bil = $third_level->bil;
                                $projek_tahun_penilaian_main_sub_perkara->no_klausa = $third_level->no_klausa;
                                $projek_tahun_penilaian_main_sub_perkara->tajuk_perkara = $third_level->tajuk_perkara;
                                $projek_tahun_penilaian_main_sub_perkara->save();
                            }
                        }
                    }
                }
            }
            return response()->json([
                'success' =>1,
            ]);
        }
    }

    public function projek_penilaian_soalan_preview($projek_tahun_id){
        $data = ProjekPenilaianController::penilaian_review_data($projek_tahun_id);

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('segment.urussetia.projek_soalan.review', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->stream();

//        $pdf = PDF::loadView('segment.urussetia.projek_soalan.review', compact('data'));
//        return $pdf->download('invoice.pdf');
    }
}
