<?php
namespace App\Http\Controllers\Segment\Urussetia\Projek;

use App\Http\Controllers\Controller;
use App\Models\Projeks\Projek;
use App\Models\Projeks\ProjeksTahun;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Main\CommonController;
use App\Models\Regular\Tahun;
use Illuminate\Support\Facades\App;
use PDF;
use Svg\Tag\Rect;
use Entrust;

class ProjekLaporanController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index(){
        $projek = Projek::where('profiles_id', Auth::user()->user_profile->id)->where('flag', 1)->where('delete_id', 0)->get();
        $tahun = Tahun::all();

        return view('segment.urussetia.projek_laporan.index',[
            'projek' => $projek,
            'tahun' => $tahun
        ]);
    }

    public function getPenilaianKeputusan(Request $request){

        $projek_id = $request->input('projek_id');
        $tahun = $request->input('tahun');
    
        $data = [];
        $projek_tahun = ProjeksTahun::where('projeks_id', $projek_id)->where('tahuns_id', $tahun)->where('status', 2)->get();

        if(count($projek_tahun) > 0){
            foreach($projek_tahun as $pt){
                $data[] = [
                    'nama' => $pt->projek_tahun_projek->nama,
                    'tahun' => $pt->projek_tahun_Tahun->tahun,
                    'penilai' => $pt->projek_tahun_Projek_tahun_penilai->nama,
                    'keputusan' => self::getKeputusan($pt)
                ];
            }
        }

//         echo '<pre>';
//         print_r($data);
//         echo '</pre>';
//         die();
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public static function getKeputusan(ProjeksTahun $projek_tahun){
        $overall = ProjekPenilaianController::penilaian_review_data($projek_tahun->id);
        
        $data = [];
       
        foreach($overall['soalan_jawapan'] as $sj){
            $data['info'][] = [
                'nama_penilaian' => $sj['nama'],
                'peratus_penilaian' => $sj['peratus'],
                'peratus' => $sj['totalOverall'],
            ];
        }

        $data['markah_keseluruhan'] = round($overall['projek_tahun']['overallSector'],2 );
        $data['gred'] = $overall['projek_tahun']['grade'];
        $data['penilaian_count'] = count($data['info']);
        $data['no_klausa'] = $overall['projek_tahun']['no_klausa'];
        $data['projek_tahun_id'] = $projek_tahun->id;
        
        return $data;
    }

    public function keseluruhan(Request $request){

        $tahun = Tahun::all();

        if($request->has('tahun_select')){
            $pass = 1;
        }else{
            $pass = 0;
        }
        
        $projek = Projek::where('profiles_id', Auth::user()->user_profile->id)->where('flag', 1)->where('delete_id', 0)->get();
        $data = [];
        if(count($projek) > 0){
            foreach($projek as $pt){
                if($pass == 1){
                    $ptProjek = ProjeksTahun::where('projeks_id', $pt->id)->where('tahuns_id', $request->input('tahun_select'))->where('status', 2)->get();
                }else{
                    $ptProjek = $pt->projek_Projek_tahun_selesai;
                }
                
                if($ptProjek){
                    foreach($ptProjek as $ptp){
                        if($ptp->status == 2){
                            $data[$pt->nama]['keputusan'][] = self::getKeputusan($ptp);
                        }
                    }
                }
            }
        }
    //    echo '<pre>';
    //    print_r($data);
    //    echo '</pre>';
    //    die();
        return view('segment.urussetia.projek_laporan.keseluruhan',[
            'data' => $data,
            'tahun' => $tahun
        ]);
    }

    public function keseluruhan_pdf($id){
        $data = ProjekPenilaianController::penilaian_review_data($id);
      
        // return view('segment.urussetia.projek_laporan.keseluruhan_pdf', compact('data'));
        //
        // $pdf = App::make('dompdf.wrapper');
        $pdf = PDF::loadView('segment.urussetia.projek_laporan.keseluruhan_pdf', compact('data'), [],  [
            'orientation' => 'L',
            'pdfaauto' => false
        ]);
        // $pdf->loadView('segment.urussetia.projek_laporan.keseluruhan_pdf', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function keseluruhan_excel($id){
        $data = ProjekPenilaianController::penilaian_review_data($id);
        return view('segment.urussetia.projek_laporan.keseluruhan_excel', compact('data'));
        
    }

    public function pejabat(Request $request){
        $tahun = Tahun::all();
        $projek = Projek::where('profiles_id', Auth::user()->user_profile->id)->where('flag', 1)->where('delete_id', 0)->get();
        $data = [];
        
        if($request->has('tahun_select')){
            $pass = 1;
        }else{
            $pass = 0;
        }

        if(count($projek) > 0){
            foreach($projek as $p){
                if($pass == 1){
                    $projek_tahun = ProjeksTahun::where('projeks_id', $p->id)->where('tahuns_id', $request->input('tahun_select'))->where('status', 2)->get();
                }else{
                    $projek_tahun = $p->projek_Projek_tahun_selesai;
                }

                if(count($projek_tahun) > 0){
                    foreach($projek_tahun as $pt){
                        $pengarah = $pt->projek_tahun_Projek_tahun_pengarah;
                        $alamat_pejabat = $pengarah->projek_tahun_pengarah_jawatan_pejabat->name;
                        
                        if(!array_key_exists($alamat_pejabat, $data)){
                            $data[$alamat_pejabat] = [
                                'bilangan' => 0,
                                'gred_a' => 0,
                                'gred_b' => 0,
                                'gred_c' => 0,
                                'gred_d' => 0,
                                'gred_e' => 0
                            ];
                        }

                        $keputusan = self::getKeputusan($pt);
                        
                        $data[$alamat_pejabat] = [
                            'bilangan' => $data[$alamat_pejabat]['bilangan'] + 1,
                            'gred_a' => $keputusan['gred'] == 'A' ? $data[$alamat_pejabat]['gred_a'] + 1 : $data[$alamat_pejabat]['gred_a'] + 0,
                            'gred_b' => $keputusan['gred'] == 'B' ? $data[$alamat_pejabat]['gred_b'] + 1 : $data[$alamat_pejabat]['gred_b'] + 0,
                            'gred_c' => $keputusan['gred'] == 'C' ? $data[$alamat_pejabat]['gred_c'] + 1 : $data[$alamat_pejabat]['gred_c'] + 0,
                            'gred_d' => $keputusan['gred'] == 'D' ? $data[$alamat_pejabat]['gred_d'] + 1 : $data[$alamat_pejabat]['gred_d'] + 0,
                            'gred_e' => $keputusan['gred'] == 'E' ? $data[$alamat_pejabat]['gred_e'] + 1 : $data[$alamat_pejabat]['gred_e'] + 0
                        ];
                    }
                }
            }
        }

        return view('segment.urussetia.projek_laporan.keseluruhan_pejabat',[
            'data' => $data,
            'tahun' => $tahun
        ]);
    }

    public function analisis(Request $request){
        $tahun = Tahun::all();
        $projek = Projek::where('profiles_id', Auth::user()->user_profile->id)->where('flag', 1)->where('delete_id', 0)->get();

        if($request->has('tahun_select')){
            $pass = 1;
        }else{
            $pass = 0;
        }

        $data = [];

        $data['mainLabel'] = [
            'A',
            'B',
            'C',
            'D',
            'E'
        ];

        $data['label'] = [
            'A',
            'B',
            'C',
            'D',
            'E'
        ];

        $data['totalProject'] = 0;

        $data['scoreSheet']['A'] = [];
        $data['scoreSheet']['B'] = [];
        $data['scoreSheet']['C'] = [];
        $data['scoreSheet']['D'] = [];
        $data['scoreSheet']['E'] = [];

        $data['project']['A'] = 0;
        $data['project']['B'] = 0;
        $data['project']['C'] = 0;
        $data['project']['D'] = 0;
        $data['project']['E'] = 0;

        if(count($projek) > 0){
            foreach($projek as $p){
                if($pass == 1){
                    $pt = ProjeksTahun::where('projeks_id', $p->id)->where('tahuns_id', $request->input('tahun_select'))->where('status', 2)->get();
                }else{
                    $pt = $p->projek_Projek_tahun_selesai;
                }

                if(count($pt) > 0){
                    foreach($pt as $ptt){
                        $overall = self::getKeputusan($ptt);
                        $resultGred = $overall['gred'];

                        switch($resultGred){
                            case 'A':
                                $data['scoreSheet']['A'][] =  $overall['markah_keseluruhan'];
                                $data['project']['A'] += 1;
                                $data['totalProject']++;
                                break;
                            case 'B':
                                $data['scoreSheet']['B'][] =  $overall['markah_keseluruhan'];
                                $data['project']['B'] += 1;
                                $data['totalProject']++;
                                break;
                            case 'C':
                                $data['scoreSheet']['C'][] =  $overall['markah_keseluruhan'];
                                $data['project']['C'] += 1;
                                $data['totalProject']++;
                                break;
                            case 'D';
                                $data['scoreSheet']['D'][] =  $overall['markah_keseluruhan'];
                                $data['project']['D'] += 1;
                                $data['totalProject']++;
                                break;
                            case 'E';
                                $data['scoreSheet']['E'][] =  $overall['markah_keseluruhan'];
                                $data['project']['E'] += 1;
                                $data['totalProject']++;
                                break;
                        }
                    }
                }
            }
        }

        foreach($data['label'] as $l){
            if(count($data['scoreSheet'][$l]) > 0){
                $data['superSheet'][] = max($data['scoreSheet'][$l]);
            }else{
                $data['superSheet'][] = 0;
            }
            
        }

        $x = 0;
        foreach($data['project'] as $tpkey => $tpVal){
            if($data['totalProject'] == 0){
                $data['superSheet'][$x] = 0;
                $data['mainLabel'][$x] = 'Gred '.$tpkey.' ('.$tpVal.' Projek | 0%)'; 
            }else{
                $percentage = round($tpVal / $data['totalProject'], 2) * 100;
                $data['superSheet'][$x] = $percentage;
                $data['mainLabel'][$x] = 'Gred '.$tpkey.' ('.$tpVal.' Projek | '.$percentage.'%)'; 
            }
            
            $x++;
        }

//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        die();

        return view('segment.urussetia.projek_laporan.analisis', [
            'data' => $data,
            'tahun' => $tahun
        ]);
    }

    public function status_penilaian(Request $request){
        $tahun = Tahun::all();
        $data['mainLabel'] = [
            'Draf',
            'Dalam Penilaian',
            'Penilaian Selesai'
        ];

        if(Entrust::hasRole('Urussetia')){
            
            $data['superSheet'] = self::getUrussetiaData($request->has('tahun_select') ? $request->input('tahun_select') : false);
            
            $data['mainLabel'][0] = 'Draf: '.$data['superSheet'][0].' Projek';
            $data['mainLabel'][1] = 'Dalam Penilaian: '.$data['superSheet'][1].' Projek';
            $data['mainLabel'][2] = 'Penilaian Selesai: '.$data['superSheet'][2].' Projek';
        }

        return view('segment.urussetia.status_penilaian.index', [
            'data' => $data,
            'tahun' => $tahun
        ]);
    }

    public static function getUrussetiaData($tahun = false){
        $data = [];
        $countable = [];
        $projek = Projek::where('profiles_id', Auth::user()->user_profile->id)->get();
        $countable['s'] = 0;
        $countable['t_s'] = 0;
        $countable['d'] = 0;
        
        if(count($projek) > 0){
            foreach($projek as $p){

                if($tahun != false || $tahun != ''){
                    $projek_tahun = ProjeksTahun::where('projeks_id', $p->id)->where('tahuns_id', $tahun)->where('flag', 1)->where('delete_id', 0)->get();
                }else{
                    $projek_tahun = $p->projek_Projek_tahun;
                }

                if($projek_tahun){
                    foreach($projek_tahun as $pt){
                        if($pt->status == 0){
                            $countable['d'] += 1;
                        }else if($pt->status == 1){
                            $countable['t_s'] += 1;
                        }else if($pt->status == 2){
                            $countable['s'] += 1;
                        }
                    }
                }
            }
        }
        
        $data = [
            $countable['d']  ,
            $countable['t_s']  ,
            $countable['s']  ,
        ];
        return $data;
    }
}
