<?php 

namespace App\Http\Controllers\Segment\Pentadbir\Audit;

use App\Audit;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class AuditController extends Controller{
    public function index(){
        return view('segment.pentadbir.audit.index');
    }

    public function list(){
        $model = Audit::limit(2000)->where('user_id', '!=', null)->get();

        return DataTables::of($model)
            ->addColumn('username', function($data){
                return strtoupper($data->auditUserName->name);
            })
            ->addColumn('created_at', function($data){
                return $data->created_at;
            })
            ->addColumn('model', function($data){
                return $data->user_type;
            })
            ->addColumn('old_val', function($data){
                return $data->old_values == '[]' ? 'New Record' : $data->old_values;
            })
            ->addColumn('new_val', function($data){
                return $data->new_values;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }
}