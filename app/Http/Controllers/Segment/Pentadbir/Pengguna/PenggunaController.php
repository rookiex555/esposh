<?php
namespace App\Http\Controllers\Segment\Pentadbir\Pengguna;

use App\Http\Controllers\Controller;
use App\Models\Entrust\UserRole;
use App\Models\Mykj\ListPegawai2;
use App\Models\Profiles\Profile;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Main\CommonController;

class PenggunaController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            CommonController::checkUserStat();
            return $next($request);
        });
    }

    public function index(){
        return view('segment.pentadbir.pengguna.index');
    }

    public function pengguna_list(){
        $model = UserRole::where('role_id', 2)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-profile-id' => function($data) {
                    return $data->userrole_users->user_profile->id;
                },
                'data-user-id' => function($data) {
                    return $data->userrole_users->id;
                },
            ])
            ->addColumn('nama', function($data){
                return strtoupper($data->userrole_users->name);
            })
            ->addColumn('email', function($data){
                return $data->userrole_users->email;
            })
            ->addColumn('penempatan', function($data){
                return $data->userrole_users->user_profile->profile_Profile_cawangan_log_active ? $data->userrole_users->user_profile->profile_Profile_cawangan_log_active->penempatan_name : $data->nokp;
            })
            ->addColumn('active', function($data){
                return $data->userrole_users->user_profile->urussetia_active;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function pengguna_tambah(Request $request){
        $no_ic = $request->input('no_ic');
        $getMaklumat = ListPegawai2::getMaklumatPegawai($no_ic);
        try {
            $user = User::createOrUpdate($getMaklumat);
            return response()->json([
               'success' => 1,
            ]);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function pengguna_aktif(Request $request){
        $profile_id = $request->input('profile_id');
        $process = $this->aktif_delete($profile_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'profile_id' => $process['profile_id'],
                'flag' => $process['flag']
            ]
        ]);
    }

    public function pengguna_delete(Request $request){
        $profile_id = $request->input('profile_id');
        $process = $this->aktif_delete($profile_id, 2);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'profile_id' => $process['profile_id'],
            ]
        ]);
    }

    public function aktif_delete($profile_id, $trigger) : array{
        $model = Profile::find($profile_id);
        if($trigger == 1){
            $model->urussetia_active = $model->urussetia_active == 1 ? 0 : 1;
        }else{
            $model->delete_id = 1;
            $role_delete = UserRole::where('role_id', 2)->where('user_id', $model->users_id)->delete();
        }

        return [
            'success' => $model->save() ? 1 : 0,
            'profile_id' => $model->id,
            'flag' => $model->urussetia_active,
        ];
    }
}
