<?php 
namespace App\Http\Controllers\Segment\Pentadbir\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Jawatan\ProjekList;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TetapanProjekController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('segment.pentadbir.tetapan.projek.index');
    }

    public function projek_list(){
        $model = ProjekList::where('flag', 1)->where('delete_id', 0)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-projek-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('nama', function($data){
                return strtoupper($data->nama);
            })
            ->addColumn('active', function($data){
                return $data->flag;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function projek_tambah(Request $request){
        try {
            $projek = ProjekList::createOrUpdate($request);
            return response()->json([
                'success' => 1,
            ]);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function projek_get_rekod(Request $request){
        $model = ProjekList::find($request->input('projek_id'));
        $data = [];

        if($model){
            $data = [
                'id' => $model->id,
                'name' => $model->nama
            ];
        }
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function projek_aktif(Request $request){
        $profile_id = $request->input('profile_id');
        $process = $this->aktif_delete($profile_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'profile_id' => $process['profile_id'],
                'flag' => $process['flag']
            ]
        ]);
    }

    public function projek_delete(Request $request){
        $projek_id = $request->input('projek_id');
        $process = $this->aktif_delete($projek_id, 2);

        return response()->json([
            'success' => $process['success'],
        ]);
    }

    public function aktif_delete($projek_id, $trigger) : array{
        $model = ProjekList::find($projek_id);
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
        ];
    }
}