<?php

namespace App\Http\Controllers\Segment\Pentadbir\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Jawatan\JawatanPejabat;
use App\Models\Jawatan\JawatanPejabatsAlamat;
use App\Models\Jawatan\JawatansWakilsCawangan;
use App\Models\Jawatan\JawatansWakilsCawangansPejabat;
use App\Models\Regular\Negeri;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JawatanCawanganPejabatController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $negeri = Negeri::all();
        return view('segment.pentadbir.tetapan.jawatan_wakil_cawangan_pejabat.index', [
            'negeri' => $negeri
        ]);
    }

    public function jawatan_cawangan_list(){
        $model = JawatansWakilsCawangan::where('flag', 1)->where('delete_id', 0)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-jawatan-wakil-cawangan-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('nama', function($data){
                if($data->negeris_id == '' || $data->negeris_id == null){
                    $negeri = '(Tiada Negeri)';
                }else{
                    $negeri = '<b>('.$data->jawatanCawanganNegeri->nama.')</b>';
                }

                return strtoupper($data->name.' '.$negeri);
            })
            ->addColumn('active', function($data){
                return $data->flag;
            })
            ->rawColumns(['active', 'action', 'nama'])
            ->make(true);
    }

    public function jawatan_cawangan_tambah(Request $request){
        try {
            $jawatan = JawatansWakilsCawangan::createOrUpdate($request);
            return response()->json($jawatan);
        }catch (Exception $e){
            return response()->json($jawatan);
        }
    }

    public function jawatan_cawangan_get_rekod(Request $request){
        $model = JawatansWakilsCawangan::find($request->input('jawatan_wakil_cawangan_id'));
        $data = [];

        if($model){
            $data = [
                'id' => $model->id,
                'name' => $model->name,
                'negeri' => $model->negeris_id
            ];
        }
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function jawatan_cawangan_aktif(Request $request){
        $profile_id = $request->input('profile_id');
        $process = $this->aktif_delete($profile_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'profile_id' => $process['profile_id'],
                'flag' => $process['flag']
            ]
        ]);
    }

    public function jawatan_cawangan_delete(Request $request){
        $jawatan_cawangan_id = $request->input('jawatan_wakil_cawangan_id');
        $process = $this->aktif_delete($jawatan_cawangan_id, 2);

        return response()->json([
            'success' => $process['success'],
        ]);
    }

    public function aktif_delete($jawatan_cawangan_id, $trigger) : array{
        $model = JawatansWakilsCawangan::find($jawatan_cawangan_id);
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
        ];
    }

    //Jawatan Alamat
    public function jawatan_cawangan_pejabat_list(Request $request){
        $jawatan_wakil_cawangan_id = $request->input('jawatan_wakil_cawangan_id');

        $model = JawatansWakilsCawangansPejabat::where('flag', 1)->where('delete_id', 0)->where('jawatans_wakils_cawangans_id', $jawatan_wakil_cawangan_id)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-jawatan-wakil-cawangan-pejabat-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('pejabat', function($data){
                return strtoupper($data->name);
            })
            ->addColumn('active', function($data){
                return $data->flag;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function jawatan_cawangan_pejabat_tambah(Request $request){
        try {
            $jawatan = JawatansWakilsCawangansPejabat::createOrUpdate($request);
            return response()->json([
                'success' => $jawatan,
                'trigger' => $request->input('trigger')
            ]);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function jawatan_cawangan_pejabat_get_rekod(Request $request){
        $model = JawatansWakilsCawangansPejabat::find($request->input('jawatan_wakil_cawangan_pejabat_id'));
        $data = [];

        if($model){
            $data = [
                'id' => $model->id,
                'name' => $model->name,
            ];
        }
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function jawatan_cawangan_pejabat_delete(Request $request){
        $jawatan_wakil_cawangan_pejabat_id = $request->input('jawatan_wakil_cawangan_pejabat_id');
        $process = $this->aktif_delete_alamat($jawatan_wakil_cawangan_pejabat_id, 2);

        return response()->json([
            'success' => $process['success'],
        ]);
    }

    public function aktif_delete_alamat($jawatan_wakil_cawangan_pejabat_id, $trigger) : array{
        $model = JawatansWakilsCawangansPejabat::find($jawatan_wakil_cawangan_pejabat_id);
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
        ];
    }
}