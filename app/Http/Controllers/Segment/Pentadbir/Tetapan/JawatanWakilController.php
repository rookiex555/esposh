<?php
namespace App\Http\Controllers\Segment\Pentadbir\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Jawatan\JawatansWakil;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JawatanWakilController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('segment.pentadbir.tetapan.jawatan_wakil.index');
    }

    public function jawatan_wakil_list(){
        $model = JawatansWakil::where('flag', 1)->where('delete_id', 0)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-jawatan-wakil-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('nama', function($data){
                return strtoupper($data->name);
            })
            ->addColumn('active', function($data){
                return $data->flag;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function jawatan_wakil_tambah(Request $request){
        try {
            $projek = JawatansWakil::createOrUpdate($request);
            return response()->json($projek);
        }catch (Exception $e){
            return response()->json($projek);
        }
    }

    public function jawatan_wakil_get_rekod(Request $request){
        $model = JawatansWakil::find($request->input('jawatan_wakil_id'));
        $data = [];

        if($model){
            $data = [
                'id' => $model->id,
                'name' => $model->name
            ];
        }
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function jawatan_wakil_aktif(Request $request){
        $profile_id = $request->input('profile_id');
        $process = $this->aktif_delete($profile_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'profile_id' => $process['profile_id'],
                'flag' => $process['flag']
            ]
        ]);
    }

    public function jawatan_wakil_delete(Request $request){
        $projek_id = $request->input('jawatan_wakil_id');
        $process = $this->aktif_delete($projek_id, 2);

        return response()->json([
            'success' => $process['success'],
        ]);
    }

    public function aktif_delete($projek_id, $trigger) : array{
        $model = JawatansWakil::find($projek_id);
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
        ];
    }
}