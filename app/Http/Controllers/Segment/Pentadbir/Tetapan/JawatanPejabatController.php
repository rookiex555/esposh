<?php

namespace App\Http\Controllers\Segment\Pentadbir\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Jawatan\JawatanPejabat;
use App\Models\Jawatan\JawatanPejabatsAlamat;
use App\Models\Regular\Negeri;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JawatanPejabatController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('segment.pentadbir.tetapan.jawatanpejabat.index');
    }

    public function jawatan_pejabat_list(){
        $model = JawatanPejabatsAlamat::where('flag', 1)->where('delete_id', 0)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-jawatan-pejabat-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('nama', function($data){
                return strtoupper($data->name);
            })
            ->addColumn('active', function($data){
                return $data->flag;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function jawatan_pejabat_tambah(Request $request){
        try {
            $jawatan = JawatanPejabatsAlamat::createOrUpdate($request);
            return response()->json([
                'success' => 1,
            ]);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function jawatan_pejabat_get_rekod(Request $request){
        $model = JawatanPejabatsAlamat::find($request->input('jawatan_id'));
        $data = [];

        if($model){
            $data = [
                'id' => $model->id,
                'name' => $model->name
            ];
        }
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function jawatan_pejabat_aktif(Request $request){
        $profile_id = $request->input('profile_id');
        $process = $this->aktif_delete($profile_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'profile_id' => $process['profile_id'],
                'flag' => $process['flag']
            ]
        ]);
    }

    public function jawatan_pejabat_delete(Request $request){
        $jawatan_pejabat_id = $request->input('jawatan_pejabat_id');
        $process = $this->aktif_delete($jawatan_pejabat_id, 2);

        return response()->json([
            'success' => $process['success'],
        ]);
    }

    public function aktif_delete($jawatan_pejabat_id, $trigger) : array{
        $model = JawatanPejabatsAlamat::find($jawatan_pejabat_id);
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
        ];
    }
}