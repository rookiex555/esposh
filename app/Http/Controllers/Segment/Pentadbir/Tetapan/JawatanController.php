<?php

namespace App\Http\Controllers\Segment\Pentadbir\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Jawatan\JawatanPejabat;
use App\Models\Jawatan\JawatanPejabatsAlamat;
use App\Models\Regular\Negeri;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JawatanController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $negeri = Negeri::all();
        return view('segment.pentadbir.tetapan.jawatan.index', [
            'negeri' => $negeri
        ]);
    }

    public function jawatan_list(){
        $model = JawatanPejabat::where('flag', 1)->where('delete_id', 0)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-jawatan-pejabat-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('nama', function($data){
                return strtoupper($data->name);
            })
            ->addColumn('active', function($data){
                return $data->flag;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function jawatan_tambah(Request $request){
        try {
            $jawatan = JawatanPejabat::createOrUpdate($request);
            return response()->json($jawatan);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function jawatan_get_rekod(Request $request){
        $model = JawatanPejabat::find($request->input('jawatan_id'));
        $data = [];

        if($model){
            $data = [
                'id' => $model->id,
                'name' => $model->name
            ];
        }
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function jawatan_aktif(Request $request){
        $profile_id = $request->input('profile_id');
        $process = $this->aktif_delete($profile_id, 1);

        return response()->json([
            'success' => $process['success'],
            'data' => [
                'profile_id' => $process['profile_id'],
                'flag' => $process['flag']
            ]
        ]);
    }

    public function jawatan_delete(Request $request){
        $jawatan_pejabat_id = $request->input('jawatan_pejabat_id');
        $process = $this->aktif_delete($jawatan_pejabat_id, 2);

        return response()->json([
            'success' => $process['success'],
        ]);
    }

    public function aktif_delete($jawatan_pejabat_id, $trigger) : array{
        $model = JawatanPejabat::find($jawatan_pejabat_id);
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
        ];
    }

    //Jawatan Alamat
    public function jawatan_alamat_list(Request $request){
        $jawatan_id = $request->input('jawatan_id');

        $model = JawatanPejabatsAlamat::where('flag', 1)->where('delete_id', 0)->where('jawatan_pejabats_id', $jawatan_id)->get();

        return DataTables::of($model)
            ->setRowAttr([
                'data-jawatan-pejabat-alamat-id' => function($data) {
                    return $data->id;
                },
            ])
            ->addColumn('full_alamat', function($data){
                $alamat_satu = $data->alamat_satu ? $data->alamat_satu.', ' : '';
                $alamat_dua = $data->alamat_dua ? $data->alamat_dua.', ' : '';
                $poskod = $data->poskod;

                return strtoupper($alamat_satu.$alamat_dua.$poskod);
            })
            ->addColumn('negeri', function($data){
                return $data->alamatNegeri->nama;
            })
            ->addColumn('active', function($data){
                return $data->flag;
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }

    public function jawatan_alamat_tambah(Request $request){
        try {
            $jawatan = JawatanPejabatsAlamat::createOrUpdate($request);
            return response()->json([
                'success' => 1,
                'trigger' => $request->input('trigger')
            ]);
        }catch (Exception $e){
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    public function jawatan_alamat_get_rekod(Request $request){
        $model = JawatanPejabatsAlamat::find($request->input('jawatan_alamat_id'));
        $data = [];

        if($model){
            $data = [
                'id' => $model->id,
                'alamat_satu' => $model->alamat_satu,
                'alamat_dua' => $model->alamat_dua,
                'poskod' => $model->poskod,
                'negeri' => $model->negeris_id
            ];
        }
        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }

    public function jawatan_alamat_delete(Request $request){
        $jawatan_pejabat_alamat_id = $request->input('jawatan_pejabat_alamat_id');
        $process = $this->aktif_delete_alamat($jawatan_pejabat_alamat_id, 2);

        return response()->json([
            'success' => $process['success'],
        ]);
    }

    public function aktif_delete_alamat($jawatan_pejabat_id, $trigger) : array{
        $model = JawatanPejabatsAlamat::find($jawatan_pejabat_id);
        $model->delete_id = 1;

        return [
            'success' => $model->save() ? 1 : 0,
        ];
    }
}