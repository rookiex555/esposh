<?php

namespace App;

use App\Models\Entrust\UserRole;
use App\Models\Profiles\Profile;
use App\Models\Profiles\ProfilesAlamatPejabat;
use App\Models\Profiles\ProfilesCawangansLog;
use App\Models\Profiles\ProfilesTelefon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable;
    use EntrustUserTrait;
    use \OwenIt\Auditing\Auditable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'nokp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_profile(){
        return $this->hasOne('App\Models\Profiles\Profile', 'users_id', 'id');
    }

    public function user_role(){
        return $this->hasMany('App\Models\Entrust\UserRole', 'user_id', 'id');
    }

    public static function createOrUpdate($peg_maklumat, $kpa_create = false){
        $user = User::where('nokp', $peg_maklumat['nokp'])->first();
        if(!$user){
            $user = new User;
            $user->nokp = $peg_maklumat['nokp'];
            $user->name = $peg_maklumat['name'];
            $user->email = $peg_maklumat['email'];
            $user->password = Hash::make('123123123');

            if($user->save()){
                $profile = new Profile;
                $profile->users_id = $user->id;
                $profile->flag = 1;
                $profile->delete_id = 0;
                $profile->urussetia_active = 1;
                $profile->kpa_active = 0;
                if($profile->save()){
                    $cawanganLogs = new ProfilesCawangansLog;
                    $cawanganLogs->profiles_id = $profile->id;
                    $cawanganLogs->sektor = $peg_maklumat['waran_split']['sektor'];
                    $cawanganLogs->cawangan = $peg_maklumat['waran_split']['cawangan'];
                    $cawanganLogs->bahagian = $peg_maklumat['waran_split']['bahagian'];
                    $cawanganLogs->unit = $peg_maklumat['waran_split']['unit'];
                    $cawanganLogs->penempatan = $peg_maklumat['waran_split']['waran_penuh'];
                    $cawanganLogs->sektor_name = $peg_maklumat['waran_name']['sektor'];
                    $cawanganLogs->cawangan_name = $peg_maklumat['waran_name']['cawangan'];
                    $cawanganLogs->bahagian_name = $peg_maklumat['waran_name']['bahagian'];
                    $cawanganLogs->unit_name = $peg_maklumat['waran_name']['unit'];
                    $cawanganLogs->penempatan_name = $peg_maklumat['waran_name']['waran_penuh'];
                    $cawanganLogs->tahun = date('Y');
                    $cawanganLogs->flag = 1;
                    $cawanganLogs->delete_id = 0;

                    if($cawanganLogs->save()){
                        $profileTelefon = new ProfilesTelefon;
                        $profileTelefon->profiles_id = $profile->id;
                        $profileTelefon->no_tel_bimbit = $peg_maklumat['tel_bimbit'];
                        $profileTelefon->no_tel_pejabat = $peg_maklumat['tel_pejabat'];
                        $profileTelefon->flag = 1;
                        $profileTelefon->delete_id = 0;

                        if($profileTelefon->save()){
                            $alamat = new ProfilesAlamatPejabat;
                            $alamat->profiles_id = $profile->id;
                            $alamat->alamat = $peg_maklumat['alamat_pejabat'];
                            $alamat->flag = 1;
                            $alamat->delete_id = 0;

                            if($alamat->save()){
                                $role_user = new UserRole;
                                $role_user->user_id = $user->id;
                                if(!$kpa_create){
                                    $role_user->role_id = 2;
                                }else{
                                    $role_user->role_id = 3;
                                }


                                if($role_user->save()){
                                    return 1;
                                }
                            }
                        }
                    }
                }
            }
        }else{
            $profile = Profile::where('users_id', $user->id)->first();

            if($profile->delete_id == 1){
                $profile->delete_id = 0;
                $profile->urussetia_active = 1;
                $profile->kpa_active = 0;
                $profile->save();
            }

            $checkRoleExist = UserRole::where('user_id', $user->id)->where('role_id', 2)->first();

            if(!$checkRoleExist){
                $role_user = new UserRole;
                $role_user->user_id = $user->id;
                $role_user->role_id = 2;
                $role_user->save();
            }

            return 1;
        }
    }
}
