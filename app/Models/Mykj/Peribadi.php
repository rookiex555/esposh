<?php
namespace App\Models\Mykj;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Peribadi extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $connection = 'pgsqlmykj';
    protected $table = 'peribadi';
}
