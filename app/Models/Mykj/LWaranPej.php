<?php

namespace App\Models\Mykj;
use OwenIt\Auditing\Contracts\Auditable;

use Illuminate\Database\Eloquent\Model;

class LWaranPej extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $connection = 'pgsqlmykj';
    protected $table = 'l_waran_pej';
//    public $timestamps = false;
}
