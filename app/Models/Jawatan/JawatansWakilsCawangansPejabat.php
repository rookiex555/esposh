<?php

namespace App\Models\Jawatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\Auditable;

class JawatansWakilsCawangansPejabat extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function jawatanAlamat(){
        return $this->hasOne('App\Models\Jawatan\JawatansWakilsCawangan', 'id', 'jawatans_wakils_cawangans_id');
    }

    public static function createOrUpdate(Request $request) : int{

        $jawatan_wakil_cawangan_pejabat_nama = $request->input('jawatan_wakil_cawangan_pejabat_nama');
        $jawatan_wakil_cawangan_pejabat_id = $request->input('jawatan_wakil_cawangan_pejabat_id');
        $jawatan_wakil_cawangan_id = $request->input('jawatan_wakil_cawangan_id');
        $trigger = $request->input('trigger');

        if($trigger == 0){
            $c_projek = JawatansWakilsCawangansPejabat::checkDuplicate($jawatan_wakil_cawangan_pejabat_nama, $jawatan_wakil_cawangan_id);
            $model = new JawatansWakilsCawangansPejabat;
            $model->jawatans_wakils_cawangans_id = $jawatan_wakil_cawangan_id;
        }else{
            $c_projek = JawatansWakilsCawangansPejabat::checkDuplicate($jawatan_wakil_cawangan_pejabat_nama, $jawatan_wakil_cawangan_id, $jawatan_wakil_cawangan_pejabat_id);
            $model = JawatansWakilsCawangansPejabat::find($jawatan_wakil_cawangan_pejabat_id);
        }

        if($c_projek){
            return 2;
        }else{
            try{
                $model->name = $jawatan_wakil_cawangan_pejabat_nama;
                $model->flag = 1;
                $model->delete_id = 0;
                $model->save();

                return 1;
            }catch (Exception $e){
                return 0;
            }
        }
    }

    public static function checkDuplicate($jawatan_wakil_cawangan_pejabat_nama, $jawatan_wakil_cawangan_id, $id = false) : bool{
        if(!$id){
            $check = JawatansWakilsCawangansPejabat::whereRaw("LOWER(name) ilike '%".$jawatan_wakil_cawangan_pejabat_nama."%' ")->where('jawatans_wakils_cawangans_id', $jawatan_wakil_cawangan_id)->where('delete_id', '!=', '1')->first();
        }else{
            $check = JawatansWakilsCawangansPejabat::whereRaw("LOWER(name) ilike '%".$jawatan_wakil_cawangan_pejabat_nama."%' ")->where('jawatans_wakils_cawangans_id', $jawatan_wakil_cawangan_id)->where('delete_id', '!=', '1')->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }
}