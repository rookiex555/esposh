<?php

namespace App\Models\Jawatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\Auditable;

class JawatansWakil extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public static function createOrUpdate(Request $request) : array{
        $jawatan_wakil_nama = $request->input('jawatan_wakil_nama');
        $trigger = $request->input('trigger');
        $jawatan_wakil_id = $request->input('jawatan_wakil_id');

        if($trigger == 0){
            $c_projek = JawatansWakil::checkDuplicate($jawatan_wakil_nama);
            $model = new JawatansWakil;
        }else{
            $c_projek = JawatansWakil::checkDuplicate($jawatan_wakil_nama, $jawatan_wakil_id);
            $model = JawatansWakil::find($jawatan_wakil_id);
        }

        if($c_projek){
            return [
                'success' => 2
            ];
        }else{
            try{
                $model->name = $jawatan_wakil_nama;
                $model->flag = 1;
                $model->delete_id = 0;
                $model->save();
                return [
                    'success' => $trigger == 0 ? 1 : 3
                ];
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }
        }
    }

    public static function checkDuplicate($jawatan_wakil_nama, $id = false) : bool{
        if(!$id){
            $check = JawatansWakil::whereRaw("LOWER(name) = '".strtolower($jawatan_wakil_nama)."' ")->where('delete_id', '!=', '1')->first();
        }else{
            $check = JawatansWakil::whereRaw("LOWER(name) = '".strtolower($jawatan_wakil_nama)."' ")->where('delete_id', '!=', '1')->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }
}