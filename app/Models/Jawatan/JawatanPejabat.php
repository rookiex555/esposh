<?php

namespace App\Models\Jawatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\Auditable;

class JawatanPejabat extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function jawatanAlamat(){
        return $this->hasMany('App\Models\Jawatan\JawatanPejabatsAlamat', 'id', 'jawatan_pejabats_id')->where('delete_id', 0)->where('flag', 1);
    }

    public static function createOrUpdate(Request $request) : array{
        $jawatan_nama = $request->input('jawatan_nama');
        $trigger = $request->input('trigger');
        $jawatan_id = $request->input('jawatan_id');

        if($trigger == 0){
            $c_projek = JawatanPejabat::checkDuplicate($jawatan_nama);
            $model = new JawatanPejabat;
        }else{
            $c_projek = JawatanPejabat::checkDuplicate($jawatan_nama, $jawatan_id);
            $model = JawatanPejabat::find($jawatan_id);
        }

        if($c_projek){
            return [
                'success' => 2
            ];
        }else{
            try{
                $model->name = $jawatan_nama;
                $model->flag = 1;
                $model->delete_id = 0;
                $model->save();

                return [
                    'success' => 1
                ];
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }
        }
    }

    public static function checkDuplicate($jawatan_nama, $id = false) : bool{
        if(!$id){
            $check = JawatanPejabat::whereRaw("LOWER(name) = '".$jawatan_nama."' ")->where('delete_id', '!=', '1')->first();
        }else{
            $check = JawatanPejabat::whereRaw("LOWER(name) = '".$jawatan_nama."' ")->where('delete_id', '!=', '1')->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }
}
