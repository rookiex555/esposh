<?php
namespace App\Models\Jawatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class ProjekList extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public static function createOrUpdate(Request $request) : array{
        $projek_nama = $request->input('projek_nama');
        $trigger = $request->input('trigger');
        $projek_id = $request->input('projek_id');

        if($trigger == 0){
            $c_projek = ProjekList::checkDuplicate($projek_nama);
            $model = new ProjekList;
        }else{
            $c_projek = ProjekList::checkDuplicate($projek_nama, $projek_id);
            $model = ProjekList::find($projek_id);
        }

        if($c_projek){
            return [
                'success' => 2
            ];
        }else{
            try{
                $model->nama = $projek_nama;
                $model->flag = 1;
                $model->delete_id = 0;
                $model->save();

                return [
                    'success' => 1
                ];
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }
        }
    }

    public static function checkDuplicate($projek_nama, $id = false) : bool{
        if(!$id){
            $check = ProjekList::whereRaw("LOWER(nama) ilike '%".$projek_nama."%' ")->where('delete_id', '!=', '1')->first();
        }else{
            $check = ProjekList::whereRaw("LOWER(nama) ilike '%".$projek_nama."%' ")->where('delete_id', '!=', '1')->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }
}