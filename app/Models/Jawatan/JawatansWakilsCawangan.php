<?php

namespace App\Models\Jawatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\Auditable;

class JawatansWakilsCawangan extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public $timestamps = false;

    public function jawatanAlamat(){
        return $this->hasMany('App\Models\Jawatan\JawatansWakilsCawangansPejabat', 'id', 'jawatans_wakils_cawangans_id')->where('delete_id', 0)->where('flag', 1);
    }

    public function jawatanCawanganNegeri(){
        return $this->hasOne('App\Models\Regular\Negeri', 'id', 'negeris_id');
    }

    public static function createOrUpdate(Request $request) : array{
        $jawatan_wakil_cawangan_nama = $request->input('jawatan_wakil_cawangan_nama');
        $jawatan_wakil_cawangan_negeri = $request->input('jawatan_wakil_cawangan_negeri');
        $trigger = $request->input('trigger');
        $jawatan_wakil_cawangan_id = $request->input('jawatan_wakil_cawangan_id');

        if($trigger == 0){
            $c_projek = JawatansWakilsCawangan::checkDuplicate($jawatan_wakil_cawangan_nama, $jawatan_wakil_cawangan_negeri);
            $model = new JawatansWakilsCawangan;
        }else{
            $c_projek = JawatansWakilsCawangan::checkDuplicate($jawatan_wakil_cawangan_nama, $jawatan_wakil_cawangan_negeri, $jawatan_wakil_cawangan_id);
            $model = JawatansWakilsCawangan::find($jawatan_wakil_cawangan_id);
        }

        if($c_projek){
            return [
                'success' => 2
            ];
        }else{
            try{
                $model->name = $jawatan_wakil_cawangan_nama;
                $model->negeris_id = $jawatan_wakil_cawangan_negeri;
                $model->flag = 1;
                $model->delete_id = 0;
                $model->save();

                return [
                    'success' => 1
                ];
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }
        }
    }

    public static function checkDuplicate($jawatan_wakil_cawangan_nama, $jawatan_wakil_cawangan_negeri, $id = false) : bool{
        if($jawatan_wakil_cawangan_negeri == ''){
            $jawatan_wakil_cawangan_negeri = null;
        }

        if(!$id){
            $check = JawatansWakilsCawangan::whereRaw("LOWER(name) ilike '%".$jawatan_wakil_cawangan_nama."%' ")->where('delete_id', '!=', '1')->where('negeris_id', $jawatan_wakil_cawangan_negeri)->first();
        }else{
            $check = JawatansWakilsCawangan::whereRaw("LOWER(name) ilike '%".$jawatan_wakil_cawangan_nama."%' ")->where('delete_id', '!=', '1')->where('negeris_id', $jawatan_wakil_cawangan_negeri)->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }
}