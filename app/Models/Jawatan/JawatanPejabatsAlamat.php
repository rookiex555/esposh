<?php

namespace App\Models\Jawatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class JawatanPejabatsAlamat extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public static function createOrUpdate(Request $request) : array{
        $jawatan_nama = $request->input('jawatan_nama');
        $trigger = $request->input('trigger');
        $jawatan_id = $request->input('jawatan_id');

        if($trigger == 0){
            $c_projek = JawatanPejabatsAlamat::checkDuplicate($jawatan_nama);
            $model = new JawatanPejabatsAlamat;
        }else{
            $c_projek = JawatanPejabatsAlamat::checkDuplicate($jawatan_nama, $jawatan_id);
            $model = JawatanPejabatsAlamat::find($jawatan_id);
        }

        if($c_projek){
            return [
                'success' => 2
            ];
        }else{
            try{
                $model->name = $jawatan_nama;
                $model->flag = 1;
                $model->delete_id = 0;
                $model->save();

                return [
                    'success' => 1
                ];
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }
        }
    }

    public static function checkDuplicate($jawatan_nama, $id = false) : bool{
        if(!$id){
            $check = JawatanPejabatsAlamat::whereRaw("LOWER(name) ilike '%".$jawatan_nama."%' ")->where('delete_id', '!=', '1')->first();
        }else{
            $check = JawatanPejabatsAlamat::whereRaw("LOWER(name) ilike '%".$jawatan_nama."%' ")->where('delete_id', '!=', '1')->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }
}
