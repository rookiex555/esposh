<?php

namespace App\Models\Entrust;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
	public $timestamps = false;
    protected $table = 'role_user';

    public function userrole_users(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function userrole_rolename(){
        return $this->hasOne('App\Models\Entrust\Role', 'id', 'role_id');
    }
}
