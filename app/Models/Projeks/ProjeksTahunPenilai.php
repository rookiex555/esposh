<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunPenilai extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahun_penilai_Projek_tahun(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahun', 'id', 'projeks_tahuns_id');
    }

    public function projek_tahuns_penilai_Projek_tahuns_penilaians_sub_perkara_penilai_skor(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor', 'id', 'projeks_tahuns_penilais_id');
    }

    public function projek_tahun_penilai_Projek_tahun_penilais_keputusan_logs(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaisKeputusanLog', 'id', 'projeks_tahuns_penilais_id');
    }
}
