<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsUpload extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public $table = 'projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads';

    public function projek_tahuns_penilaians_sub_perkara_penilais_skors_upload_Projek_tahuns_penilaians_sub_perkara_penilai_skor(){
        $this->hasOne('App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor', 'projeks_tahuns_penilaians_sub_perkara_penilais_skor_id', 'id');
    }
}
