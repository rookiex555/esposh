<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsCatatansUpload extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads';

    public function projek_tahun_catatans_upload_Projek_tahun(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahun', 'projeks_tahuns_id', 'id');
    }
}
