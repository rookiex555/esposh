<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsWakil extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahun_wakil_Projek_tahun(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahun', 'projeks_tahuns_id', 'id');
    }

    public function projek_tahun_wakil_jawatan(){
        return $this->hasOne('App\Models\Jawatan\JawatansWakil', 'id', 'jawatans_wakil_id');
    }

    public function projek_tahun_wakil_jawatan_alamat(){
        return $this->hasOne('App\Models\Jawatan\JawatansWakilsCawangansPejabat', 'id', 'jawatans_wakils_cawangans_pejabats_id');
    }
}
