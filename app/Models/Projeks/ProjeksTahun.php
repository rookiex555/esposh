<?php
namespace App\Models\Projeks;

use App\Models\Entrust\UserRole;
use App\Models\Mykj\ListPegawai2;
use App\Models\Profiles\Profile;
use App\Models\Profiles\ProfilesAlamatPejabat;
use App\Models\Profiles\ProfilesCawanganLog;
use App\Models\Profiles\ProfilesCawangansLog;
use App\Models\Profiles\ProfilesTelefon;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use OwenIt\Auditing\Contracts\Auditable;
use Mail;

class ProjeksTahun extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahun_projek(){
        return $this->hasOne('App\Models\Projeks\Projek', 'id', 'projeks_id');
    }

    public function projek_tahun_Tahun(){
        return $this->hasOne('App\Models\Regular\Tahun', 'id', 'tahuns_id');
    }

    public function projek_tahun_Projek_tahun_penilai(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunPenilai', 'projeks_tahuns_id', 'id')->orderBy('id', 'desc');
    }

    public function projek_tahun_Projek_tahun_pengarah(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsPengarah', 'projeks_tahuns_id', 'id');
    }

    public function projek_tahun_Projek_jawatan_pejabat(){
        return $this->hasOne('App\Models\Jawatan\JawatanPejabatsAlamat', 'id', 'jawatans_id');
    }

    public function projek_tahun_Projek_tahun_catatans_upload(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsCatatansUpload', 'id', 'projeks_tahuns_id');
    }

    public function projek_tahun_Projek_tahun_penilaian(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaian', 'projeks_tahuns_id', 'id')->where('flag', 1)->where('delete_id', 0)->orderBy('ref_id', 'asc');
    }

    public function projek_tahun_Projek_tahun_wakil(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsWakil', 'projeks_tahuns_id', 'id');
    }

    public function projek_tahun_Projek_tahun_penilais_keputusan_logs(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaisKeputusanLog', 'projeks_tahuns_id', 'id');
    }

    public static function getProjek($projek_penilaian_id) : array{
        $model = ProjeksTahun::find($projek_penilaian_id);

        $data = [];
        $data['id'] = $model->id;
        $data['pengarah'] = [
            'jawatan' => $model->projek_tahun_Projek_tahun_pengarah->jawatan_pejabats_id,
            'nama' => $model->projek_tahun_Projek_tahun_pengarah->jawatan_pejabats_alamats_id
        ];

        $data['wakil'] = [
            'jawatan' => $model->projek_tahun_Projek_tahun_wakil->jawatans_wakil_id,
            'nama' => $model->projek_tahun_Projek_tahun_wakil->jawatans_wakils_cawangans_pejabats_id
        ];

        $data['penilai'] = [
            'nokp' => $model->projek_tahun_Projek_tahun_penilai->nokp,
            'nama' => $model->projek_tahun_Projek_tahun_penilai->nama,
        ];

        $data['no_klausa'] = $model->no_klausa;
        $data['tkh_milik_tpk'] = date("d-m-Y", strtotime($model->tkh_milik_tpk));
        $data['tkh_siap_asal'] = date("d-m-Y", strtotime($model->tkh_siap_asal));
        $data['tkh_siap_semasa'] = date("d-m-Y", strtotime($model->tkh_siap_semasa));
        $data['tkh_penilaian'] = date("d-m-Y", strtotime($model->tkh_penilaian));
        $data['kemajuan_penilaian'] = $model->kemajuan_penilaian;
        return $data;
    }

    public static function createOrUpdate(Request $request){
        $projek_penilaian_no_klausa = $request->input('projek_penilaian_no_klausa');
        $projek_penilaian_pengarah_jawatan = $request->input('projek_penilaian_pengarah_jawatan');
        $projek_penilaian_pengarah_nama = $request->input('projek_penilaian_pengarah_nama');
        $projek_penilaian_wakil_jawatan = $request->input('projek_penilaian_wakil_jawatan');
        $projek_penilaian_wakil_nama = $request->input('projek_penilaian_wakil_nama');
        $projek_penilaian_tkh_tapak = date("Y-m-d", strtotime($request->input('projek_penilaian_tkh_tapak')));
        $projek_penilaian_tkh_asal = date("Y-m-d", strtotime($request->input('projek_penilaian_tkh_asal')));
        $projek_penilaian_tkh_semasa = date("Y-m-d", strtotime($request->input('projek_penilaian_tkh_semasa')));
        $projek_penilaian_tkh_penilaian = date("Y-m-d", strtotime($request->input('projek_penilaian_tkh_penilaian')));
        $projek_penilaian_kemajuan_fizikal = $request->input('projek_penilaian_kemajuan_fizikal');
        $projek_penilaian_penilai = $request->input('projek_penilaian_penilai');
        $projek_id = $request->input('projek_id');
        $tahun_id = $request->input('tahun_id');
        $projek_penilaian_id = $request->input('projek_penilaian_id');
        $trigger = $request->input('trigger');
        
        if($trigger == 0){
            $c_klausa = ProjeksTahun::checkDuplicate($projek_penilaian_no_klausa, $projek_id);
            $projek_tahun = new ProjeksTahun;
            $projek_tahun->flag = 1;
            $projek_tahun->delete_id = 0;
            $projek_tahun->projeks_id = $projek_id;
            $projek_tahun->tahuns_id = $tahun_id;
            $projek_tahun->status = 0;

            $projek_tahun_wakil = new ProjeksTahunsWakil;
            $projek_tahun_wakil->flag = 1;
            $projek_tahun_wakil->delete_id = 0;

            $projek_tahun_pengarah = new ProjeksTahunsPengarah;
            $projek_tahun_pengarah->flag = 1;
            $projek_tahun_pengarah->delete_id = 0;

        }else{
            $c_klausa = ProjeksTahun::checkDuplicate($projek_penilaian_no_klausa,$projek_id, $projek_penilaian_id);
            $projek_tahun = ProjeksTahun::find($projek_penilaian_id);

            $projek_tahun_wakil = ProjeksTahunsWakil::where('projeks_tahuns_id', $projek_tahun->id)->orderBy('id', 'desc')->first();
            $projek_tahun_pengarah = ProjeksTahunsPengarah::where('projeks_tahuns_id', $projek_tahun->id)->orderBy('id', 'desc')->first();
        }

        // if($c_klausa){
        //     return [
        //         'success' => 2
        //     ];
        // }else{
            try{
                $projek_tahun->no_klausa = $projek_penilaian_no_klausa;
                $projek_tahun->tkh_milik_tpk = $projek_penilaian_tkh_tapak;
                $projek_tahun->tkh_siap_asal = $projek_penilaian_tkh_asal;
                $projek_tahun->tkh_siap_semasa = $projek_penilaian_tkh_semasa;
                $projek_tahun->tkh_penilaian = $projek_penilaian_tkh_penilaian;
                $projek_tahun->kemajuan_penilaian = $projek_penilaian_kemajuan_fizikal;

                if($projek_tahun->save()){
                    $projek_tahun_wakil->projeks_tahuns_id = $projek_tahun->id;
                    $projek_tahun_wakil->jawatans_wakil_id = $projek_penilaian_wakil_jawatan;
                    $projek_tahun_wakil->jawatans_wakils_cawangans_pejabats_id = $projek_penilaian_wakil_nama;

                    if($projek_tahun_wakil->save()){
                        $projek_tahun_pengarah->projeks_tahuns_id = $projek_tahun->id;
                        $projek_tahun_pengarah->jawatan_pejabats_id = $projek_penilaian_pengarah_jawatan;
                        $projek_tahun_pengarah->jawatan_pejabats_alamats_id = $projek_penilaian_pengarah_nama;

                        if($projek_tahun_pengarah->save()){
                            $current_penilai = $projek_tahun->projek_tahun_Projek_tahun_penilai;
                            if($current_penilai){
                                if($current_penilai->nokp != $projek_penilaian_penilai){
                                    $penilai = self::updateOrCreatePenilai($projek_penilaian_penilai);
                                    self::createProjekPenilai($projek_tahun->id, $penilai);
                                }
                            }else{
                                $penilai = self::updateOrCreatePenilai($projek_penilaian_penilai);
                                self::createProjekPenilai($projek_tahun->id, $penilai);
                            }

                            return [
                                'success' => 1
                            ];
                        }
                    }
                }
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }

        // }
    }

    public static function checkDuplicate($noKlausa, $projek_id, $id = false) : bool{
        if(!$id){
            $check = ProjeksTahun::whereRaw("LOWER(no_klausa) ilike '%".$noKlausa."%' ")->where('delete_id', '!=', '1')->where('projeks_id', $projek_id)->first();
        }else{
            $check = ProjeksTahun::whereRaw("LOWER(no_klausa) ilike '%".$noKlausa."%' ")->where('delete_id', '!=', '1')->where('projeks_id', $projek_id)->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }

    public static function createProjekPenilai($projek_tahun_id, $penilai){
        $checkPenilai = ProjeksTahunPenilai::where('projeks_tahuns_id', $projek_tahun_id)->first();

        if(!$checkPenilai){
            $model = new ProjeksTahunPenilai;
            $model->projeks_tahuns_id = $projek_tahun_id;
            $model->nokp = $penilai['nokp'];
            $model->email = $penilai['email'];
            $model->nama = $penilai['name'];
            $model->status = 0;
            $model->flag = 1;
            $model->delete_id = 0;
            if($model->save()){
                $email = [
                    $model->email, 
                    // 'akiyamasensei555@gmail.com',
                    $model->nama,
                ];

                $data = [
                    'penilai' => $model->nama,
                    'nama' => $model->projek_tahun_penilai_Projek_tahun->projek_tahun_projek->nama,
                    'tkh_penilaian' => $model->projek_tahun_penilai_Projek_tahun->tkh_penilaian,
                    'no_klausa' => $model->projek_tahun_penilai_Projek_tahun->no_klausa
                ];
        
                Mail::send('segment/mail/newKPA', $data, function($message) use ($email){
                    $message->to($email[0], $email[1])
                    ->subject('Pelantikan '.$email[1].' Sebagai KPA');
                    $message->from($email[0],'EASPOSH');
                });
                
                return 1;
            }else{
                return 0;
            }
        }else{
            if($checkPenilai->nokp != $penilai['nokp']){
                $previous_nokp = $checkPenilai->nokp;
                $checkPenilai->nokp = $penilai['nokp'];
                $checkPenilai->email = $penilai['email'];
                $checkPenilai->nama = $penilai['name'];
                $checkPenilai->status = 0;
                $checkPenilai->save();

                $getUser = User::where('nokp', $previous_nokp)->first();
                $checkPenilaiExistOther = ProjeksTahunPenilai::where('nokp', $previous_nokp)->where('projeks_tahuns_id', '!=', $projek_tahun_id)->first();
                if(!$checkPenilaiExistOther){
                    UserRole::where('user_id', $getUser->id)->where('role_id', 3)->delete();
                }
            }else{
                $user = User::where('nokp', $penilai['nokp'])->first();
                $check_role_user = UserRole::where('user_id', $user->id)->where('role_id', 3)->first();
                if(!$check_role_user){
                    $role_user = new UserRole;
                    $role_user->user_id = $user->id;
                    $role_user->role_id = 3;
                    $role_user->save();
                }
            }
            return 1;
        }
    }

    public static function updateOrCreatePenilai($no_ic){
        $peg_maklumat = ListPegawai2::getMaklumatPegawai($no_ic);

        $user = User::where('nokp', $peg_maklumat['nokp'])->first();
        if(!$user){
            $user = new User;
            $user->nokp = $peg_maklumat['nokp'];
            $user->name = $peg_maklumat['name'];
            $user->email = $peg_maklumat['email'];
            $user->password = Hash::make('123123123');

            if($user->save()){
                $profile = new Profile;
                $profile->users_id = $user->id;
                $profile->flag = 1;
                $profile->delete_id = 0;
                $profile->urussetia_active = 0;
                $profile->kpa_active = 1;
                if($profile->save()){
                    $cawanganLogs = new ProfilesCawangansLog;
                    $cawanganLogs->profiles_id = $profile->id;
                    $cawanganLogs->sektor = $peg_maklumat['waran_split']['sektor'];
                    $cawanganLogs->cawangan = $peg_maklumat['waran_split']['cawangan'];
                    $cawanganLogs->bahagian = $peg_maklumat['waran_split']['bahagian'];
                    $cawanganLogs->unit = $peg_maklumat['waran_split']['unit'];
                    $cawanganLogs->penempatan = $peg_maklumat['waran_split']['waran_penuh'];
                    $cawanganLogs->sektor_name = $peg_maklumat['waran_name']['sektor'];
                    $cawanganLogs->cawangan_name = $peg_maklumat['waran_name']['cawangan'];
                    $cawanganLogs->bahagian_name = $peg_maklumat['waran_name']['bahagian'];
                    $cawanganLogs->unit_name = $peg_maklumat['waran_name']['unit'];
                    $cawanganLogs->penempatan_name = $peg_maklumat['waran_name']['waran_penuh'];
                    $cawanganLogs->tahun = date('Y');
                    $cawanganLogs->flag = 1;
                    $cawanganLogs->delete_id = 0;

                    if($cawanganLogs->save()){
                        $profileTelefon = new ProfilesTelefon;
                        $profileTelefon->profiles_id = $profile->id;
                        $profileTelefon->no_tel_bimbit = $peg_maklumat['tel_bimbit'];
                        $profileTelefon->no_tel_pejabat = $peg_maklumat['tel_pejabat'];
                        $profileTelefon->flag = 1;
                        $profileTelefon->delete_id = 0;

                        if($profileTelefon->save()){
                            $alamat = new ProfilesAlamatPejabat;
                            $alamat->profiles_id = $profile->id;
                            $alamat->alamat = $peg_maklumat['alamat_pejabat'];
                            $alamat->flag = 1;
                            $alamat->delete_id = 0;

                            if($alamat->save()){
                                $role_user = new UserRole;
                                $role_user->user_id = $user->id;
                                $role_user->role_id = 3;

                                if($role_user->save()){
                                    return $peg_maklumat;
                                }
                            }
                        }
                    }
                }
            }
        }else {
            $profile = Profile::where('users_id', $user->id)->first();
            $profile->kpa_active = 1;
            $profile->save();

            $check_role_user = UserRole::where('user_id', $user->id)->where('role_id', 3)->first();
            if(!$check_role_user){
                $role_user = new UserRole;
                $role_user->user_id = $user->id;
                $role_user->role_id = 3;
                $role_user->save();
            }
            return $peg_maklumat;
        }
    }
}
