<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\Auditable;

class Projek extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function projek_Profile(){
        return $this->hasOne('App\Models\Profiles\Profile', 'profiles_id', 'id');
    }

    public function projek_Projek_tahun(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahun', 'projeks_id', 'id')->where('delete_id', 0)->where('flag', 1);
    }

    public function projek_Projek_tahun_selesai(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahun', 'projeks_id', 'id')->where('delete_id', 0)->where('flag', 1)->where('status', 2);
    }

    public static function getProjek($projek_id) : array{
        $model = Projek::find($projek_id);

        $data = [];
        $data['id'] = $model->id;
        $data['nama'] = $model->nama;
        return $data;
    }

    public static function createOrUpdate(Request $projekRequest) : array{
        $projek_nama = $projekRequest->input('projek_nama');
        $trigger = $projekRequest->input('trigger');
        $projek_id = $projekRequest->input('projek_id');

        if($trigger == 0){
            $c_projek = Projek::checkDuplicate($projek_nama);
            $model = new Projek;
        }else{
            $c_projek = Projek::checkDuplicate($projek_nama, $projek_id);
            $model = Projek::find($projek_id);
        }

        if($c_projek){
            return [
                'success' => 2
            ];
        }else{
            try{

                $model->profiles_id = Auth::user()->user_profile->id;
                $model->nama = $projek_nama;
                $model->flag = 1;
                $model->delete_id = 0;

                $model->save();

                return [
                    'success' => 1
                ];
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }
        }
    }

    public static function checkDuplicate($projek_nama, $id = false) : bool{
        if(!$id){
            $check = Projek::whereRaw("LOWER(nama) ilike '%".$projek_nama."%' ")->where('delete_id', '!=', '1')->where('profiles_id', Auth::user()->user_profile->id)->first();
        }else{
            $check = Projek::whereRaw("LOWER(nama) ilike '%".$projek_nama."%' ")->where('delete_id', '!=', '1')->where('profiles_id', Auth::user()->user_profile->id)->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }
}
