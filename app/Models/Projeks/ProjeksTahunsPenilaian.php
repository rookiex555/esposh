<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsPenilaian extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahun_penilaian_Projek_tahun(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahun', 'projeks_tahuns_id', 'id');
    }

    public function projek_tahun_penilaian_Projek_tahuns_penilaians_sub_penilaians(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaian', 'projeks_tahuns_penilaians_id', 'id')->where('delete_id', 0);
    }

    public function projek_tahun_penilaian_Projeks_tahuns_penilais_keputusan_log(){
       return  $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaisKeputusanLog', 'projeks_tahuns_penilaians_id', 'id');
    }

    public static function createOrUpdate(Request $request){
        $projek_penilaian_soalan_main_bil = $request->input('projek_penilaian_soalan_main_bil');
        $projek_penilaian_soalan_main_nama = $request->input('projek_penilaian_soalan_main_nama');
        $projek_penilaian_soalan_main_peratus = $request->input('projek_penilaian_soalan_main_peratus');
        $projek_tahun_id = $request->input('projek_tahun_id');
        $trigger = $request->input('trigger');
        $projek_penilaian_soalan_main_id = $request->input('projek_penilaian_soalan_main_id');

        if($trigger == 0){
            $c_nama = ProjeksTahunsPenilaian::checkDuplicate($projek_penilaian_soalan_main_nama, $projek_tahun_id);
            $projek_tahun_penilaian_main = new ProjeksTahunsPenilaian;
            $projek_tahun_penilaian_main->flag = 1;
            $projek_tahun_penilaian_main->delete_id = 0;
            $projek_tahun_penilaian_main->projeks_tahuns_id = $projek_tahun_id;
            $projek_tahun_penilaian_main->ref_id = ProjeksTahunsPenilaian::getNewRef($projek_tahun_id);
        }else{
            $c_nama = ProjeksTahunsPenilaian::checkDuplicate($projek_penilaian_soalan_main_nama, $projek_tahun_id, $projek_penilaian_soalan_main_id);
            $projek_tahun_penilaian_main = ProjeksTahunsPenilaian::find($projek_penilaian_soalan_main_id);
        }

        if($c_nama){
            return [
                'success' => 2
            ];
        }else{
            try{
                $projek_tahun_penilaian_main->bil = $projek_penilaian_soalan_main_bil;
                $projek_tahun_penilaian_main->nama = $projek_penilaian_soalan_main_nama;
                $projek_tahun_penilaian_main->peratus_penilaian = $projek_penilaian_soalan_main_peratus;

                if($projek_tahun_penilaian_main->save()){
                    return [
                        'success' => 1,
                        'data' => [
                            'title' => $projek_tahun_penilaian_main->bil ? '('.$projek_tahun_penilaian_main->bil.') '.$projek_tahun_penilaian_main->nama : $projek_tahun_penilaian_main->nama,
                            'percentage' => $projek_tahun_penilaian_main->peratus_penilaian,
                            'id' => $projek_tahun_penilaian_main->id,
                            'ref_id' => $projek_tahun_penilaian_main->ref_id
                        ],
                    ];
                }
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }

        }
    }

    public static function checkDuplicate($nama, $projek_tahun_id, $id = false) : bool{
        if(!$id){
            $check = ProjeksTahunsPenilaian::whereRaw("LOWER(nama) ilike '%".$nama."%' ")->where('projeks_tahuns_id', $projek_tahun_id)->where('delete_id', '!=', '1')->first();
        }else{
            $check = ProjeksTahunsPenilaian::whereRaw("LOWER(nama) ilike '%".$nama."%' ")->where('delete_id', '!=', '1')->where('projeks_tahuns_id', $projek_tahun_id)->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }

    public static function getNewRef($id){
        $model = ProjeksTahunsPenilaian::where('projeks_tahuns_id', $id)->count();

        return $model + 1;
    }

    public static function getAllData($projek_tahun_id){
        $data = [];
        $model = ProjeksTahunsPenilaian::where('projeks_tahuns_id', $projek_tahun_id)->where('delete_id', 0)->orderBy('ref_id', 'asc')->get();

        if($model){
            foreach($model as $m){
                $data[] = [
                    'id' => $m->id,
                    'ref_id' => $m->ref_id,
                    'nama' => $m->bil ? '('.$m->bil.') '.$m->nama : $m->nama,
                    'percentage' => $m->peratus_penilaian,
                    'sub' => ProjeksTahunsPenilaiansSubPenilaian::getAllData($m->id),
                ];
            }
        }

        return $data;
    }
}
