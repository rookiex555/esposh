<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsPenilaiansSubPenilaiansPerkara extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'projeks_tahuns_penilaians_sub_penilaians_perkaras';

    public function projek_tahuns_penilaians_sub_penilaians_perkara_Projek_tahuns_penilaians_sub_penilaians(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaian', 'id', 'projeks_tahuns_penilaians_sub_penilaians_id');
    }

    public function projek_tahuns_penilaians_sub_penilaians_perkara_Projek_tahuns_penilaians_sub_perkara_penilai_skor(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor', 'id', 'projeks_sub_penilaians_perkaras_id');
    }

    public static function getAllData($sub_id) : array{
        $model = ProjeksTahunsPenilaiansSubPenilaiansPerkara::where('projeks_tahuns_penilaians_sub_penilaians_id', $sub_id)->where('delete_id', 0)->orderBy('ref_id', 'asc')->get();

        $data = [];

        if($model){
            foreach($model as $m){
                $data[] = [
                    'id' => $m->id,
                    'bil' => $m->bil,
                    'tajuk_perkara' => $m->tajuk_perkara,
                    'no_klausa' => $m->no_klausa,
                    'ref_id' => $m->ref_id,
                    'sub_id' => $m->projeks_tahuns_penilaians_sub_penilaians_id,
                    'main_id' => $m->projek_tahuns_penilaians_sub_penilaians_perkara_Projek_tahuns_penilaians_sub_penilaians->projek_tahuns_penilaians_sub_penilaians_Projek_tahun_penilaian->id,
                ];
            }
        }

        return $data;
    }

    public static function createOrUpdate(Request $request){
        $projek_penilaian_soalan_main_sub_perkara_bilangan = $request->input('projek_penilaian_soalan_main_sub_perkara_bilangan');
        $projek_penilaian_soalan_main_sub_perkara_no_klausa = $request->input('projek_penilaian_soalan_main_sub_perkara_no_klausa');
        $projek_penilaian_soalan_main_sub_perkara_soalan = $request->input('projek_penilaian_soalan_main_sub_perkara_soalan');
        $main_id = $request->input('main_id');
        $main_sub_id = $request->input('main_sub_id');

        $trigger = $request->input('trigger');
        $projek_penilaian_soalan_main_sub_perkara_id = $request->input('projek_penilaian_soalan_main_sub_perkara_id');

        if($trigger == 0){
            $c_bil = ProjeksTahunsPenilaiansSubPenilaiansPerkara::checkDuplicate($projek_penilaian_soalan_main_sub_perkara_bilangan, $main_sub_id);
            $projek_tahun_penilaian_main_sub_perkara = new ProjeksTahunsPenilaiansSubPenilaiansPerkara;
            $projek_tahun_penilaian_main_sub_perkara->flag = 1;
            $projek_tahun_penilaian_main_sub_perkara->delete_id = 0;
            $projek_tahun_penilaian_main_sub_perkara->projeks_tahuns_penilaians_sub_penilaians_id = $main_sub_id;
            $projek_tahun_penilaian_main_sub_perkara->ref_id = ProjeksTahunsPenilaiansSubPenilaiansPerkara::getNewRef($main_sub_id);
        }else{
            $c_bil = ProjeksTahunsPenilaiansSubPenilaiansPerkara::checkDuplicate($projek_penilaian_soalan_main_sub_perkara_bilangan, $main_sub_id,$projek_penilaian_soalan_main_sub_perkara_id);
            $projek_tahun_penilaian_main_sub_perkara = ProjeksTahunsPenilaiansSubPenilaiansPerkara::find($projek_penilaian_soalan_main_sub_perkara_id);
        }

        if($c_bil){
            return [
                'success' => 2
            ];
        }else{
            try{
                $projek_tahun_penilaian_main_sub_perkara->bil = $projek_penilaian_soalan_main_sub_perkara_bilangan;
                $projek_tahun_penilaian_main_sub_perkara->no_klausa = $projek_penilaian_soalan_main_sub_perkara_no_klausa;
                $projek_tahun_penilaian_main_sub_perkara->tajuk_perkara = $projek_penilaian_soalan_main_sub_perkara_soalan;

                if($projek_tahun_penilaian_main_sub_perkara->save()){
                    return [
                        'success' => 1,
                        'data' => [
                            'bil' => $projek_tahun_penilaian_main_sub_perkara->bil,
                            'tajuk_perkara' => $projek_tahun_penilaian_main_sub_perkara->tajuk_perkara,
                            'no_klausa' => $projek_tahun_penilaian_main_sub_perkara->no_klausa,
                            'sub_id' => $projek_tahun_penilaian_main_sub_perkara->projeks_tahuns_penilaians_sub_penilaians_id,
                            'id' => $projek_tahun_penilaian_main_sub_perkara->id,
                            'main_id' => $projek_tahun_penilaian_main_sub_perkara->projek_tahuns_penilaians_sub_penilaians_perkara_Projek_tahuns_penilaians_sub_penilaians->projek_tahuns_penilaians_sub_penilaians_Projek_tahun_penilaian->id,
                            'ref_id' => $projek_tahun_penilaian_main_sub_perkara->ref_id
                        ],
                    ];
                }
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }

        }
    }

    public static function checkDuplicate($bil, $main_sub_id, $id = false) : bool{
        if(!$id){
            $check = ProjeksTahunsPenilaiansSubPenilaiansPerkara::whereRaw("LOWER(bil) ilike '%".$bil."%' ")->where('projeks_tahuns_penilaians_sub_penilaians_id', $main_sub_id)->where('delete_id', '!=', '1')->first();
        }else{
            $check = ProjeksTahunsPenilaiansSubPenilaiansPerkara::whereRaw("LOWER(bil) ilike '%".$bil."%' ")->where('projeks_tahuns_penilaians_sub_penilaians_id', $main_sub_id)->where('delete_id', '!=', '1')->where('id', '!=', $id)->first();
        }

        return (bool)$check;
    }

    public static function getNewRef($id){
        $model = ProjeksTahunsPenilaiansSubPenilaiansPerkara::where('projeks_tahuns_penilaians_sub_penilaians_id', $id)->count();

        return $model + 1;
    }
}
