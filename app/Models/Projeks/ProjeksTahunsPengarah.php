<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsPengarah extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahun_pengarah_Projek_tahun(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahun', 'projeks_tahuns_id', 'id');
    }

    public function projek_tahun_pengarah_jawatan(){
        return $this->hasOne('App\Models\Jawatan\JawatanPejabat', 'id', 'jawatan_pejabats_id');
    }

    public function projek_tahun_pengarah_jawatan_pejabat(){
        return $this->hasOne('App\Models\Jawatan\JawatanPejabatsAlamat', 'id', 'jawatan_pejabats_alamats_id');
    }

    public function projek_tahun_pengarah_jawatan_alamat(){
        return $this->hasOne('App\Models\Jawatan\JawatanPejabatsAlamat', 'id', 'jawatans_id');
    }
}
