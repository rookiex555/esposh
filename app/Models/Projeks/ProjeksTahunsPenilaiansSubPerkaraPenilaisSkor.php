<?php
namespace App\Models\Projeks;

use App\Http\Controllers\Main\CommonController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_penilaians_perkara(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaiansPerkara', 'projeks_sub_penilaians_perkaras_id', 'id');
    }

    public function projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilai(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunPenilai', 'projeks_tahuns_penilais_id', 'id');
    }

    public function projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_perkara_penilais_skors_catatan(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsCatatan', 'projeks_tahuns_penilaians_sub_perkara_penilais_skor_id', 'id');
    }

    public function projek_tahuns_penilaians_sub_perkara_penilai_skor_Projek_tahuns_penilaians_sub_perkara_penilais_skors_uploads(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsUpload', 'projeks_tahuns_penilaians_sub_perkara_penilais_skors_id', 'id')->orderBy('ref_id', 'asc');
    }

    public static function save_jawapan(Request $data){
        $ans = json_decode($data->input('answerArr'));
        $penilai_id = $data->input('penilai_id');
        $projek_tahun_id = $data->input('projek_tahun_id');
        $save_trigger = $data->input('save_trigger');

        if($save_trigger == 1){
            $projek_tahun = ProjeksTahun::find($projek_tahun_id);
            $projek_tahun->status = 2;
            $projek_tahun->save();

            $projek_penilai = ProjeksTahunPenilai::find($penilai_id);
            $projek_penilai->status = 2;
            $projek_penilai->save();
        }

        foreach($ans as $a){
            $catatan = $a->catatan;
            $skor = $a->skor;
            $perkara_id = $a->perkara_id;

            $main_skor_arr = [
                $catatan,
                $skor,
            ];

            $image_one = $data->file('img_1_perkara_id_'.$perkara_id.'');
            $image_two = $data->file('img_2_perkara_id_'.$perkara_id.'');
            $image_three = $data->file('img_3_perkara_id_'.$perkara_id.'');
            $image_four =  $data->file('img_4_perkara_id_'.$perkara_id.'');

            $main_skor_img_arr = [
                $image_one,
                $image_two,
                $image_three,
                $image_four
            ];

            $skor_main = self::createOrUpdate($penilai_id, $perkara_id, $main_skor_arr, $main_skor_img_arr);
        }

        if($save_trigger == 1){
            ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor::kiraanSkor($projek_tahun_id, $penilai_id);
        }

        return $save_trigger;
    }

    public static function createOrUpdate($penilai_id, $perkara_id, $data, $data_img){
        $model = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor::where('projeks_tahuns_penilais_id', $penilai_id)->where('projeks_sub_penilaians_perkaras_id', $perkara_id)->first();

        if(!$model){
            $skorModel = new ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor;
            $skorModel->flag = 1;
            $skorModel->delete_id = 0;
        }else{
            $skorModel = $model;
        }
        $skorModel->projeks_tahuns_penilais_id = $penilai_id;
        $skorModel->projeks_sub_penilaians_perkaras_id = $perkara_id;
        $skorModel->skor = $data[1] != '' ? $data[1] : null;

        if($skorModel->save()){
            $catatan = self::createOrUpdateCatatan($skorModel->id, $data[0], $data_img);
        }
    }

    public static function createOrUpdateCatatan($skor_id, $catatan, $data_img){
        $model = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsCatatan::where('projeks_tahuns_penilaians_sub_perkara_penilais_skor_id', $skor_id)->first();

        if(!$model){
            $skorCatatan = new ProjeksTahunsPenilaiansSubPerkaraPenilaisSkorsCatatan;
            $skorCatatan->flag = 1;
            $skorCatatan->delete_id = 0;
        }else{
            $skorCatatan = $model;
        }

        $skorCatatan->projeks_tahuns_penilaians_sub_perkara_penilais_skor_id = $skor_id;
        $skorCatatan->catatan = $catatan;

        if($skorCatatan->save()){
            $img_perkara = self::createOrUpdateUploads($skor_id, $data_img);
        }else{
            $img_perkara = self::createOrUpdateUploads($skor_id, $data_img);
        }
    }

    public static function createOrUpdateUploads($skor_id, $data_img){

        
        $model = ProjeksTahunsCatatansUpload::where('projeks_tahuns_penilaians_sub_perkara_penilais_skors_id', $skor_id)->orderBy('ref_id', 'asc')->get();
        // echo '<pre>';
        // print_r($model);
        // echo '</pre>';
        // die();
        if(count($model) == 0){
            $img_c = 0;
            $ref_id = 1;

            foreach($data_img as $img){

                $new_image = new ProjeksTahunsCatatansUpload;
                $new_image->projeks_tahuns_penilaians_sub_perkara_penilais_skors_id = $skor_id;
                $new_image->ref_id = $ref_id;
                $new_image->flag = 1;
                $new_image->delete_id = 0;

                if($img != ''){
                    $new_image->uploads_location = '/uploads/jawapan_penilaian/'.$skor_id.'';
                    $uploadImage = CommonController::upload_image($img, public_path('/uploads/jawapan_penilaian/'.$skor_id.''));
                    $new_image->uploads_name = $uploadImage;
                }
                $new_image->save();
                $img_c++;
                $ref_id++;
            }
        }else{
            $img_c = 0;
            $model_c = count($data_img);
            foreach($model as $m){
                if(isset($data_img[$img_c]) && $data_img[$img_c] != ''){
                    if($m->uploads_name){
                        unlink(public_path('/uploads/jawapan_penilaian/'.$skor_id.'/'.$m->uploads_name));
                    }
                    $m->uploads_location = '/uploads/jawapan_penilaian/'.$skor_id.'';
                    $uploadImage = CommonController::upload_image($data_img[$img_c], public_path('/uploads/jawapan_penilaian/'.$skor_id.''));
                    $m->uploads_name = $uploadImage;
                    $m->save();
                }
                $img_c++;
            }
        }
    }

    public static function kiraanSkor($projek_tahun_id, $penilai_id){

        $pt = ProjeksTahun::find($projek_tahun_id);
        $penilaians = $pt->projek_tahun_Projek_tahun_penilaian;

        foreach($penilaians as $p){
            $p_id = $p->id;
            $p_peratus = $p->peratus_penilaian;
            $sub_p = $p->projek_tahun_penilaian_Projek_tahuns_penilaians_sub_penilaians;

            $skorZero = 0;
            $skorOne = 0;
            $skorTwo = 0;
            $skorTb = 0;
            $totalSoalan = 0;

            foreach($sub_p as $sp){
                $perkaras = $sp->projek_tahuns_penilaians_sub_penilaians_Projek_tahuns_penilaians_sub_penilaians_perkara;
                foreach($perkaras as $sp_per){
                    $totalSoalan++;

                    $perkara_id = $sp_per->id;
                    $penilai_skor = ProjeksTahunsPenilaiansSubPerkaraPenilaisSkor::where('projeks_tahuns_penilais_id', $penilai_id)->where('projeks_sub_penilaians_perkaras_id', $perkara_id)->where('delete_id', 0)->where('flag', 1)->first();
                    switch ($penilai_skor->skor){
                        case 0:
                            $skorZero++;
                            break;
                        case 1:
                            $skorOne++;
                            break;
                        case 2:
                            $skorTwo++;
                            break;
                        case 'tb':
                            $skorTb++;
                            break;
                    }
                }
            }
            $skorZeroM = $skorZero * 0;
            $skorOneM = $skorOne * 0.5;
            $skorTwoM = $skorTwo * 1;
            $totalUpperSkor = $skorZeroM + $skorOneM + $skorTwoM;

            $calculate = number_format(($totalUpperSkor / ($totalSoalan - $skorTb)) * $p_peratus, 2);
            $keputusanLog = new ProjeksTahunsPenilaisKeputusanLog;
            $keputusanLog->projeks_tahuns_penilais_id = $penilai_id;
            $keputusanLog->projeks_tahuns_penilaians_id = $p_id;
            $keputusanLog->projeks_tahuns_id = $projek_tahun_id;
            $keputusanLog->peratus = $calculate;
            $keputusanLog->flag = 1;
            $keputusanLog->delete_id = 0;
            $keputusanLog->save();
        }
    }
}
