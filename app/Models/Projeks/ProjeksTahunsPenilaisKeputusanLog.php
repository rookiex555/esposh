<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsPenilaisKeputusanLog extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahun_penilais_keputusan_logs_Projek_tahun(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahun', 'projeks_tahuns_id', 'id');
    }

    public function projek_tahun_penilais_keputusan_logs_Projek_tahuns_penilaian(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsPenilaian', 'id', 'projeks_tahuns_penilaians_id');
    }

    public function projek_tahun_penilais_keputusan_logs_Projek_tahun_penilais(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunPenilai', 'projeks_tahuns_penilais_id', 'id');
    }
}
