<?php
namespace App\Models\Projeks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class ProjeksTahunsPenilaiansSubPenilaian extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public function projek_tahuns_penilaians_sub_penilaians_Projek_tahun_penilaian(){
        return $this->hasOne('App\Models\Projeks\ProjeksTahunsPenilaian', 'id', 'projeks_tahuns_penilaians_id');
    }

    public function projek_tahuns_penilaians_sub_penilaians_Projek_tahuns_penilaians_sub_penilaians_perkara(){
        return $this->hasMany('App\Models\Projeks\ProjeksTahunsPenilaiansSubPenilaiansPerkara', 'projeks_tahuns_penilaians_sub_penilaians_id', 'id')->where('delete_id', 0);
    }

    public static function createOrUpdate(Request $request){
        $projek_penilaian_soalan_main_sub_perkara = $request->input('projek_penilaian_soalan_main_sub_perkara');
        $projek_penilaian_soalan_main_sub_no_klausa = $request->input('projek_penilaian_soalan_main_sub_no_klausa');
        $projek_penilaian_soalan_main_sub_bilangan = $request->input('projek_penilaian_soalan_main_sub_bilangan');
        $projek_penilaian_soalan_main_id = $request->input('projek_penilaian_soalan_main_id');

        $trigger = $request->input('trigger');
        $projek_penilaian_soalan_main_sub_id = $request->input('projek_penilaian_soalan_main_sub_id');

        $projek_penilaian_soalan_main_id = $request->input('projek_penilaian_soalan_main_id');

        if($trigger == 0){
            $c_bil = ProjeksTahunsPenilaiansSubPenilaian::checkDuplicate($projek_penilaian_soalan_main_sub_bilangan, $projek_penilaian_soalan_main_id);
            $projek_tahun_penilaian_main_sub = new ProjeksTahunsPenilaiansSubPenilaian();
            $projek_tahun_penilaian_main_sub->flag = 1;
            $projek_tahun_penilaian_main_sub->delete_id = 0;
            $projek_tahun_penilaian_main_sub->projeks_tahuns_penilaians_id = $projek_penilaian_soalan_main_id;
            $projek_tahun_penilaian_main_sub->ref_id = ProjeksTahunsPenilaiansSubPenilaian::getNewRef($projek_penilaian_soalan_main_id);
        }else{
            $c_bil = ProjeksTahunsPenilaiansSubPenilaian::checkDuplicate($projek_penilaian_soalan_main_sub_bilangan, $projek_penilaian_soalan_main_id, $projek_penilaian_soalan_main_sub_id);
            $projek_tahun_penilaian_main_sub = ProjeksTahunsPenilaiansSubPenilaian::find($projek_penilaian_soalan_main_sub_id);
        }

        if($c_bil){
            return [
                'success' => 2
            ];
        }else{
            try{
                $projek_tahun_penilaian_main_sub->bil = $projek_penilaian_soalan_main_sub_bilangan;
                $projek_tahun_penilaian_main_sub->tajuk_perkara = $projek_penilaian_soalan_main_sub_perkara;
                $projek_tahun_penilaian_main_sub->no_klausa = $projek_penilaian_soalan_main_sub_no_klausa;

                if($projek_tahun_penilaian_main_sub->save()){
                    return [
                        'success' => 1,
                        'data' => [
                            'perkara' => '('.$projek_tahun_penilaian_main_sub->bil.')'.' '.$projek_tahun_penilaian_main_sub->tajuk_perkara,
                            'no_klausa' => $projek_tahun_penilaian_main_sub->no_klausa,
                            'bil' => $projek_tahun_penilaian_main_sub->bil,
                            'id' => $projek_tahun_penilaian_main_sub->id,
                            'ref_id' => $projek_tahun_penilaian_main_sub->ref_id,
                            'projek_tahun_penilaian_id' => $projek_tahun_penilaian_main_sub->projeks_tahuns_penilaians_id
                        ],
                    ];
                }
            }catch (Exception $e){
                return [
                    'success' => 0
                ];
            }

        }
    }

    public static function checkDuplicate($bil, $projek_penilaian_soalan_main_id, $id = false) : bool{
        if(!$id){
            $check = ProjeksTahunsPenilaiansSubPenilaian::whereRaw("LOWER(bil) ilike '%".$bil."%' ")->where('projeks_tahuns_penilaians_id', $projek_penilaian_soalan_main_id)->where('delete_id', '!=', '1')->first();
        }else{
            $check = ProjeksTahunsPenilaiansSubPenilaian::whereRaw("LOWER(no_klausa) ilike '%".$bil."%' ")->where('projeks_tahuns_penilaians_id', $projek_penilaian_soalan_main_id)->where('delete_id', '!=', '1')->where('id', '!=', $id)->first();
        }
        return (bool)$check;
    }

    public static function getNewRef($id){
        $model = ProjeksTahunsPenilaiansSubPenilaian::where('projeks_tahuns_penilaians_id', $id)->count();

        return $model + 1;
    }

    public static function getAllData($projek_tahun_penilaian_id) : array{
        $model = ProjeksTahunsPenilaiansSubPenilaian::where('projeks_tahuns_penilaians_id', $projek_tahun_penilaian_id)->where('delete_id', 0)->orderBy('ref_id', 'asc')->get();

        $data = [];

        if($model){
            foreach($model as $m){
                $data[] = [
                    'id' => $m->id,
                    'bil' => $m->bil,
                    'no_klausa' => $m->no_klausa,
                    'tajuk_perkara' => $m->tajuk_perkara,
                    'ref_id' => $m->ref_id,
                    'soalan' => ProjeksTahunsPenilaiansSubPenilaiansPerkara::getAllData($m->id)
                ];
            }
        }

        return $data;
    }
}
