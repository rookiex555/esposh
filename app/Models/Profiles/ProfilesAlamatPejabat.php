<?php
namespace App\Models\Profiles;
use OwenIt\Auditing\Contracts\Auditable;

use Illuminate\Database\Eloquent\Model;

class ProfilesAlamatPejabat extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function profile_alamat_pejabat_Profile(){
        $this->hasOne('App\Models\Profiles\Profile', 'id', 'profiles_id');
    }
}
