<?php
namespace App\Models\Profiles;
use OwenIt\Auditing\Contracts\Auditable;

use Illuminate\Database\Eloquent\Model;

class ProfilesCawangansLog extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function profile_cawangan_log_Profile(){
        return $this->hasOne('App\Models\Profiles\Profile', 'id', 'profiles_id');
    }
}
