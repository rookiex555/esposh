


<?php $__env->startSection('customCSS'); ?>
    <?php echo $__env->make('segment.layouts.asset_include_links.datatable.css.datatable_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('segment.layouts.asset_include_links.select2.css.select2_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Urus Setia</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Projek</a>
                                </li>
                                <li class="breadcrumb-item active">Senarai
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <table class="projek-table table">
                                <thead>
                                    <tr>
                                        <th>Nama</th>

                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
    <?php echo $__env->make('segment.urussetia.projek.modals.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customJS'); ?>
    
    <?php echo $__env->make('segment.layouts.asset_include_links.datatable.js.datatable_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('segment.layouts.asset_include_links.common.js.common_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
    <?php echo $__env->make('segment.layouts.asset_include_links.select2.js.select2_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;

    
    <script src="<?php echo e(asset('js_helper/segment/urussetia/projek/swal.js')); ?>"></script>
    <script src="<?php echo e(asset('js_helper/segment/urussetia/projek/page_settings.js')); ?>"></script>
    <script src="<?php echo e(asset('js_helper/segment/urussetia/projek/datatable.js')); ?>"></script>
    <script src="<?php echo e(asset('js_helper/segment/urussetia/projek/ajax.js')); ?>"></script>
    <script src="<?php echo e(asset('js_helper/segment/urussetia/projek/index.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('segment.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>