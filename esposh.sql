--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: dbms_lock; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dbms_lock;


ALTER SCHEMA dbms_lock OWNER TO postgres;

--
-- Name: dbms_profiler; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dbms_profiler;


ALTER SCHEMA dbms_profiler OWNER TO postgres;

--
-- Name: dbms_random; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dbms_random;


ALTER SCHEMA dbms_random OWNER TO postgres;

--
-- Name: dbms_scheduler; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dbms_scheduler;


ALTER SCHEMA dbms_scheduler OWNER TO postgres;

--
-- Name: dbo; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dbo;


ALTER SCHEMA dbo OWNER TO postgres;

--
-- Name: SCHEMA dbo; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA dbo IS 'dbo schema';


--
-- Name: sys; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sys;


ALTER SCHEMA sys OWNER TO postgres;

--
-- Name: SCHEMA sys; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sys IS 'sys schema';


--
-- Name: dss_freq_enum; Type: TYPE; Schema: sys; Owner: postgres
--

CREATE TYPE sys.dss_freq_enum AS ENUM (
    'YEARLY',
    'MONTHLY',
    'WEEKLY',
    'DAILY',
    'HOURLY',
    'MINUTELY',
    'SECONDLY'
);


ALTER TYPE sys.dss_freq_enum OWNER TO postgres;

--
-- Name: dss_program_type_enum; Type: TYPE; Schema: sys; Owner: postgres
--

CREATE TYPE sys.dss_program_type_enum AS ENUM (
    'PLSQL_BLOCK',
    'STORED_PROCEDURE'
);


ALTER TYPE sys.dss_program_type_enum OWNER TO postgres;

--
-- Name: scheduler_0100_component_name_type; Type: TYPE; Schema: sys; Owner: postgres
--

CREATE TYPE sys.scheduler_0100_component_name_type AS (
	dsc_name character varying
);


ALTER TYPE sys.scheduler_0100_component_name_type OWNER TO postgres;

--
-- Name: scheduler_0250_program_argument_type; Type: TYPE; Schema: sys; Owner: postgres
--

CREATE TYPE sys.scheduler_0250_program_argument_type AS (
	dspa_program_id integer,
	dspa_owner character varying,
	dspa_argument_position integer,
	dspa_argument_name character varying,
	dspa_argument_type character varying,
	dspa_default_value character varying,
	dspa_default_value_set boolean,
	dspa_out_argument boolean
);


ALTER TYPE sys.scheduler_0250_program_argument_type OWNER TO postgres;

--
-- Name: scheduler_0400_job_type; Type: TYPE; Schema: sys; Owner: postgres
--

CREATE TYPE sys.scheduler_0400_job_type AS (
	dsj_owner character varying,
	dsj_job_name character varying,
	dsj_job_id integer,
	dsj_program_id integer,
	dsj_schedule_id integer,
	dsj_job_class character varying,
	dsj_enabled boolean,
	dsj_auto_drop boolean,
	dsj_search_path text,
	dsj_comments character varying
);


ALTER TYPE sys.scheduler_0400_job_type OWNER TO postgres;

--
-- Name: scheduler_0450_job_argument_type; Type: TYPE; Schema: sys; Owner: postgres
--

CREATE TYPE sys.scheduler_0450_job_argument_type AS (
	dsja_job_id integer,
	dsja_owner character varying,
	dsja_argument_position integer,
	dsja_argument_name character varying,
	dsja_argument_value character varying
);


ALTER TYPE sys.scheduler_0450_job_argument_type OWNER TO postgres;

--
-- Name: rand(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rand() RETURNS double precision
    LANGUAGE sql
    AS $$SELECT random();$$;


ALTER FUNCTION public.rand() OWNER TO postgres;

--
-- Name: substring_index(text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.substring_index(text, text, integer) RETURNS text
    LANGUAGE sql
    AS $_$SELECT array_to_string((string_to_array($1, $2)) [1:$3], $2);$_$;


ALTER FUNCTION public.substring_index(text, text, integer) OWNER TO postgres;

--
-- Name: edbreport(bigint, bigint); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.edbreport(beginsnap bigint, endsnap bigint) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$ declare
-- v_topn controls how many rows to report by section
-- v_stattype determines where user,sys or all data is used
v_topn		int := 10;
v_stattype	text := 'all';

v_start 	bigint;
v_end   	bigint;
v_db		text;
v_dbsize	text;
v_tspsize	text;
v_tabsize	numeric;
v_version	text;
v_textstr   	text;
v_currdt	text;

snaprec     	record;
spcrec		record;
nsprec		record;
tabrec		record;
parmrec		record;
toptabrec	record;
topidxrec	record;
rec		record;
dmlrec		record;
bufrec		record;

v_nspcnt	numeric := 0;
have_pg_buffercache_ext bool;

begin

v_start := beginsnap;
v_end   := endsnap;
-- get date/time
-- get period from snapids

select current_database() into v_db;
select version() into v_version;
select current_date into v_currdt;

for snaprec in select sn1.edb_id, sn1.snap_tm as start_tm, sn2.edb_id,
    sn2.snap_tm as end_tm
    from edb$snap as sn1, edb$snap as sn2
    where sn1.edb_id = v_start
    and sn2.edb_id = v_end
    order by sn1.edb_id LOOP

v_textstr := '   EnterpriseDB Report for database '||v_db||'        '||v_currdt;
return next v_textstr;
v_textstr := 'Version: '||v_version;
return next v_textstr;
v_textstr := null;
return next v_textstr;

v_textstr := '     Begin snapshot: '||v_start::text||' at '||snaprec.start_tm;
return next v_textstr;
v_textstr := '     End snapshot:   '||v_end::text||' at '||snaprec.end_tm;
return next v_textstr;

end loop;

-- Get database size
v_textstr := null;
return next v_textstr;

select pg_size_pretty(pg_database_size(v_db)) into v_dbsize;
v_textstr := 'Size of database '||v_db||' is '||v_dbsize;
return next v_textstr;

-- Get tablespace info
for spcrec in select spcname,usename from pg_catalog.pg_tablespace as tsp, pg_user as usr
	where tsp.spcowner = usr.usesysid LOOP

select pg_size_pretty(pg_tablespace_size(spcrec.spcname)) into v_tspsize;

v_textstr := '     Tablespace: '||spcrec.spcname||' Size: '||v_tspsize||' Owner: '||spcrec.usename;
return next v_textstr;

end loop;
v_textstr := null;
return next v_textstr;

-- Get schema info
if v_version like 'EnterpriseDB%' then
	for nsprec in select nspname,usename from pg_catalog.pg_namespace as nsp, pg_user as usr
	where usr.usesysid = nsp.nspowner
	and nsp.nspname not in ('sys','dbo','information_schema','pg_toast','pg_catalog')
	and nsp.nspname not like E'pg\\_temp\\_%'
	and nspheadsrc is null LOOP

	v_nspcnt := 0;

	for tabrec in select schemaname,tablename from pg_tables where schemaname = nsprec.nspname LOOP
	select pg_total_relation_size(tabrec.schemaname||'.'||tabrec.tablename) into v_tabsize;

	v_nspcnt := v_nspcnt + v_tabsize;
	end loop;

	v_textstr := 'Schema: '||rpad(nsprec.nspname,30,' ')||' Size: '||rpad(pg_size_pretty(v_nspcnt::bigint),15,' ')||
	' Owner: '||rpad(nsprec.usename,20,' ');
	return next v_textstr;

	end loop;
else
	for nsprec in select nspname,usename from pg_catalog.pg_namespace as nsp, pg_user as usr
	where usr.usesysid = nsp.nspowner
	and nsp.nspname not in ('sys','dbo','information_schema','pg_toast','pg_catalog')
	and nsp.nspname not like E'pg\\_temp\\_%'
	 LOOP

	v_nspcnt := 0;

	for tabrec in select schemaname,tablename from pg_tables where schemaname = nsprec.nspname LOOP
	select pg_total_relation_size(tabrec.schemaname||'.'||tabrec.tablename) into v_tabsize;

	v_nspcnt := v_nspcnt + v_tabsize;
	end loop;

	v_textstr := 'Schema: '||rpad(nsprec.nspname,30,' ')||' Size: '||rpad(pg_size_pretty(v_nspcnt::bigint),15,' ')||
	' Owner: '||rpad(nsprec.usename,20,' ');
	return next v_textstr;

	end loop;
end if;

v_textstr := null;
return next v_textstr;

-- Get Top 10 largest tables
v_textstr := '               Top 10 Relations by pages';
return next v_textstr;
v_textstr := null;
return next v_textstr;

v_textstr := rpad('TABLE',45,' ')||rpad('RELPAGES',10,' ');
return next v_textstr;
v_textstr := '----------------------------------------------------------------------------------';
return next v_textstr;

for toptabrec in SELECT relname, relpages FROM pg_class 
where relkind = 'r' 
ORDER BY relpages DESC
limit 10 loop

v_textstr := rpad(toptabrec.relname,45,' ')||' '||rpad(toptabrec.relpages::text,10,' ');
return next v_textstr;
end loop;

v_textstr := null;
return next v_textstr;

-- Get Top 10 largest indexes
v_textstr := '               Top 10 Indexes by pages';
return next v_textstr;
v_textstr := null;
return next v_textstr;

v_textstr := rpad('INDEX',45,' ')||rpad('RELPAGES',10,' ');
return next v_textstr;
v_textstr := '----------------------------------------------------------------------------------';
return next v_textstr;

for topidxrec in SELECT relname, relpages FROM pg_class 
where relkind = 'i' 
ORDER BY relpages DESC
limit 10 loop

v_textstr := rpad(topidxrec.relname,45,' ')||' '||rpad(topidxrec.relpages::text,10,' ');
return next v_textstr;

end loop;
v_textstr := null;
return next v_textstr;

-- Top 10 relations by update,delete,insert activity
v_textstr := '               Top 10 Relations by DML';
return next v_textstr;
v_textstr := null;
return next v_textstr;

v_textstr := rpad('SCHEMA',15,' ')||' '||rpad('RELATION',45,' ')||rpad('UPDATES',10,' ')||
rpad('DELETES',10,' ')||rpad('INSERTS',10,' ');
return next v_textstr;
v_textstr := '--------------------------------------------------------------------------------------------------';
return next v_textstr;

for dmlrec in
select schemaname as SCHEMA,
		relname as RELATION,
		n_tup_upd as UPDATES,
		n_tup_del as DELETES,
		n_tup_ins as INSERTS
from pg_stat_user_tables
order by n_tup_upd desc,n_tup_del desc, n_tup_ins desc,schemaname,relname
limit 10 LOOP

v_textstr := rpad(dmlrec.schema,15,' ')||' '||rpad(dmlrec.relation,45,' ')||rpad(dmlrec.updates::text,10,' ')||
rpad(dmlrec.deletes::text,10,' ')||rpad(dmlrec.inserts::text,10,' ');
return next v_textstr;

end loop;

v_textstr := null;
return next v_textstr;

-- Call stat functions
for rec in (select * from  stat_db_rpt(v_start::int4,v_end::int4) z )
	loop
		v_textstr := rec.z;
  		return next v_textstr;
	end loop;
v_textstr := null;
return next v_textstr;

-- If pg_buffercache extension is installed, fetch buffer usage information
select count(*) > 0 from pg_extension where extname = 'pg_buffercache' into have_pg_buffercache_ext;

if have_pg_buffercache_ext then
	-- Top 10 Relations in buffer cache
	v_textstr := '  DATA from pg_buffercache';
	return next v_textstr;
	v_textstr := null;
	return next v_textstr;

	v_textstr := rpad('RELATION',35,' ')||' '||rpad('BUFFERS',10,' ');
	return next v_textstr;
	v_textstr := '-----------------------------------------------------------------------------';
	return next v_textstr;
	for bufrec in
		SELECT c.relname, count(*) AS buffers
		FROM pg_class c
		INNER JOIN pg_buffercache b ON b.relfilenode = c.relfilenode
		INNER JOIN pg_database d ON (b.reldatabase = d.oid AND d.datname = current_database())
		GROUP BY c.relname
		ORDER BY 2 DESC LIMIT 10
	loop
		v_textstr := rpad(bufrec.relname,35,' ')||' '||rpad(bufrec.buffers::text,10,' ');
		return next v_textstr;

	end loop;
	v_textstr := null;
	return next v_textstr;
else
	v_textstr := '  DATA from pg_buffercache not included because pg_buffercache is not installed';
	return next v_textstr;
	v_textstr := null;
	return next v_textstr;
end if;

for rec in (select * from  stat_tables_rpt(v_start::int4,v_end::int4,v_topn,v_stattype) z )
	loop
  		v_textstr := rec.z;
  		return next v_textstr;
	end loop;
v_textstr := null;
return next v_textstr;

for rec in (select * from  statio_tables_rpt(v_start::int4,v_end::int4,v_topn,v_stattype) z )
	loop
  		v_textstr := rec.z;
  		return next v_textstr;
	end loop;
v_textstr := null;
return next v_textstr;
 
for rec in (select * from  stat_indexes_rpt(v_start::int4,v_end::int4,v_topn,v_stattype) z )
	loop
  		v_textstr := rec.z;
  		return next v_textstr;
	end loop;
v_textstr := null;
return next v_textstr;

for rec in (select * from  statio_indexes_rpt(v_start::int4,v_end::int4,v_topn,v_stattype) z )
	loop
  		v_textstr := rec.z;
  		return next v_textstr;
	end loop;
v_textstr := null;
return next v_textstr;

-- Incluce DRITA system data, if Enterprisedb
if v_version like 'EnterpriseDB%' then
    v_textstr := '   System Wait Information';
    return next v_textstr;
	v_textstr := null;
	return next v_textstr;

	for rec in (select * from  sys_rpt(v_start::int4,v_end::int4,v_topn) z )
	loop
  		v_textstr := rec.z;
  		return next v_textstr;
	end loop;
	v_textstr := null;
	return next v_textstr;
end if;

-- Get database parameters
v_textstr := '   Database Parameters from postgresql.conf ';
return next v_textstr;
v_textstr := null;
return next v_textstr;
v_textstr := 'PARAMETER                         SETTING                                  CONTEXT     MINVAL       MAXVAL       ';
return next v_textstr;
v_textstr := '-------------------------------------------------------------------------------------------------------------------------';
return next v_textstr;

for parmrec in select name as parm,
	-- coalesce(unit,'') as unit,
    setting as setting,
-- category,
	coalesce(context,'') as context,
--	coalesce(vartype,'') as vartype,	
	coalesce(min_val,'') as minval,
	coalesce(max_val,'') as maxval
 from pg_settings 
 order by name LOOP

v_textstr := rpad(parmrec.parm,35,' ') -- ||rpad(parmrec.unit,5,' ')
||rpad(parmrec.setting,40,' ')||' '||rpad(parmrec.context,12,' ')||
rpad(parmrec.minval::text,13,' ')||rpad(parmrec.maxval::text,12,' ');
return next v_textstr;

end loop;
end;
               $_$;


ALTER FUNCTION sys.edbreport(beginsnap bigint, endsnap bigint) OWNER TO postgres;

--
-- Name: edbsnap(); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.edbsnap() RETURNS text
    LANGUAGE plpgsql
    AS $_$    
 
declare
v_seq   bigint;
v_db    name;
msg     text;
v_version text;
v_stats_cnt	numeric :=0;

begin
select nextval('snapshot_num_seq') into v_seq;
select current_database() into v_db;
select version() into v_version;
/*
select count(*) into v_stats_cnt from pg_settings
	where name in ('stats_block_level','stats_row_level')
	and setting = 'on';

if v_stats_cnt < 2 then
	msg := 'Stats_block_level and stats_row_level parameters should be set = on.';
	return msg;
	exit;
end if;
*/
	
insert into edb$snap
values (v_seq,
        v_db,
     now(),
     null      ,
     null,
     null,
     'N')
;

if v_version like 'EnterpriseDB%' then
	insert into edb$system_waits
		select v_seq,v_db,
		wait_name  ,
		wait_count  ,
		avg_wait  ,
		max_wait  ,
		total_wait
		from system_waits;
 
	insert into edb$session_waits
		select v_seq, v_db,
		backend_id   ,
		wait_name   ,
		wait_count     ,
		avg_wait_time    ,
		max_wait_time    ,
		total_wait_time  
		, usename, query
			from session_waits LEFT OUTER JOIN pg_stat_activity 
			on backend_id = pg_stat_activity.pid;

	insert into edb$session_wait_history
		select v_seq,
		v_db,
		backend_id   ,
		seq          ,
		wait_name    ,
		elapsed      ,
		p1          ,
		p2           ,
		p3           
		from session_wait_history;
end if;

------------------------------ NEW ------------------------------
insert into edb$stat_all_tables
select v_seq,v_db,
relid              ,                     
 schemaname         ,                 
 relname            ,                
 seq_scan            ,                  
 seq_tup_read        ,                  
 idx_scan            ,                  
 idx_tup_fetch       ,                  
 n_tup_ins           ,                  
 n_tup_upd          ,                 
 n_tup_del         -- ,                 
 -- last_vacuum         ,
 -- last_autovacuum     ,
 -- last_analyze        ,
 -- last_autoanalyze   
from pg_stat_all_tables;

insert into edb$stat_all_indexes
select v_seq,v_db,
relid           ,   
 indexrelid       ,   
 schemaname       ,
 relname          ,
 indexrelname     ,
 idx_scan         ,
 idx_tup_read     ,
 idx_tup_fetch   
from pg_stat_all_indexes;

insert into edb$statio_all_tables
select v_seq,v_db,
relid             ,  
 schemaname        ,
 relname            ,
 heap_blks_read     ,
 heap_blks_hit     , 
 heap_blks_icache_hit,
 idx_blks_read      ,
 idx_blks_hit       ,
 idx_blks_icache_hit,
 toast_blks_read    ,
 toast_blks_hit     ,
 toast_blks_icache_hit,
 tidx_blks_read     ,
 tidx_blks_hit      ,
 tidx_blks_icache_hit
from pg_statio_all_tables;

insert into edb$statio_all_indexes
select v_seq,v_db,
relid           ,  
 indexrelid        ,  
 schemaname      , 
 relname          ,
 indexrelname     ,
 idx_blks_read    ,
 idx_blks_hit     ,
 idx_blks_icache_hit
from pg_statio_all_indexes;

insert into edb$stat_database
select v_seq,v_db,
datid           ,    
 datname          ,
 numbackends      ,
 xact_commit      ,
 xact_rollback   , 
 blks_read         ,
 blks_hit          ,
 blks_icache_hit
from pg_stat_database;
------------------------------ END OF NEW -----------------------
 
msg := 'Statement processed.';
return msg;
/* 
exception
when others then
msg := 'Unsupported version.';
return msg;
*/ 
end;

  $_$;


ALTER FUNCTION sys.edbsnap() OWNER TO postgres;

--
-- Name: get_snaps(); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.get_snaps() RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$    

declare
textstr text;
rec RECORD;
begin

for rec in
(select edb_id, snap_tm 
from edb$snap
order by edb_id  )  loop
textstr := rec.edb_id||' '||rec.snap_tm;
return next textstr;
end loop;


return;
end;

  $_$;


ALTER FUNCTION sys.get_snaps() OWNER TO postgres;

--
-- Name: purgesnap(integer, integer); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.purgesnap(integer, integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$ 
declare
bid     alias for $1;
eid     alias for $2;
msg text;

rowcnt  int;

begin

delete from edb$system_waits where edb_id between bid and eid;
delete from edb$session_waits where edb_id between bid and eid; 
delete from edb$session_wait_history where edb_id between bid and eid; 
delete from edb$snap where edb_id between bid and eid; 
-- added 01/07/2008 - P. Steinheuser
delete from edb$stat_database where edb_id between bid and eid; 
delete from edb$stat_all_tables where edb_id between bid and eid; 
delete from edb$statio_all_tables where edb_id between bid and eid; 
delete from edb$stat_all_indexes where edb_id between bid and eid; 
delete from edb$statio_all_indexes where edb_id between bid and eid; 

get diagnostics rowcnt = ROW_COUNT;

if rowcnt = 0  then
  msg := 'Snapshots not found.';
else
    msg := 'Snapshots in range '||bid||' to '||eid||' deleted.';
end if;

-- msg := 'Rows deleted = '||rowcnt;
  return msg;

exception
when others then
msg := 'Function failed.';
return msg;
end;

   $_$;


ALTER FUNCTION sys.purgesnap(integer, integer) OWNER TO postgres;

--
-- Name: sess_rpt(integer, integer, integer); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.sess_rpt(integer, integer, integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$       

declare
bid     alias for $1;
eid     alias for $2;
topn    alias for $3;

textstr text;
rec RECORD;
begin

textstr := 'ID   '||' '||rpad('USER',10,' ')||' '||rpad('WAIT NAME',30,' ')||' '||rpad('COUNT',5,' ')||
' '||rpad('TIME(ms)',12,' ')||' '||rpad('% WAIT SESS',10)||' '||'% WAIT ALL';

return next textstr;
textstr := '---------------------------------------------------------------------------------------------------';
return next textstr;

for rec in (select fv.backend_id,
        fv.usename,
        fv.wait_name,
        fv.waitcnt,
        fv.totwait,
        -- sw.sumwait, tw.twaits
        round(100* (fv.totwait/sw.sumwait),2) as pctwaitsess,
        round(100 *(fv.totwait/tw.twaits),2) as pctwaitall 
 from (
 select e.backend_id, 
          e.usename,
          e.wait_name,
          abs(e.wait_count - b.wait_count)  as waitcnt,   		 
          abs(e.total_wait_time - b.total_wait_time)  as totwait      
             from  edb$session_waits  as b
                    ,  edb$session_waits  as e
                  where b.edb_id              	= bid
                  and e.edb_id              	= eid
                  and b.dbname              	= e.dbname
                  and b.wait_name           	= e.wait_name
                  and b.backend_id 		= e.backend_id
                  and (b.total_wait_time > 0 or e.total_wait_time > 0)
              order by backend_id, totwait desc ) as fv , 
 	(select e2.backend_id,sum(abs(e2.total_wait_time-b2.total_wait_time)) as sumwait 
		from edb$session_waits b2,
                    edb$session_waits e2
		where b2.edb_id = bid 
                    and e2.edb_id = eid
                      and b2.backend_id = e2.backend_id
                      and b2.wait_name = e2.wait_name
            	group by e2.backend_id having sum(abs(e2.total_wait_time-b2.total_wait_time)) > 0 
                order by e2.backend_id 	) as sw  ,          
    (select sum(abs(e3.total_wait_time - b3.total_wait_time)) as twaits  
    		from edb$session_waits b3, edb$session_waits e3
 		where b3.edb_id = bid and e3.edb_id = eid
                and b3.backend_id = e3.backend_id
		and b3.wait_name = e3.wait_name) as tw
where fv.backend_id = sw.backend_id
        limit topn)  LOOP   
                                 
textstr := rec.backend_id||' '||rpad(rec.usename,10,' ')||' '||rpad(rec.wait_name,30,' ')||' '||
rpad(rec.waitcnt::text,5,' ')||' '||rpad(rec.totwait::text,12,' ')||' '||rpad(rec.pctwaitsess::text,10,' ')||' '||rec.pctwaitall;

return next textstr;
end loop;

return;
end;
 $_$;


ALTER FUNCTION sys.sess_rpt(integer, integer, integer) OWNER TO postgres;

--
-- Name: sesshist_rpt(integer, integer); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.sesshist_rpt(integer, integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$         

declare
snapid     	alias for $1;
sessid  	alias for $2;

textstr text;
rec RECORD;
begin

textstr := 'ID   '||' '||rpad('USER',10,' ')||rpad('SEQ',5,' ')||' '||rpad('WAIT NAME',24,' ')||' '||
rpad('ELAPSED(ms)',12,' ')||' '||rpad('File',6,' ')||' '||rpad('Name',20,' ')||' '||rpad('# of Blk',10,' ')||' '||rpad('Sum of Blks',12,' ');

return next textstr;
textstr := '---------------------------------------------------------------------------------------------------';
return next textstr;

for rec in (
   select b.backend_id, 
          w.usename,
	  b.seq,
          b.wait_name,
          b.elapsed,   		 
          b.p1,     
          c.relname as relname,   
	  b.p2 as p2,     
	  sum(b.p3) as p3          
            from  edb$session_wait_history  as b,
                  edb$session_waits as w,
                  (select relfilenode,relname from pg_class
                  union
                  select 0,'N/A') as c  -- added this to handle file=0 for query plan
                  where b.edb_id              	= snapid
                  and b.backend_id              = sessid
		  and b.backend_id              = w.backend_id
		  and b.dbname                  = w.dbname
		  and b.wait_name               = w.wait_name
		  and b.edb_id                  = w.edb_id
                  and b.p1                      = c.relfilenode
             group by b.backend_id, 
          w.usename,
	  b.seq,
          b.wait_name,
          b.elapsed,
           b.p1 , c.relname, b.p2      
              order by backend_id, seq   
         )  LOOP   
                                 
textstr := rec.backend_id||' '||rpad(rec.usename,10,' ')||' '||rpad(rec.seq::text,5,' ')||' '||rpad(rec.wait_name,24,' ')||
' '||rpad(rec.elapsed::text,12,' ')||' '||rpad(rec.p1::text,6,' ')||' '||rpad(rec.relname,20,' ')||' '||rpad(rec.p2::text,10,' ')||' '||rpad(rec.p3::text,12,' ');

return next textstr;
end loop;

return;
end;
   $_$;


ALTER FUNCTION sys.sesshist_rpt(integer, integer) OWNER TO postgres;

--
-- Name: sessid_rpt(integer, integer, integer); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.sessid_rpt(integer, integer, integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$       

declare
bid     alias for $1;
eid     alias for $2;
sessid    alias for $3;

textstr text;
rec RECORD;
begin

textstr := 'ID   '||' '||rpad('USER',10,' ')||' '||rpad('WAIT NAME',30,' ')||' '||rpad('COUNT',5,' ')||
' '||rpad('TIME(ms)',12,' ')||' '||rpad('% WAIT SESS',10)||' '||'% WAIT ALL';

return next textstr;
textstr := '---------------------------------------------------------------------------------------------------';
return next textstr;

for rec in (select fv.backend_id,
        fv.usename,
        fv.wait_name,
        fv.waitcnt,
        fv.totwait,
        round(100* (fv.totwait/sw.sumwait),2) as pctwaitsess,
        round(100 *(fv.totwait/tw.twaits),2) as pctwaitall 
 from (
 select e.backend_id, 
          e.usename,
          e.wait_name,
          abs(e.wait_count - b.wait_count)  as waitcnt,   		 
          abs(e.total_wait_time - b.total_wait_time)  as totwait      
            from  edb$session_waits  as b
                    ,  edb$session_waits  as e
                  where b.edb_id              	= bid
                  and e.edb_id              	= eid
                  and b.dbname              	= e.dbname
                  and b.wait_name           	= e.wait_name
                  and b.backend_id 		= e.backend_id
		and b.backend_id	= sessid
                  and (b.total_wait_time > 0 or e.total_wait_time > 0)
              order by backend_id, totwait desc ) as fv , 
 	(select e2.backend_id,sum(abs(e2.total_wait_time-b2.total_wait_time)) as sumwait 
		from edb$session_waits b2,
                    edb$session_waits e2
		where b2.edb_id = bid 
                    and e2.edb_id = eid
		and e2.wait_name = b2.wait_name
                      and b2.backend_id = e2.backend_id
		and b2.backend_id = sessid
            	group by e2.backend_id having sum(abs(e2.total_wait_time-b2.total_wait_time)) > 0 
                order by e2.backend_id 	) as sw  ,          
    (select sum(abs(e3.total_wait_time - b3.total_wait_time)) as twaits  
    		from edb$session_waits b3, edb$session_waits e3
 		where b3.edb_id = bid and e3.edb_id = eid
		and b3.backend_id = e3.backend_id
		and b3.wait_name = e3.wait_name) as tw
where fv.backend_id = sw.backend_id
        -- limit topn
)  LOOP   
                                 
textstr := rec.backend_id||' '||rpad(rec.usename,10,' ')||' '||rpad(rec.wait_name,30,' ')||' '||
rpad(rec.waitcnt::text,5,' ')||' '||rpad(rec.totwait::text,12,' ')||' '||rpad(rec.pctwaitsess::text,10,' ')||' '||rec.pctwaitall;

return next textstr;
end loop;

return;
end;
 $_$;


ALTER FUNCTION sys.sessid_rpt(integer, integer, integer) OWNER TO postgres;

--
-- Name: stat_db_rpt(integer, integer); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.stat_db_rpt(p_bid integer, p_eid integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$declare
v_bid       int4;
v_eid       int4;
v_db        text;

v_hitrate	numeric;

textstr text;
rec RECORD;

begin

select current_database() into v_db;

textstr := '  DATA from pg_stat_database';
return next textstr;
textstr := null;
return next textstr;

textstr := rpad('DATABASE',10,' ')||' '||rpad('NUMBACKENDS',12,' ')||' '||rpad('XACT COMMIT',12,' ')||
' '||rpad('XACT ROLLBACK',15,' ')||' '||rpad('BLKS READ',10,' ')||' '||rpad('BLKS HIT',10,' ')||
' '||rpad('BLKS ICACHE HIT',20,' ')||' '||rpad('HIT RATIO',10,' ')||' '||rpad('ICACHE HIT RATIO',20,' ');
return next textstr;
textstr := rpad('-', 126, '-'); -- (126 = 10*4 + 12*2 + 15 + 20*2 + 6 spaces)
return next textstr;
 
v_bid := p_bid;
v_eid := p_eid;

-----------------------------------------------------------------------------
-- NOTE: blks_fetch = blks_read + blks_hit + blks_icache_hit
-- blks_read are actual reads from the disk if the blk is not found in the 
-- shared buffers or the InfiniteCache
-- hence, hitrate is calculated as blks_hit / blks_fetch, 
-- similarly icachehitrate is calculated as blks_icache_hit / blks_fetch
-----------------------------------------------------------------------------
for rec in (
select e.datname,
	(e.numbackends - b.numbackends)  as numbackends,   	 
    (e.xact_commit - b.xact_commit)  as xact_commit,       
	(e.xact_rollback - b.xact_rollback) as xact_rollback,
	(e.blks_read - b.blks_read) as blks_read,
	(e.blks_hit - b.blks_hit) as blks_hit,
	(e.blks_icache_hit - b.blks_icache_hit) as blks_icache_hit,
    round(100 * ( (e.blks_hit - b.blks_hit)/( (e.blks_read - b.blks_read) + (e.blks_hit - b.blks_hit) + (e.blks_icache_hit - b.blks_icache_hit) )::numeric),2) as hitrate,
    round(100 * ( (e.blks_icache_hit - b.blks_icache_hit)/( (e.blks_read - b.blks_read) + (e.blks_hit - b.blks_hit) + (e.blks_icache_hit - b.blks_icache_hit) )::numeric),2) as icachehitrate
from edb$stat_database as e
    ,edb$stat_database as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.datname = v_db 
and e.datname = v_db
and ((e.blks_read - b.blks_read) + (e.blks_hit - b.blks_hit) + (e.blks_icache_hit - b.blks_icache_hit)) <> 0)
	 LOOP

textstr := rpad(rec.datname,10,' ')||' '||rpad(rec.numbackends::text,12,' ')||' '||rpad(rec.xact_commit::text,12,' ')||
' '||rpad(rec.xact_rollback::text,15,' ')||' '||rpad(rec.blks_read::text,10,' ')||' '||rpad(rec.blks_hit::text,10,' ')||
' '||rpad(rec.blks_icache_hit::text,20,' ')||' '||rpad(rec.hitrate::text,10,' ')||' '||rpad(rec.icachehitrate::text,20,' ');
return next textstr;
end loop;

return;
end;
         $_$;


ALTER FUNCTION sys.stat_db_rpt(p_bid integer, p_eid integer) OWNER TO postgres;

--
-- Name: stat_indexes_rpt(integer, integer, integer, text); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.stat_indexes_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$declare
v_bid       int4;
v_eid       int4;
v_topn      int4;
v_stat      text;

textstr text;
rec RECORD;

refcur  refcursor;

begin
textstr := '  DATA from pg_stat_all_indexes';
return next textstr;
textstr := null;
return next textstr;

textstr := rpad('SCHEMA',20,' ')||' '||rpad('RELATION',25,' ')||' '||rpad('INDEX',35,' ')||' '||rpad('IDX SCAN',10,' ')||
' '||rpad('IDX TUP READ',12,' ')||' '||rpad('IDX TUP FETCH',15,' ');
return next textstr;
textstr := '-------------------------------------------------------------------------------------------------------------------------';
return next textstr;
 
v_bid := p_bid;
v_eid := p_eid;
v_topn := p_topn;
v_stat := upper(p_stat);

if v_stat not in ('ALL','USER','SYS') then
	raise exception 'Invalid stat type.';
end if;

if v_stat = 'ALL' then
 open refcur for
select e.schemaname,
	e.relname,
	e.indexrelname,
    e.idx_scan - b.idx_scan  as idx_scan,   	 
    e.idx_tup_read - b.idx_tup_read  as idx_tup_read,       
	e.idx_tup_fetch - b.idx_tup_fetch as idx_tup_fetch
from edb$stat_all_indexes as e
    ,edb$stat_all_indexes as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.indexrelname	  = e.indexrelname
    order by idx_scan desc, indexrelname   
    limit p_topn;
elsif v_stat = 'USER' then
  open refcur for
select e.schemaname,
	e.relname,
	e.indexrelname,
    e.idx_scan - b.idx_scan  as idx_scan,   	 
    e.idx_tup_read - b.idx_tup_read  as idx_tup_read,       
	e.idx_tup_fetch - b.idx_tup_fetch as idx_tup_fetch
from edb$stat_all_indexes as e
    ,edb$stat_all_indexes as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.indexrelname	  = e.indexrelname
and e.schemaname in (select distinct schemaname from pg_stat_user_indexes)
    order by idx_scan desc, indexrelname   
    limit p_topn;
elsif v_stat = 'SYS' then
  open refcur for
select e.schemaname,
	e.relname,
	e.indexrelname,
    e.idx_scan - b.idx_scan  as idx_scan,   	 
    e.idx_tup_read - b.idx_tup_read  as idx_tup_read,       
	e.idx_tup_fetch - b.idx_tup_fetch as idx_tup_fetch
from edb$stat_all_indexes as e
    ,edb$stat_all_indexes as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.indexrelname	  = e.indexrelname
and e.schemaname in (select distinct schemaname from pg_stat_sys_indexes)
    order by idx_scan desc, indexrelname   
    limit p_topn;
end if;

loop
fetch refcur into rec;
exit when not found;

textstr := rpad(rec.schemaname,20,' ')||' '||rpad(rec.relname,25,' ')||' '||rpad(rec.indexrelname,35,' ')||
' '||rpad(rec.idx_scan::text,10,' ')||' '||rpad(rec.idx_tup_read::text,12,' ')||' '||rpad(rec.idx_tup_fetch::text,15,' ');
return next textstr;
end loop;

close refcur;
return;
end;
          $_$;


ALTER FUNCTION sys.stat_indexes_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) OWNER TO postgres;

--
-- Name: stat_tables_rpt(integer, integer, integer, text); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.stat_tables_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$declare
v_bid       int4;
v_eid       int4;
v_topn      int4;
v_stat      text;

textstr text;
rec RECORD;

refcur  refcursor;

begin
textstr := '  DATA from pg_stat_all_tables ordered by seq scan';
return next textstr;
textstr := null;
return next textstr;

textstr := rpad('SCHEMA',20,' ')||' '||rpad('RELATION',30,' ')||' '||rpad('SEQ SCAN',10,' ')||
' '||rpad('REL TUP READ',12,' ')||' '||rpad('IDX SCAN',10,' ')||' '||rpad('IDX TUP READ',12,' ')||
' '||rpad('INS',6,' ')||' '||rpad('UPD',6,' ')||' '||rpad('DEL',6,' ');
return next textstr;
textstr := '-----------------------------------------------------------------------------------------------------------------------';
return next textstr;
 
v_bid := p_bid;
v_eid := p_eid;
v_topn := p_topn;
v_stat := upper(p_stat);

if v_stat not in ('ALL','USER','SYS') then
	raise exception 'Invalid stat type.';
end if;

if v_stat = 'ALL' then
 open refcur for
select e.schemaname,
	 e.relname,
     e.seq_scan - b.seq_scan   as seq_scan,   	 
     e.seq_tup_read - b.seq_tup_read  as seq_tup_read,       
	 e.idx_scan - b.idx_scan as idx_scan,
	 e.idx_tup_fetch - b.idx_tup_fetch as idx_tup_fetch,
	 e.n_tup_ins - b.n_tup_ins as ins,
	 e.n_tup_upd - b.n_tup_upd as upd,
	 e.n_tup_del - b.n_tup_del as del 
from edb$stat_all_tables as e
    ,edb$stat_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		      = e.relname
and b.idx_scan is not null
and e.idx_scan is not null
and b.idx_tup_fetch is not null
and e.idx_tup_fetch is not null
    order by seq_scan desc, relname   
    limit p_topn;
elsif v_stat = 'USER' then
  open refcur for
select e.schemaname,
	e.relname,
    e.seq_scan - b.seq_scan   as seq_scan,   	 
    e.seq_tup_read - b.seq_tup_read   as seq_tup_read,       
	e.idx_scan - b.idx_scan  as idx_scan,
	e.idx_tup_fetch - b.idx_tup_fetch  as idx_tup_fetch,
	e.n_tup_ins - b.n_tup_ins  as ins,
	e.n_tup_upd - b.n_tup_upd  as upd,
	e.n_tup_del - b.n_tup_del  as del 
from edb$stat_all_tables as e
    ,edb$stat_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.idx_scan is not null
and e.idx_scan is not null
and b.idx_tup_fetch is not null
and e.idx_tup_fetch is not null
and e.schemaname in (select distinct schemaname from pg_stat_user_tables)
    order by seq_scan desc, relname   
    limit p_topn;
elsif v_stat = 'SYS' then
  open refcur for
select e.schemaname,
	e.relname,
    e.seq_scan - b.seq_scan  as seq_scan,   	 
    e.seq_tup_read - b.seq_tup_read  as seq_tup_read,       
	e.idx_scan - b.idx_scan as idx_scan,
	e.idx_tup_fetch - b.idx_tup_fetch as idx_tup_fetch,
	e.n_tup_ins - b.n_tup_ins as ins,
	e.n_tup_upd - b.n_tup_upd as upd,
	e.n_tup_del - b.n_tup_del as del 
from edb$stat_all_tables as e
    ,edb$stat_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.idx_scan is not null
and e.idx_scan is not null
and b.idx_tup_fetch is not null
and e.idx_tup_fetch is not null
and e.schemaname in (select distinct schemaname from pg_stat_sys_tables)
    order by seq_scan desc, relname   
    limit p_topn;
end if;

loop
fetch refcur into rec;
exit when not found;

textstr := rpad(rec.schemaname,20,' ')||' '||rpad(rec.relname,30,' ')||' '||rpad(rec.seq_scan::text,10,' ')||
' '||rpad(rec.seq_tup_read::text,12,' ')||' '||rpad(rec.idx_scan::text,10,' ')||' '||rpad(rec.idx_tup_fetch::text,12,' ')||
' '||rpad(rec.ins::text,6,' ')||' '||rpad(rec.upd::text,6,' ')||' '||rpad(rec.del::text,6,' ');
return next textstr;
end loop;

close refcur;

textstr := null;
return next textstr;
textstr := '  DATA from pg_stat_all_tables ordered by rel tup read';
return next textstr;
textstr := null;
return next textstr;

textstr := rpad('SCHEMA',20,' ')||' '||rpad('RELATION',30,' ')||' '||rpad('SEQ SCAN',10,' ')||
' '||rpad('REL TUP READ',12,' ')||' '||rpad('IDX SCAN',10,' ')||' '||rpad('IDX TUP READ',12,' ')||
' '||rpad('INS',6,' ')||' '||rpad('UPD',6,' ')||' '||rpad('DEL',6,' ');
return next textstr;
textstr := '-----------------------------------------------------------------------------------------------------------------------';
return next textstr;

if v_stat = 'ALL' then
 open refcur for
select e.schemaname,
	 e.relname,
     e.seq_scan - b.seq_scan   as seq_scan,   	 
     e.seq_tup_read - b.seq_tup_read  as seq_tup_read,       
	 e.idx_scan - b.idx_scan as idx_scan,
	 e.idx_tup_fetch - b.idx_tup_fetch as idx_tup_fetch,
	 e.n_tup_ins - b.n_tup_ins as ins,
	 e.n_tup_upd - b.n_tup_upd as upd,
	 e.n_tup_del - b.n_tup_del as del 
from edb$stat_all_tables as e
    ,edb$stat_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		      = e.relname
and b.idx_scan is not null
and e.idx_scan is not null
and b.idx_tup_fetch is not null
and e.idx_tup_fetch is not null
    order by seq_tup_read desc, relname   
    limit p_topn;
elsif v_stat = 'USER' then
  open refcur for
select e.schemaname,
	e.relname,
    e.seq_scan - b.seq_scan   as seq_scan,   	 
    e.seq_tup_read - b.seq_tup_read   as seq_tup_read,       
	e.idx_scan - b.idx_scan  as idx_scan,
	e.idx_tup_fetch - b.idx_tup_fetch  as idx_tup_fetch,
	e.n_tup_ins - b.n_tup_ins  as ins,
	e.n_tup_upd - b.n_tup_upd  as upd,
	e.n_tup_del - b.n_tup_del  as del 
from edb$stat_all_tables as e
    ,edb$stat_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.idx_scan is not null
and e.idx_scan is not null
and b.idx_tup_fetch is not null
and e.idx_tup_fetch is not null
and e.schemaname in (select distinct schemaname from pg_stat_user_tables)
    order by seq_tup_read desc, relname   
    limit p_topn;
elsif v_stat = 'SYS' then
  open refcur for
select e.schemaname,
	e.relname,
    e.seq_scan - b.seq_scan  as seq_scan,   	 
    e.seq_tup_read - b.seq_tup_read  as seq_tup_read,       
	e.idx_scan - b.idx_scan as idx_scan,
	e.idx_tup_fetch - b.idx_tup_fetch as idx_tup_fetch,
	e.n_tup_ins - b.n_tup_ins as ins,
	e.n_tup_upd - b.n_tup_upd as upd,
	e.n_tup_del - b.n_tup_del as del 
from edb$stat_all_tables as e
    ,edb$stat_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.idx_scan is not null
and e.idx_scan is not null
and b.idx_tup_fetch is not null
and e.idx_tup_fetch is not null
and e.schemaname in (select distinct schemaname from pg_stat_sys_tables)
    order by seq_tup_read desc, relname   
    limit p_topn;
end if;

loop
fetch refcur into rec;
exit when not found;

textstr := rpad(rec.schemaname,20,' ')||' '||rpad(rec.relname,30,' ')||' '||rpad(rec.seq_scan::text,10,' ')||
' '||rpad(rec.seq_tup_read::text,12,' ')||' '||rpad(rec.idx_scan::text,10,' ')||' '||rpad(rec.idx_tup_fetch::text,12,' ')||
' '||rpad(rec.ins::text,6,' ')||' '||rpad(rec.upd::text,6,' ')||' '||rpad(rec.del::text,6,' ');
return next textstr;
end loop;

close refcur;

return;
end;
         $_$;


ALTER FUNCTION sys.stat_tables_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) OWNER TO postgres;

--
-- Name: statio_indexes_rpt(integer, integer, integer, text); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.statio_indexes_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$declare
v_bid       int4;
v_eid       int4;
v_topn      int4;
v_stat      text;

textstr text;
rec RECORD;

refcur  refcursor;

begin
textstr := '  DATA from pg_statio_all_indexes';
return next textstr;
textstr := null;
return next textstr;

textstr := rpad('SCHEMA',20,' ')||' '||rpad('RELATION',25,' ')||' '||rpad('INDEX',35,' ')||' '||rpad('IDX BLKS READ',15,' ')||
' '||rpad('IDX BLKS HIT',15,' ')||' '||rpad('IDX BLKS ICACHE HIT',20,' ');
return next textstr;
textstr := rpad('-',135,'-'); -- (135 = 20*2 + 25 + 35 + 15*2 + 5 spaces)
return next textstr;
 
v_bid := p_bid;
v_eid := p_eid;
v_topn := p_topn;
v_stat := upper(p_stat);

if v_stat not in ('ALL','USER','SYS') then
	raise exception 'Invalid stat type.';
end if;

if v_stat = 'ALL' then
 open refcur for
select e.schemaname,
	e.relname,
	e.indexrelname,
    e.idx_blks_read - b.idx_blks_read  as idx_blks_read,       
	e.idx_blks_hit - b.idx_blks_hit as idx_blks_hit,
	e.idx_blks_icache_hit - b.idx_blks_icache_hit as idx_blks_icache_hit
from edb$statio_all_indexes as e
    ,edb$statio_all_indexes as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.indexrelname	  = e.indexrelname
    order by idx_blks_hit desc, indexrelname   
    limit p_topn;
elsif v_stat = 'USER' then
  open refcur for
select e.schemaname,
	e.relname,
	e.indexrelname,
    e.idx_blks_read - b.idx_blks_read  as idx_blks_read,       
	e.idx_blks_hit - b.idx_blks_hit as idx_blks_hit,
	e.idx_blks_icache_hit - b.idx_blks_icache_hit as idx_blks_icache_hit
from edb$statio_all_indexes as e
    ,edb$statio_all_indexes as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.indexrelname	  = e.indexrelname
and e.schemaname in (select distinct schemaname from pg_statio_user_indexes)
    order by idx_blks_hit desc, indexrelname   
    limit p_topn;
elsif v_stat = 'SYS' then
  open refcur for
select e.schemaname,
	e.relname,
	e.indexrelname,
    e.idx_blks_read - b.idx_blks_read  as idx_blks_read,       
	e.idx_blks_hit - b.idx_blks_hit as idx_blks_hit,
	e.idx_blks_icache_hit - b.idx_blks_icache_hit as idx_blks_icache_hit
from edb$statio_all_indexes as e
    ,edb$statio_all_indexes as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and b.indexrelname	  = e.indexrelname
and e.schemaname in (select distinct schemaname from pg_statio_sys_indexes)
    order by idx_blks_hit desc, indexrelname   
    limit p_topn;
end if;

loop
fetch refcur into rec;
exit when not found;

textstr := rpad(rec.schemaname,20,' ')||' '||rpad(rec.relname,25,' ')||' '||rpad(rec.indexrelname,35,' ')||
' '||rpad(rec.idx_blks_read::text,15,' ')||' '||rpad(rec.idx_blks_hit::text,15,' ')||' '||rpad(rec.idx_blks_icache_hit::text,20,' ');
return next textstr;
end loop;

close refcur;
return;
end;
         $_$;


ALTER FUNCTION sys.statio_indexes_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) OWNER TO postgres;

--
-- Name: statio_tables_rpt(integer, integer, integer, text); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.statio_tables_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$declare
v_bid       int4;
v_eid       int4;
v_topn      int4;
v_stat      text;

textstr text;
rec RECORD;

refcur  refcursor;

begin
textstr := '  DATA from pg_statio_all_tables';
return next textstr;
textstr := null;
return next textstr;

textstr := rpad('SCHEMA',20,' ')||' '||rpad('RELATION',20,' ')||
' '||rpad('HEAP',8,' ') ||' '||rpad('HEAP',8,' ') ||' '||rpad('HEAP',8,' ')||
' '||rpad('IDX',8,' ')  ||' '||rpad('IDX',8,' ')  ||' '||rpad('IDX',8,' ')||
' '||rpad('TOAST',8,' ')||' '||rpad('TOAST',8,' ')||' '||rpad('TOAST',8,' ')||
' '||rpad('TIDX',8,' ') ||' '||rpad('TIDX',8,' ') ||' '||rpad('TIDX',8,' ');
return next textstr;
textstr := rpad('      ',20,' ')||' '||rpad('        ',20,' ')||
' '||rpad('READ',8,' ')||' '||rpad('HIT',8,' ')||' '||rpad('ICACHE',8,' ')||
' '||rpad('READ',8,' ')||' '||rpad('HIT',8,' ')||' '||rpad('ICACHE',8,' ')||
' '||rpad('READ',8,' ')||' '||rpad('HIT',8,' ')||' '||rpad('ICACHE',8,' ')||
' '||rpad('READ',8,' ')||' '||rpad('HIT',8,' ')||' '||rpad('ICACHE',8,' ');
return next textstr;
textstr := rpad('      ',20,' ')||' '||rpad('        ',20,' ')||
' '||rpad('    ',8,' ')||' '||rpad('   ',8,' ')||' '||rpad('HIT',8,' ')||
' '||rpad('    ',8,' ')||' '||rpad('   ',8,' ')||' '||rpad('HIT',8,' ')||
' '||rpad('    ',8,' ')||' '||rpad('   ',8,' ')||' '||rpad('HIT',8,' ')||
' '||rpad('    ',8,' ')||' '||rpad('   ',8,' ')||' '||rpad('HIT',8,' ');
return next textstr;
textstr := rpad('-', 149, '-'); -- (149 = 20*2 + 8*12 + 13 spaces)
return next textstr;
 
v_bid := p_bid;
v_eid := p_eid;
v_topn := p_topn;
v_stat := upper(p_stat);

if v_stat not in ('ALL','USER','SYS') then
	raise exception 'Invalid stat type.';
end if;

if v_stat = 'ALL' then
 open refcur for
 select e.schemaname,
	e.relname,
    e.heap_blks_read - b.heap_blks_read  as heap_blks_read,   	 
    e.heap_blks_hit - b.heap_blks_hit  as heap_blks_hit,       
    e.heap_blks_icache_hit - b.heap_blks_icache_hit  as heap_blks_icache_hit,
	coalesce(e.idx_blks_read,0) - coalesce(b.idx_blks_read,0) as idx_blks_read,
	coalesce(e.idx_blks_hit,0) - coalesce(b.idx_blks_hit,0) as idx_blks_hit,
        coalesce(e.idx_blks_icache_hit,0) - coalesce(b.idx_blks_icache_hit,0) as idx_blks_icache_hit,
	coalesce(e.toast_blks_read,0) - coalesce(b.toast_blks_read,0) as toast_blks_read,
	coalesce(e.toast_blks_hit,0) - coalesce(b.toast_blks_hit,0) as toast_blks_hit,
        coalesce(e.toast_blks_icache_hit,0) - coalesce(b.toast_blks_icache_hit,0) as toast_blks_icache_hit,
	coalesce(e.tidx_blks_read,0) - coalesce(b.tidx_blks_read,0) as tidx_blks_read,
 	coalesce(e.tidx_blks_hit,0) - coalesce(b.tidx_blks_hit,0) as tidx_blks_hit,
        coalesce(e.tidx_blks_icache_hit,0) - coalesce(b.tidx_blks_icache_hit,0) as tidx_blks_icache_hit
from edb$statio_all_tables as e
    ,edb$statio_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
    order by heap_blks_hit desc, relname   
    limit p_topn;
elsif v_stat = 'USER' then
  open refcur for
select e.schemaname,
	e.relname,
    e.heap_blks_read - b.heap_blks_read  as heap_blks_read,   	 
    e.heap_blks_hit - b.heap_blks_hit  as heap_blks_hit,       
    e.heap_blks_icache_hit - b.heap_blks_icache_hit  as heap_blks_icache_hit,
	coalesce(e.idx_blks_read,0) - coalesce(b.idx_blks_read,0) as idx_blks_read,
	coalesce(e.idx_blks_hit,0) - coalesce(b.idx_blks_hit,0) as idx_blks_hit,
	coalesce(e.idx_blks_icache_hit,0) - coalesce(b.idx_blks_icache_hit,0) as idx_blks_icache_hit,
	coalesce(e.toast_blks_read,0) - coalesce(b.toast_blks_read,0) as toast_blks_read,
	coalesce(e.toast_blks_hit,0) - coalesce(b.toast_blks_hit,0) as toast_blks_hit,
	coalesce(e.toast_blks_icache_hit,0) - coalesce(b.toast_blks_icache_hit,0) as toast_blks_icache_hit,
	coalesce(e.tidx_blks_read,0) - coalesce(b.tidx_blks_read,0) as tidx_blks_read,
 	coalesce(e.tidx_blks_hit,0) - coalesce(b.tidx_blks_hit,0) as tidx_blks_hit,
 	coalesce(e.tidx_blks_icache_hit,0) - coalesce(b.tidx_blks_icache_hit,0) as tidx_blks_icache_hit
from edb$statio_all_tables as e
    ,edb$statio_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and e.schemaname in (select distinct schemaname from pg_statio_user_tables)
    order by heap_blks_read desc, relname   
    limit p_topn;
elsif v_stat = 'SYS' then
  open refcur for
select e.schemaname,
	e.relname,
    e.heap_blks_read - b.heap_blks_read  as heap_blks_read,   	 
    e.heap_blks_hit - b.heap_blks_hit  as heap_blks_hit,       
    e.heap_blks_icache_hit - b.heap_blks_icache_hit  as heap_blks_icache_hit,
	coalesce(e.idx_blks_read,0) - coalesce(b.idx_blks_read,0) as idx_blks_read,
	coalesce(e.idx_blks_hit,0) - coalesce(b.idx_blks_hit,0) as idx_blks_hit,
	coalesce(e.idx_blks_icache_hit,0) - coalesce(b.idx_blks_icache_hit,0) as idx_blks_icache_hit,
	coalesce(e.toast_blks_read,0) - coalesce(b.toast_blks_read,0) as toast_blks_read,
	coalesce(e.toast_blks_hit,0) - coalesce(b.toast_blks_hit,0) as toast_blks_hit,
	coalesce(e.toast_blks_icache_hit,0) - coalesce(b.toast_blks_icache_hit,0) as toast_blks_icache_hit,
	coalesce(e.tidx_blks_read,0) - coalesce(b.tidx_blks_read,0) as tidx_blks_read,
 	coalesce(e.tidx_blks_hit,0) - coalesce(b.tidx_blks_hit,0) as tidx_blks_hit,
 	coalesce(e.tidx_blks_icache_hit,0) - coalesce(b.tidx_blks_icache_hit,0) as tidx_blks_icache_hit
from edb$statio_all_tables as e
    ,edb$statio_all_tables as b
where b.edb_id            = p_bid
and e.edb_id              = p_eid
and b.dbname              = e.dbname
and b.schemaname          = e.schemaname
and b.relname		  = e.relname
and e.schemaname in (select distinct schemaname from pg_statio_sys_tables)
    order by heap_blks_read desc, relname   
    limit p_topn;
end if;

loop
fetch refcur into rec;
exit when not found;

textstr := rpad(rec.schemaname,20,' ')||' '||rpad(rec.relname,20,' ')||
' '||rpad(rec.heap_blks_read::text,8,' ') ||' '||rpad(rec.heap_blks_hit::text,8,' ') ||' '||rpad(rec.heap_blks_icache_hit::text,8,' ')||
' '||rpad(rec.idx_blks_read::text,8,' ')  ||' '||rpad(rec.idx_blks_hit::text,8,' ')  ||' '||rpad(rec.idx_blks_icache_hit::text,8,' ')||
' '||rpad(rec.toast_blks_read::text,8,' ')||' '||rpad(rec.toast_blks_hit::text,8,' ')||' '||rpad(rec.toast_blks_icache_hit::text,8,' ')||
' '||rpad(rec.tidx_blks_read::text,8,' ') ||' '||rpad(rec.tidx_blks_hit::text,8,' ') ||' '||rpad(rec.tidx_blks_icache_hit::text,8,' ');
return next textstr;
end loop;

close refcur;
return;
end;
          $_$;


ALTER FUNCTION sys.statio_tables_rpt(p_bid integer, p_eid integer, p_topn integer, p_stat text) OWNER TO postgres;

--
-- Name: sys_rpt(integer, integer, integer); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.sys_rpt(integer, integer, integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$      

declare
bid     alias for $1;
eid     alias for $2;
topn    alias for $3;

textstr text;
rec RECORD;
begin

textstr := rpad('WAIT NAME',40,' ')||' '||rpad('COUNT',10,' ')||' '||rpad('WAIT TIME',15,' ')||
' '||'% WAIT';
return next textstr;
textstr := '---------------------------------------------------------------------------';
return next textstr;

for rec in (
select e.wait_name,
        abs(e.wait_count - b.wait_count)  as wait_count,   -- sum of all session wait counts
        abs(e.totalwait - b.totalwait)  as totalwait,      -- sum of all session wait times
        round(100* (sumwait/twaits),2) as pctwait     -- divides total waits per event/total of all waits 
                 from  edb$system_waits as b
                    ,  edb$system_waits  as e
    , (select b.wait_name ,sum(abs(e.totalwait - b.totalwait)) as sumwait 
		from edb$system_waits b,
		edb$system_waits e
	where b.edb_id = bid
	and e.edb_id = eid
	and b.wait_name = e.wait_name 
        group by b.wait_name
        ) as sw,           			-- this gets total waits per event for snap period
        (select sum(abs(e.totalwait - b.totalwait)) as twaits 
     		from edb$system_waits b,
			edb$system_waits e
		where b.edb_id = bid
		and e.edb_id = eid
		and b.wait_name = e.wait_name) as tw  -- this gets sum of all waits per snap period
where b.edb_id              = bid
and e.edb_id              = eid
and b.dbname              = e.dbname
and b.wait_name           = e.wait_name
and e.wait_name          = sw.wait_name
and twaits               > 0              -- avoids divide by zero error
    order by pctwait desc, totalwait desc 
    limit topn)  LOOP                               

textstr := rpad(rec.wait_name,40,' ')||' '||rpad(rec.wait_count::text,10,' ')||' '||rpad(rec.totalwait::text,15,' ')||
' '||rec.pctwait;
return next textstr;
end loop;

return;
end;
    $_$;


ALTER FUNCTION sys.sys_rpt(integer, integer, integer) OWNER TO postgres;

--
-- Name: truncsnap(); Type: FUNCTION; Schema: sys; Owner: postgres
--

CREATE FUNCTION sys.truncsnap() RETURNS text
    LANGUAGE plpgsql
    AS $_$ declare
msg text;

begin

truncate table edb$system_waits;
truncate table edb$session_waits;
truncate table edb$session_wait_history;
truncate table edb$snap;
-- added 01/07/2008 - P. Steinheuser
truncate table edb$stat_database;
truncate table edb$stat_all_tables;
truncate table edb$statio_all_tables;
truncate table edb$stat_all_indexes;
truncate table edb$statio_all_indexes;

msg := 'Snapshots truncated.';
return msg;

exception
when others then
msg := 'Function failed.';
return msg;
end;

   $_$;


ALTER FUNCTION sys.truncsnap() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: jawatans; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jawatans (
    id integer NOT NULL,
    name character varying(255),
    flag integer DEFAULT 1,
    delete_id integer DEFAULT 0,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.jawatans OWNER TO postgres;

--
-- Name: jawatans_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jawatans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jawatans_id_seq OWNER TO postgres;

--
-- Name: jawatans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jawatans_id_seq OWNED BY public.jawatans.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: permission_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permission_role (
    permission_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.permission_role OWNER TO postgres;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255),
    description character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_id_seq OWNER TO postgres;

--
-- Name: profiles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profiles (
    id integer DEFAULT nextval('public.profiles_id_seq'::regclass) NOT NULL,
    users_id integer NOT NULL,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.profiles OWNER TO postgres;

--
-- Name: profiles_alamat_pejabats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profiles_alamat_pejabats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_alamat_pejabats_id_seq OWNER TO postgres;

--
-- Name: profiles_alamat_pejabats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profiles_alamat_pejabats (
    id integer DEFAULT nextval('public.profiles_alamat_pejabats_id_seq'::regclass) NOT NULL,
    profiles_id integer,
    alamat text,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.profiles_alamat_pejabats OWNER TO postgres;

--
-- Name: profiles_cawangans_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profiles_cawangans_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_cawangans_logs_id_seq OWNER TO postgres;

--
-- Name: profiles_cawangans_logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profiles_cawangans_logs (
    id integer DEFAULT nextval('public.profiles_cawangans_logs_id_seq'::regclass) NOT NULL,
    profiles_id integer,
    neg_perseketuan integer,
    sektor text,
    cawangan text,
    bahagian text,
    unit text,
    tahun bigint,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    penempatan text,
    sektor_name text,
    cawangan_name text,
    bahagian_name text,
    unit_name text,
    penempatan_name text
);


ALTER TABLE public.profiles_cawangans_logs OWNER TO postgres;

--
-- Name: profiles_telefons_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profiles_telefons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_telefons_id_seq OWNER TO postgres;

--
-- Name: profiles_telefons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profiles_telefons (
    id integer DEFAULT nextval('public.profiles_telefons_id_seq'::regclass) NOT NULL,
    profiles_id integer,
    no_tel_pejabat character varying(255),
    no_tel_bimbit character varying(255),
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.profiles_telefons OWNER TO postgres;

--
-- Name: projeks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_id_seq OWNER TO postgres;

--
-- Name: projeks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks (
    id integer DEFAULT nextval('public.projeks_id_seq'::regclass) NOT NULL,
    profiles_id integer,
    nama character varying(255),
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.projeks OWNER TO postgres;

--
-- Name: projeks_tahun_penilais_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahun_penilais_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahun_penilais_id_seq OWNER TO postgres;

--
-- Name: projeks_tahun_penilais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahun_penilais (
    id integer DEFAULT nextval('public.projeks_tahun_penilais_id_seq'::regclass) NOT NULL,
    projeks_tahuns_id integer,
    nokp bigint,
    nama character varying(255),
    email character varying(255),
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    status integer
);


ALTER TABLE public.projeks_tahun_penilais OWNER TO postgres;

--
-- Name: projeks_tahuns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns (
    id integer DEFAULT nextval('public.projeks_tahuns_id_seq'::regclass) NOT NULL,
    projeks_id integer,
    tahuns_id integer,
    tkh_milik_tpk date,
    tkh_siap_asal date,
    tkh_siap_semasa date,
    tkh_penilaian date,
    no_klausa character varying(255),
    kemajuan_penilaian integer,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    status integer
);


ALTER TABLE public.projeks_tahuns OWNER TO postgres;

--
-- Name: projeks_tahuns_catatans_uploads; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_catatans_uploads (
    id integer,
    projeks_tahuns_id integer,
    catatan text,
    uploads character varying(255),
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.projeks_tahuns_catatans_uploads OWNER TO postgres;

--
-- Name: projeks_tahuns_pengarahs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_pengarahs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_pengarahs_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns_pengarahs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_pengarahs (
    id integer DEFAULT nextval('public.projeks_tahuns_pengarahs_id_seq'::regclass) NOT NULL,
    projeks_tahuns_id integer,
    jawatans_id integer,
    nama character varying(255),
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.projeks_tahuns_pengarahs OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_penilaians_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_penilaians_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_penilaians (
    id integer DEFAULT nextval('public.projeks_tahuns_penilaians_id_seq'::regclass) NOT NULL,
    projeks_tahuns_id integer,
    nama character varying(255),
    peratus_penilaian real,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    ref_id integer
);


ALTER TABLE public.projeks_tahuns_penilaians OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_penilaians_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_penilaians_sub_penilaians_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_penilaians_sub_penilaians_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_penilaians; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_penilaians_sub_penilaians (
    id integer DEFAULT nextval('public.projeks_tahuns_penilaians_sub_penilaians_id_seq'::regclass) NOT NULL,
    projeks_tahuns_penilaians_id integer,
    bil character varying(50),
    tajuk_perkara character varying(255),
    no_klausa character varying(255),
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    ref_id integer
);


ALTER TABLE public.projeks_tahuns_penilaians_sub_penilaians OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_penilaians_perkaras_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_penilaians_sub_penilaians_perkaras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_penilaians_sub_penilaians_perkaras_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_penilaians_perkaras; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_penilaians_sub_penilaians_perkaras (
    id integer DEFAULT nextval('public.projeks_tahuns_penilaians_sub_penilaians_perkaras_id_seq'::regclass) NOT NULL,
    projeks_tahuns_penilaians_sub_penilaians_id integer,
    bil character varying(255),
    tajuk_perkara character varying(500),
    no_klausa character varying(255),
    question_log integer,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    ref_id integer
);


ALTER TABLE public.projeks_tahuns_penilaians_sub_penilaians_perkaras OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors (
    id integer DEFAULT nextval('public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_id_seq'::regclass) NOT NULL,
    projeks_tahuns_penilais_id integer,
    projeks_sub_penilaians_perkaras_id integer,
    skor text,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans_i; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans_i
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans_i OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans (
    id integer DEFAULT nextval('public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans_i'::regclass) NOT NULL,
    projeks_tahuns_penilaians_sub_perkara_penilais_skor_id integer,
    catatan text,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans OWNER TO postgres;

--
-- Name: skor_uploading; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.skor_uploading
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skor_uploading OWNER TO postgres;

--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads (
    id integer DEFAULT nextval('public.skor_uploading'::regclass) NOT NULL,
    projeks_tahuns_penilaians_sub_perkara_penilais_skors_id integer,
    uploads_name text,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    ref_id integer,
    uploads_location text
);


ALTER TABLE public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads OWNER TO postgres;

--
-- Name: projeks_tahuns_penilais_keputusan_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_penilais_keputusan_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_penilais_keputusan_logs_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns_penilais_keputusan_logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_penilais_keputusan_logs (
    id integer DEFAULT nextval('public.projeks_tahuns_penilais_keputusan_logs_id_seq'::regclass) NOT NULL,
    projeks_tahuns_penilais_id integer,
    projeks_tahuns_penilaians_id integer,
    projeks_tahuns_id integer,
    peratus text,
    question_history_log integer,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.projeks_tahuns_penilais_keputusan_logs OWNER TO postgres;

--
-- Name: projeks_tahuns_wakils_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projeks_tahuns_wakils_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projeks_tahuns_wakils_id_seq OWNER TO postgres;

--
-- Name: projeks_tahuns_wakils; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projeks_tahuns_wakils (
    id integer DEFAULT nextval('public.projeks_tahuns_wakils_id_seq'::regclass) NOT NULL,
    projeks_tahuns_id integer,
    jawatans_id integer,
    nama character varying(255),
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.projeks_tahuns_wakils OWNER TO postgres;

--
-- Name: role_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_user_id_seq OWNER TO postgres;

--
-- Name: role_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_user (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    id integer DEFAULT nextval('public.role_user_id_seq'::regclass) NOT NULL
);


ALTER TABLE public.role_user OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer DEFAULT nextval('public.roles_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255),
    description character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: tahuns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tahuns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tahuns_id_seq OWNER TO postgres;

--
-- Name: tahuns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tahuns (
    id integer DEFAULT nextval('public.tahuns_id_seq'::regclass) NOT NULL,
    tahun integer,
    flag integer,
    delete_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.tahuns OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    nokp bigint
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: edb$session_wait_history; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$session_wait_history" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    backend_id bigint NOT NULL,
    seq bigint NOT NULL,
    wait_name text,
    elapsed bigint,
    p1 bigint,
    p2 bigint,
    p3 bigint
);


ALTER TABLE sys."edb$session_wait_history" OWNER TO postgres;

--
-- Name: edb$session_waits; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$session_waits" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    backend_id bigint NOT NULL,
    wait_name text NOT NULL,
    wait_count bigint,
    avg_wait_time numeric,
    max_wait_time numeric(50,6),
    total_wait_time numeric(50,6),
    usename name,
    query text
);


ALTER TABLE sys."edb$session_waits" OWNER TO postgres;

--
-- Name: edb$snap; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$snap" (
    edb_id bigint NOT NULL,
    dbname name,
    snap_tm timestamp without time zone,
    start_tm timestamp without time zone,
    backend_id bigint,
    comment text,
    baseline_ind character(1)
);


ALTER TABLE sys."edb$snap" OWNER TO postgres;

--
-- Name: edb$stat_all_indexes; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$stat_all_indexes" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    relid oid NOT NULL,
    indexrelid oid NOT NULL,
    schemaname name,
    relname name,
    indexrelname name,
    idx_scan bigint,
    idx_tup_read bigint,
    idx_tup_fetch bigint
);


ALTER TABLE sys."edb$stat_all_indexes" OWNER TO postgres;

--
-- Name: edb$stat_all_tables; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$stat_all_tables" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    relid oid NOT NULL,
    schemaname name,
    relname name,
    seq_scan bigint,
    seq_tup_read bigint,
    idx_scan bigint,
    idx_tup_fetch bigint,
    n_tup_ins bigint,
    n_tup_upd bigint,
    n_tup_del bigint,
    last_vacuum timestamp with time zone,
    last_autovacuum timestamp with time zone,
    last_analyze timestamp with time zone,
    last_autoanalyze timestamp with time zone
);


ALTER TABLE sys."edb$stat_all_tables" OWNER TO postgres;

--
-- Name: edb$stat_database; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$stat_database" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    datid oid NOT NULL,
    datname name,
    numbackends integer,
    xact_commit bigint,
    xact_rollback bigint,
    blks_read bigint,
    blks_hit bigint,
    blks_icache_hit bigint
);


ALTER TABLE sys."edb$stat_database" OWNER TO postgres;

--
-- Name: edb$statio_all_indexes; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$statio_all_indexes" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    relid oid NOT NULL,
    indexrelid oid NOT NULL,
    schemaname name,
    relname name,
    indexrelname name,
    idx_blks_read bigint,
    idx_blks_hit bigint,
    idx_blks_icache_hit bigint
);


ALTER TABLE sys."edb$statio_all_indexes" OWNER TO postgres;

--
-- Name: edb$statio_all_tables; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$statio_all_tables" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    relid oid NOT NULL,
    schemaname name,
    relname name,
    heap_blks_read bigint,
    heap_blks_hit bigint,
    heap_blks_icache_hit bigint,
    idx_blks_read bigint,
    idx_blks_hit bigint,
    idx_blks_icache_hit bigint,
    toast_blks_read bigint,
    toast_blks_hit bigint,
    toast_blks_icache_hit bigint,
    tidx_blks_read bigint,
    tidx_blks_hit bigint,
    tidx_blks_icache_hit bigint
);


ALTER TABLE sys."edb$statio_all_tables" OWNER TO postgres;

--
-- Name: edb$system_waits; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys."edb$system_waits" (
    edb_id bigint NOT NULL,
    dbname name NOT NULL,
    wait_name text NOT NULL,
    wait_count bigint,
    avg_wait numeric,
    max_wait numeric(50,6),
    totalwait numeric(50,6)
);


ALTER TABLE sys."edb$system_waits" OWNER TO postgres;

--
-- Name: plsql_profiler_rawdata; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys.plsql_profiler_rawdata (
    runid integer,
    sourcecode text,
    func_oid oid,
    line_number integer,
    exec_count bigint,
    tuples_returned bigint,
    time_total double precision,
    time_shortest double precision,
    time_longest double precision,
    num_scans bigint,
    tuples_fetched bigint,
    tuples_inserted bigint,
    tuples_updated bigint,
    tuples_deleted bigint,
    blocks_fetched bigint,
    blocks_hit bigint,
    wal_write bigint,
    wal_flush bigint,
    wal_file_sync bigint,
    buffer_free_list_lock_acquire bigint,
    shmem_index_lock_acquire bigint,
    oid_gen_lock_acquire bigint,
    xid_gen_lock_acquire bigint,
    proc_array_lock_acquire bigint,
    sinval_lock_acquire bigint,
    freespace_lock_acquire bigint,
    wal_insert_lock_acquire bigint,
    wal_write_lock_acquire bigint,
    control_file_lock_acquire bigint,
    checkpoint_lock_acquire bigint,
    clog_control_lock_acquire bigint,
    subtrans_control_lock_acquire bigint,
    multi_xact_gen_lock_acquire bigint,
    multi_xact_offset_lock_acquire bigint,
    multi_xact_member_lock_acquire bigint,
    rel_cache_init_lock_acquire bigint,
    bgwriter_communication_lock_acquire bigint,
    two_phase_state_lock_acquire bigint,
    tablespace_create_lock_acquire bigint,
    btree_vacuum_lock_acquire bigint,
    add_in_shmem_lock_acquire bigint,
    autovacuum_lock_acquire bigint,
    autovacuum_schedule_lock_acquire bigint,
    syncscan_lock_acquire bigint,
    icache_lock_acquire bigint,
    breakpoint_lock_acquire bigint,
    lwlock_acquire bigint,
    db_file_read bigint,
    db_file_write bigint,
    db_file_sync bigint,
    db_file_extend bigint,
    sql_parse bigint,
    query_plan bigint,
    infinitecache_read bigint,
    infinitecache_write bigint,
    wal_write_time bigint,
    wal_flush_time bigint,
    wal_file_sync_time bigint,
    buffer_free_list_lock_acquire_time bigint,
    shmem_index_lock_acquire_time bigint,
    oid_gen_lock_acquire_time bigint,
    xid_gen_lock_acquire_time bigint,
    proc_array_lock_acquire_time bigint,
    sinval_lock_acquire_time bigint,
    freespace_lock_acquire_time bigint,
    wal_insert_lock_acquire_time bigint,
    wal_write_lock_acquire_time bigint,
    control_file_lock_acquire_time bigint,
    checkpoint_lock_acquire_time bigint,
    clog_control_lock_acquire_time bigint,
    subtrans_control_lock_acquire_time bigint,
    multi_xact_gen_lock_acquire_time bigint,
    multi_xact_offset_lock_acquire_time bigint,
    multi_xact_member_lock_acquire_time bigint,
    rel_cache_init_lock_acquire_time bigint,
    bgwriter_communication_lock_acquire_time bigint,
    two_phase_state_lock_acquire_time bigint,
    tablespace_create_lock_acquire_time bigint,
    btree_vacuum_lock_acquire_time bigint,
    add_in_shmem_lock_acquire_time bigint,
    autovacuum_lock_acquire_time bigint,
    autovacuum_schedule_lock_acquire_time bigint,
    syncscan_lock_acquire_time bigint,
    icache_lock_acquire_time bigint,
    breakpoint_lock_acquire_time bigint,
    lwlock_acquire_time bigint,
    db_file_read_time bigint,
    db_file_write_time bigint,
    db_file_sync_time bigint,
    db_file_extend_time bigint,
    sql_parse_time bigint,
    query_plan_time bigint,
    infinitecache_read_time bigint,
    infinitecache_write_time bigint,
    totalwaits bigint,
    totalwaittime bigint
);


ALTER TABLE sys.plsql_profiler_rawdata OWNER TO postgres;

--
-- Name: plsql_profiler_runid; Type: SEQUENCE; Schema: sys; Owner: postgres
--

CREATE SEQUENCE sys.plsql_profiler_runid
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sys.plsql_profiler_runid OWNER TO postgres;

--
-- Name: plsql_profiler_runs; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys.plsql_profiler_runs (
    runid integer NOT NULL,
    related_run integer,
    run_owner text,
    run_date timestamp without time zone,
    run_comment text,
    run_total_time bigint,
    run_system_info text,
    run_comment1 text,
    spare1 text
);


ALTER TABLE sys.plsql_profiler_runs OWNER TO postgres;

--
-- Name: plsql_profiler_units; Type: TABLE; Schema: sys; Owner: postgres
--

CREATE TABLE sys.plsql_profiler_units (
    runid integer,
    unit_number oid,
    unit_type text,
    unit_owner text,
    unit_name text,
    unit_timestamp timestamp without time zone,
    total_time bigint,
    spare1 bigint,
    spare2 bigint
);


ALTER TABLE sys.plsql_profiler_units OWNER TO postgres;

--
-- Name: snapshot_num_seq; Type: SEQUENCE; Schema: sys; Owner: postgres
--

CREATE SEQUENCE sys.snapshot_num_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sys.snapshot_num_seq OWNER TO postgres;

--
-- Name: jawatans id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jawatans ALTER COLUMN id SET DEFAULT nextval('public.jawatans_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: jawatans; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jawatans (id, name, flag, delete_id, created_at, updated_at) FROM stdin;
1	Pengarah	1	0	2021-10-21 11:01:31.81706	2021-10-21 11:01:31.81706
2	Wakil	1	0	2021-10-21 11:01:31.81706	2021-10-21 11:01:31.81706
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2021_08_06_085859_entrust_setup_tables	1
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permission_role (permission_id, role_id) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, name, display_name, description, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: profiles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profiles (id, users_id, flag, delete_id, created_at, updated_at) FROM stdin;
20	57	1	0	2021-10-21 09:58:17	2021-10-21 09:58:17
21	58	1	0	2021-10-21 09:58:43	2021-10-21 09:58:43
22	59	1	0	2021-10-21 10:07:35	2021-10-21 10:07:35
23	60	1	1	2021-10-21 10:07:41	2021-10-21 10:22:30
26	63	1	0	2021-10-22 03:19:56	2021-10-22 03:19:56
27	64	1	0	2021-10-22 03:54:33	2021-10-22 03:54:33
28	65	1	0	2021-10-22 03:54:48	2021-10-22 03:54:48
\.


--
-- Data for Name: profiles_alamat_pejabats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profiles_alamat_pejabats (id, profiles_id, alamat, flag, delete_id, created_at, updated_at) FROM stdin;
15	20	bahagian pemulihan projek perumahan swasta jabatan perumahan negara\r\naras 33, no.51 persiaran perdana\r\npresint 4,62100 putrajaya	1	0	2021-10-21 09:58:17	2021-10-21 09:58:17
16	21	JABATAN KERJA RAYA MALAYSIA, CAWANGAN DASAR DAN PENGURUSAN KORPORAT, BAHAGIAN PENGURUSAN KUALITI, TINGKAT 31, MENARA KERJA RAYA (BLOK G), IBU PEJABAT JKR MALAYSIA, JALAN SULTAN SALAHUDDIN, 50480 KUALA LUMPUR	1	0	2021-10-21 09:58:43	2021-10-21 09:58:43
17	22		1	0	2021-10-21 10:07:35	2021-10-21 10:07:35
18	23		1	0	2021-10-21 10:07:41	2021-10-21 10:07:41
20	26	unit pembangunan dan senggaraan, politeknik kuching, km22, jalan matang, 93050 kuching,sarawak, malaysia	1	0	2021-10-22 03:19:56	2021-10-22 03:19:56
21	27	Cawangan Kerja Kesihatan	1	0	2021-10-22 03:54:33	2021-10-22 03:54:33
22	28	CAWANGAN PENGURUSAN PROJEK KOMPLEKS, TINGKAT 27 & 28,  MENARA PJD NO. 50 JALAN TUN RAZAK 50400 KUALA LUMPUR	1	0	2021-10-22 03:54:48	2021-10-22 03:54:48
\.


--
-- Data for Name: profiles_cawangans_logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profiles_cawangans_logs (id, profiles_id, neg_perseketuan, sektor, cawangan, bahagian, unit, tahun, flag, delete_id, created_at, updated_at, penempatan, sektor_name, cawangan_name, bahagian_name, unit_name, penempatan_name) FROM stdin;
15	20	\N	070000000000	072100000000	072108000000	072108010000	2021	1	0	2021-10-21 09:58:17	2021-10-21 09:58:17	072108010300	Lain-lain Kementerian	Kementerian Perumahan dan Kerajaan Tempatan	Jabatan Perumahan Negara	Program Operasi	Bahagian Pemulihan Projek Terbengkalai
16	21	\N	020000000000	020300000000	020304000000	020304010000	2021	1	0	2021-10-21 09:58:43	2021-10-21 09:58:43	020304010000	Pengurusan Operasi Dan Strategik	Cawangan Dasar dan Pengurusan Korporat	Bahagian Pengurusan Kualiti	Unit Pembangunan Sistem	Unit Pembangunan Sistem
17	22	\N	020000000000	020300000000	020307000000	020307000000	2021	1	0	2021-10-21 10:07:35	2021-10-21 10:07:35	020307000000	Pengurusan Operasi Dan Strategik	Cawangan Dasar dan Pengurusan Korporat	Bahagian Teknologi Maklumat	Bahagian Teknologi Maklumat	Bahagian Teknologi Maklumat
18	23	\N	010000000000	010400000000	010400000000	010400000000	2021	1	0	2021-10-21 10:07:41	2021-10-21 10:07:41	010400000000	Kementerian Kerja Raya	Kementerian Kerja Raya (Pengurusan)	Kementerian Kerja Raya (Pengurusan)	Kementerian Kerja Raya (Pengurusan)	Kementerian Kerja Raya (Pengurusan)
20	26	\N	070000000000	071000000000	071002000000	071002020000	2021	1	0	2021-10-22 03:19:56	2021-10-22 03:19:56	071002020106	Lain-lain Kementerian	Kementerian Pendidikan	Pendidikan Tinggi	Jabatan Pendidikan Politeknik	Politeknik Kuching Sarawak
21	27	\N	040000000000	040400000000	040401000000	040401000000	2021	1	0	2021-10-22 03:54:33	2021-10-22 03:54:33	040401000000	Sektor Bangunan	Cawangan Kerja Kesihatan	Bahagian Penyelarasan dan Khidmat Sokongan	Bahagian Penyelarasan dan Khidmat Sokongan	Bahagian Penyelarasan dan Khidmat Sokongan
22	28	\N	020000000000	020200000000	020202000000	020202000000	2021	1	0	2021-10-22 03:54:48	2021-10-22 03:54:48	020202000000	Pengurusan Operasi Dan Strategik	Cawangan Perancangan Aset Bersepadu	Bahagian Pengurusan Projek Kompleks (BPPK)	Bahagian Pengurusan Projek Kompleks (BPPK)	Bahagian Pengurusan Projek Kompleks (BPPK)
\.


--
-- Data for Name: profiles_telefons; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profiles_telefons (id, profiles_id, no_tel_pejabat, no_tel_bimbit, flag, delete_id, created_at, updated_at) FROM stdin;
15	20	03-88914268	010-4564807	1	0	2021-10-21 09:58:17	2021-10-21 09:58:17
16	21	03-26188686	019-2244638	1	0	2021-10-21 09:58:43	2021-10-21 09:58:43
17	22	03-26107094	0192624883	1	0	2021-10-21 10:07:35	2021-10-21 10:07:35
18	23			1	0	2021-10-21 10:07:41	2021-10-21 10:07:41
20	26	082845596/7/8	0148845312	1	0	2021-10-22 03:19:56	2021-10-22 03:19:56
21	27	03-26189595	013-7048430	1	0	2021-10-22 03:54:33	2021-10-22 03:54:33
22	28	03-40518816	013-8293362	1	0	2021-10-22 03:54:48	2021-10-22 03:54:48
\.


--
-- Data for Name: projeks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks (id, profiles_id, nama, flag, delete_id, created_at, updated_at) FROM stdin;
9	22	Project A	1	0	2021-10-21 10:27:08	2021-10-21 10:27:08
\.


--
-- Data for Name: projeks_tahun_penilais; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahun_penilais (id, projeks_tahuns_id, nokp, nama, email, flag, delete_id, created_at, updated_at, status) FROM stdin;
20	69	920627135550	JOCELYN ANAK MEDOI	jocelyn@poliku.edu.my	1	0	2021-10-22 03:19:56	2021-10-22 03:49:35	0
21	70	910502016700	ADIBAH DIYANA BINTI NORJAN	adibahdiyana@jkr.gov.my	1	0	2021-10-22 03:50:39	2021-10-22 03:54:33	0
22	71	640223065131	MOHD DZOLDI BIN AWANG	NORHASANK@KKR.GOV.MY	1	0	2021-10-22 03:51:55	2021-10-22 03:55:39	0
\.


--
-- Data for Name: projeks_tahuns; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns (id, projeks_id, tahuns_id, tkh_milik_tpk, tkh_siap_asal, tkh_siap_semasa, tkh_penilaian, no_klausa, kemajuan_penilaian, flag, delete_id, created_at, updated_at, status) FROM stdin;
69	9	3	2021-10-14	2021-10-26	2021-10-27	2021-10-28	AAA	50	1	0	2021-10-22 03:19:56	2021-10-22 03:19:56	0
70	9	3	2021-10-21	2021-10-26	2021-10-23	2021-10-24	BBB	40	1	0	2021-10-22 03:50:39	2021-10-22 03:50:39	0
71	9	3	2021-10-22	2021-10-27	2021-10-29	2021-10-26	CCC	50	1	0	2021-10-22 03:51:55	2021-10-22 03:51:55	0
\.


--
-- Data for Name: projeks_tahuns_catatans_uploads; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_catatans_uploads (id, projeks_tahuns_id, catatan, uploads, flag, delete_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_pengarahs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_pengarahs (id, projeks_tahuns_id, jawatans_id, nama, flag, delete_id, created_at, updated_at) FROM stdin;
55	69	1	Meor	1	0	2021-10-22 03:19:56	2021-10-22 03:19:56
56	70	1	Meor	1	0	2021-10-22 03:50:39	2021-10-22 03:50:39
57	71	1	Meor	1	0	2021-10-22 03:51:55	2021-10-22 03:51:55
\.


--
-- Data for Name: projeks_tahuns_penilaians; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_penilaians (id, projeks_tahuns_id, nama, peratus_penilaian, flag, delete_id, created_at, updated_at, ref_id) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_penilaians_sub_penilaians; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_penilaians_sub_penilaians (id, projeks_tahuns_penilaians_id, bil, tajuk_perkara, no_klausa, flag, delete_id, created_at, updated_at, ref_id) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_penilaians_sub_penilaians_perkaras; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_penilaians_sub_penilaians_perkaras (id, projeks_tahuns_penilaians_sub_penilaians_id, bil, tajuk_perkara, no_klausa, question_log, flag, delete_id, created_at, updated_at, ref_id) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors (id, projeks_tahuns_penilais_id, projeks_sub_penilaians_perkaras_id, skor, flag, delete_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans (id, projeks_tahuns_penilaians_sub_perkara_penilais_skor_id, catatan, flag, delete_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads (id, projeks_tahuns_penilaians_sub_perkara_penilais_skors_id, uploads_name, flag, delete_id, created_at, updated_at, ref_id, uploads_location) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_penilais_keputusan_logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_penilais_keputusan_logs (id, projeks_tahuns_penilais_id, projeks_tahuns_penilaians_id, projeks_tahuns_id, peratus, question_history_log, flag, delete_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: projeks_tahuns_wakils; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projeks_tahuns_wakils (id, projeks_tahuns_id, jawatans_id, nama, flag, delete_id, created_at, updated_at) FROM stdin;
55	69	2	Rizzal	1	0	2021-10-22 03:19:56	2021-10-22 03:19:56
56	70	2	Yana	1	0	2021-10-22 03:50:39	2021-10-22 03:50:39
57	71	2	Yana	1	0	2021-10-22 03:51:55	2021-10-22 03:51:55
\.


--
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_user (user_id, role_id, id) FROM stdin;
37	1	2
57	2	20
58	2	21
59	2	22
63	3	30
64	3	33
60	3	35
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, display_name, description, created_at, updated_at) FROM stdin;
1	Pentadbir	Pentadbir	Pentadbir User	\N	\N
2	Urussetia	Urussetia	Urussetia User	\N	\N
3	KPA	Ketua Penilaian Audit	KPA User	\N	\N
\.


--
-- Data for Name: tahuns; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tahuns (id, tahun, flag, delete_id, created_at, updated_at) FROM stdin;
3	2021	1	0	2021-08-27 10:30:04	2021-08-27 10:30:04
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, nokp) FROM stdin;
37	Meor Admin	admin@admin.com	\N	$2y$10$e/9AVbMtrBZDvWr5fPbfoOb1IIfB1T0O2SIFgPozYRrCS/PuG2uCm	\N	\N	\N	\N
57	SITI AISYAH BINTI HARUN	aisyahharun@kpkt.gov.my	\N	$2y$10$nHhMj86ygg1OJFDf/B434OpL12IIFYHB64Cp.YvIqfGgD5EfUCgkO	\N	2021-10-21 09:58:17	2021-10-21 09:58:17	880214086574
58	SHAHROM DARMAWAN BIN MOHD YUNUS	shahromdmy@jkr.gov.my	\N	$2y$10$clXduQxiDXZgSnhiaxmzt.jKE4wKgP78pQjlXbW0vquTJWl.GbZSm	\N	2021-10-21 09:58:43	2021-10-21 09:58:43	850719146061
59	ZUREHAN BINTI CHE ANI	zurehan@jkr.gov.my	\N	$2y$10$Gu0Mf8jX.VRMIZuGppnZ5.xKJr35wyM4oQSoODyc8M6ls3HbMPoPG	\N	2021-10-21 10:07:35	2021-10-21 10:07:35	840312086010
60	MOHD DZOLDI BIN AWANG	NORHASANK@KKR.GOV.MY	\N	$2y$10$FJFz0WmGM8FfUJodqr1N.eMNygktUWmKhaoARaCcXvx.w5LqSkbUK	\N	2021-10-21 10:07:41	2021-10-21 10:07:41	640223065131
63	JOCELYN ANAK MEDOI	jocelyn@poliku.edu.my	\N	$2y$10$Su.vTIGm.BQOlyJqBpyr/ulfy9mG/Ayjc2BgbbObJY8YSwgjN35Bq	\N	2021-10-22 03:19:56	2021-10-22 03:19:56	920627135550
64	ADIBAH DIYANA BINTI NORJAN	adibahdiyana@jkr.gov.my	\N	$2y$10$YOr3gnOAcICGIu5bi.AfyevzVblJ2V2Rf5lTizSdUpcuevjri/igS	\N	2021-10-22 03:54:33	2021-10-22 03:54:33	910502016700
65	MAGDELENE ANAK JAPET	Magdelene@jkr.gov.my	\N	$2y$10$5uRuB7iKLJojyYFkhIXtBuzeegs78WMD6wfJeIXqIbAtwBIzZKXqi	\N	2021-10-22 03:54:48	2021-10-22 03:54:48	870714525554
\.


--
-- Data for Name: edb$session_wait_history; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$session_wait_history" (edb_id, dbname, backend_id, seq, wait_name, elapsed, p1, p2, p3) FROM stdin;
\.


--
-- Data for Name: edb$session_waits; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$session_waits" (edb_id, dbname, backend_id, wait_name, wait_count, avg_wait_time, max_wait_time, total_wait_time, usename, query) FROM stdin;
\.


--
-- Data for Name: edb$snap; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$snap" (edb_id, dbname, snap_tm, start_tm, backend_id, comment, baseline_ind) FROM stdin;
\.


--
-- Data for Name: edb$stat_all_indexes; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$stat_all_indexes" (edb_id, dbname, relid, indexrelid, schemaname, relname, indexrelname, idx_scan, idx_tup_read, idx_tup_fetch) FROM stdin;
\.


--
-- Data for Name: edb$stat_all_tables; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$stat_all_tables" (edb_id, dbname, relid, schemaname, relname, seq_scan, seq_tup_read, idx_scan, idx_tup_fetch, n_tup_ins, n_tup_upd, n_tup_del, last_vacuum, last_autovacuum, last_analyze, last_autoanalyze) FROM stdin;
\.


--
-- Data for Name: edb$stat_database; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$stat_database" (edb_id, dbname, datid, datname, numbackends, xact_commit, xact_rollback, blks_read, blks_hit, blks_icache_hit) FROM stdin;
\.


--
-- Data for Name: edb$statio_all_indexes; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$statio_all_indexes" (edb_id, dbname, relid, indexrelid, schemaname, relname, indexrelname, idx_blks_read, idx_blks_hit, idx_blks_icache_hit) FROM stdin;
\.


--
-- Data for Name: edb$statio_all_tables; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$statio_all_tables" (edb_id, dbname, relid, schemaname, relname, heap_blks_read, heap_blks_hit, heap_blks_icache_hit, idx_blks_read, idx_blks_hit, idx_blks_icache_hit, toast_blks_read, toast_blks_hit, toast_blks_icache_hit, tidx_blks_read, tidx_blks_hit, tidx_blks_icache_hit) FROM stdin;
\.


--
-- Data for Name: edb$system_waits; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys."edb$system_waits" (edb_id, dbname, wait_name, wait_count, avg_wait, max_wait, totalwait) FROM stdin;
\.


--
-- Data for Name: plsql_profiler_rawdata; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys.plsql_profiler_rawdata (runid, sourcecode, func_oid, line_number, exec_count, tuples_returned, time_total, time_shortest, time_longest, num_scans, tuples_fetched, tuples_inserted, tuples_updated, tuples_deleted, blocks_fetched, blocks_hit, wal_write, wal_flush, wal_file_sync, buffer_free_list_lock_acquire, shmem_index_lock_acquire, oid_gen_lock_acquire, xid_gen_lock_acquire, proc_array_lock_acquire, sinval_lock_acquire, freespace_lock_acquire, wal_insert_lock_acquire, wal_write_lock_acquire, control_file_lock_acquire, checkpoint_lock_acquire, clog_control_lock_acquire, subtrans_control_lock_acquire, multi_xact_gen_lock_acquire, multi_xact_offset_lock_acquire, multi_xact_member_lock_acquire, rel_cache_init_lock_acquire, bgwriter_communication_lock_acquire, two_phase_state_lock_acquire, tablespace_create_lock_acquire, btree_vacuum_lock_acquire, add_in_shmem_lock_acquire, autovacuum_lock_acquire, autovacuum_schedule_lock_acquire, syncscan_lock_acquire, icache_lock_acquire, breakpoint_lock_acquire, lwlock_acquire, db_file_read, db_file_write, db_file_sync, db_file_extend, sql_parse, query_plan, infinitecache_read, infinitecache_write, wal_write_time, wal_flush_time, wal_file_sync_time, buffer_free_list_lock_acquire_time, shmem_index_lock_acquire_time, oid_gen_lock_acquire_time, xid_gen_lock_acquire_time, proc_array_lock_acquire_time, sinval_lock_acquire_time, freespace_lock_acquire_time, wal_insert_lock_acquire_time, wal_write_lock_acquire_time, control_file_lock_acquire_time, checkpoint_lock_acquire_time, clog_control_lock_acquire_time, subtrans_control_lock_acquire_time, multi_xact_gen_lock_acquire_time, multi_xact_offset_lock_acquire_time, multi_xact_member_lock_acquire_time, rel_cache_init_lock_acquire_time, bgwriter_communication_lock_acquire_time, two_phase_state_lock_acquire_time, tablespace_create_lock_acquire_time, btree_vacuum_lock_acquire_time, add_in_shmem_lock_acquire_time, autovacuum_lock_acquire_time, autovacuum_schedule_lock_acquire_time, syncscan_lock_acquire_time, icache_lock_acquire_time, breakpoint_lock_acquire_time, lwlock_acquire_time, db_file_read_time, db_file_write_time, db_file_sync_time, db_file_extend_time, sql_parse_time, query_plan_time, infinitecache_read_time, infinitecache_write_time, totalwaits, totalwaittime) FROM stdin;
\.


--
-- Data for Name: plsql_profiler_runs; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys.plsql_profiler_runs (runid, related_run, run_owner, run_date, run_comment, run_total_time, run_system_info, run_comment1, spare1) FROM stdin;
\.


--
-- Data for Name: plsql_profiler_units; Type: TABLE DATA; Schema: sys; Owner: postgres
--

COPY sys.plsql_profiler_units (runid, unit_number, unit_type, unit_owner, unit_name, unit_timestamp, total_time, spare1, spare2) FROM stdin;
\.


--
-- Name: jawatans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jawatans_id_seq', 2, true);


--
-- Name: profiles_alamat_pejabats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profiles_alamat_pejabats_id_seq', 22, true);


--
-- Name: profiles_cawangans_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profiles_cawangans_logs_id_seq', 22, true);


--
-- Name: profiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profiles_id_seq', 28, true);


--
-- Name: profiles_telefons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profiles_telefons_id_seq', 22, true);


--
-- Name: projeks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_id_seq', 9, true);


--
-- Name: projeks_tahun_penilais_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahun_penilais_id_seq', 22, true);


--
-- Name: projeks_tahuns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_id_seq', 71, true);


--
-- Name: projeks_tahuns_pengarahs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_pengarahs_id_seq', 57, true);


--
-- Name: projeks_tahuns_penilaians_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_penilaians_id_seq', 14, true);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_penilaians_sub_penilaians_id_seq', 19, true);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians_perkaras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_penilaians_sub_penilaians_perkaras_id_seq', 34, true);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans_i; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans_i', 35, true);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_id_seq', 35, true);


--
-- Name: projeks_tahuns_penilais_keputusan_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_penilais_keputusan_logs_id_seq', 8, true);


--
-- Name: projeks_tahuns_wakils_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projeks_tahuns_wakils_id_seq', 57, true);


--
-- Name: role_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_user_id_seq', 35, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 3, true);


--
-- Name: skor_uploading; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.skor_uploading', 128, true);


--
-- Name: tahuns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tahuns_id_seq', 3, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 65, true);


--
-- Name: plsql_profiler_runid; Type: SEQUENCE SET; Schema: sys; Owner: postgres
--

SELECT pg_catalog.setval('sys.plsql_profiler_runid', 1, false);


--
-- Name: snapshot_num_seq; Type: SEQUENCE SET; Schema: sys; Owner: postgres
--

SELECT pg_catalog.setval('sys.snapshot_num_seq', 1, false);


--
-- Name: jawatans jawatans_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jawatans
    ADD CONSTRAINT jawatans_pk PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: permission_role permission_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: permissions permissions_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_name_unique UNIQUE (name);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: profiles_cawangans_logs profiles_alamat_cawangans_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles_cawangans_logs
    ADD CONSTRAINT profiles_alamat_cawangans_pk PRIMARY KEY (id);


--
-- Name: profiles profiles_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles
    ADD CONSTRAINT profiles_pk PRIMARY KEY (id);


--
-- Name: profiles_telefons profiles_telefons_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles_telefons
    ADD CONSTRAINT profiles_telefons_pk PRIMARY KEY (id);


--
-- Name: projeks projeks_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks
    ADD CONSTRAINT projeks_pk PRIMARY KEY (id);


--
-- Name: projeks_tahun_penilais projeks_tahun_penilais_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahun_penilais
    ADD CONSTRAINT projeks_tahun_penilais_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_pengarahs projeks_tahuns_pengarah_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_pengarahs
    ADD CONSTRAINT projeks_tahuns_pengarah_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_penilaians projeks_tahuns_penilaians_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians
    ADD CONSTRAINT projeks_tahuns_penilaians_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians_perkaras projeks_tahuns_penilaians_sub_penilaians_perkaras_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_penilaians_perkaras
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_penilaians_perkaras_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians projeks_tahuns_penilaians_sub_penilaians_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_penilaians
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_penilaians_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans projeks_tahuns_penilaians_sub_perkara_penilais_skor_catatans_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_perkara_penilais_skor_catatans_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors projeks_tahuns_penilaians_sub_perkara_skors_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_perkara_skors_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_penilais_keputusan_logs projeks_tahuns_penilais_keputusan_logs_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilais_keputusan_logs
    ADD CONSTRAINT projeks_tahuns_penilais_keputusan_logs_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns projeks_tahuns_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns
    ADD CONSTRAINT projeks_tahuns_pk PRIMARY KEY (id);


--
-- Name: projeks_tahuns_wakils projeks_tahuns_wakils_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_wakils
    ADD CONSTRAINT projeks_tahuns_wakils_pk PRIMARY KEY (id);


--
-- Name: role_user role_user_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_pk PRIMARY KEY (id);


--
-- Name: role_user role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_pkey UNIQUE (user_id, role_id);


--
-- Name: roles roles_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: tahuns tahuns_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tahuns
    ADD CONSTRAINT tahuns_pk PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: edb$stat_database edb$stat_db_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$stat_database"
    ADD CONSTRAINT "edb$stat_db_pk" PRIMARY KEY (edb_id, dbname, datid);


--
-- Name: edb$stat_all_indexes edb$stat_idx_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$stat_all_indexes"
    ADD CONSTRAINT "edb$stat_idx_pk" PRIMARY KEY (edb_id, dbname, relid, indexrelid);


--
-- Name: edb$stat_all_tables edb$stat_tab_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$stat_all_tables"
    ADD CONSTRAINT "edb$stat_tab_pk" PRIMARY KEY (edb_id, dbname, relid);


--
-- Name: edb$statio_all_indexes edb$statio_idx_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$statio_all_indexes"
    ADD CONSTRAINT "edb$statio_idx_pk" PRIMARY KEY (edb_id, dbname, relid, indexrelid);


--
-- Name: edb$statio_all_tables edb$statio_tab_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$statio_all_tables"
    ADD CONSTRAINT "edb$statio_tab_pk" PRIMARY KEY (edb_id, dbname, relid);


--
-- Name: plsql_profiler_runs plsql_profiler_runs_pkey; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys.plsql_profiler_runs
    ADD CONSTRAINT plsql_profiler_runs_pkey PRIMARY KEY (runid);


--
-- Name: edb$session_wait_history session_waits_hist_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$session_wait_history"
    ADD CONSTRAINT session_waits_hist_pk PRIMARY KEY (edb_id, dbname, backend_id, seq);


--
-- Name: edb$session_waits session_waits_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$session_waits"
    ADD CONSTRAINT session_waits_pk PRIMARY KEY (edb_id, dbname, backend_id, wait_name);


--
-- Name: edb$snap snap_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$snap"
    ADD CONSTRAINT snap_pk PRIMARY KEY (edb_id);


--
-- Name: edb$system_waits system_waits_pk; Type: CONSTRAINT; Schema: sys; Owner: postgres
--

ALTER TABLE ONLY sys."edb$system_waits"
    ADD CONSTRAINT system_waits_pk PRIMARY KEY (edb_id, dbname, wait_name);


--
-- Name: jawatans_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX jawatans_id_uindex ON public.jawatans USING btree (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: profiles_alamat_cawangans_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX profiles_alamat_cawangans_id_uindex ON public.profiles_cawangans_logs USING btree (id);


--
-- Name: profiles_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX profiles_id_uindex ON public.profiles USING btree (id);


--
-- Name: profiles_telefons_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX profiles_telefons_id_uindex ON public.profiles_telefons USING btree (id);


--
-- Name: projeks_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_id_uindex ON public.projeks USING btree (id);


--
-- Name: projeks_tahun_penilais_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahun_penilais_id_uindex ON public.projeks_tahun_penilais USING btree (id);


--
-- Name: projeks_tahuns_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_id_uindex ON public.projeks_tahuns USING btree (id);


--
-- Name: projeks_tahuns_pengarah_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_pengarah_id_uindex ON public.projeks_tahuns_pengarahs USING btree (id);


--
-- Name: projeks_tahuns_penilaians_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_penilaians_id_uindex ON public.projeks_tahuns_penilaians USING btree (id);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_penilaians_sub_penilaians_id_uindex ON public.projeks_tahuns_penilaians_sub_penilaians USING btree (id);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians_perkaras_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_penilaians_sub_penilaians_perkaras_id_uindex ON public.projeks_tahuns_penilaians_sub_penilaians_perkaras USING btree (id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skor_catatans_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_penilaians_sub_perkara_penilais_skor_catatans_id ON public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans USING btree (id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads_id ON public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads USING btree (id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_skors_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_penilaians_sub_perkara_skors_id_uindex ON public.projeks_tahuns_penilaians_sub_perkara_penilais_skors USING btree (id);


--
-- Name: projeks_tahuns_penilais_keputusan_logs_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_penilais_keputusan_logs_id_uindex ON public.projeks_tahuns_penilais_keputusan_logs USING btree (id);


--
-- Name: projeks_tahuns_wakils_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX projeks_tahuns_wakils_id_uindex ON public.projeks_tahuns_wakils USING btree (id);


--
-- Name: role_user_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX role_user_id_uindex ON public.role_user USING btree (id);


--
-- Name: tahuns_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX tahuns_id_uindex ON public.tahuns USING btree (id);


--
-- Name: permission_role permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permission_role permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: profiles_cawangans_logs profiles_alamat_cawangans_profiles_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles_cawangans_logs
    ADD CONSTRAINT profiles_alamat_cawangans_profiles_id_fk FOREIGN KEY (profiles_id) REFERENCES public.profiles(id);


--
-- Name: profiles_alamat_pejabats profiles_alamat_pejabats_profiles_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles_alamat_pejabats
    ADD CONSTRAINT profiles_alamat_pejabats_profiles_id_fk FOREIGN KEY (profiles_id) REFERENCES public.profiles(id);


--
-- Name: profiles_telefons profiles_telefons_profiles_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles_telefons
    ADD CONSTRAINT profiles_telefons_profiles_id_fk FOREIGN KEY (profiles_id) REFERENCES public.profiles(id);


--
-- Name: profiles profiles_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles
    ADD CONSTRAINT profiles_users_id_fk FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: projeks projeks_profiles_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks
    ADD CONSTRAINT projeks_profiles_id_fk FOREIGN KEY (profiles_id) REFERENCES public.profiles(id);


--
-- Name: projeks_tahun_penilais projeks_tahun_penilais_projeks_tahuns_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahun_penilais
    ADD CONSTRAINT projeks_tahun_penilais_projeks_tahuns_id_fk FOREIGN KEY (projeks_tahuns_id) REFERENCES public.projeks_tahuns(id);


--
-- Name: projeks_tahuns_catatans_uploads projeks_tahuns_catatans_uploads_projeks_tahuns_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_catatans_uploads
    ADD CONSTRAINT projeks_tahuns_catatans_uploads_projeks_tahuns_id_fk FOREIGN KEY (projeks_tahuns_id) REFERENCES public.projeks_tahuns(id);


--
-- Name: projeks_tahuns_pengarahs projeks_tahuns_pengarah_projeks_tahuns_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_pengarahs
    ADD CONSTRAINT projeks_tahuns_pengarah_projeks_tahuns_id_fk FOREIGN KEY (projeks_tahuns_id) REFERENCES public.projeks_tahuns(id);


--
-- Name: projeks_tahuns_pengarahs projeks_tahuns_pengarahs_jawatans_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_pengarahs
    ADD CONSTRAINT projeks_tahuns_pengarahs_jawatans_id_fk FOREIGN KEY (jawatans_id) REFERENCES public.jawatans(id);


--
-- Name: projeks_tahuns_penilaians projeks_tahuns_penilaians_projeks_tahuns_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians
    ADD CONSTRAINT projeks_tahuns_penilaians_projeks_tahuns_id_fk FOREIGN KEY (projeks_tahuns_id) REFERENCES public.projeks_tahuns(id);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians_perkaras projeks_tahuns_penilaians_sub_penilaians_perkaras_projeks_tahun; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_penilaians_perkaras
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_penilaians_perkaras_projeks_tahun FOREIGN KEY (projeks_tahuns_penilaians_sub_penilaians_id) REFERENCES public.projeks_tahuns_penilaians_sub_penilaians(id);


--
-- Name: projeks_tahuns_penilaians_sub_penilaians projeks_tahuns_penilaians_sub_penilaians_projeks_tahuns_penilai; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_penilaians
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_penilaians_projeks_tahuns_penilai FOREIGN KEY (projeks_tahuns_penilaians_id) REFERENCES public.projeks_tahuns_penilaians(id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans projeks_tahuns_penilaians_sub_perkara_penilais_skor_catatans_pr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_catatans
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_perkara_penilais_skor_catatans_pr FOREIGN KEY (projeks_tahuns_penilaians_sub_perkara_penilais_skor_id) REFERENCES public.projeks_tahuns_penilaians_sub_perkara_penilais_skors(id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads_pr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_perkara_penilais_skors_uploads_pr FOREIGN KEY (projeks_tahuns_penilaians_sub_perkara_penilais_skors_id) REFERENCES public.projeks_tahuns_penilaians_sub_perkara_penilais_skors(id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors projeks_tahuns_penilaians_sub_perkara_skors_projeks_tahun_penil; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_perkara_skors_projeks_tahun_penil FOREIGN KEY (projeks_tahuns_penilais_id) REFERENCES public.projeks_tahun_penilais(id);


--
-- Name: projeks_tahuns_penilaians_sub_perkara_penilais_skors projeks_tahuns_penilaians_sub_perkara_skors_projeks_tahuns_peni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilaians_sub_perkara_penilais_skors
    ADD CONSTRAINT projeks_tahuns_penilaians_sub_perkara_skors_projeks_tahuns_peni FOREIGN KEY (projeks_sub_penilaians_perkaras_id) REFERENCES public.projeks_tahuns_penilaians_sub_penilaians_perkaras(id);


--
-- Name: projeks_tahuns_penilais_keputusan_logs projeks_tahuns_penilais_keputusan_logs_projeks_tahun_penilais_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilais_keputusan_logs
    ADD CONSTRAINT projeks_tahuns_penilais_keputusan_logs_projeks_tahun_penilais_i FOREIGN KEY (projeks_tahuns_penilais_id) REFERENCES public.projeks_tahun_penilais(id);


--
-- Name: projeks_tahuns_penilais_keputusan_logs projeks_tahuns_penilais_keputusan_logs_projeks_tahuns_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilais_keputusan_logs
    ADD CONSTRAINT projeks_tahuns_penilais_keputusan_logs_projeks_tahuns_id_fk FOREIGN KEY (projeks_tahuns_id) REFERENCES public.projeks_tahuns(id);


--
-- Name: projeks_tahuns_penilais_keputusan_logs projeks_tahuns_penilais_keputusan_logs_projeks_tahuns_penilaian; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_penilais_keputusan_logs
    ADD CONSTRAINT projeks_tahuns_penilais_keputusan_logs_projeks_tahuns_penilaian FOREIGN KEY (projeks_tahuns_penilaians_id) REFERENCES public.projeks_tahuns_penilaians(id);


--
-- Name: projeks_tahuns projeks_tahuns_projeks_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns
    ADD CONSTRAINT projeks_tahuns_projeks_id_fk FOREIGN KEY (projeks_id) REFERENCES public.projeks(id);


--
-- Name: projeks_tahuns projeks_tahuns_tahuns_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns
    ADD CONSTRAINT projeks_tahuns_tahuns_id_fk FOREIGN KEY (tahuns_id) REFERENCES public.tahuns(id);


--
-- Name: projeks_tahuns_wakils projeks_tahuns_wakils_jawatans_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_wakils
    ADD CONSTRAINT projeks_tahuns_wakils_jawatans_id_fk FOREIGN KEY (jawatans_id) REFERENCES public.jawatans(id);


--
-- Name: projeks_tahuns_wakils projeks_tahuns_wakils_projeks_tahuns_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projeks_tahuns_wakils
    ADD CONSTRAINT projeks_tahuns_wakils_projeks_tahuns_id_fk FOREIGN KEY (projeks_tahuns_id) REFERENCES public.projeks_tahuns(id);


--
-- Name: role_user role_user_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role_user role_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

