function checkInput(classValue, fieldName ,value, type){
	if(type == 'string'){
		var check = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;
		if(check.test(value) == false){
			$(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Harus Mengandungi Abjad Dan Ruang Sahaja');
			return false;
		}else{
			clearInvalid(classValue);
		}
	}else if(type == 'mix'){
		clearInvalid(classValue);
	}else if(type == 'int'){
		var check = /^\d+$/.test(value);
		if(check == false){
			$(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Harus Dalam Bilangan Bulat');
			return false;
		}else{
			clearInvalid(classValue);
		}
	}else if(type == 'double'){
		var check = /[^\.].+/.test(value);
		if(check == false){
			$(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Harus Dalam Perpuluhan');
			return false;
		}else{
			clearInvalid(classValue);
		}
	}else if(type == 'email'){
		var check = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
		if(check == false){
			return false;
		}else{
			clearInvalid(classValue);
		}
	}else if(type == 'datedash'){
		var check = /^\d{4}-\d{2}-\d{2}$/.test(value);
		if(check == false){
			return false;
		}else{
			clearInvalid(classValue);
		}
	}else if(type == 'datedashstandard'){
		var check = /^\d{2}-\d{2}-\d{4}$/.test(value);
		if(check == false){
			return false;
		}else{
			clearInvalid(classValue);
		}
	}else if(type == 'picture'){
		if($(value).attr('href') == '#'){
			if($(classValue)[0].files[0] == '' || typeof $(classValue)[0].files[0] == 'undefined'){
				$(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').attr('style', 'display:block').html(fieldName + ' Harus Diisi');
				return false;
			}else{
				clearInvalid(classValue);
			}
		}else{
			clearInvalid(classValue);
		}
	}else if(type == 'intdoublemix'){
		var check = /^[+-]?\d+(\.\d+)?$/.test(value);
		if(check == false){
			$(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Harus Dalam Perpuluhan Atau Bilangan Bulat');
			return false;
		}else{
			clearInvalid(classValue);
		}
	}

	return true;
}

function checkAppendInput(parent_row, row, classValue, fieldName ,value, type){
	if(type == 'string'){
		var check = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;
		if(check.test(value) == false){
			$(''+ parent_row +'[data-row='+ row +']').find(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Field Must Only Contain Alphabets and Spaces');
			return false;
		}else{
			clearAppendInvalid(parent_row, row, classValue);
		}
	}else if(type == 'int'){
		var check = /^\d+$/.test(value);
		if(check == false){
			$(''+ parent_row +'[data-row='+ row +']').find(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Field Must Only Contain Numeric');
			return false;
		}else{
			clearAppendInvalid(parent_row, row, classValue);
		}
	}else if(type == 'double'){
		var check = /[^\.].+/.test(value);
		if(check == false){
			$(''+ parent_row +'[data-row='+ row +']').find(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Field Must Be In Decimal');
			return false;
		}else{
			clearAppendInvalid(parent_row, row, classValue);
		}
	}else if(type == 'email'){
		var check = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
		if(check == false){
			return false;
		}else{
			clearAppendInvalid(parent_row, row, classValue);
		}
	}

	return true;
}

function clearInvalid(classValue){
	$(classValue).removeClass('is-invalid').closest('.form-group').find('.invalid-feedback').attr('style', 'display:none').html('')
}

function clearAppendInvalid(parent_row, row, classValue){
	$(''+ parent_row +'[data-row='+ row +']').find(classValue).closest('.form-group').find('.is-invalid').removeClass('is-invalid').find('.invalid-feedback').html('');
}


function validateImageSize(file, fixedwidth, fixedheight){
	data = new FormData();
	data.append('image', file);
    $.ajax({
        type:'POST',
        url:window.location.origin + '/3ps_revamp/accreditation/validation/imagesize',
        data:data,
        dataType: 'json',
        processData: false,
        contentType: false,
        context: this,
        async:false,
        success: function(data) {
            if(data['width'] >= fixedwidth && data['height'] >=fixedheight){
            	formtoast_danger('Image Must Be in ' + fixedwidth +'px by ' + fixedheight + 'px');
            	return false;
            }else{
            	return true;
            }
        }
    });
    // return response;
}