function checkEmptyFields(fieldArray){
	var passed = 0;
    for(var x = 0;x<fieldArray.length;x++){
        var classValue = fieldArray[x][0];
    	var inputValue = fieldArray[x][1];
        var type = fieldArray[x][2];
    	var fieldName = fieldArray[x][3];
        if(inputValue == '' || typeof inputValue == 'undefined'){
            if(type == 'picture'){
                if($(classValue).closest('.form-group').find('.upload-preview').attr('href') == getUrl() + '/' || typeof $(classValue).closest('.form-group').find('.upload-preview').attr('href') == 'undefined' || $(classValue).closest('.form-group').find('.upload-preview').attr('href') == '' || $(classValue).closest('.form-group').find('.upload-preview').attr('href') == getUrl()){
                    if($(classValue)[0].files[0] == '' || typeof $(classValue)[0].files[0] == 'undefined'){
                        $(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').attr('style', 'display:block').html(fieldName + ' Perlu Dimuat Turun');
                        passed = 1;
                    }else{
                        clearInvalid(classValue);
                    }
                }
            }else{
                // console.log(classValue);
                // return false;
                $(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Perlu Diisi');
                passed = 1;
            }
        }else if(inputValue != '' || typeof inputValue != 'undefined'){

        	if(checkInput(classValue, fieldName, inputValue, type) == false){
        		passed = 1;
        	}
        }
    }
    if(passed == 1){
    	return false;
    }else{
    	return true;
    }
}

function checkEmptyFieldsAppend(fieldArray){
    var passed = 0;
    for(var x = 0;x<fieldArray.length;x++){
        var classValue = fieldArray[x][0];
        var inputValue = fieldArray[x][1];
        var fieldName = fieldArray[x][3];
        var row = fieldArray[x][4];
        var parent_row = fieldArray[x][5];

        if(inputValue == '' || typeof inputValue == 'undefined'){
            $(''+ parent_row +'[data-row='+ row +']').find(classValue).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').html(fieldName + ' Cannot Be Empty');
            passed = 1;
        }else if(inputValue != '' || typeof inputValue != 'undefined'){
            var type = fieldArray[x][2];
            if(checkAppendInput(parent_row, row, classValue, fieldName, inputValue, type) == false){
                passed = 1;
            }
        }
    }
    if(passed == 1){
        return false;
    }else{
        return true;
    }
}

function postEmptyFields(fieldArray){
    for(var x = 0;x<fieldArray.length;x++){
        var inputClass = fieldArray[x][0];
        var inputType = fieldArray[x][1];

        if(inputType == 'text'){
            $(inputClass).val('').closest('.form-group').find('.invalid-feedback').html('');
        }else if(inputType == 'textarea'){
            $(inputClass).html('').closest('.form-group').find('.invalid-feedback').html('');
        }else if(inputType == 'dropdown'){
            $(inputClass).val('').trigger('change').closest('.form-group').find('.invalid-feedback').html('');
        }else if(inputType == 'photo'){
			$(inputClass).attr('style', '');
		}else if(inputType == 'checkbox'){
            $(inputClass).each(function(i, obj) {
                var check = $(this).prop('checked');
                if(check){
                    $(this).prop('checked', false);
                }
            });
        }
    }
}

function checkPicData(data){
    var pass = 0;
    for(var x = 0;x < data.length;x ++){
        var pic = data[x][0];

        if(pic === '' || typeof pic === 'undefined'){

        }else{
            var picName = data[x][0]['name'].toLowerCase();
            var className = data[x][1];
            var filetype = data[x][2];
            var extension = picName.substr( (picName.lastIndexOf('.') +1) );
            if(filetype.includes(extension)){
                clearInvalid(className);
            }else{
                $(className).addClass('is-invalid').closest('.form-group').find('.invalid-feedback').attr('style', 'display:block').html('Perlu Format '+ filetype);

                pass = 1;
            }
        }
    }

    if(pass == 1){
        return false;
    }else{
        return true;
    }
}
