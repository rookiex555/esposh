var token = $('._token').val();

function dropwdownYear(className, range){
    
}

function dropdown_populate(selectorClass, inputClass, listType, model, queryString = [], idSelected = null){
    data = new FormData;
    data.append('model', model);
    data.append('queryString', JSON.stringify(queryString));
    data.append('_token', token);

    ajax_common('POST', '/common/get-listing', data, 0, selectorClass, inputClass, listType, idSelected);
}

function ajax_common(methods,url, data, postfunc, selectorClass, inputClass, listType, idSelected){
    $.ajax({
        type:methods,
        url:getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
           if(postfunc == 0){
                $(selectorClass).empty();
                var append = '';
                if(data['data'].length > 0){
                    for(var x = 0; x < data['data'].length; x++){
                        if(listType == 'checkbox'){
                            append += '<div class="col-xl-4 col-md-4 col-12">' +
                                '<div class="form-group">' +
                                    '<div class="custom-control custom-control-primary custom-checkbox">' +
                                        '<input type="checkbox" class="custom-control-input '+ inputClass +'" id="'+ data['data'][x]['value'] +'" data-role-id="'+ data['data'][x]['value'] +'"/>' +
                                        '<label class="custom-control-label" for="'+ data['data'][x]['value'] +'">'+ data['data'][x]['label'] +'</label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                        }else if(listType == 'dropdown'){
                            if(x == 0){
                                append += '<option value="">Please Select</option>';
                            }
                            append += '<option value="'+ data['data'][x]['value'] +'">'+ data['data'][x]['label'] +'</option>';
                        }else if(listType == 'dropdownWithSelected'){
                            if(x == 0){
                                append += '<option value="">Please Select</option>';
                            }
                            if(idSelected == data['data'][x]['value']){
                                selected = 'selected';
                            }else{
                                selected = '';
                            }

                            append += '<option '+ selected +' value="'+ data['data'][x]['value'] +'">'+ data['data'][x]['label'] +'</option>';
                        }
                    } 
                }else{
                    append += '<option>Tiada Senarai</option>';
                }
                if(listType != 'dropdownWithSelected'){
                    $(selectorClass).append(append);
                }else{
                    $(selectorClass).append(append);
                    $(selectorClass).val(idSelected).trigger('change');
                }
           }
        }
    });
}
