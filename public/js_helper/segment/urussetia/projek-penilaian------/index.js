let projek_id = $('.projek-id').val();
let tahun_id = $('.tahun-id').val();

$(document).on('click', '.add-projek-penilaian, .view-projek, .projek-penilaian-update, .projek-penilaian-soalan', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('add-projek-penilaian')){
        postEmptyFields([
            ['.projek-penilaian-no-klausa', 'text'],
            ['.projek-penilaian-pengarah-jawatan', 'dropdown'],
            ['.projek-penilaian-pengarah-nama', 'text'],
            ['.projek-penilaian-wakil-jawatan', 'dropdown'],
            ['.projek-penilaian-wakil-nama', 'text'],
            ['.projek-penilaian-tkh-tapak', 'text'],
            ['.projek-penilaian-tkh-asal', 'text'],
            ['.projek-penilaian-tkh-semasa', 'text'],
            ['.projek-penilaian-tkh-penilaian', 'text'],
            ['.projek-penilaian-kemajuan-fizikal', 'text'],
            ['.projek-penilaian-dokumen', 'text'],
        ]);
        $('.post-add-projek-penilaian').attr('style', 'display:block');
        $('.post-update-projek-penilaian').attr('style', 'display:none');
        $('.projek-penilaian-modal').modal('show');
    }else if(selectedClass.hasClass('projek-penilaian-update')){
        let projek_penilaian_id = selectedClass.closest('tr').attr('data-projek-penilaian-id');
        $('.projek-penilaian-id').val(projek_penilaian_id);
        $('.post-add-projek-penilaian').attr('style', 'display:none');
        $('.post-update-projek-penilaian').attr('style', 'display:block');

        let data = new FormData;
        data.append('projek_penilaian_id', projek_penilaian_id);
        data.append('_token', getToken());
        ajax('/urussetia/projek-penilaian/get-projek-penilaian', data, 2);
    }else if(selectedClass.hasClass('projek-penilaian-soalan')){
        let projek_penilaian_id = selectedClass.closest('tr').attr('data-projek-penilaian-id');
        window.location.href = getUrl() + '/urussetia/projek-penilaian-soalan/all/' + projek_penilaian_id;
    }
});

$(document).on('click', '.post-add-projek-penilaian, .post-update-projek-penilaian', function(){
    let projek_penilaian_no_klausa = $('.projek-penilaian-no-klausa').val();
    let projek_penilaian_pengarah_jawatan = $('.projek-penilaian-pengarah-jawatan').val();
    let projek_penilaian_pengarah_nama = $('.projek-penilaian-pengarah-nama').val();
    let projek_penilaian_wakil_jawatan = $('.projek-penilaian-wakil-jawatan').val();
    let projek_penilaian_wakil_nama = $('.projek-penilaian-wakil-nama').val();
    let projek_penilaian_tkh_tapak = $('.projek-penilaian-tkh-tapak').val();
    let projek_penilaian_tkh_asal = $('.projek-penilaian-tkh-asal').val();
    let projek_penilaian_tkh_semasa = $('.projek-penilaian-tkh-semasa').val();
    let projek_penilaian_tkh_penilaian = $('.projek-penilaian-tkh-penilaian').val();
    let projek_penilaian_kemajuan_fizikal = $('.projek-penilaian-kemajuan-fizikal').val();
    let projek_penilaian_penilai = $('.projek-penilaian-penilai').val();

    let selectedClass  = $(this);

    let check = checkEmptyFields([
        ['.projek-penilaian-no-klausa', projek_penilaian_no_klausa, 'mix', 'No. Klausa'],
        ['.projek-penilaian-pengarah-jawatan', projek_penilaian_pengarah_jawatan, 'int', 'Jawatan Pengarah'],
        ['.projek-penilaian-pengarah-nama', projek_penilaian_pengarah_nama, 'string', 'Nama Pengarah'],
        ['.projek-penilaian-wakil-jawatan', projek_penilaian_wakil_jawatan, 'int', 'Jawatan Wakil'],
        ['.projek-penilaian-wakil-nama', projek_penilaian_wakil_nama, 'string', 'Nama Wakil'],
        ['.projek-penilaian-tkh-tapak', projek_penilaian_tkh_tapak, 'datedash', 'Tarikh Tapak'],
        ['.projek-penilaian-tkh-asal', projek_penilaian_tkh_asal, 'datedash', 'Tarikh Asal'],
        ['.projek-penilaian-tkh-semasa', projek_penilaian_tkh_semasa, 'datedash', 'Tarikh Semasa'],
        ['.projek-penilaian-tkh-penilaian', projek_penilaian_tkh_penilaian, 'datedash', 'Tarikh Penilaian'],
        ['.projek-penilaian-kemajuan-fizikal', projek_penilaian_kemajuan_fizikal, 'int', 'Kemajuan Fizikal'],
        ['.projek-penilaian-penilai', projek_penilaian_penilai, 'int', 'Penilai (KPA)'],
    ]);

    if(check == false){
        return false;
    }

    let data = new FormData;
    data.append('projek_penilaian_no_klausa', projek_penilaian_no_klausa);
    data.append('projek_penilaian_pengarah_jawatan', projek_penilaian_pengarah_jawatan);
    data.append('projek_penilaian_pengarah_nama', projek_penilaian_pengarah_nama);
    data.append('projek_penilaian_wakil_jawatan', projek_penilaian_wakil_jawatan);
    data.append('projek_penilaian_wakil_nama', projek_penilaian_wakil_nama);
    data.append('projek_penilaian_tkh_tapak', projek_penilaian_tkh_tapak);
    data.append('projek_penilaian_tkh_asal', projek_penilaian_tkh_asal);
    data.append('projek_penilaian_tkh_semasa', projek_penilaian_tkh_semasa);
    data.append('projek_penilaian_tkh_penilaian', projek_penilaian_tkh_penilaian);
    data.append('projek_penilaian_kemajuan_fizikal', projek_penilaian_kemajuan_fizikal);
    data.append('projek_penilaian_penilai', projek_penilaian_penilai);
    data.append('_token', getToken());

    let trigger;
    let url;
    if(selectedClass.hasClass('post-add-projek-penilaian')){
        data.append('projek_id', projek_id);
        data.append('tahun_id', tahun_id);
        data.append('trigger', 0);
        url = '/urussetia/projek-penilaian/tambah';
        trigger = 0;
    }else{
        data.append('projek_penilaian_id', $('.projek-penilaian-id').val());
        data.append('trigger', 1);
        url = '/urussetia/projek-penilaian/update';
        trigger = 3;
    }
    ajax(url, data, trigger);
});

$(document).on('click', '.projek-penilaian-delete, .projek-penilaian-aktif', function(){
    let selectedClass = $(this);
    let projek_penilaian_id = selectedClass.closest('tr').attr('data-projek-penilaian-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('projek_penilaian_id', projek_penilaian_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('projek-penilaian-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Seluruh Data Penilaian Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/urussetia/projek-penilaian/delete',
                data: data,
                postfunc: 0
            }
        });
    }else if(selectedClass.hasClass('projek-penilaian-aktif')){
        trigger = 1;
        url = '/urussetia/projek-penilaian/aktif';
        ajax(url, data, trigger);
    }
});

$(document).on('click', '.projek-penilaian-soalan-send', function(){

    let penilaian_id = $(this).closest('tr').attr('data-projek-penilaian-id');

    let data = new FormData;
    data.append('penilaian_id', penilaian_id);
    data.append('_token', getToken());

    swalAjax({
        titleText : 'Adakah Anda Pasti?',
        mainText : 'Soalan Penilaian Tidak Boleh Dikemaskini Selepas Penghantaran',
        icon: 'warning',
        confirmButtonText: 'Hantar',
        postData: {
            url : '/urussetia/projek-penilaian/hantar',
            data: data,
            postfunc: 1
        }
    });
});

$(document).on('click', '.projek-penilaian-review', function(){
    let projek_penilaian_id = $(this).closest('.projek-penilaian-row').attr('data-projek-penilaian-id');

    window.location.href = getUrl() + '/urussetia/projek-penilaian/review/' + projek_penilaian_id;
});
