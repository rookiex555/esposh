$('.projek-penilaian-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: getUrl() + '/urussetia/projek-penilaian/list/table/' + $('.projek-id').val() + '/' + $('.tahun-id').val(),
    lengthChange:true,
    columns: [
        { data: 'no_klausa' },
        { data: 'penilai' },
        { data: 'tkh_penilaian' },
        { data: 'status_penilaian' },
        // { data: 'active' },
        { data: 'action' },
    ],
    createdRow: function( row, data, dataIndex ) {
        $(row).addClass('projek-penilaian-row');
    },
    columnDefs: [
        // {
        //     // Actions
        //     targets: -2,
        //     title: 'Aktif',
        //     orderable: false,
        //     render: function (data, type, full, meta) {
        //         let row_flag = full.flag;
        //         let outLine = row_flag == 1 ? 'btn-outline-success' : 'btn-outline-danger';
        //         return (
        //             '<button type="button" class="btn btn-icon '+ outLine +' mr-1 mb-1 waves-effect waves-light projek-penilaian-aktif">'+ feather.icons['power'].toSvg() +'</button>'
        //         );
        //     }
        // },
        {
            // Actions
            targets: -1,
            title: 'Tindakan',
            orderable: false,
            render: function (data, type, full, meta) {
                let status = full.status;
                let buttonList;
                if(status == 0){
                    buttonList =
                        '<button type="button" class="btn btn-icon btn-outline-info mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan" data-toggle="tooltip" data-placement="top" title="Konfigurasi Soalan">'+ feather.icons['list'].toSvg() +'</button>' +
                        '<button type="button" class="btn btn-icon btn-outline-success mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-send" data-toggle="tooltip" data-placement="top" title="Hantar Penilaian Kepada KPA">'+ feather.icons['mail'].toSvg() +'</button>' +
                        '<button type="button" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light projek-penilaian-update" data-toggle="tooltip" data-placement="top" title="Kemaskini Penilaian">'+ feather.icons['edit'].toSvg() +'</button>' +
                        '<button type="button" class="btn btn-icon btn-outline-danger mr-1 mb-1 waves-effect waves-light projek-penilaian-delete" data-toggle="tooltip" data-placement="top" title="Padam">'+ feather.icons['trash-2'].toSvg() +'</button>';

                }else if(status == 2 || status == 1){
                    buttonList = '<button type="button" class="btn btn-icon btn-outline-success mr-1 mb-1 waves-effect waves-light projek-penilaian-review" data-toggle="tooltip" data-placement="top" title="Preview Penilaian">'+ feather.icons['list'].toSvg() +'</button>';
                }
                return (
                    buttonList
                );
            }
        }
    ],
    dom:
        '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    lengthMenu: [7, 10, 25, 50, 75, 100],
    buttons: [,
        {
            text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Tambah Penilaian',
            className: 'create-new btn btn-primary add-projek-penilaian',
            attr: {
                'data-toggle': 'modal',
                'data-target': '#modals-slide-in'
            },
            init: function (api, node, config) {
                $(node).removeClass('btn-secondary');
            }
        }
    ],
    language: {
        paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
        }
    }
});
