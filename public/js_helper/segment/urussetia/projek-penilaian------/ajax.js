function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.projek-penilaian-modal').modal('hide');
                    $('.projek-penilaian-table').DataTable().ajax.reload(null, false);
                    toasting('Maklumat Penilaian Asas Ditambah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Maklumat Penilaian Asas Sudah Wujud', 'error');
                }
            }else if(postfunc == 1){
                if(success == 1){
                    let projek_penilaian_id = parseData.projek_penilaian_id;
                    if(parseData.flag == 0){
                        $('.projek-penilaian-row[data-projek-penilaian-id='+ projek_penilaian_id +']').find('.projek-penilaian-aktif').removeClass('btn-outline-success').addClass('btn-outline-danger');
                    }else{
                        $('.projek-penilaian-row[data-projek-penilaian-id='+ projek_penilaian_id +']').find('.projek-penilaian-aktif').removeClass('btn-outline-danger').addClass('btn-outline-success');
                    }
                    $.unblockUI();
                }
            }else if(postfunc == 2){
                if(success == 1){
                    $('.projek-penilaian-no-klausa').val(parseData.no_klausa);
                    $('.projek-penilaian-pengarah-jawatan').val(parseData.pengarah.jawatan).trigger('change');
                    $('.projek-penilaian-pengarah-nama').val(parseData.pengarah.nama);
                    $('.projek-penilaian-wakil-jawatan').val(parseData.wakil.jawatan).trigger('change');
                    $('.projek-penilaian-wakil-nama').val(parseData.wakil.nama);
                    $('.projek-penilaian-penilai').append('<option value="'+ parseData.penilai.nokp +'" selected="selected">'+ parseData.penilai.nama +'</option>');
                    $('.projek-penilaian-tkh-tapak').val(parseData.tkh_milik_tpk);
                    $('.projek-penilaian-tkh-asal').val(parseData.tkh_siap_asal);
                    $('.projek-penilaian-tkh-semasa').val(parseData.tkh_siap_semasa);
                    $('.projek-penilaian-tkh-penilaian').val(parseData.tkh_penilaian);
                    $('.projek-penilaian-kemajuan-fizikal').val(parseData.kemajuan_penilaian);
                    $('.projek-penilaian-modal').modal('show');
                    $.unblockUI();
                }
            }else if(postfunc == 3){
                if(success == 1){
                    $('.projek-penilaian-modal').modal('hide');
                    $('.projek-penilaian-table').DataTable().ajax.reload(null, false);
                    toasting('Maklumat Penilaian Asas Telah Dikemaskini', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Maklumat Penilaian Asas Sudah Wujud', 'error');
                }
            }
        }
    });
}
