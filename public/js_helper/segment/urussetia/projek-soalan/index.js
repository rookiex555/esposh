let projek_tahun_id = $('.projek-tahun-id').val();

//For MAIN SEKTOR
$(document).on('click', '.projek-penilaian-soalan-main-add, .projek-penilaian-soalan-main-update, .projek-penilaian-soalan-main-delete', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('projek-penilaian-soalan-main-add')){
        postEmptyFields([
            ['.projek-penilaian-soalan-main-bil', 'text'],
            ['.projek-penilaian-soalan-main-nama', 'text'],
            ['.projek-penilaian-soalan-main-peratus', 'text'],
        ]);
        $('.post-add-projek-penilaian-soalan-main').attr('style', 'display:block');
        $('.post-update-projek-penilaian-soalan-main').attr('style', 'display:none');
        $('.projek-penilaian-soalan-main-modal').modal('show');
    }else if(selectedClass.hasClass('projek-penilaian-soalan-main-update')){
        let selectedClass = $(this);

        let projek_penilaian_soalan_main_id = selectedClass.closest('.penilaian-soalan-main-row').attr('data-penilaian-soalan-main-id');
        $('.projek-penilaian-soalan-main-id').val(projek_penilaian_soalan_main_id);
        let data = new FormData;
        data.append('projek_penilaian_soalan_main_id', projek_penilaian_soalan_main_id);
        data.append('_token', getToken());

        ajax('/urussetia/projek-penilaian-soalan/main/get-soalan-main', data, 1);
        $('.post-add-projek-penilaian-soalan-main').attr('style', 'display:none');
        $('.post-update-projek-penilaian-soalan-main').attr('style', 'display:block');
        $('.projek-penilaian-soalan-main-modal').modal('show');
    }else if(selectedClass.hasClass('projek-penilaian-soalan-main-delete')){
        let data = new FormData;
        data.append('projek_penilaian_id', selectedClass.closest('.penilaian-soalan-main-row').attr('data-penilaian-soalan-main-id'));
        data.append('_token', getToken());
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Seluruh Data Penilaian Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/urussetia/projek-penilaian-soalan/main/delete',
                data: data,
                postfunc: 0
            }
        });
    }
});

$(document).on('click', '.post-add-projek-penilaian-soalan-main, .post-update-projek-penilaian-soalan-main', function(){
    let projek_penilaian_soalan_main_bil = $('.projek-penilaian-soalan-main-bil').val();
    let projek_penilaian_soalan_main_nama = $('.projek-penilaian-soalan-main-nama').val();
    let projek_penilaian_soalan_main_peratus = $('.projek-penilaian-soalan-main-peratus').val();

    let selectedClass  = $(this);

    let check = checkEmptyFields([
        ['.projek-penilaian-soalan-main-nama', projek_penilaian_soalan_main_nama, 'mix', 'Nama Sektor'],
        ['.projek-penilaian-soalan-main-peratus', projek_penilaian_soalan_main_peratus, 'int', 'Peratus Penilaian'],
    ]);

    if(check == false){
        return false;
    }

    let data = new FormData;
    data.append('projek_penilaian_soalan_main_bil', projek_penilaian_soalan_main_bil);
    data.append('projek_penilaian_soalan_main_nama', projek_penilaian_soalan_main_nama);
    data.append('projek_penilaian_soalan_main_peratus', projek_penilaian_soalan_main_peratus);
    data.append('_token', getToken());

    let trigger;
    let url;
    if(selectedClass.hasClass('post-add-projek-penilaian-soalan-main')){
        data.append('projek_tahun_id', projek_tahun_id);
        data.append('trigger', 0);
        url = '/urussetia/projek-penilaian-soalan/main/tambah';
        trigger = 0;
    }else{
        data.append('projek_penilaian_soalan_main_id', $('.projek-penilaian-soalan-main-id').val());
        data.append('trigger', 1);
        url = '/urussetia/projek-penilaian-soalan/main/update';
        trigger = 2;
    }
    ajax(url, data, trigger);
});

//FOR MAIN SEKTOR SUB
$(document).on('click', '.projek-penilaian-soalan-main-sub-list, .projek-penilaian-soalan-main-add, .projek-penilaian-soalan-main-sub-update, .projek-penilaian-soalan-main-sub-delete', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('projek-penilaian-soalan-main-sub-list')){

        $('.main-sub-tambah-row').attr('style', 'display:block');
        $('.main-sub-update-row').attr('style', 'display:none');
        let selectedClass = $(this);
        let projek_penilaian_soalan_main_id = selectedClass.closest('.penilaian-soalan-main-row').attr('data-penilaian-soalan-main-id');

        loadMainSubTable({
            className: '.projek-penilaian-soalan-main-sub-table',
            url: '/urussetia/projek-penilaian-soalan-sub/list/table/' + projek_penilaian_soalan_main_id
        });
        $('.projek-penilaian-soalan-main-id').val(projek_penilaian_soalan_main_id);

        $('.projek-penilaian-soalan-main-sub-modal').modal('show');
    }else if(selectedClass.hasClass('projek-penilaian-soalan-main-sub-update')){
        let selectedClass = $(this);

        let projek_penilaian_soalan_main_sub_id = selectedClass.closest('.projek-penilaian-soalan-main-sub-row').attr('data-projek-penilaian-soalan-main-sub-id');
        $('.projek-penilaian-soalan-main-sub-id').val(projek_penilaian_soalan_main_sub_id);

        $('.main-sub-tambah-row').attr('style', 'display:none');
        $('.main-sub-update-row').attr('style', '');

        let data = new FormData;
        data.append('projek_penilaian_soalan_main_sub_id', projek_penilaian_soalan_main_sub_id);
        data.append('_token', getToken());

        ajax('/urussetia/projek-penilaian-soalan/main/sub/get-soalan-sub', data, 5);
    }else if(selectedClass.hasClass('projek-penilaian-soalan-main-sub-delete')){
        let data = new FormData;
        data.append('projek_penilaian_soalan_main_sub_id', selectedClass.closest('.projek-penilaian-soalan-main-sub-row').attr('data-projek-penilaian-soalan-main-sub-id'));
        data.append('_token', getToken());
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Sub Sektor Penilaian Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/urussetia/projek-penilaian-soalan/main/sub/delete',
                data: data,
                postfunc: 1
            }
        });
    }
});

$(document).on('click', '.post-cancel-projek-penilaian-soalan-main-sub', function(){
    $('.main-sub-tambah-row').attr('style', 'display:block');
    $('.main-sub-update-row').attr('style', 'display:none');
    $('.projek-penilaian-soalan-main-sub-bilangan').val('');
    $('.projek-penilaian-soalan-main-sub-no-klausa').val('');
    $('.projek-penilaian-soalan-main-sub-perkara').val('');
});

$(document).on('click', '.post-add-projek-penilaian-soalan-main-sub, .post-update-projek-penilaian-soalan-main-sub', function(){
    let projek_penilaian_soalan_main_sub_perkara = $('.projek-penilaian-soalan-main-sub-perkara').val();
    let projek_penilaian_soalan_main_sub_no_klausa = $('.projek-penilaian-soalan-main-sub-no-klausa').val();
    let projek_penilaian_soalan_main_sub_bilangan = $('.projek-penilaian-soalan-main-sub-bilangan').val();
    let projek_penilaian_soalan_main_id = $('.projek-penilaian-soalan-main-id').val();


    let selectedClass  = $(this);

    let check = checkEmptyFields([
        ['.projek-penilaian-soalan-main-sub-bilangan', projek_penilaian_soalan_main_sub_bilangan, 'mix', 'No. Bilangan'],
        ['.projek-penilaian-soalan-main-sub-perkara', projek_penilaian_soalan_main_sub_perkara, 'mix', 'Tajuk Sub Penilaian'],
    ]);

    if(check == false){
        return false;
    }

    let data = new FormData;
    data.append('projek_penilaian_soalan_main_sub_perkara', projek_penilaian_soalan_main_sub_perkara);
    data.append('projek_penilaian_soalan_main_sub_no_klausa', projek_penilaian_soalan_main_sub_no_klausa);
    data.append('projek_penilaian_soalan_main_sub_bilangan', projek_penilaian_soalan_main_sub_bilangan);
    data.append('_token', getToken());

    let trigger;
    let url;
    if(selectedClass.hasClass('post-add-projek-penilaian-soalan-main-sub')){
        data.append('projek_penilaian_soalan_main_id', projek_penilaian_soalan_main_id);
        data.append('trigger', 0);
        url = '/urussetia/projek-penilaian-soalan/main/sub/tambah';
        trigger = 3;
    }else{
        data.append('projek_penilaian_soalan_main_sub_id', $('.projek-penilaian-soalan-main-sub-id').val());
        data.append('trigger', 1);
        url = '/urussetia/projek-penilaian-soalan/main/sub/update';
        trigger = 4;
    }
    ajax(url, data, trigger);
});

//For SUB PERKARA
$(document).on('click', '.penilaian-soalan-main-sub-row', function(){
    let sub_row_id = $(this).attr('data-id');
    let main_row_id =  $(this).closest('.penilaian-soalan-main-row').attr('data-penilaian-soalan-main-id');
    $(this).closest('.penilaian-soalan-main-row').find('.projek-penilaian-soalan-main-sub-perkara-main-pressed-id').val(main_row_id);
    $(this).closest('.penilaian-soalan-main-row').find('.projek-penilaian-soalan-main-sub-perkara-sub-pressed-id').val(sub_row_id);
});

$(document).on('click', '.projek-penilaian-soalan-main-sub-perkara-add, .projek-penilaian-soalan-main-sub-perkara-update, .projek-penilaian-soalan-main-sub-perkara-delete', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('projek-penilaian-soalan-main-sub-perkara-add')){

        $('.post-add-projek-penilaian-soalan-main-sub-perkara').attr('style', 'display:block');
        $('.post-update-projek-penilaian-soalan-main-sub-perkara').attr('style', 'display:none');

        postEmptyFields([
            ['.projek-penilaian-soalan-main-sub-perkara-bilangan', 'text'],
            ['.projek-penilaian-soalan-main-sub-perkara-no-klausa', 'text'],
            ['.projek-penilaian-soalan-main-sub-perkara-soalan', 'text'],
        ]);

        let main_id = $(this).closest('.penilaian-soalan-main-row').find('.projek-penilaian-soalan-main-sub-perkara-main-pressed-id').val();
        let main_sub_id = $(this).closest('.penilaian-soalan-main-row').find('.projek-penilaian-soalan-main-sub-perkara-sub-pressed-id').val();
        $('.projek-penilaian-soalan-main-current-id').val(main_id);
        $('.projek-penilaian-soalan-main-sub-current-id').val(main_sub_id);

        if(main_id == '' && main_sub_id == ''){
            toasting('Sila Pilih Sub Sektor Sebelum Menambah Soalan', 'error');
            return false;
        }

        $('.projek-penilaian-soalan-main-sub-perkara-modal').modal('show');
    }else if(selectedClass.hasClass('projek-penilaian-soalan-main-sub-perkara-update')){
        let selectedClass = $(this);

        let projek_penilaian_soalan_main_sub_perkara_id = selectedClass.closest('.penilaian-soalan-main-sub-perkara-row').attr('data-id');
        $('.projek-penilaian-soalan-main-sub-perkara-id').val(projek_penilaian_soalan_main_sub_perkara_id);


        $('.post-add-projek-penilaian-soalan-main-sub-perkara').attr('style', 'display:none');
        $('.post-update-projek-penilaian-soalan-main-sub-perkara').attr('style', 'display:block');

        let data = new FormData;
        data.append('projek_penilaian_soalan_main_sub_perkara_id', projek_penilaian_soalan_main_sub_perkara_id);
        data.append('_token', getToken());

        ajax('/urussetia/projek-penilaian-soalan/main/sub/perkara/get-soalan', data, 7);
        $('.projek-penilaian-soalan-main-sub-perkara-modal').modal('show');
    }else if(selectedClass.hasClass('projek-penilaian-soalan-main-sub-perkara-delete')){
        let data = new FormData;
        data.append('projek_penilaian_soalan_main_sub_perkara_id', selectedClass.closest('.penilaian-soalan-main-sub-perkara-row').attr('data-id'));
        data.append('_token', getToken());
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Soalan Penilaian Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/urussetia/projek-penilaian-soalan/main/sub/perkara/delete',
                data: data,
                postfunc: 2
            }
        });
    }
});

$(document).on('click', '.post-add-projek-penilaian-soalan-main-sub-perkara, .post-update-projek-penilaian-soalan-main-sub-perkara', function(){
    let projek_penilaian_soalan_main_sub_perkara_bilangan = $('.projek-penilaian-soalan-main-sub-perkara-bilangan').val();
    let projek_penilaian_soalan_main_sub_perkara_no_klausa = $('.projek-penilaian-soalan-main-sub-perkara-no-klausa').val();
    let projek_penilaian_soalan_main_sub_perkara_soalan = $('.projek-penilaian-soalan-main-sub-perkara-soalan').val();

    let main_id = $('.projek-penilaian-soalan-main-current-id').val();
    let main_sub_id = $('.projek-penilaian-soalan-main-sub-current-id').val();

    let selectedClass  = $(this);

    let check = checkEmptyFields([
        ['.projek-penilaian-soalan-main-sub-perkara-bilangan', projek_penilaian_soalan_main_sub_perkara_bilangan, 'mix', 'No. Bilangan'],
        ['.projek-penilaian-soalan-main-sub-perkara-soalan', projek_penilaian_soalan_main_sub_perkara_soalan, 'mix', 'Tajuk Soalan'],
    ]);

    if(check == false){
        return false;
    }

    let data = new FormData;
    data.append('projek_penilaian_soalan_main_sub_perkara_bilangan', projek_penilaian_soalan_main_sub_perkara_bilangan);
    data.append('projek_penilaian_soalan_main_sub_perkara_no_klausa', projek_penilaian_soalan_main_sub_perkara_no_klausa);
    data.append('projek_penilaian_soalan_main_sub_perkara_soalan', projek_penilaian_soalan_main_sub_perkara_soalan);
    data.append('_token', getToken());

    let trigger;
    let url;
    if(selectedClass.hasClass('post-add-projek-penilaian-soalan-main-sub-perkara')){
        data.append('main_id', main_id);
        data.append('main_sub_id', main_sub_id);
        data.append('trigger', 0);
        url = '/urussetia/projek-penilaian-soalan/main/sub/perkara/tambah';
        trigger = 6;
    }else{
        data.append('projek_penilaian_soalan_main_sub_perkara_id', $('.projek-penilaian-soalan-main-sub-perkara-id').val());
        data.append('trigger', 1);
        url = '/urussetia/projek-penilaian-soalan/main/sub/perkara/update';
        trigger = 8;
    }
    ajax(url, data, trigger);
});

//Penilai Go
$(document).on('click', '.projek-penilaian-soalan-siap', function(){
    window.location.href = getUrl() + '/urussetia/projek-penilaian/list/' + $('.projek-tahun-id').val() + '/' + $('.tahun-id').val();
});

//Penilaian Duplicate
$(document).on('click', '.projek-penilaian-soalan-duplicate', function(){
    $('.projek-penilaian-soalan-duplicate-modal').modal('show');
});

$(document).on('click', '.post-projek-penilaian-soalan-duplicate', function(){
    let selectedClass  = $(this);
    let projek_penilaian_soalan_duplicate_pilihan = $('.projek-penilaian-soalan-duplicate-pilihan').val();

    let check = checkEmptyFields([
        ['.projek-penilaian-soalan-duplicate-pilihan', projek_penilaian_soalan_duplicate_pilihan, 'mix', 'Sila Pilih Penilaian']
    ]);

    if(check == false){
        return false;
    }

    let data = new FormData;
    data.append('pilihan', projek_penilaian_soalan_duplicate_pilihan);
    data.append('selected_projek_tahun_id', $('.projek-tahun-id').val());
    data.append('_token', getToken());

    swalAjax({
        titleText : 'Adakah Anda Pasti?',
        mainText : 'Seluruh Soalan Penilaian Akan ditiru',
        icon: 'warning',
        confirmButtonText: 'Duplicate',
        postData: {
            url : '/urussetia/projek-penilaian-soalan/duplicate/confirm',
            data: data,
            postfunc: 3
        }
    });
});
