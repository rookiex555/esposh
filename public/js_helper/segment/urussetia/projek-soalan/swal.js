function swalAjax({titleText, mainText, icon, confirmButtonText, postData}){
    Swal.fire({
        title: titleText,
        text: mainText,
        icon: icon,
        showCancelButton: true,
        confirmButtonText: confirmButtonText,
        customClass: {
            confirmButton: 'btn btn-warning',
            cancelButton: 'btn btn-outline-danger ml-1'
        },
        buttonsStyling: false
    }).then(function (result) {
        if (result.value) {

            swalAjaxFire(postData);
        }
    });
}


function swalAjaxFire(postData){
    let url = postData.url;
    let data = postData.data;
    let postfunc = postData.postfunc;

    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ parseData.projek_penilaian_main_id +']').remove();
                    swalPostFire('success', 'Berjaya Dipadam', 'Sektor Sudah Dipadam');
                }
                $.unblockUI();
            }else if(postfunc == 1){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-sub-table').DataTable().ajax.reload(null, false);
                    let projek_penilaian_soalan_main_sub_id = parseData.projek_penilaian_soalan_main_sub_id;
                    let projeks_tahuns_penilaians_id = parseData.projeks_tahuns_penilaians_id;
                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ projeks_tahuns_penilaians_id +']').find('.penilaian-soalan-main-sub-row[data-id='+ projek_penilaian_soalan_main_sub_id +']').remove();

                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ projeks_tahuns_penilaians_id +']').find('.penilaian-soalan-main-sub-row-show-pane[id=soalan-main-sub-'+ projek_penilaian_soalan_main_sub_id +']').remove();

                    swalPostFire('success', 'Berjaya Dipadam', 'Sub Sektor Sudah Dipadam');
                    $.unblockUI();
                }
            }else if(postfunc == 2){
                $('.penilaian-soalan-main-sub-row-show-pane[id=soalan-main-sub-'+ parseData.sub_id +']').find('.penilaian-soalan-main-sub-perkara-row[data-id='+ parseData.id +']').remove();
                swalPostFire('success', 'Berjaya Dipadam', 'Soalan Sudah Dipadam');
                $.unblockUI();
            }else if(postfunc == 3){
                location.reload();
            }
        }
    });
}

function swalPostFire(icon, title, mainText){
    Swal.fire({
        icon: icon,
        title: title,
        text: mainText,
        customClass: {
            confirmButton: 'btn btn-success'
        }
    });
}
