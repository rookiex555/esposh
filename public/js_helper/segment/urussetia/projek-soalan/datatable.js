function loadMainSubTable({className, url}){
    $(className).DataTable({
        processing: true,
        serverSide: true,
        ajax: getUrl() + url,
        lengthChange:true,
        columns: [
            { data: 'tajuk_perkara' },
            { data: 'no_klausa' },
            { data: 'active' },
            { data: 'action' },
        ],
        createdRow: function( row, data, dataIndex ) {
            $(row).addClass('projek-penilaian-soalan-main-sub-row');
        },
        columnDefs: [
            {
                // Actions
                targets: -2,
                title: 'Aktif',
                orderable: false,
                render: function (data, type, full, meta) {
                    let row_flag = full.flag;
                    let outLine = row_flag == 1 ? 'btn-outline-success' : 'btn-outline-danger';
                    return (
                        '<button type="button" class="btn btn-icon '+ outLine +' mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-sub-aktif">'+ feather.icons['power'].toSvg() +'</button>'
                    );
                }
            },
            {
                // Actions
                targets: -1,
                title: 'Tindakan',
                orderable: false,
                render: function (data, type, full, meta) {
                    return (
                        '<button type="button" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-sub-update">'+ feather.icons['edit'].toSvg() +'</button>' +
                        '<button type="button" class="btn btn-icon btn-outline-danger mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-sub-delete">'+ feather.icons['trash-2'].toSvg() +'</button>'
                    );
                }
            }
        ],
        dom:
            '<"card-header border-bottom p-1"<"head-label">><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        lengthMenu: [7, 10, 25, 50, 75, 100],
        buttons: [
        ],
        language: {
            paginate: {
                // remove previous & next text from pagination
                previous: '&nbsp;',
                next: '&nbsp;'
            }
        },
        destroy: true
    });

}
