function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;
            if(postfunc == 0){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-modal').modal('hide');

                    $('.penilaian-main-append').append(appPenilaianMain({
                        id: parseData.id,
                        title: parseData.title,
                        percentage: parseData.percentage,
                        ref_id: parseData.ref_id,
                    }));
                    
                    toasting('Sektor Penilaian Ditambah', 'success');
                    $('[data-toggle="tooltip"]').tooltip();
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Sektor Penilaian Sudah Wujud', 'error');
                }
            }else if(postfunc == 1){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-bil').val(parseData.bil);
                    $('.projek-penilaian-soalan-main-nama').val(parseData.nama);
                    $('.projek-penilaian-soalan-main-peratus').val(parseData.percentage);

                    $.unblockUI();
                }
            }else if(postfunc == 2){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-modal').modal('hide');
                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ parseData.id +']').find('.card-title').html(parseData.title + ' ('+ parseData.percentage +'%)')
                    toasting('Sektor Penilaian Telah Dikemaskini', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Sektor Penilaian Sudah Wujud', 'error');
                }
            }else if(postfunc == 3){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-sub-table').DataTable().ajax.reload(null, false);
                    let projek_soalan_main_id = parseData.projek_tahun_penilaian_id;
                    let projek_tahun_penilaian_sub_id = parseData.id;
                    let title = parseData.perkara;
                    let anchor = 'soalan-main-sub-'+ projek_tahun_penilaian_sub_id +'';
                    let ref_id = parseData.ref_id;

                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ projek_soalan_main_id +']').find('.penilaian-soalan-main-sub-row-append').append(appPenilaianSub({
                        id: projek_tahun_penilaian_sub_id,
                        anchor: anchor,
                        ref_id: ref_id,
                        title: title
                    }));
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ projek_soalan_main_id +']').find('.penilaian-soalan-main-sub-soalan-row-append').append(appPenilaianSubPane({
                        anchor: anchor,
                    }));

                    toasting('Sub Sektor Penilaian Telah Ditambah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Sub Sektor Penilaian Sudah Wujud', 'error');
                }
            }else if(postfunc == 4){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-sub-table').DataTable().ajax.reload(null, false);
                    let projek_soalan_main_id = parseData.projek_tahun_penilaian_id;
                    let projek_tahun_penilaian_sub_id = parseData.id;
                    let title = parseData.perkara;
                    let anchor = 'soalan-main-sub-'+ projek_tahun_penilaian_sub_id +'';

                    $('.projek-penilaian-soalan-main-sub-bilangan').val('');
                    $('.projek-penilaian-soalan-main-sub-no-klausa').val('');
                    $('.projek-penilaian-soalan-main-sub-perkara').val('');

                    $('.main-sub-tambah-row').attr('style', '');
                    $('.main-sub-update-row').attr('style', 'display:none');

                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ projek_soalan_main_id +']').find('.penilaian-soalan-main-sub-row[data-id='+ projek_tahun_penilaian_sub_id +']').text(title);
                    toasting('Sub Sektor Penilaian Telah Dikemaskini', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Sub Sektor Penilaian Sudah Wujud', 'error');
                }
            }else if(postfunc == 5){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-sub-bilangan').val(parseData.bil);
                    $('.projek-penilaian-soalan-main-sub-no-klausa').val(parseData.no_klausa);
                    $('.projek-penilaian-soalan-main-sub-perkara').val(parseData.tajuk_perkara);
                    $.unblockUI();
                }
            }else if(postfunc == 6){
                if(success == 1){
                    let bil = parseData.bil;
                    let tajuk_perkara = parseData.tajuk_perkara;
                    let sub_id = parseData.sub_id;
                    let id = parseData.id;
                    let main_id = parseData.main_id;
                    let ref_id = parseData.ref_id;

                    $('.penilaian-soalan-main-row[data-penilaian-soalan-main-id='+ main_id +']').find('.penilaian-soalan-main-sub-row-show-pane[id=soalan-main-sub-'+ sub_id +']').find('.penilaian-main-sub-soalan-drag').append(appPenilaianSoalan({
                        id: id,
                        sub_id: sub_id,
                        ref_id: ref_id,
                        title: '(' + bil + ') ' +  tajuk_perkara
                    }));
                    $('[data-toggle="tooltip"]').tooltip();
                    postEmptyFields([
                        ['.projek-penilaian-soalan-main-sub-perkara-bilangan', 'text'],
                        ['.projek-penilaian-soalan-main-sub-perkara-no-klausa', 'text'],
                        ['.projek-penilaian-soalan-main-sub-perkara-soalan', 'text'],
                    ]);
                    toasting('Sub Sektor Penilaian Telah Ditambah', 'success');
                }
            }else if(postfunc == 7){
                if(success == 1){
                    $('.projek-penilaian-soalan-main-sub-perkara-bilangan').val(parseData.bil);
                    $('.projek-penilaian-soalan-main-sub-perkara-no-klausa').val(parseData.no_klausa);
                    $('.projek-penilaian-soalan-main-sub-perkara-soalan').val(parseData.tajuk_perkara);

                    $.unblockUI();
                }
            }else if(postfunc == 8){
                if(success == 1){
                    $('.penilaian-soalan-main-sub-row-show-pane[id=soalan-main-sub-'+ parseData.sub_id +']').find('.penilaian-soalan-main-sub-perkara-row[data-id='+ parseData.id +']').find('.soalan-title').html('(' + parseData.bil + ') ' + parseData.tajuk_perkara);
                    $('.projek-penilaian-soalan-main-sub-perkara-modal').modal('hide');
                    toasting('Soalan Penilaian Telah Dikemaskini', 'success');
                }else if(success == 2){
                    toasting('Soalan Penilaian Sudah Wujud', 'error');
                }
            }else if(postfunc == 9){
                if(success == 1){
                    window.open(getUrl() + '/urussetia/projek-penilaian-soalan/preview/' + parseData.penilaian_id);
                }else{
                    toasting('Sila Lengkapkan Senarai Soalan', 'error');
                }

            }
        }
    });
}
