function appPenilaianMain({title, percentage, id, ref_id}){
    return '<div class="row penilaian-soalan-main-row" data-penilaian-soalan-main-id="'+ id +'" data-penilaian-soalan-main-ref-id="'+ ref_id +'">' +
        '<div class="col-sm-12">' +
            '<div class="card">' +
                '<div class="card-header">' +
                    '<h4 class="card-title projek-penilaian-soalan-main">'+ title +' ('+ percentage +'%)</h4>' +
                    '<span>' +
                        '<button type="button" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-sub-list" data-toggle="tooltip" data-placement="top" title="Tambah Sub Tajuk">' +
                            feather.icons['align-justify'].toSvg() +
                        '</button>' +
                        '<button type="button" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-update" data-toggle="tooltip" data-placement="top" title="Kemaskini Tajuk">' +
                            feather.icons['edit'].toSvg() +
                        '<button type="button" class="btn btn-icon btn-outline-danger mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-delete" data-toggle="tooltip" data-placement="top" title="Delete Tajuk">' +
                            feather.icons['trash'].toSvg() +
                    '</span>' +
                '</div>' +
                '<div class="card-body">' +
                    '<div class="row mt-1">' +
                        '<div class="col-md-4 col-sm-12">' +
                            '<div class="list-group penilaian-main-sub-drag penilaian-soalan-main-sub-row-append" id="list-tab" role="tablist">' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-8 col-sm-12">' +
                            '<button class="btn btn-primary projek-penilaian-soalan-main-sub-perkara-add" style="width: 100%">Tambah Soalan Baru </button>' +
                            '<input type="hidden" class="projek-penilaian-soalan-main-sub-perkara-main-pressed-id" value="">' +
                            '<input type="hidden" class="projek-penilaian-soalan-main-sub-perkara-sub-pressed-id" value="">' +
                            '<hr>' +
                                '<div class="tab-content penilaian-soalan-main-sub-soalan-row-append" id="nav-tabContent"></div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';
}

function appPenilaianSub({title, anchor, id, ref_id}){
    return '<a class="list-group-item list-group-item-action penilaian-soalan-main-sub-row" data-toggle="list" href="#'+ anchor +'" role="tab" aria-controls="" data-ref-id="'+ ref_id +'" data-id="'+ id +'">'+ title +'</a>';
}

function appPenilaianSubPane({anchor}){
    return '<div class="tab-pane fade show penilaian-soalan-main-sub-row-show-pane" id="'+ anchor +'" role="tabpanel">' +
        '<ul class="list-group penilaian-main-sub-soalan-drag">' +
        '</ul>' +
        '</div>';
}

function appPenilaianSoalan({sub_id, id, ref_id, title}){
    return '<li class="list-group-item d-flex justify-content-between align-items-center penilaian-soalan-main-sub-perkara-row" data-penilaian-soalan-main-sub-id="'+ sub_id +'" data-penilaian-soalan-main-sub-ref-id="'+ ref_id +'" data-id="'+ id +'">' +
        '<div class="col-md-11">' +
            '<span class="soalan-title">'+ title +'</span>' +
        '</div>' +
        '<div class="col-md-1">' +
            '<span>' +
                '<div class="dropdown">' +
                    '<button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">' +
                        feather.icons['more-vertical'].toSvg() +
                    '</button>' +
                    '<div class="dropdown-menu">' +
                        '<a class="dropdown-item projek-penilaian-soalan-main-sub-perkara-update" href="javascript:void(0);">' +
                        feather.icons['edit-2'].toSvg() +
                            '<span>Edit</span>' +
                        '</a>' +
                        '<a class="dropdown-item projek-penilaian-soalan-main-sub-perkara-delete" href="javascript:void(0);">' +
                        feather.icons['trash'].toSvg() +
                        '<span>Delete</span>' +
                        '</a>' +
                    '</div>' +
                '</div>' +
            '</span>' +
        '</div>' +
    '</li>';
}

