function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.penilaian_keputusan_result').empty();

                    if(parseData.length > 0){
                        for(j = 0; j < parseData.length; j++){
                            if(parseData[j].keputusan.penilaian_count > 0){
                                let bil = 1;
                                for(let x=0; x < parseData[j].keputusan.penilaian_count; x++){
                                    let nama_penilaian = parseData[j].keputusan.info[x].nama_penilaian;
                                    let peratus_penilaian = parseData[j].keputusan.info[x].peratus_penilaian;
                                    let peratus = parseData[j].keputusan.info[x].peratus;
                                    let append ='<tr>' +
                                                    '<td>'+ bil +'</td>' +
                                                    '<td>'+ nama_penilaian +' ('+ peratus_penilaian+')</td>' +
                                                    '<td>'+ peratus +'%</td>' +
                                                '</tr>';
                                    $('.penilaian_keputusan_result').append(append);
                                    bil++;
                                }
                                let seluruh_count = parseData[j].keputusan.penilaian_count + 1;
                                let gred_count = parseData[j].keputusan.penilaian_count + 2;
                                let keseluruhan = '<tr>' +
                                                        '<td>'+ seluruh_count +'</td>' +
                                                        '<td>Markah Keseluruhan (100%)</td>' +
                                                        '<td>'+ parseData[j].keputusan.markah_keseluruhan +'%</td>' +
                                                '</tr>';
        
                                let currentGred = parseData[j].keputusan.gred;
                                let gred = '<tr>' +
                                                '<td>'+ gred_count +'</td>' +
                                                '<td>Gred</td>' +
                                                '<td>'+ currentGred +'</td>' +
                                            '</tr>';
                                $('.penilaian_keputusan_result').append(keseluruhan);
                                $('.penilaian_keputusan_result').append(gred);
                            }
                        }
                    }else{
                        let append = '<tr style="text-align: center">' +
                            '<td colspan="3">Sila Pilih Penilaian' +
                            '</td>' +
                        '</tr>';
                        $('.penilaian_keputusan_result').append(append);
                    }
                    $.unblockUI();
                }
            }
        }
    });
}
