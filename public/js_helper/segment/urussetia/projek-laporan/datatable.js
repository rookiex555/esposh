// $('.projek-table').DataTable({
//     processing: true,
//     serverSide: true,
//     ajax: window.location.origin + '/urussetia/projek/list',
//     lengthChange:true,
//     columns: [
//         { data: 'projek_name' },
//         { data: 'active' },
//         { data: 'action' },
//     ],
//     createdRow: function( row, data, dataIndex ) {
//         $(row).addClass('projek-row');
//     },
//     columnDefs: [
//         {
//             // Actions
//             targets: -2,
//             title: 'Aktif',
//             orderable: false,
//             render: function (data, type, full, meta) {
//                 let row_flag = full.flag;
//                 let outLine = row_flag == 1 ? 'btn-outline-success' : 'btn-outline-danger';
//                 return (
//                     '<button type="button" class="btn btn-icon '+ outLine +' mr-1 mb-1 waves-effect waves-light projek-aktif">'+ feather.icons['power'].toSvg() +'</button>'
//                 );
//             }
//         },
//         {
//             // Actions
//             targets: -1,
//             title: 'Aksi',
//             orderable: false,
//             render: function (data, type, full, meta) {
//                 return (
//                     '<button type="button" class="btn btn-icon btn-outline-info mr-1 mb-1 waves-effect waves-light projek-tahunan">'+ feather.icons['list'].toSvg() +'</button>' +
//                     '<button type="button" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light projek-update">'+ feather.icons['edit'].toSvg() +'</button>' +
//                     '<button type="button" class="btn btn-icon btn-outline-danger mr-1 mb-1 waves-effect waves-light projek-delete">'+ feather.icons['trash-2'].toSvg() +'</button>'
//                 );
//             }
//         }
//     ],
//     dom:
//         '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
//     lengthMenu: [7, 10, 25, 50, 75, 100],
//     buttons: [,
//         {
//             text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Tambah Projek',
//             className: 'create-new btn btn-primary add-projek',
//             attr: {
//                 'data-toggle': 'modal',
//                 'data-target': '#modals-slide-in'
//             },
//             init: function (api, node, config) {
//                 $(node).removeClass('btn-secondary');
//             }
//         }
//     ],
//     language: {
//         paginate: {
//             // remove previous & next text from pagination
//             previous: '&nbsp;',
//             next: '&nbsp;'
//         }
//     }
// });
