$(document).on('click', '.add-projek, .view-projek, .projek-update, .projek-tahunan', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('add-projek')){
        postEmptyFields([
            ['.projek-nama', 'text'],
        ]);
        $('.post-add-projek').attr('style', 'display:block');
        $('.post-update-projek').attr('style', 'display:none');
        $('.projek-modal').modal('show');
    }else if(selectedClass.hasClass('projek-update')){
        let projek_id = selectedClass.closest('tr').attr('data-projek-id');
        $('.projek-id').val(projek_id);
        $('.post-add-projek').attr('style', 'display:none');
        $('.post-update-projek').attr('style', 'display:block');

        let data = new FormData;
        data.append('projek_id', projek_id);
        data.append('_token', getToken());
        ajax('/urussetia/projek/get-projek', data, 2);
    }else if(selectedClass.hasClass('projek-tahunan')){
        let projek_id = selectedClass.closest('tr').attr('data-projek-id');
        window.location.href = getUrl() + '/urussetia/projek-tahunan/tahun/' + projek_id;
    }
});


$(document).on('change', '.select-penilaian', function(){
    let project_id = $(this).val();
    let tahun = $('.select-penilaian-tahun').val()

    if(project_id == '' || typeof project_id == 'undefined'){
        return false;
    }

    if(tahun == '' || typeof tahun == 'undefined'){
        toasting('Sila Pilih Tahun', 'error');
        postEmptyFields([
            ['.select-penilaian', 'dropdown'],
        ]);
        return false;
    }

    let data = new FormData;
    data.append('projek_id', project_id);
    data.append('tahun', tahun);
    data.append('_token', getToken());
    ajax('/urussetia/projek/laporan/get-penilaian', data, 0);
});
