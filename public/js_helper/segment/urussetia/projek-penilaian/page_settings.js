select2PencarianPengguna('.projek-penilaian-penilai');
timePicker('.projek-penilaian-tkh-tapak');
timePicker('.projek-penilaian-tkh-asal');
timePicker('.projek-penilaian-tkh-semasa');
timePicker('.projek-penilaian-tkh-penilaian');
function select2PencarianPengguna(className){
    $(className).wrap('<div class="position-relative"></div>').select2({
        dropdownAutoWidth: true,
        dropdownParent: $(className).parent(),
        width: '100%',
        language: {
            inputTooShort: function(){
                return 'Sekurang-kurangnya mengisi satu huruf...';
            },
            searching: function(){
                return 'Sedang Mencari Pengguna...';
            }
        },
        ajax: {
            url: getUrl() + '/common/pengguna/carian',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                let parseData = data.data;
                return {
                    results: parseData,
                    pagination: {
                        more: params.page * 30 < parseData.length
                    }
                };
            },
            cache: true
        },
        placeholder: 'Sila Isi Nama Pengguna',
        minimumInputLength: 1,
    });
}


function timePicker(className){
    $(className).flatpickr({dateFormat: "d-m-Y"},);
}

$('.select3').each(function () {
    var $this = $(this);
    $this.wrap('<div class="position-relative"></div>');
    $this.select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        dropdownAutoWidth: true,
        width: '100%',
        dropdownParent: $this.parent()
    });
});