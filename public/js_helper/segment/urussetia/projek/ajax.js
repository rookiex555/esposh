function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.projek-modal').modal('hide');
                    $('.projek-table').DataTable().ajax.reload(null, false);
                    toasting('Projek Ditambah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Projek Sudah Wujud', 'error');
                }
            }else if(postfunc == 1){
                if(success == 1){
                    let projek_id = parseData.projek_id;
                    if(parseData.flag == 0){
                        $('.projek-row[data-projek-id='+ projek_id +']').find('.projek-aktif').removeClass('btn-outline-success').addClass('btn-outline-danger');
                    }else{
                        $('.projek-row[data-projek-id='+ projek_id +']').find('.projek-aktif').removeClass('btn-outline-danger').addClass('btn-outline-success');
                    }
                    $.unblockUI();
                }
            }else if(postfunc == 2){
                if(success == 1){
                    $('.projek-nama').val(parseData.nama);
                    $('.projek-modal').modal('show');
                    $.unblockUI();
                }
            }else if(postfunc == 3){
                if(success == 1){
                    $('.projek-modal').modal('hide');
                    $('.projek-table').DataTable().ajax.reload(null, false);
                    toasting('Projek Telah Dikemaskini', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Projek Sudah Wujud', 'error');
                }
            }
        }
    });
}
