$(document).on('click', '.add-projek, .view-projek, .projek-update, .projek-tahunan', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('add-projek')){
        postEmptyFields([
            ['.projek-nama', 'text'],
        ]);
        $('.post-add-projek').attr('style', 'display:block');
        $('.post-update-projek').attr('style', 'display:none');
        $('.projek-modal').modal('show');
        $('.modal-title').html('Tambah Projek');
    }else if(selectedClass.hasClass('projek-update')){
        let projek_id = selectedClass.closest('tr').attr('data-projek-id');
        $('.projek-id').val(projek_id);
        $('.post-add-projek').attr('style', 'display:none');
        $('.post-update-projek').attr('style', 'display:block');

        let data = new FormData;
        data.append('projek_id', projek_id);
        data.append('_token', getToken());
        ajax('/urussetia/projek/get-projek', data, 2);
        $('.modal-title').html('Kemaskini Projek');
    }else if(selectedClass.hasClass('projek-tahunan')){
        let projek_id = selectedClass.closest('tr').attr('data-projek-id');
        window.location.href = getUrl() + '/urussetia/projek-tahunan/tahun/' + projek_id;
    }
});

$(document).on('click', '.post-add-projek, .post-update-projek', function(){
    let projek_nama = $('.projek-nama').val();
    let selectedClass  = $(this);
    let check = checkEmptyFields([
        ['.projek-nama', projek_nama, 'mix', 'Nama'],
    ]);

    if(check == false){
        return false;
    }

    let data = new FormData;
    data.append('projek_nama', projek_nama);
    data.append('_token', getToken());
    let trigger;
    let url;
    if(selectedClass.hasClass('post-add-projek')){
        data.append('trigger', 0);
        url = '/urussetia/projek/tambah';
        trigger = 0;
    }else{
        data.append('projek_id', $('.projek-id').val());
        data.append('trigger', 1);
        url = '/urussetia/projek/update';
        trigger = 3;
    }
    ajax(url, data, trigger);
});

$(document).on('click', '.projek-delete, .projek-aktif', function(){
    let selectedClass = $(this);
    let projek_id = selectedClass.closest('tr').attr('data-projek-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('projek_id', projek_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('projek-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Projek Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/urussetia/projek/delete',
                data: data,
                postfunc: 0
            }
        });
    }else if(selectedClass.hasClass('projek-aktif')){
        trigger = 1;
        url = '/urussetia/projek/aktif';
        ajax(url, data, trigger);
    }
});
