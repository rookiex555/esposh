$('.projek-tahunan-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: getUrl() + '/urussetia/projek-tahunan/list',
    lengthChange:true,
    columns: [
        { data: 'tahun' },
        // { data: 'jumlah_penilaian' },
        { data: 'action' },
    ],
    createdRow: function( row, data, dataIndex ) {
        $(row).addClass('projek-tahunan-row');
    },
    columnDefs: [
        {
            targets: -1,
            title: 'Tindakan',
            orderable: false,
            render: function (data, type, full, meta) {
                return (
                    '<button type="button" class="btn btn-icon btn-outline-info mr-1 mb-1 waves-effect waves-light projek-tahunan-penilaian" data-toggle="tooltip" data-placement="top" title="Senarai Penilaian">'+ feather.icons['list'].toSvg() +'</button>'
                );
            }
        }
    ],
    dom:
        '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    lengthMenu: [7, 10, 25, 50, 75, 100],
    buttons: [
        {
            text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Tambah Projek',
            className: 'create-new btn btn-primary add-projek',
            attr: {
                'data-toggle': 'modal',
                'data-target': '#modals-slide-in'
            },
            init: function (api, node, config) {
                $(node).removeClass('btn-secondary');
            }
        }
    ],
    language: {
        paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
        }
    }
});
