function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    if(parseData.save_trigger == 0){
                        swalPostFire('info', 'Draft Telah Disimpan');
                    }
                    $.unblockUI();
                }
            }else if(postfunc == 1){
                let upload_id = parseData.id;
                $('.padam-gambar-one-preview[data-id='+ upload_id +']').attr('style', 'display:none').closest('.form-group').find('.upload-preview').attr('style', 'display:none');
            }else if(postfunc == 2){
                let upload_id = parseData.id;
                $('.padam-gambar-two-preview[data-id='+ upload_id +']').attr('style', 'display:none').closest('.form-group').find('.upload-preview').attr('style', 'display:none');
            }else if(postfunc == 3){
                let upload_id = parseData.id;
                $('.padam-gambar-three-preview[data-id='+ upload_id +']').attr('style', 'display:none').closest('.form-group').find('.upload-preview').attr('style', 'display:none');
            }else if(postfunc == 4){
                let upload_id = parseData.id;
                $('.padam-gambar-four-preview[data-id='+ upload_id +']').attr('style', 'display:none').closest('.form-group').find('.upload-preview').attr('style', 'display:none');
            }else if(postfunc == 5){
                let data = new FormData();

                data.append('projek_tahun_id', $('.projek-tahun-id').val());
                data.append('penilai_id', $('.penilai-id').val());
                data.append('_token', getToken());

                ajax('/kpa/penilaian/jawab-get', data, 6);
            }else if(postfunc == 6){
                Object.values(parseData.pen_score).forEach(function(val){
                    $('.sector-get[data-id='+ val.id +']').html(val.score);
                })

                $('#overallScore').html(parseData.totalOverall);
            }
        }
    });
}
