var penilai_id = $('.penilai-id').val();
var projek_tahun_id = $('.projek-tahun-id').val();
var total_soalan = 0;

$('.perkara-soalan-row').each(function(){
    total_soalan++;
});

$(document).on('click', '.post-penilaian-draft-send, .post-penilaian-actual-send', function(){
    let save_trigger = $(this).attr('data-save');
    runSave(save_trigger);
});

$(document).on('click', '.scoreCheck',function(){
    runSave(0, false);
})

$(document).on('click', '.padam-gambar-one-preview', function(){
    let upload_id = $(this).attr('data-id');
    let data = new FormData;
    data.append('_token', getToken());
    data.append('upload_id', upload_id);

    ajax('/kpa/penilaian/padam-preview', data, 1);
})

$(document).on('click', '.padam-gambar-two-preview', function(){
    let upload_id = $(this).attr('data-id');
    let data = new FormData;
    data.append('_token', getToken());
    data.append('upload_id', upload_id);
    
    ajax('/kpa/penilaian/padam-preview', data, 2);
})

$(document).on('click', '.padam-gambar-three-preview', function(){
    let upload_id = $(this).attr('data-id');
    let data = new FormData;
    data.append('_token', getToken());
    data.append('upload_id', upload_id);
    
    ajax('/kpa/penilaian/padam-preview', data, 3);
})

$(document).on('click', '.padam-gambar-four-preview', function(){
    let upload_id = $(this).attr('data-id');
    let data = new FormData;
    data.append('_token', getToken());
    data.append('upload_id', upload_id);
    
    ajax('/kpa/penilaian/padam-preview', data, 4);
})

function runSave(save_trigger = 0, normal = true){
    let answerArr = [];
    let pass = 0;
    var passSkor = 0;
    let data = new FormData;

    // $.blockUI();
    $('.perkara-penilaian-row').each(function(){
        let selectedClass = $(this);
        let perkara_id = selectedClass.attr('data-id');

        let perkara_row_input_image_one = selectedClass.find('.perkara-row-input-image-one')[0].files[0];
        let perkara_row_input_image_two = selectedClass.find('.perkara-row-input-image-two')[0].files[0];
        let perkara_row_input_image_three = selectedClass.find('.perkara-row-input-image-three')[0].files[0];
        let perkara_row_input_image_four = selectedClass.find('.perkara-row-input-image-four')[0].files[0];
        let perkara_row_input_catatan = selectedClass.closest('tr').find('.perkara-row-input-catatan').val();

        let rowPassSkor = 0;
        let skor = '';
        $('.perkara-row-input-skor-'+ perkara_id +'').each(function(){
            if($(this).is(':checked')){
                passSkor++;
                rowPassSkor = 1;
                skor =  $(this).closest('.perkara-check-col').attr('data-skor');
            }
        });

        if(save_trigger == 1 && skor != 'tb'){

            let check = checkEmptyFields([
                // ['.perkara-row-input-image-one[data-perkara-id='+ perkara_id +']', perkara_row_input_image_one, 'picture', 'Dokumen JPG'],
                // ['.perkara-row-input-image-two[data-perkara-id='+ perkara_id +']', perkara_row_input_image_two, 'picture', 'Dokumen JPG'],
                // ['.perkara-row-input-image-three[data-perkara-id='+ perkara_id +']', perkara_row_input_image_three, 'picture', 'Dokumen JPG'],
                // ['.perkara-row-input-image-four[data-perkara-id='+ perkara_id +']', perkara_row_input_image_four, 'picture', 'Dokumen PDF'],
                ['.perkara-row-input-catatan[data-perkara-id='+ perkara_id +']', perkara_row_input_catatan, 'mix', 'Catatan'],
            ]);

            if(check == false){
                pass = 1
            }

            if(rowPassSkor == 0){
                $('.perkara-row-input-skor-'+ perkara_id +'').each(function(){
                    $(this).closest('.form-group').find('.invalid-feedback').html('Skor Tiada').attr('style', 'color:red;display:block');
                });
            }else{
                $('.perkara-row-input-skor-'+ perkara_id +'').each(function(){
                    $(this).closest('.form-group').find('.invalid-feedback').html('').attr('style', 'color:red;display:none');
                });
            }

            // let checkfirstFourPic = checkPicData([
            //     [perkara_row_input_image_one, '.perkara-row-input-image-one[data-perkara-id='+ perkara_id +']', 'jpg'],
            //     [perkara_row_input_image_four, '.perkara-row-input-image-four[data-perkara-id='+ perkara_id +']', 'pdf'],
            // ]);

        }else{
            // let checkfirstFourPic = checkPicData([
            //     [perkara_row_input_image_one, '.perkara-row-input-image-one[data-perkara-id='+ perkara_id +']', 'jpg'],
            //     [perkara_row_input_image_four, '.perkara-row-input-image-four[data-perkara-id='+ perkara_id +']', 'pdf'],
            // ]);

        }

        if(typeof perkara_row_input_image_one != 'undefined'){
            if(!checkPicData([
                [perkara_row_input_image_one, '.perkara-row-input-image-one[data-perkara-id='+ perkara_id +']', ['jpg', 'jpeg' ,'png']],
            ])){
                pass = 1;
            };
        }

        if(typeof perkara_row_input_image_two != 'undefined'){
            if(!checkPicData([
                [perkara_row_input_image_two, '.perkara-row-input-image-two[data-perkara-id='+ perkara_id +']', ['jpg', 'jpeg' ,'png']],
            ])){
                pass = 1;
            };
        }

        if(typeof perkara_row_input_image_three != 'undefined'){
            if(!checkPicData([
                [perkara_row_input_image_three, '.perkara-row-input-image-three[data-perkara-id='+ perkara_id +']', ['jpg', 'jpeg' ,'png']],
            ])){
                pass = 1;
            };
        }

        if(typeof perkara_row_input_image_four != 'undefined'){
            if(!checkPicData([
                [perkara_row_input_image_four, '.perkara-row-input-image-four[data-perkara-id='+ perkara_id +']', 'pdf'],
            ])){
                pass = 1;
            };
        }

        data.append('img_1_perkara_id_'+ perkara_id +'', perkara_row_input_image_one);
        data.append('img_2_perkara_id_'+ perkara_id +'', perkara_row_input_image_two);
        data.append('img_3_perkara_id_'+ perkara_id +'', perkara_row_input_image_three);
        data.append('img_4_perkara_id_'+ perkara_id +'', perkara_row_input_image_four);

        let ans = {
            'catatan' : perkara_row_input_catatan,
            'skor': skor,
            'perkara_id': perkara_id
        };
        answerArr.push(ans);
    });
    // return false;
    if(save_trigger == 1){
        if(pass == 1 || passSkor < total_soalan){
            toasting('Sila Jawab Semua Yang Diperlukan', 'error');
            // $.unblockUI();
            return false;
        }
    }else{
        if(pass == 1){
            toasting('Sila Jawab Semua Yang Diperlukan', 'error');
            // $.unblockUI();
            return false;
        }
    }
    // alert(pass);
    // return false;
    data.append('answerArr', JSON.stringify(answerArr));
    data.append('penilai_id', penilai_id);
    data.append('projek_tahun_id', projek_tahun_id);
    data.append('save_trigger', save_trigger);
    data.append('_token', getToken());

    console.log(normal);
    if(save_trigger == 0){
        if(normal == true){
            ajax('/kpa/penilaian/jawab', data, 0);
        }else{
            ajax('/kpa/penilaian/jawab', data, 5);
        }

    }else{
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Anda Tidak Boleh Kemaskini Selepas Menghantar',
            icon: 'warning',
            confirmButtonText: 'Hantar',
            postData: {
                url : '/kpa/penilaian/jawab',
                data: data,
                postfunc: 0
            }
        });
    }
}



