$(document).ready(function(){
    $('.toggle-main-button').trigger('click');

    btnDraf = document.getElementById("btnDraf");
    btnHantar = document.getElementById("btnHantar");

    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        btnDraf.style.display = "block";
        btnHantar.style.display = "block";
        } else {
            btnDraf.style.display = "none";
            btnHantar.style.display = "none";
        }
    }
});

$(document).on('change', '.perkara-row-input-image-one', function(){
    $(this).closest('.form-group').find('.padam-gambar-one').attr('style', 'text-decoration:underline');
})

$(document).on('click', '.padam-gambar-one', function(){
    $(this).closest('.form-group').find('.perkara-row-input-image-one').val('').closest('.custom-file').find('.custom-file-label').html('Pilih Fail');
    $(this).closest('.form-group').find('.padam-gambar-one').attr('style', 'display:none');
})

$(document).on('change', '.perkara-row-input-image-two', function(){
    $(this).closest('.form-group').find('.padam-gambar-two').attr('style', 'text-decoration:underline');
})

$(document).on('click', '.padam-gambar-two', function(){
    $(this).closest('.form-group').find('.perkara-row-input-image-two').val('').closest('.custom-file').find('.custom-file-label').html('Pilih Fail');
    $(this).closest('.form-group').find('.padam-gambar-two').attr('style', 'display:none');
})

$(document).on('change', '.perkara-row-input-image-three', function(){
    $(this).closest('.form-group').find('.padam-gambar-three').attr('style', 'text-decoration:underline');
})

$(document).on('click', '.padam-gambar-three', function(){
    $(this).closest('.form-group').find('.perkara-row-input-image-three').val('').closest('.custom-file').find('.custom-file-label').html('Pilih Fail');
    $(this).closest('.form-group').find('.padam-gambar-three').attr('style', 'display:none');
})

$(document).on('change', '.perkara-row-input-image-four', function(){
    $(this).closest('.form-group').find('.padam-gambar-four').attr('style', 'text-decoration:underline');
})

$(document).on('click', '.padam-gambar-four', function(){
    $(this).closest('.form-group').find('.perkara-row-input-image-four').val('').closest('.custom-file').find('.custom-file-label').html('Pilih Fail');
    $(this).closest('.form-group').find('.padam-gambar-four').attr('style', 'display:none');
})


