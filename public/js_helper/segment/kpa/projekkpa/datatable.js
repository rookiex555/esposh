$('.projek-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: getUrl() + '/kpa/penilaian/list',
    lengthChange:true,
    columns: [
        { data: 'projek_name' },
        { data: 'tahun' },
        { data: 'tarikh_penilaian' },
        { data: 'status_penilaian' },
        { data: 'action' },
    ],
    createdRow: function( row, data, dataIndex ) {
        $(row).addClass('projek-row');
    },
    columnDefs: [
        {
            // Actions
            targets: -1,
            title: 'Tindakan',
            orderable: false,
            render: function (data, type, full, meta) {
                if(full.status_penilaian == 'SELESAI'){
                    return (
                        // '<button type="button" class="btn btn-icon btn-outline-success mr-1 mb-1 waves-effect waves-light penilaian-review" data-toggle="tooltip" data-placement="top" title="Preview Penilaian">'+ feather.icons['list'].toSvg() +'</button>' +

                        '<button type="button" class="btn btn-icon btn-outline-success mr-1 mb-1 waves-effect waves-light penilaian-preview-selesai" data-toggle="tooltip" data-placement="top" title="Preview Keputusan">'+ feather.icons['book-open'].toSvg() +'</button>' +
                        '<button type="button" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light penilaian-print" data-toggle="tooltip" data-placement="top" title="Cetak Keputusan (PDF)">'+ feather.icons['printer'].toSvg() +'</button>'
                    );
                }else{
                    return (
                        '<button type="button" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light penilaian-ubah-tkh" data-toggle="tooltip" data-placement="top" title="Ubah Tarikh Penilaian">'+ feather.icons['rewind'].toSvg() +'</button>' +
                        '<button type="button" class="btn btn-icon btn-outline-info mr-1 mb-1 waves-effect waves-light penilaian-jawab" data-toggle="tooltip" data-placement="top" title="Jawab Soalan">'+ feather.icons['list'].toSvg() +'</button>'
                    );
                }
            }
        }
    ],
    dom:
        '<"card-header border-bottom p-1"<"head-label">><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    lengthMenu: [7, 10, 25, 50, 75, 100],
    buttons: [,
        {
            text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Tambah Projek',
            className: 'create-new btn btn-primary add-projek',
            attr: {
                'data-toggle': 'modal',
                'data-target': '#modals-slide-in'
            },
            init: function (api, node, config) {
                $(node).removeClass('btn-secondary');
            }
        }
    ],
    language: {
        paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
        }
    }
});
