function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.penilaian-date-modal').modal('hide');
                    $('.tkh-penilaian').val('');
                    $('.projek-table').DataTable().ajax.reload(null, false);
                    toasting('Tarikh Penilaian Diubah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Projek Sudah Wujud', 'error');
                }
            }
        }
    });
}
