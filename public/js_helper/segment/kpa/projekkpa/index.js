$(document).on('click', '.penilaian-jawab', function(){
    let projek_tahun_id = $(this).closest('tr').attr('data-projek-tahun-id');
    let penilai_id = $(this).closest('tr').attr('data-penilai-id');

    window.location.href = getUrl() + '/kpa/penilaian/kpa-get-all-soalan' + '/' + projek_tahun_id + '/' + penilai_id;
});

$(document).on('click', '.penilaian-review', function(){
    let projek_tahun_id = $(this).closest('tr').attr('data-projek-tahun-id');

    window.location.href = getUrl() + '/urussetia/projek-penilaian/review/' + projek_tahun_id;
});

$(document).on('click', '.penilaian-ubah-tkh', function(){
    $('.projek-penilaian-id').val($(this).closest('tr').attr('data-projek-tahun-id'));
    $('.penilaian-date-modal').modal('show');
});

$(document).on('click', '.post-update-tkh-penilaian', function(){
    let projek_tahun_id = $('.projek-penilaian-id').val();
    let tkh_select = $('.tkh-penilaian').val();
    let check = checkEmptyFields([
        ['.tkh-penilaian', tkh_select, 'datedashstandard', 'Tarikh Penilaian'],
    ]);

    if(check == false){
        return false;
    }

    let data = new FormData;
    data.append('projek_tahun_id', projek_tahun_id);
    data.append('tkh_penilaian', tkh_select);
    data.append('_token', getToken());

    ajax('/kpa/penilaian/ubah-tkh', data, 0);
});
