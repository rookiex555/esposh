function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.jawatan-wakil-modal').modal('hide');
                    $('.jawatan-wakil-table').DataTable().ajax.reload(null, false);
                    toasting('Jawatan Ditambah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Jawatan Sudah Wujud', 'error');
                }else if(success == 3){
                    $('.jawatan-wakil-modal').modal('hide');
                    $('.jawatan-wakil-table').DataTable().ajax.reload(null, false);
                    toasting('Jawatan Dikemaskini', 'warning');
                }
                $.unblockUI();
            }else if(postfunc == 1){
                if(success == 1){
                    $('.jawatan-wakil-nama').val(parseData.name);
                    $('.jawatan-wakil-modal').modal('show');
                }
            }
        }
    });
}
