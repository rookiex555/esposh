$(document).on('click', '.add-jawatan-wakil, .jawatan-wakil-update', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('add-jawatan-wakil')){
        postEmptyFields([
            ['.jawatan-wakil-nama', 'text'],
        ]);
        $('.post-add-jawatan-wakil').attr('style', '');
        $('.post-update-jawatan-wakil').attr('style', 'display:none');
        $('.jawatan-wakil-modal').modal('show');
        $('.modal-title').html('Tambah Jawatan Wakil');
    }else if(selectedClass.hasClass('jawatan-wakil-update')){
        let jawatan_wakil_id = selectedClass.closest('tr').attr('data-jawatan-wakil-id');
        $('.jawatan-wakil-id').val(jawatan_wakil_id);
        $('.post-add-jawatan-wakil').attr('style', 'display:none');
        $('.post-update-jawatan-wakil').attr('style', '');

        let data = new FormData;
        data.append('jawatan_wakil_id', jawatan_wakil_id);
        data.append('_token', getToken());
        ajax('/admin/pentadbir/tetapan/jawatan-wakil/get-jawatan-wakil', data, 1);
        $('.modal-title').html('Kemaskini Jawatan Wakil');
    }
});

$(document).on('click', '.post-add-jawatan-wakil, .post-update-jawatan-wakil', function(){
    let jawatan_wakil_nama = $('.jawatan-wakil-nama').val();
    let trigger;
    let check = checkEmptyFields([
        ['.jawatan-wakil-nama', jawatan_wakil_nama, 'mix', 'Nama'],
    ]);

    if(check == false){
        return false;
    }

    if($(this).hasClass('post-add-jawatan-wakil')){
        trigger = 0;
    }else{
        trigger = 1;
    }

    let data = new FormData;
    data.append('jawatan_wakil_nama', jawatan_wakil_nama);
    data.append('jawatan_wakil_id', $('.jawatan-wakil-id').val());
    data.append('trigger', trigger);
    data.append('_token', getToken());

    ajax('/admin/pentadbir/tetapan/jawatan-wakil/tambah', data, 0);
});

$(document).on('click', '.jawatan-wakil-delete', function(){
    let selectedClass = $(this);
    let jawatan_wakil_id = selectedClass.closest('tr').attr('data-jawatan-wakil-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('jawatan_wakil_id', jawatan_wakil_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('jawatan-wakil-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Jawatan Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/admin/pentadbir/tetapan/jawatan-wakil/delete',
                data: data,
                postfunc: 0
            }
        });
    }
});
