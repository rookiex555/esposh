function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.jawatan-wakil-cawangan-modal').modal('hide');
                    $('.jawatan-wakil-cawangan-table').DataTable().ajax.reload(null, false);
                    toasting('Cawangan Wakil Ditambah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Cawangan Sudah Wujud', 'error');
                }
                $.unblockUI();
            }else if(postfunc == 1){
                if(success == 1){
                    $('.jawatan-wakil-cawangan-nama').val(parseData.name);
                    $('.jawatan-wakil-cawangan-negeri').val(parseData.negeri).trigger('change');
                    $('.jawatan-wakil-cawangan-modal').modal('show');
                }
            }else if(postfunc == 2){
                if(success == 1){
                    if(data.trigger == 0){
                        toasting('Pejabat Ditambah', 'success');
                    }else{
                        toasting('Pejabat Dikemaskini', 'warning');
                    }
                    $('.jawatan-wakil-cawangan-pejabat-table').DataTable().ajax.reload(null, false);

                    $.unblockUI();
                }else if(success == 2){
                    toasting('Pejabat Untuk Negeri Ini Sudah Wujud', 'error');
                }
                $.unblockUI();
            }else if(postfunc == 3){
                if(success == 1){
                    $('.jawatan-wakil-cawangan-pejabat-nama').val(parseData.name);

                    $('.add-cawangan-pejabat-button').attr('style', 'display:none');
                    $('.add-new-cawangan-pejabat-button').attr('style', '');
                    $('.update-cawangan-pejabat-button').attr('style', '');
                }
                $.unblockUI();
            }
        }
    });
}
