//Cawangan
$(document).on('click', '.add-jawatan-wakil-cawangan, .jawatan-wakil-cawangan-update, .jawatan-wakil-cawangan-pejabat-list, .jawatan-wakil-cawangan-pejabat-update, .post-add-jawatan-wakil-cawangan-pejabat-baru', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('add-jawatan-wakil-cawangan')){
        postEmptyFields([
            ['.jawatan-wakil-cawangan-nama', 'text'],
            ['.jawatan-wakil-cawangan-negeri', 'text'],
        ]);
        $('.post-add-jawatan-wakil-cawangan').attr('style', '');
        $('.post-update-jawatan-wakil-cawangan').attr('style', 'display:none');
        $('.jawatan-wakil-cawangan-modal').modal('show');
        $('.modal-title').html('Tambah Pejabat Wakil');
    }else if(selectedClass.hasClass('jawatan-wakil-cawangan-update')){
        let jawatan_wakil_cawangan_id = selectedClass.closest('tr').attr('data-jawatan-wakil-cawangan-id');
        $('.jawatan-wakil-cawangan-id').val(jawatan_wakil_cawangan_id);
        $('.post-add-jawatan-wakil-cawangan').attr('style', 'display:none');
        $('.post-update-jawatan-wakil-cawangan').attr('style', '');

        let data = new FormData;
        data.append('jawatan_wakil_cawangan_id', jawatan_wakil_cawangan_id);
        data.append('_token', getToken());
        ajax('/admin/pentadbir/tetapan/jawatan-wakil/cawangan/get-cawangan', data, 1);
        $('.modal-title').html('Kemaskini Pejabat Wakil');
    }else if(selectedClass.hasClass('jawatan-wakil-cawangan-pejabat-list')){
        let jawatan_wakil_cawangan_id = $(this).closest('tr').attr('data-jawatan-wakil-cawangan-id');
        $('.jawatan-wakil-cawangan-id').val(jawatan_wakil_cawangan_id);
        pejabat_table(jawatan_wakil_cawangan_id);
        $('.add-cawangan-pejabat-button').attr('style', '');
        $('.update-cawangan-pejabat-button').attr('style', 'display:none');
        $('.add-new-cawangan-pejabat-button').attr('style', 'display:none');
        $('.jawatan-wakil-cawangan-pejabat-modal').modal('show');
    }else if(selectedClass.hasClass('jawatan-wakil-cawangan-pejabat-update')){
        postEmptyFields([
            ['.jawatan-wakil-cawangan-pejabat-nama', 'textarea'],
        ]);
        let jawatan_wakil_cawangan_pejabat_id = $(this).closest('tr').attr('data-jawatan-wakil-cawangan-pejabat-id');
        $('.jawatan-wakil-cawangan-pejabat-id').val(jawatan_wakil_cawangan_pejabat_id);

        let data = new FormData;
        data.append('jawatan_wakil_cawangan_pejabat_id', jawatan_wakil_cawangan_pejabat_id);
        data.append('_token', getToken());
        ajax('/admin/pentadbir/tetapan/jawatan-wakil/cawangan/pejabat/get-pejabat-wakil', data, 3);
    }else if(selectedClass.hasClass('post-add-jawatan-wakil-cawangan-pejabat-baru')){
        postEmptyFields([
            ['.jawatan-wakil-cawangan-pejabat-nama', 'textarea'],
        ]);
        $('.add-cawangan-pejabat-button').attr('style', '');
        $('.update-cawangan-pejabat-button').attr('style', 'display:none');
        $('.add-new-cawangan-pejabat-button').attr('style', 'display:none');
    }
});

$(document).on('click', '.post-add-jawatan-wakil-cawangan, .post-update-jawatan-wakil-cawangan', function(){
    let jawatan_wakil_cawangan_nama = $('.jawatan-wakil-cawangan-nama').val();
    let jawatan_wakil_cawangan_negeri = $('.jawatan-wakil-cawangan-negeri').val();

    let trigger;
    let check = checkEmptyFields([
        ['.jawatan-wakil-cawangan-nama', jawatan_wakil_cawangan_nama, 'mix', 'Nama'],
    ]);

    if(check == false){
        return false;
    }

    if($(this).hasClass('post-add-jawatan-wakil-cawangan')){
        trigger = 0;
    }else{
        trigger = 1;
    }

    let data = new FormData;
    data.append('jawatan_wakil_cawangan_nama', jawatan_wakil_cawangan_nama);
    data.append('jawatan_wakil_cawangan_negeri', jawatan_wakil_cawangan_negeri);
    data.append('jawatan_wakil_cawangan_id', $('.jawatan-wakil-cawangan-id').val());
    data.append('trigger', trigger);
    data.append('_token', getToken());

    ajax('/admin/pentadbir/tetapan/jawatan-wakil/cawangan/tambah', data, 0);
});

$(document).on('click', '.jawatan-wakil-cawangan-delete', function(){
    let selectedClass = $(this);
    let jawatan_wakil_cawangan_id = selectedClass.closest('tr').attr('data-jawatan-wakil-cawangan-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('jawatan_wakil_cawangan_id', jawatan_wakil_cawangan_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('jawatan-wakil-cawangan-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Cawangan Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/admin/pentadbir/tetapan/jawatan-wakil/cawangan/delete',
                data: data,
                postfunc: 0
            }
        });
    }else if(selectedClass.hasClass('pengguna-aktif')){
        data.append('trigger', 1);
        trigger = 2;
        url = '/admin/pentadbir/pengguna/aktif';
        ajax(url, data, trigger);
    }
});

//Pejabat
$(document).on('click', '.post-add-jawatan-wakil-cawangan-pejabat, .post-update-jawatan-wakil-cawangan-pejabat', function(){
    let jawatan_wakil_cawangan_pejabat_nama = $('.jawatan-wakil-cawangan-pejabat-nama').val();
    let jawatan_wakil_cawangan_id = $('.jawatan-wakil-cawangan-id').val();

    let trigger;
    let check = checkEmptyFields([
        ['.jawatan-wakil-cawangan-pejabat-nama', jawatan_wakil_cawangan_pejabat_nama, 'mix', 'Nama'],
    ]);

    if(check == false){
        return false;
    }

    if($(this).hasClass('post-add-jawatan-wakil-cawangan-pejabat')){
        trigger = 0;
    }else{
        trigger = 1;
    }

    let data = new FormData;
    data.append('jawatan_wakil_cawangan_pejabat_nama', jawatan_wakil_cawangan_pejabat_nama);
    data.append('jawatan_wakil_cawangan_pejabat_id', $('.jawatan-wakil-cawangan-pejabat-id').val());
    data.append('jawatan_wakil_cawangan_id', jawatan_wakil_cawangan_id);
    data.append('trigger', trigger);
    data.append('_token', getToken());

    ajax('/admin/pentadbir/tetapan/jawatan-wakil/cawangan/pejabat/tambah', data, 2);
});

$(document).on('click', '.jawatan-wakil-cawangan-pejabat-delete', function(){
    let selectedClass = $(this);
    let jawatan_wakil_cawangan_pejabat_id = selectedClass.closest('tr').attr('data-jawatan-wakil-cawangan-pejabat-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('jawatan_wakil_cawangan_pejabat_id', jawatan_wakil_cawangan_pejabat_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('jawatan-wakil-cawangan-pejabat-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Pejabat Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/admin/pentadbir/tetapan/jawatan-wakil/cawangan/pejabat/delete',
                data: data,
                postfunc: 1
            }
        });
    }
});
