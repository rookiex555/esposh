$(document).on('click', '.add-projek, .projek-update', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('add-projek')){
        postEmptyFields([
            ['.projek-nama', 'text'],
        ]);
        $('.post-add-projek').attr('style', '');
        $('.post-update-projek').attr('style', 'display:none');
        $('.projek-modal').modal('show');
        $('.modal-title').html('Tambah Projek');
    }else if(selectedClass.hasClass('projek-update')){
        let projek_id = selectedClass.closest('tr').attr('data-projek-id');
        $('.projek-id').val(projek_id);
        $('.post-add-projek').attr('style', 'display:none');
        $('.post-update-projek').attr('style', '');

        let data = new FormData;
        data.append('projek_id', projek_id);
        data.append('_token', getToken());
        ajax('/admin/pentadbir/tetapan/projek/get-projek', data, 1);
        $('.modal-title').html('Kemaskini Urus Setia');
    }
});

$(document).on('click', '.post-add-projek, .post-update-projek', function(){
    let projek_nama = $('.projek-nama').val();
    let trigger;
    let check = checkEmptyFields([
        ['.projek-nama', projek_nama, 'mix', 'Nama'],
    ]);

    if(check == false){
        return false;
    }

    if($(this).hasClass('post-add-projek')){
        trigger = 0;
    }else{
        trigger = 1;
    }

    let data = new FormData;
    data.append('projek_nama', projek_nama);
    data.append('projek_id', $('.projek-id').val());
    data.append('trigger', trigger);
    data.append('_token', getToken());

    ajax('/admin/pentadbir/tetapan/projek/tambah', data, 0);
});

$(document).on('click', '.projek-delete', function(){
    let selectedClass = $(this);
    let projek_id = selectedClass.closest('tr').attr('data-projek-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('projek_id', projek_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('projek-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Projek Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/admin/pentadbir/tetapan/projek/delete',
                data: data,
                postfunc: 0
            }
        });
    }
});
