function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.jawatan-modal').modal('hide');
                    $('.jawatan-table').DataTable().ajax.reload(null, false);
                    toasting('Jawatan Ditambah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Jawatan Sudah Wujud', 'error');
                }
                $.unblockUI();
            }else if(postfunc == 1){
                if(success == 1){
                    $('.jawatan-nama').val(parseData.name);
                    $('.jawatan-modal').modal('show');
                }
            }else if(postfunc == 2){
                if(success == 1){
                    if(data.trigger == 0){
                        toasting('Alamat Ditambah', 'success');
                    }else{
                        toasting('Alamat Dikemaskini', 'warning');
                    }
                    $('.jawatan-alamat-table').DataTable().ajax.reload(null, false);

                    $.unblockUI();
                }else if(success == 2){
                    toasting('Alamat Sudah Wujud', 'error');
                }
                $.unblockUI();
            }else if(postfunc == 3){
                if(success == 1){
                    $('.jawatan-alamat-satu').val(parseData.alamat_satu);
                    $('.jawatan-alamat-dua').val(parseData.alamat_dua);
                    $('.jawatan-alamat-poskod').val(parseData.poskod);
                    $('.jawatan-alamat-negeri').val(parseData.negeri);

                    $('.add-alamat-button').attr('style', 'display:none');
                    $('.add-new-alamat-button').attr('style', '');
                    $('.update-alamat-button').attr('style', '');
                }
                $.unblockUI();
            }
        }
    });
}
