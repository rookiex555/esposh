function ajax(url, data, postfunc){
    // $.blockUI();
    $.ajax({
        type:'POST',
        url: getUrl() + url,
        data:data,
        dataType: "json",
        processData: false,
        contentType: false,
        context: this,
        success: function(data) {
            let success = data.success;
            let parseData = data.data;

            if(postfunc == 0){
                if(success == 1){
                    $('.projek-modal').modal('hide');
                    $('.projek-table').DataTable().ajax.reload(null, false);
                    toasting('Projek Ditambah', 'success');
                    $.unblockUI();
                }else if(success == 2){
                    toasting('Projek Sudah Wujud', 'error');
                }
                $.unblockUI();
            }else if(postfunc == 1){
                if(success == 1){
                    $('.projek-nama').val(parseData.name);
                    $('.projek-modal').modal('show');
                }
            }
        }
    });
}
