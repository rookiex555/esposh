$(document).on('click', '.add-jawatan, .jawatan-update, .jawatan-alamat-list, .jawatan-alamat-update, .post-add-jawatan-alamat-baru', function(){
    let selectedClass = $(this);
    if(selectedClass.hasClass('add-jawatan')){
        postEmptyFields([
            ['.jawatan-nama', 'text'],
        ]);
        $('.post-add-jawatan').attr('style', '');
        $('.post-update-jawatan').attr('style', 'display:none');
        $('.jawatan-modal').modal('show');
    }else if(selectedClass.hasClass('jawatan-update')){
        let jawatan_id = selectedClass.closest('tr').attr('data-jawatan-pejabat-id');
        $('.jawatan-id').val(jawatan_id);
        $('.post-add-jawatan').attr('style', 'display:none');
        $('.post-update-jawatan').attr('style', '');

        let data = new FormData;
        data.append('jawatan_id', jawatan_id);
        data.append('_token', getToken());
        ajax('/admin/pentadbir/tetapan/jawatan/get-jawatan', data, 1);
    }else if(selectedClass.hasClass('jawatan-alamat-list')){
        let jawatan_id = $(this).closest('tr').attr('data-jawatan-pejabat-id');
        $('.jawatan-id').val(jawatan_id);
        alamat_table(jawatan_id);
        $('.add-alamat-button').attr('style', '');
        $('.update-alamat-button').attr('style', 'display:none');
        $('.add-new-alamat-button').attr('style', 'display:none');
        $('.jawatan-alamat-modal').modal('show');
    }else if(selectedClass.hasClass('jawatan-alamat-update')){
        postEmptyFields([
            ['.jawatan-alamat-satu', 'textarea'],
            ['.jawatan-alamat-dua', 'textarea'],
            ['.jawatan-alamat-poskod', 'text'],
            ['.jawatan-alamat-negeri', 'dropdown'],
        ]);
        let jawatan_alamat_id = $(this).closest('tr').attr('data-jawatan-pejabat-alamat-id');
        $('.jawatan-alamat-id').val(jawatan_alamat_id);

        let data = new FormData;
        data.append('jawatan_alamat_id', jawatan_alamat_id);
        data.append('_token', getToken());
        ajax('/admin/pentadbir/tetapan/jawatan/alamat/get-alamat', data, 3);
    }else if(selectedClass.hasClass('post-add-jawatan-alamat-baru')){
        postEmptyFields([
            ['.jawatan-alamat-satu', 'textarea'],
            ['.jawatan-alamat-dua', 'textarea'],
            ['.jawatan-alamat-poskod', 'text'],
            ['.jawatan-alamat-negeri', 'dropdown'],
        ]);
        $('.add-alamat-button').attr('style', '');
        $('.update-alamat-button').attr('style', 'display:none');
        $('.add-new-alamat-button').attr('style', 'display:none');
    }
});

$(document).on('click', '.post-add-jawatan, .post-update-jawatan', function(){
    let jawatan_nama = $('.jawatan-nama').val();
    let trigger;
    let check = checkEmptyFields([
        ['.jawatan-nama', jawatan_nama, 'mix', 'Nama'],
    ]);

    if(check == false){
        return false;
    }

    if($(this).hasClass('post-add-jawatan')){
        trigger = 0;
    }else{
        trigger = 1;
    }

    let data = new FormData;
    data.append('jawatan_nama', jawatan_nama);
    data.append('jawatan_id', $('.jawatan-id').val());
    data.append('trigger', trigger);
    data.append('_token', getToken());

    ajax('/admin/pentadbir/tetapan/jawatan/tambah', data, 0);
});

$(document).on('click', '.jawatan-delete', function(){
    let selectedClass = $(this);
    let jawatan_pejabat_id = selectedClass.closest('tr').attr('data-jawatan-pejabat-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('jawatan_pejabat_id', jawatan_pejabat_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('jawatan-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Jawatan Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/admin/pentadbir/tetapan/jawatan/delete',
                data: data,
                postfunc: 0
            }
        });
    }else if(selectedClass.hasClass('pengguna-aktif')){
        data.append('trigger', 1);
        trigger = 2;
        url = '/admin/pentadbir/pengguna/aktif';
        ajax(url, data, trigger);
    }
});

$(document).on('click', '.post-add-jawatan-alamat, .post-update-jawatan-alamat', function(){
    let jawatan_alamat_satu = $('.jawatan-alamat-satu').val();
    let jawatan_alamat_dua = $('.jawatan-alamat-dua').val();
    let jawatan_alamat_poskod = $('.jawatan-alamat-poskod').val();
    let jawatan_alamat_negeri = $('.jawatan-alamat-negeri').val();
    let jawatan_id = $('.jawatan-id').val();

    let trigger;
    let check = checkEmptyFields([
        ['.jawatan-alamat-satu', jawatan_alamat_satu, 'mix', 'Alamat'],
        ['.jawatan-alamat-poskod', jawatan_alamat_poskod, 'int', 'Poskod'],
        ['.jawatan-alamat-negeri', jawatan_alamat_negeri, 'int', 'Negeri'],
    ]);

    if(check == false){
        return false;
    }

    if($(this).hasClass('post-add-jawatan-alamat')){
        trigger = 0;
    }else{
        trigger = 1;
    }

    let data = new FormData;
    data.append('jawatan_alamat_satu', jawatan_alamat_satu);
    data.append('jawatan_alamat_dua', jawatan_alamat_dua);
    data.append('jawatan_alamat_poskod', jawatan_alamat_poskod);
    data.append('jawatan_alamat_negeri', jawatan_alamat_negeri);
    data.append('jawatan_alamat_id', $('.jawatan-alamat-id').val());
    data.append('jawatan_id', jawatan_id);
    data.append('trigger', trigger);
    data.append('_token', getToken());

    ajax('/admin/pentadbir/tetapan/jawatan/alamat/tambah', data, 2);
});

$(document).on('click', '.jawatan-alamat-delete', function(){
    let selectedClass = $(this);
    let jawatan_pejabat_alamat_id = selectedClass.closest('tr').attr('data-jawatan-pejabat-alamat-id');
    let trigger;
    let url;

    let data = new FormData;
    data.append('jawatan_pejabat_alamat_id', jawatan_pejabat_alamat_id);
    data.append('_token', getToken());

    if(selectedClass.hasClass('jawatan-alamat-delete')){
        swalAjax({
            titleText : 'Adakah Anda Pasti?',
            mainText : 'Alamat Akan Dipadam',
            icon: 'error',
            confirmButtonText: 'Padam',
            postData: {
                url : '/admin/pentadbir/tetapan/jawatan/alamat/delete',
                data: data,
                postfunc: 1
            }
        });
    }
});
