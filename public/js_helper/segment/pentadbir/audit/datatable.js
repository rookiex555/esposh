$('.audit-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: getUrl() + '/admin/pentadbir/audit/list',
    lengthChange:true,
    columns: [
        { data: 'username' },
        { data: 'created_at' },
        { data: 'model' },
        { data: 'old_val' },
        { data: 'new_val' },
    ],
    columnDefs: [
        {
            targets: 0,
            render: function ( data, type, row ) {
                return data.length > 20 ?
                data.substr( 0, 20 ) +'…' :
                data;
            }
        },
        {
            targets: 3,
            render: function ( data, type, row ) {
                return data.length > 30 ?
                data.substr( 0, 30 ) +'…' :
                data;
            }
        },
        {
            targets: 4,
            render: function ( data, type, row ) {
                return data.length > 30 ?
                data.substr( 0, 30 ) +'…' :
                data;
            }
        },
    ],
    dom:
        '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    lengthMenu: [7, 10, 25, 50, 75, 100],
    buttons: [,
        // {
        //     text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Tambah Urus Setia',
        //     className: 'create-new btn btn-primary add-pengguna',
        //     attr: {
        //         'data-toggle': 'modal',
        //         'data-target': '#modals-slide-in'
        //     },
        //     init: function (api, node, config) {
        //         $(node).removeClass('btn-secondary');
        //     }
        // }
    ],
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal({
                header: function (row) {
                    var data = row.data();
                    return 'Details of ' + data['full_name'];
                }
            }),
            type: 'column',
            renderer: function (api, rowIdx, columns) {
                var data = $.map(columns, function (col, i) {
                    return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                        ? '<tr data-dt-row="' +
                        col.rowIndex +
                        '" data-dt-column="' +
                        col.columnIndex +
                        '">' +
                        '<td>' +
                        col.title +
                        ':' +
                        '</td> ' +
                        '<td>' +
                        col.data +
                        '</td>' +
                        '</tr>'
                        : '';
                }).join('');

                return data ? $('<table class="table"/>').append(data) : false;
            }
        }
    },
    language: {
        paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
        }
    }
});
