<table style="font-family: 'Montserrat',Arial,sans-serif; width: 100%;" width="100%" cellpadding="0" cellspacing="0" role="presentation">
    <tr>
        <td class="sm-px-24" style="--bg-opacity: 1; background-color: #ffffff; background-color: rgba(255, 255, 255, var(--bg-opacity)); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; font-size: 14px; line-height: 24px; padding: 48px; text-align: left; --text-opacity: 1; color: #626262; color: rgba(98, 98, 98, var(--text-opacity));" bgcolor="rgba(255, 255, 255, var(--bg-opacity))" align="left">
            <p style="font-weight: 600; font-size: 18px; margin-bottom: 0;">Assalamualaikum Wbt dan Salam Sejahtera</p><br>
            <p style="margin: 0 0 10px;">
                YBhg. Datuk / Dato’/ Tuan / Puan,
            </p>
            <br>
            <p style="font-weight: 700; font-size: 20px; margin-top: 0; --text-opacity: 1; color: #ff5850; color: rgba(255, 88, 80, var(--text-opacity));">PELANTIKAN SEBAGAI KETUA PENILAI AUDIT BAGI AUDIT DALAM SPKKP PROJEK JKR MALAYSIA</p>
            <p style="">
                Dengan segala hormatnya merujuk perkara di atas.
            </p>
            <table style="font-family: 'Montserrat',Arial,sans-serif; width: 100%;" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                <tr>
                    <td style="font-family: 'Montserrat',Arial,sans-serif; padding-top: 0px; padding-bottom: 10px;">
                        <div style="--bg-opacity: 1; background-color: #eceff1; background-color: rgba(236, 239, 241, var(--bg-opacity)); height: 1px; line-height: 1px;">&zwnj;</div>
                    </td>
                </tr>
            </table>
            <p class="sm-leading-32" style="font-weight: 600; font-size: 20px; margin: 0 0 0px; --text-opacity: 1; color: #263238; color: rgba(38, 50, 56, var(--text-opacity));">
            </p>
            <p style="margin: 0 0 16px;">
                2. &emsp; Tahniah diucapkan kepada YBhg. Datuk / Dato’ / Tuan / Puan kerana telah dilantik sebagai Ketua Penilai Audit bagi projek {{$nama}}. YBhg. Datuk / Dato’ / Tuan / Puan juga boleh mengakses Sistem EA SPOSH di laman web SPB atau melalui pautan ini {{url('/esposh')}} dengan menggunakan ID Single Sign On (SSO).
            </p>
            <p style="margin: 0 0 16px;">
                3. &emsp; Semoga dengan pelantikan ini, segala amanah dan tanggungjawab yang digalas dapat dilaksanakan dengan cemerlang. Sehubungan itu, kerjasama dan prihatian YBhg. Datuk / Dato’ / Tuan / Puan amat dihargai.
            </p>
            <p style="margin: 0 0 16px;">Sekian, Terima Kasih</p>
            <p style="margin: 0 0 16px;" style="font-weight:bold">“WAWASAN KEMAKMURAN BERSAMA 2030”</p>
            <p style="margin: 0 0 16px;" style="font-weight:bold">“BERKHIDMAT UNTUK NEGARA”</p>

            <p style="margin: 0 0 5px;" style="font-weight:bold">Saya yang menjalankan amanah,</p>
            <br>
            <p style="margin: 0 0 2px;" style="font-weight:bold">Urus Setia EA-SPOSH</p>
            <p style="margin: 0 0 0px;" style="font-weight:bold">Unit Keselamatan dan Kesihatan<br>Bahagian Pengurusan Kualiti<br>Cawangan Dasar dan Pengurusan Korporat <br>JKR Malaysia</p>
        </td>
    </tr>
    <tr>
        <td style="font-family: 'Montserrat',Arial,sans-serif; height: 20px;" height="20"></td>
    </tr>
</table>