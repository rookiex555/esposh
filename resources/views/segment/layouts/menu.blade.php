<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{url('/')}}"><span class="brand-logo">
                <img src="{{url('images/jkrlogo.png')}}">
                            </span>
                    <h2 class="brand-text" style="color: yellow">EASPOSH</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle toggle-main-button pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @role('Pentadbir')
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Pengguna</span><i data-feather="more-horizontal"></i>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/pengguna"><i data-feather="users"></i><span class="menu-title text-truncate" data-i18n="Email">Urus Setia</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/pengguna/kpa"><i data-feather="users"></i><span class="menu-title text-truncate" data-i18n="Email">KPA</span></a>
            </li>

            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Audit</span><i data-feather="more-horizontal"></i>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/audit"><i data-feather="users"></i><span class="menu-title text-truncate" data-i18n="Email">Laporan Audit Trail</span></a>
            </li>

            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Tetapan</span><i data-feather="more-horizontal"></i>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/tetapan/projek"><i data-feather="align-center"></i><span class="menu-title text-truncate" data-i18n="Email">Projek</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/tetapan/jawatan"><i data-feather="briefcase"></i><span class="menu-title text-truncate" data-i18n="Email">Jawatan Pengarah</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/tetapan/jawatan/pejabat"><i data-feather="cloud-lightning"></i><span class="menu-title text-truncate" data-i18n="Email">Pejabat Pengarah</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/tetapan/jawatan-wakil"><i data-feather="cpu"></i><span class="menu-title text-truncate" data-i18n="Email">Jawatan Wakil</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/admin/pentadbir/tetapan/jawatan-wakil/cawangan"><i data-feather="figma"></i><span class="menu-title text-truncate" data-i18n="Email">Pejabat Wakil</span></a>
            </li>
            @endrole
            @role('Urussetia')
                @if(Auth::user()->user_profile->urussetia_active == 1)
                    <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Urus Setia</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/urussetia/projek"><i data-feather="hard-drive"></i><span class="menu-title text-truncate" data-i18n="Email">Projek</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/urussetia/projek/laporan"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Email">Laporan</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/urussetia/projek/laporan/keseluruhan"><i data-feather="folder"></i><span class="menu-title text-truncate" data-i18n="Email">Laporan Keseluruhan</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/urussetia/projek/laporan/pejabat"><i data-feather="folder"></i><span class="menu-title text-truncate" data-i18n="Email">Laporan Pejabat</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/urussetia/projek/laporan/analisis"><i data-feather="folder"></i><span class="menu-title text-truncate" data-i18n="Email">Analisis</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/urussetia/projek/laporan/status-penilaian"><i data-feather="folder"></i><span class="menu-title text-truncate" data-i18n="Email">Status Penilaian</span></a>
                    </li>
                @endif
            @endrole
            @role('KPA')
                @if(Auth::user()->user_profile->kpa_active == 1)
                    <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">KPA</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{Request::root()}}/kpa/penilaian"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Email">Penilaian</span></a>
                    </li>
                @endif
            @endrole
        </ul>
    </div>
</div>
