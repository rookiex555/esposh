<tr class="perkara-soalan-row">
    <td>{{$perkara['bil']}}</td>
    <td>{{$perkara['no_klausa']}}</td>
    <td>{{$perkara['tajuk_perkara']}}</td>
    <td class="perkara-penilaian-row" data-id="{{$perkara['id']}}">
        <div class="form-group">
            <label for="customFile">Gambar 1 (JPG,JPEG,PNG)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-one" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-one" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            @if(count($perkara['jawapan']['uploads']) > 0)
                <div class="with-preview"><a class="upload-preview" target="_blank" href="{{$perkara['jawapan']['uploads']['img1'][0]}}">{{$perkara['jawapan']['uploads']['img1'][1]}}</a></div>
                @if($perkara['jawapan']['uploads']['img1'][1])
                    <a class="padam-gambar-one-preview" style="text-decoration:underline" data-id="{{$perkara['jawapan']['uploads']['img1'][2]}}">Padam Preview</a>
                @endif
            @endif
        </div>
        <div class="form-group">
            <label for="customFile">Gambar 2 (JPG,JPEG,PNG)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-two" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-two" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            @if(count($perkara['jawapan']['uploads']) > 0)
                <div class="with-preview"><a class="upload-preview" target="_blank" href="{{$perkara['jawapan']['uploads']['img2'][0]}}">{{$perkara['jawapan']['uploads']['img2'][1]}}</a></div>
                @if($perkara['jawapan']['uploads']['img2'][1])
                    <a class="padam-gambar-two-preview" style="text-decoration:underline" data-id="{{$perkara['jawapan']['uploads']['img2'][2]}}">Padam Preview</a>
                @endif
                <div></div>
            @endif
        </div>
        <div class="form-group">
            <label for="customFile">Gambar 3 (JPG,JPEG,PNG)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-three" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-three" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            @if(count($perkara['jawapan']['uploads']) > 0)
                <div class="with-preview"><a class="upload-preview" target="_blank" href="{{$perkara['jawapan']['uploads']['img3'][0]}}">{{$perkara['jawapan']['uploads']['img3'][1]}}</a></div>
                @if($perkara['jawapan']['uploads']['img3'][1])
                    <a class="padam-gambar-three-preview" style="text-decoration:underline" data-id="{{$perkara['jawapan']['uploads']['img3'][2]}}">Padam Preview</a>
                @endif
            @endif
        </div>
        <div class="form-group">
            <label for="customFile">Dokumen (PDF)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-four" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-four" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            @if(count($perkara['jawapan']['uploads']) > 0)
                <div class="with-preview"><a class="upload-preview" target="_blank" href="{{$perkara['jawapan']['uploads']['img4'][0]}}">{{$perkara['jawapan']['uploads']['img4'][1]}}</a></div>
                @if($perkara['jawapan']['uploads']['img4'][1])
                    <a class="padam-gambar-four-preview" style="text-decoration:underline" data-id="{{$perkara['jawapan']['uploads']['img4'][2]}}">Padam Preview</a>
                @endif
            @endif
        </div>
    </td>
    <td>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Catatan</label>
            <textarea class="form-control perkara-row-input-catatan" id="exampleFormControlTextarea1" rows="10" placeholder="Catatan" data-perkara-id="{{$perkara['id']}}">{{$perkara['jawapan']['catatan']}}</textarea>
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="0">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" {{$perkara['jawapan']['skor'] == 0 && $perkara['jawapan']['skor'] != null? 'checked' : ''}}/>
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="1">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" {{$perkara['jawapan']['skor'] == 1 ? 'checked' : ''}}/>
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="2">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" {{$perkara['jawapan']['skor'] == 2 ? 'checked' : ''}}/>
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="tb">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" {{$perkara['jawapan']['skor'] == 'tb' ? 'checked' : ''}}/>
            <div class="invalid-feedback"></div>
        </div>
    </td>
</tr>
