<tr class="perkara-soalan-row">
    <td>{{$perkara['bil']}}</td>
    <td>{{$perkara['no_klausa']}}</td>
    <td>{{$perkara['tajuk_perkara']}}</td>
    <td class="perkara-penilaian-row" data-id="{{$perkara['id']}}">
        <div class="form-group">
            <label for="customFile">Gambar 1 (JPG,JPEG,PNG)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-one" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-one" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            <div class="with-preview"><a class="upload-preview" target="_blank" href=""></a></div>
            <div></div>
        </div>
        <div class="form-group">
            <label for="customFile">Gambar 2 (JPG,JPEG,PNG)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-two" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-two" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            <div class="with-preview"><a class="upload-preview" target="_blank" href="{{Request::root()}}"></a></div>
            <div></div>
            <div></div>
        </div>
        <div class="form-group">
            <label for="customFile">Gambar 3 (JPG,JPEG,PNG)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-three" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-three" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            <div class="with-preview"><a class="upload-preview" target="_blank" href="{{Request::root()}}"></a></div>
            <div></div>
        </div>
        <div class="form-group">
            <label for="customFile">Dokumen (PDF)</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input perkara-row-input-image-four" id="customFile" data-perkara-id="{{$perkara['id']}}" value=""/>
                <label class="custom-file-label" for="customFile">Pilih Fail</label>
            </div>
            <a class="padam-gambar-four" style="display:none">Padam</a>
            <div class="invalid-feedback"></div>
            <div class="with-preview"><a class="upload-preview" target="_blank" href="{{Request::root()}}"></a></div>
            <div></div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Catatan</label>
            <textarea class="form-control perkara-row-input-catatan" id="exampleFormControlTextarea1" rows="10" placeholder="Catatan" data-perkara-id="{{$perkara['id']}}"></textarea>
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="0">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" />
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="1">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" />
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="2">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" />
            <div class="invalid-feedback"></div>
        </div>
    </td>
    <td>
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="tb">
            <input class="form-check-input perkara-row-input-skor-{{$perkara['id']}} scoreCheck" type="radio" name="perkara-row-input-skor-{{$perkara['id']}}" id="" value="option1" />
            <div class="invalid-feedback"></div>
        </div>
    </td>
</tr>
