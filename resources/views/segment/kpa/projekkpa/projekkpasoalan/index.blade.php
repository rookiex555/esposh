@extends('segment.layouts.main')

@section('customCSS')
<style>
    .centerCell {
        text-align: center;
        vertical-align: middle !important;
        font-weight: bold !important;
    }

    table, th, td {
        border: 1px solid black;
    }

    #btnDraf {
        display: none; /* Hidden by default */
        position: fixed; /* Fixed/sticky position */
        bottom: 20px; /* Place the button at the bottom of the page */
        right: 200px; /* Place the button 30px from the right */
        z-index: 99; /* Make sure it does not overlap */
        border: none; /* Remove borders */
        outline: none; /* Remove outline */
        background-color: red; /* Set a background color */
        color: white; /* Text color */
        cursor: pointer; /* Add a mouse pointer on hover */
        padding: 15px; /* Some padding */
        border-radius: 10px; /* Rounded corners */
        font-size: 18px; /* Increase font size */
    }

    #btnDraf:hover {
        background-color: #555; /* Add a dark-grey background on hover */
    }

    #btnHantar {
        display: none; /* Hidden by default */
        position: fixed; /* Fixed/sticky position */
        bottom: 20px; /* Place the button at the bottom of the page */
        right: 100px; /* Place the button 30px from the right */
        z-index: 99; /* Make sure it does not overlap */
        border: none; /* Remove borders */
        outline: none; /* Remove outline */
        background-color: red; /* Set a background color */
        color: white; /* Text color */
        cursor: pointer; /* Add a mouse pointer on hover */
        padding: 15px; /* Some padding */
        border-radius: 10px; /* Rounded corners */
        font-size: 18px; /* Increase font size */
    }

    #btnHantar:hover {
        background-color: #555; /* Add a dark-grey background on hover */
    }

    .rightCell {
        text-align: right;
        vertical-align: middle !important;
        font-weight: bold !important;
    }

    .leftCell {
        text-align: left;
        vertical-align: middle !important;
        font-weight: bold !important;
    }

    img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
</style>
@include('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css')
@endsection

@section('content')
<button id="btnDraf" class="btn btn-info post-penilaian-draft-send" data-save="0">Simpan</button>
<button id="btnHantar" class="btn btn-success post-penilaian-actual-send" data-save="1">Hantar</button>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">KPA</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Penilaian</a>
                                </li>
                                <li class="breadcrumb-item active">Senarai
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr style="font-weight: bold;text-align:center">
                                        <td colspan="2">Petunjuk Skor</td>
                                    </tr>
                                    <tr style="font-weight: bold">
                                        <td>Skala Markah</td>
                                        <td>Status</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="background-color: red;color:black">
                                        <td>0</td>
                                        <td>Tiada Bukti Pelaksanaan</td>
                                    </tr>
                                    <tr style="background-color: yellow;color:black">
                                        <td>1</td>
                                        <td>Dilaksana Dan Perlu Dipertingkatkan</td>
                                    </tr>
                                    <tr style="background-color: green;color:black">
                                        <td>2</td>
                                        <td>Dilaksana Dengan Cemerlang</td>
                                    </tr>
                                    <tr style="background-color: black;color:white">
                                        <td>TB</td>
                                        <td>Tidak Berkaitan</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @foreach($data['penilaian'] as $penilaian)
            <section id="basic-datatable main-penilaian-row" data-id="{{$penilaian['id']}}" data-peratus="{{$penilaian['peratus']}}">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{$penilaian['nama']}}</h4>
                            </div>
                            <div class="card-body">
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr style="text-align: center">
                                            <th class="centerCell" rowspan="2">BIL</th>
                                            <th class="centerCell" rowspan="2">KLAUSA</th>
                                            <th class="centerCell" rowspan="2" style="width:20%">PERKARA</th>
                                            <th class="centerCell" rowspan="2">PENEMUAN / BUKTI BERGAMBAR</th>
                                            <th class="centerCell" rowspan="2">CATATAN</th>
                                            <th colspan="4">SKOR</th>
                                        </tr>
                                        <tr style="">
                                            <th class="centerCell" style="background-color: red;color:black">0</th>
                                            <th class="centerCell" style="background-color: yellow;color:black">1</th>
                                            <th class="centerCell" style="background-color: green;color:black">2</th>
                                            <th class="centerCell" style="background-color: black;color:white">TB</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr style="background-color: lightyellow;font-weight: bold">
                                            <td>{{$penilaian['bil']}}</td>
                                            <td></td>
                                            <td colspan="7" style="text-decoration: underline;">{{$penilaian['nama']}} ({{$penilaian['peratus']}}%)</td>
                                        </tr>
                                        @foreach($penilaian['sub_pen'] as $sub_pen)
                                        <tr style="background-color: lightgrey;font-weight: bold">
                                            <td>{{$sub_pen['bil']}}</td>
                                            <td>{{$sub_pen['no_klausa']}}</td>
                                            <td colspan="7">{{$sub_pen['tajuk_perkara']}}</td>
                                        </tr>
                                            @foreach($sub_pen['perkara'] as $perkara)
                                                @if(empty($perkara['jawapan']))
                                                    @include('segment.kpa.projekkpa.projekkpasoalan.noJawapan')
                                                @else
                                                    @include('segment.kpa.projekkpa.projekkpasoalan.withJawapan')
                                                @endif
                                            @endforeach
                                        @endforeach
                                        <tr style="background-color: rgb(97, 255, 253)">
                                            <td colspan="5" style="text-align: right;font-weight: bold">Markah {{ ucwords(strtolower($penilaian['nama'])) }}:</td>
                                            <td colspan="5" style="text-align: center; font-weight: bold">(<span class="sector-get" data-id="{{$penilaian['id']}}">{{ $data['pen_score'][$penilaian['id']]['score'] }}</span> / {{ $data['pen_score'][$penilaian['id']]['full_score'] }}) % </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endforeach
            <section id="basic-datatable main-penilaian-row">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tr style="background-color: rgb(125,240,115)">
                                    <td class="leftCell" colspan="1" style="text-align: right;width:960px">Markah Keseluruhan</td>
                                    <td class="rightCell" rowspan="2" style="text-align: center">(<span id="overallScore">{{ $data['totalOverall'] }}</span> / 100) %</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
    <input type="hidden" class="penilai-id" value="{{$penilai_id}}">
    <input type="hidden" class="projek-tahun-id" value="{{$projek_tahun_id}}">
    <input type="hidden" class="total-soalan" value="">
@endsection

@section('customJS')
    @include('segment.layouts.asset_include_links.common.js.common_js')
    @include('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js')

    {{--  Custom files  --}}
    <script src="{{ asset('js_helper/segment/kpa/projekkpasoalan/swal.js') }}"></script>
    <script src="{{ asset('js_helper/segment/kpa/projekkpasoalan/page_settings.js') }}"></script>
    <script src="{{ asset('js_helper/segment/kpa/projekkpasoalan/ajax.js') }}"></script>
    <script src="{{ asset('js_helper/segment/kpa/projekkpasoalan/datatable.js') }}"></script>
    <script src="{{ asset('js_helper/segment/kpa/projekkpasoalan/index.js') }}"></script>
@endsection
