@extends('segment.layouts.main')

@section('customCSS')
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/app-assets/css/plugins/charts/chart-apex.css') }}">
    @include('segment.layouts.asset_include_links.c3.css.c3_css')
@endsection

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Ecommerce Starts -->
                <section id="dashboard-ecommerce">
                    <div class="row match-height">
                        <div class="col-lg-12 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-sm-row flex-column justify-content-md-between align-items-start justify-content-start">
                                    <div >
                                        <h4 class="card-title">Penilaian Selesai/Tidak Selesai</h4>
                                    </div>
                                </div>
                                <div id="penilaian-selesai"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('customJS')
    @include('segment.layouts.asset_include_links.c3.js.c3_js')
@endsection

@section('customJSDashboard')
    <script>
        getGraph(0, 'pie', '#penilaian-selesai')
        function getGraph(trigger, chartType, className){
            data = new FormData;
            data.append('_token', getToken());
            data.append('trigger', trigger);

            $.ajax({
                type:'POST',
                url: getUrl() + '/dashboard/get-chart-data-selesai',
                data:data,
                dataType: "json",
                processData: false,
                contentType: false,
                context: this,
                success: function(data) {
                    if(chartType == 'pie'){
                        pieChart(data['data'], className);
                    }
                }
            });
        }

        function pieChart(data, className){
            // bindto: '#pichart',
            var chart = c3.generate({
                bindto: className,
                data: {
                    // iris data from R
                    columns: data,
                    type : 'pie',
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                pie: {
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }
                },
                tooltip: {
                    format: {
                        value: function (value, ratio, id) {
                            return value;
                        }
                    }
                }
            });
        }
    </script>
@endsection

