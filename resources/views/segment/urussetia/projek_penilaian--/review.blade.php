@extends('segment.layouts.main')

@section('customCSS')
    <style>
        .centerCell {
            text-align: center;
            vertical-align: middle !important;
            font-weight: bold !important;
        }

        table, th, td {
            border: 1px solid black;
        }

        div.a {
            text-align: center;
            }
    </style>
    @include('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css')
@endsection

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">KPA</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Penilaian</a>
                                    </li>
                                    <li class="breadcrumb-item active">Senarai
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row mb-2">
                    <div class="col-md-4">
                        <a class="btn btn-warning" href="{{ url()->previous() }}">Kembali</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Maklumat Penilaian</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        Nama Projek: {{$data['projek']['nama']}}
                                    </div>
                                    <div class="col-md-12">
                                        Tahun: {{$data['projek']['tahun']}}
                                    </div>
                                    <div class="col-md-4">
                                        Tarikh Milik: {{$data['projek_tahun']['tkh_milik']}}
                                    </div>
                                    <div class="col-md-4">
                                        Tarikh Milik Tapak: {{$data['projek_tahun']['tkh_milik_tpk']}}
                                    </div>
                                    <div class="col-md-4">
                                        Tarikh Siap Asal: {{$data['projek_tahun']['tkh_siap_asal']}}
                                    </div>
                                    <div class="col-md-4">
                                        Tarikh Siap Semasa: {{$data['projek_tahun']['tkh_siap_semasa']}}
                                    </div>
                                    <div class="col-md-4">
                                        Tarikh Penilaian: {{$data['projek_tahun']['tkh_penilaian']}}
                                    </div>
                                    <div class="col-md-4">
                                        No Klausa: {{$data['projek_tahun']['no_klausa']}}
                                    </div>
                                    <div class="col-md-4">
                                        Kemajuan Fizikal: {{$data['projek_tahun']['kemajuan_fizikal']}}
                                    </div>
                                    <div class="col-md-12">
                                        Penilai: {{$data['projek_tahun']['penilai']['nama']}} ({{$data['projek_tahun']['penilai']['nokp']}})
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @foreach($data['soalan_jawapan'] as $penilaian)
                    <section id="basic-datatable main-penilaian-row">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{$penilaian['nama']}}</h4>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr style="text-align: center">
                                                <th class="centerCell" rowspan="2">BIL</th>
                                                <th class="centerCell" rowspan="2">KLAUSA <br>{{$data['projek_tahun']['no_klausa']}}</th>
                                                <th class="centerCell" rowspan="2" style="width:20%">PERKARA</th>
                                                <th class="centerCell" rowspan="2">PENEMUAN / BUKTI BERGAMBAR</th>
                                                <th class="centerCell" rowspan="2">CATATAN</th>
                                                <th colspan="4">SKOR</th>
                                            </tr>
                                            <tr style="">
                                                <th class="centerCell" style="background-color: red;color:black">0</th>
                                                <th class="centerCell" style="background-color: yellow;color:black">1</th>
                                                <th class="centerCell" style="background-color: green;color:black">2</th>
                                                <th class="centerCell" style="background-color: black;color:white">TB</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr style="background-color: lightyellow;font-weight: bold">
                                                <td>A</td>
                                                <td></td>
                                                <td colspan="6" style="text-decoration: underline;">{{$penilaian['nama']}} ({{$penilaian['peratus']}})</td>
                                            </tr>
                                            @foreach($penilaian['sub'] as $sub_pen)
                                                <tr style="background-color: lightgrey;font-weight: bold">
                                                    <td>{{$sub_pen['bil']}}</td>
                                                    <td>{{$sub_pen['no_klausa']}}</td>
                                                    <td colspan="6">{{$sub_pen['nama']}}</td>
                                                </tr>
                                                @foreach($sub_pen['jawapan_skor'] as $perkara)
                                                    @include('segment.urussetia.projek_penilaian.review_jawapan')
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('customJS')
    @include('segment.layouts.asset_include_links.common.js.common_js')
    @include('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js')

@endsection
