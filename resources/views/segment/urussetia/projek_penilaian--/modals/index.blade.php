<div class="modal fade text-left modal-primary projek-penilaian-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">No. Klausa</label>
                            <input type="text" class="form-control projek-penilaian-no-klausa" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Pengarah Projek / Pegawai Penguasa</label>
                             <select class="projek-penilaian-pengarah form-control " id="select2-ajax"></select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Wakil Pengarah Projek / Wakil Pegawai Penguasa</label>
                             <select class="projek-penilaian-wakil form-control" id="select24-ajax"></select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Milik Tapak</label>
                            <input type="text" class="form-control projek-penilaian-tkh-tapak" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Milik Asal</label>
                            <input type="text" class="form-control projek-penilaian-tkh-asal" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Siap Semasa</label>
                            <input type="text" class="form-control projek-penilaian-tkh-semasa" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Penilaian</label>
                            <input type="text" class="form-control projek-penilaian-tkh-penilaian" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Kemajuan Fizikal Semasa Penilaian (%)</label>
                            <input type="text" class="form-control projek-penilaian-kemajuan-fizikal" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Penilai(KPA)</label>
                            <select class="projek-penilaian-penilai form-control" id="select25-ajax"></select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success post-add-projek-penilaian">Tambah</button>
                <button type="button" class="btn btn-warning post-update-projek-penilaian">Kemaskini</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="projek-penilaian-id" value="">

