<div class="modal fade text-left modal-primary projek-penilaian-soalan-main-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Bil</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-bil" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Nama</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-nama" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Peratus Penilaian (%)</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-peratus" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success post-add-projek-penilaian-soalan-main">Tambah</button>
                <button type="button" class="btn btn-warning post-update-projek-penilaian-soalan-main">Kemaskini</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="projek-penilaian-soalan-main-id" value="">

<div class="modal fade text-left modal-primary projek-penilaian-soalan-main-sub-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Bil.</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-sub-bilangan" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">No. Klausa</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-sub-no-klausa" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Perkara</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-sub-perkara" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12 mb-1 main-sub-tambah-row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success post-add-projek-penilaian-soalan-main-sub" style="width: 100%">Tambah</button>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12 mb-1 row main-sub-update-row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-warning post-update-projek-penilaian-soalan-main-sub" style="width: 100%">Kemaskini</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-danger post-cancel-projek-penilaian-soalan-main-sub" style="width: 100%">Cancel</button>
                        </div>
                    </div>
                </div>
                <hr>
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <table class="projek-penilaian-soalan-main-sub-table table">
                                    <thead>
                                    <tr>
                                        <th>Tajuk</th>
                                        <th>Bilangan</th>
                                        <th>Aktif</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="projek-penilaian-soalan-main-sub-id" value="">

<div class="modal fade text-left modal-primary projek-penilaian-soalan-main-sub-perkara-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Bil.</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-sub-perkara-bilangan" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">No. Klausa</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-sub-perkara-no-klausa" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Soalan</label>
                            <input type="text" class="form-control projek-penilaian-soalan-main-sub-perkara-soalan" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success post-add-projek-penilaian-soalan-main-sub-perkara">Tambah</button>
                <button type="button" class="btn btn-warning post-update-projek-penilaian-soalan-main-sub-perkara">Kemaskini</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="projek-penilaian-soalan-main-current-id" value="">
<input type="hidden" class="projek-penilaian-soalan-main-sub-current-id" value="">

<input type="hidden" class="projek-penilaian-soalan-main-sub-perkara-id" value="">

<div class="modal fade text-left modal-primary projek-penilaian-soalan-duplicate-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Duplicate Penilaian Yang Telah Selesai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Penilaian</label>
                            <select class="form-control projek-penilaian-soalan-duplicate-pilihan">
                                <option value="">Sila Pilih</option>
                                @if(count($projekdropdown))
                                    @foreach($projekdropdown as $pd)
                                        <option value="{{$pd->projek_id}}/{{$pd->projek_tahun_id}}">{{$pd->projek}}/{{$pd->tahun}}/Penilai: {{$pd->nama}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success post-projek-penilaian-soalan-duplicate">Duplicate</button>
            </div>
        </div>
    </div>
</div>