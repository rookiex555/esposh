<?php
$fileName = $data['projek']['nama'].'-'.$data['projek']['tahun'] . ".xls";

// Headers for download
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>

<style>
    .centerCell {
        text-align: center;
        vertical-align: middle !important;
        font-weight: bold !important;
    }

    table, th, td {
        border: 1px solid black;
    }

    div.a {
            text-align: center;
    }
</style>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" style="text-decoration: underline">Maklumat Audit Dalam SPKKP Projek</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    Nama Projek: {{$data['projek']['nama']}}
                                </div>
                                <div class="col-md-12">
                                    Tahun: {{$data['projek']['tahun']}}
                                </div>
                                <table>
                                    <tr>
                                        <td>Tarikh Milik: {{$data['projek_tahun']['tkh_milik']}}</td>
                                        <td>Tarikh Siap Asal: <br>{{$data['projek_tahun']['tkh_siap_asal']}}</td>
                                        <td>Tarikh Siap Semasa: <br>{{$data['projek_tahun']['tkh_siap_semasa']}}</td>
                                        <td>Tarikh Milik Tapak: <br>{{$data['projek_tahun']['tkh_milik_tpk']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tarikh Milik: <br>{{$data['projek_tahun']['tkh_milik']}}</td>
                                        <td>Tarikh Siap Asal: <br>{{$data['projek_tahun']['tkh_siap_asal']}}</td>
                                        <td>Tarikh Siap Semasa: <br>{{$data['projek_tahun']['tkh_siap_semasa']}}</td>
                                        <td>Tarikh Penilaian: <br>{{$data['projek_tahun']['tkh_penilaian']}}</td>
                                    </tr>
                                    <tr>
                                        <td>No Klausa: <br>{{$data['projek_tahun']['no_klausa']}}</td>
                                        <td>Kemajuan Fizikal: <br>{{$data['projek_tahun']['kemajuan_fizikal']}}</td>
                                        <td colspan="2">
                                            Penilai: {{$data['projek_tahun']['penilai']['nama']}}
                                            NOKP: ({{$data['projek_tahun']['penilai']['nokp']}})</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(isset($data['soalan_jawapan']))
                <?php $pass = 1; ?>
                @foreach($data['soalan_jawapan'] as $penilaian)
                    <section id="basic-datatable main-penilaian-row">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title" style="text-decoration: underline">{{$penilaian['nama']}}</h4>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table" style="width: 100% !important;">
{{--                                            <thead>--}}
                                            @if($pass == 1)
                                                <tr style="text-align: center">
                                                    <th class="centerCell" rowspan="2">BIL</th>
                                                    <th class="centerCell" rowspan="2">KLAUSA <br>{{$data['projek_tahun']['no_klausa']}}</th>
                                                    <th class="centerCell" rowspan="2" style="width:20%">PERKARA</th>
                                                    <th class="centerCell" rowspan="2">PENEMUAN / BUKTI BERGAMBAR</th>
                                                    <th class="centerCell" rowspan="2">CATATAN</th>
                                                    <th colspan="4">SKOR</th>
                                                </tr>
                                                <tr style="">
                                                    <th class="centerCell" style="background-color: red;color:black">0</th>
                                                    <th class="centerCell" style="background-color: yellow;color:black">1</th>
                                                    <th class="centerCell" style="background-color: green;color:black">2</th>
                                                    <th class="centerCell" style="background-color: black;color:white">TB</th>
                                                </tr>
                                                <?php $pass++; ?>
                                            @endif
{{--                                            </thead>--}}
                                            <tbody>
                                            <tr style="background-color: lightyellow;font-weight: bold">
                                                <td>
                                                    <div class="a">{{ $penilaian['bil'] ? $penilaian['bil'] : '-' }}</div>
                                                </td>
                                                <td></td>
                                                <td colspan="7" style="text-decoration: underline;">{{$penilaian['nama']}} ({{$penilaian['peratus']}})</td>
                                            </tr>
                                            @foreach($penilaian['sub'] as $sub_pen)
                                                <tr style="background-color: lightgrey;font-weight: bold">
                                                    <td>
                                                        <div class="a">{{$sub_pen['bil']}}</div>
                                                    </td>
                                                    <td>
                                                        <div class="a">{{$sub_pen['no_klausa']}}</div>
                                                    </td>
                                                    <td colspan="7">{{$sub_pen['nama']}}</td>
                                                </tr>
                                                @foreach($sub_pen['jawapan_skor'] as $perkara)
                                                    @include('segment.urussetia.projek_soalan.review_jawapan')
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <hr>
                @endforeach
            @endif
        </div>
    </div>
</div>
