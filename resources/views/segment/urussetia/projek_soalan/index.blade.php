@extends('segment.layouts.main')

@section('customCSS')
    @include('segment.layouts.asset_include_links.dragdrop.css.dragdrop_css')
    @include('segment.layouts.asset_include_links.datatable.css.datatable_css')
    @include('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css')

    <style>
        #btnDraf {
            display: none; /* Hidden by default */
            position: fixed; /* Fixed/sticky position */
            bottom: 20px; /* Place the button at the bottom of the page */
            right: 200px; /* Place the button 30px from the right */
            z-index: 99; /* Make sure it does not overlap */
            border: none; /* Remove borders */
            outline: none; /* Remove outline */
            background-color: red; /* Set a background color */
            color: white; /* Text color */
            cursor: pointer; /* Add a mouse pointer on hover */
            padding: 15px; /* Some padding */
            border-radius: 10px; /* Rounded corners */
            font-size: 18px; /* Increase font size */
        }

        #btnDraf:hover {
            background-color: #555; /* Add a dark-grey background on hover */
        }
    </style>
@endsection

@section('content')
<button id="btnDraf" class="btn btn-warning kembali-ke-penilaian" data-save="0">Kembali Ke Penilaian</button>
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-12 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-md-7">
                        <h2 class="content-header-title float-left mb-0">Urus Setia</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Projek</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">{{$projek_name}}</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">{{$tahun_name}}</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">{{$no_klausa}}</a>
                                </li>
                                <li class="breadcrumb-item active">Senarai
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary projek-penilaian-soalan-preview" style="width: 100%">Preview</button>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-info projek-penilaian-soalan-duplicate" style="width: 100%">Duplicate Penilaian</button>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <button class="btn btn-primary projek-penilaian-soalan-main-add" style="width: 100%">Tambah Kategori Baru</button>
            </div>
        </div>
        <hr>
        <div class="content-body">
            <section id="collapsible" class="penilaian-main-append penilaian-main-drag">
                @if(!empty($data))
                    @foreach($data as $d)
                        <div class="row penilaian-soalan-main-row" data-penilaian-soalan-main-id="{{$d['id']}}" data-penilaian-soalan-main-ref-id="{{$d['ref_id']}}">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title projek-penilaian-soalan-main">{{$d['nama']}} ({{$d['percentage']}}%)</h4>
                                        <span>
                                            <button type="button" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-sub-list" data-toggle="tooltip" data-placement="top" title="Tambah Sub Tajuk">
                                                <i data-feather="align-justify"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-update" data-toggle="tooltip" data-placement="top" title="Kemaskini Tajuk">
                                                <i data-feather="edit"></i>
                                            <button type="button" class="btn btn-icon btn-outline-danger mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-delete" data-toggle="tooltip" data-placement="top" title="Delete Tajuk">
                                                <i data-feather="trash"></i>
                                            </button>
                                        </span>
                                    </div>
                                <div class="card-body">
                                    <div class="row mt-1">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="list-group penilaian-main-sub-drag penilaian-soalan-main-sub-row-append" id="list-tab" role="tablist">
                                                @if(!empty($d['sub']))
                                                    @foreach($d['sub'] as $sub)
                                                        <a class="list-group-item list-group-item-action penilaian-soalan-main-sub-row" data-toggle="list" href="#soalan-main-sub-{{$sub['id']}}" role="tab" aria-controls="" data-ref-id="{{$sub['ref_id']}}" data-id="{{$sub['id']}}">{{ '('.$sub['bil'].')'}}{{$sub['tajuk_perkara']}}</a>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <button class="btn btn-primary projek-penilaian-soalan-main-sub-perkara-add" style="width: 100%">Tambah Soalan Baru</button>
                                            <input type="hidden" class="projek-penilaian-soalan-main-sub-perkara-main-pressed-id" value="">
                                            <input type="hidden" class="projek-penilaian-soalan-main-sub-perkara-sub-pressed-id" value="">
                                            <hr>
                                            <div class="tab-content penilaian-soalan-main-sub-soalan-row-append" id="nav-tabContent">
                                                @if(!empty($d['sub']))
                                                    @foreach($d['sub'] as $sub)
                                                        <div class="tab-pane fade show penilaian-soalan-main-sub-row-show-pane" id="soalan-main-sub-{{$sub['id']}}" role="tabpanel">
                                                            <ul class="list-group penilaian-main-sub-soalan-drag">
                                                                @if(!empty($sub['soalan']))
                                                                    @foreach($sub['soalan'] as $soalan)
                                                                        <li class="list-group-item d-flex justify-content-between align-items-center penilaian-soalan-main-sub-perkara-row" data-penilaian-soalan-main-sub-id="{{$soalan['sub_id']}}" data-penilaian-soalan-main-sub-ref-id="{{$soalan['main_id']}}" data-id="{{$soalan['id']}}">
                                                                            <div class="col-md-11">
                                                                                <span class="soalan-title">{{'('.$soalan['bil'].')'}} {{$soalan['tajuk_perkara']}}</span>
                                                                                </div>
                                                                            <div class="col-md-1">
                                                                                <span>
                                                                                        <div class="dropdown">
                                                                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                                <i data-feather="more-vertical"></i>
                                                                                            </button>
                                                                                            <div class="dropdown-menu">
                                                                                                <a class="dropdown-item projek-penilaian-soalan-main-sub-perkara-update" href="javascript:void(0);">
                                                                                                <i data-feather="edit-2"></i>
                                                                                                    <span>Edit</span>
                                                                                                </a>
                                                                                                <a class="dropdown-item projek-penilaian-soalan-main-sub-perkara-delete" href="javascript:void(0);">
                                                                                                <i data-feather="trash"></i>
                                                                                                <span>Delete</span>
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </span>
                                                                                </div>
                                                                            </li>
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </section>
            <div class="col-md-12">
                <a href="{{ url()->previous() }}" class="btn btn-warning projek-penilaian-soalan-siap2" style="width: 100%">Kembali</a>
            </div>
        </div>
    </div>
</div>
@include('segment.urussetia.projek_soalan.modals.index')
<input type="hidden" class="projek-tahun-id" value="{{$projek_tahun_id}}">
<input type="hidden" class="tahun-id" value="{{$tahun_id}}">
<input type="hidden" class="main-projek-id" value="{{$projek_id}}">
@endsection

@section('customJS')
    @include('segment.layouts.asset_include_links.common.js.common_js')
    @include('segment.layouts.asset_include_links.datatable.js.datatable_js')
    @include('segment.layouts.asset_include_links.dragdrop.js.dragdrop_js')
    @include('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js')
    <script src="{{ asset('templates/app-assets/js/scripts/jqueryui/jqueryui.js') }}"></script>
    {{--  Custom files  --}}
    <script src="{{ asset('js_helper/segment/urussetia/projek-soalan/append.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-soalan/swal.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-soalan/page_settings.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-soalan/ajax.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-soalan/datatable.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-soalan/index.js') }}"></script>
@endsection
