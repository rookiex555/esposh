<div class="row">
    <div class="col-sm-12">
        <div class="card penilaian-soalan-main-row" data-penilaian-soalan-main-id="" data-penilaian-soalan-main-ref-id="">
            <div class="card-header">
                <h4 class="card-title projek-penilaian-soalan-main">PENILAIAN DOKUMENTASI (40%)</h4>
                <span>
                    <button type="button" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-sub-list"><i data-feather="align-justify"></i></button>
                    <button type="button" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-update"><i data-feather="edit"></i></button>
                    <button type="button" class="btn btn-icon btn-outline-danger mr-1 mb-1 waves-effect waves-light projek-penilaian-soalan-main-sub-delete"><i data-feather="trash"></i></button>
                </span>
            </div>
            <div class="card-body">
                <div class="row mt-1">
                    <div class="col-md-4 col-sm-12">
                        <div class="list-group penilaian-main-sub-drag" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="list-home">DASAR KKP</a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <button class="btn btn-primary projek-penilaian-soalan-main-add" style="width: 100%">Tambah Soalan Baru</button>
                        <hr>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                                <ul class="list-group penilaian-main-sub-soalan-drag">
                                    <li class="list-group-item d-flex justify-content-between align-items-center penilaian-soalan-main-sub-row" data-penilaian-soalan-main-sub-id="" data-penilaian-soalan-main-sub-ref-id="">
                                        <div class="col-md-11">
                                            <span>dsadsadsda</span>
                                        </div>
                                        <div class="col-md-1">
                                            <span>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item projek-penilaian-soalan-main-sub-update" href="javascript:void(0);">
                                                            <i data-feather="edit-2" class="mr-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item projek-penilaian-soalan-main-sub-delete" href="javascript:void(0);">
                                                            <i data-feather="trash" class="mr-50"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
