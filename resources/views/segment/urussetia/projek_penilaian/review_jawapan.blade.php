<tr class="perkara-soalan-row">
    <td>
        <div class="a">{{$perkara['bil']}}</div>
    </td>
    <td>
        <div class="a">{{$perkara['no_klausa']}}</div>
    </td>
    <td>
        <div class="a">{{$perkara['nama']}}</div>
        </td>
    <td class="perkara-penilaian-row">
        @if($perkara['skor'] != 'tb')
            <div class="form-group">
                <div class="a">
                <label for="customFile" style="text-decoration: underline">Dokumen Sokongan</label></div>
                @if(!empty($perkara['uploads']))
                    <?php $x = 1 ?>
                    @foreach($perkara['uploads'] as $uploads)
                        <!-- <div class="a">
                            <a target="_blank" href="{{Request::root()}}/{{$uploads['link']}}" style="font-weight: bold">{{$x}}. {{$uploads['name']}}
                        </div><br> -->
                        <div class="a">
                            <img alt="" src="{{Request::root()}}/{{$uploads['link']}}" style="width:60%" width="150" height="180"></a>
                        </div><br>
                        <?php $x++ ?>
                    @endforeach
                @endif
            </div>
        @else
            <div>Tidak Berkenaan</div>
        @endif
    </td>
    <td>
         <div class="form-group">
                <label for="exampleFormControlTextarea1" style="text-decoration: solid"></label>
                <div class="a" style="font-weight: bold"><span>{{$perkara['catatan'] ? $perkara['catatan'] : 'Tiada Catatan'}}</span></div>
            </div>
    </td>
    <td>
        <div class="a">
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="0">
            @if($perkara['skor'] == 0 && $perkara['skor'] !='tb')
                <div><i data-feather="check" style="color:green"></i></div>
            @else
                <div>-</div>
            @endif
        </div>
    </div>
    </td>
    <td>
        <div class="a">
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="1">
            @if($perkara['skor'] == 1)
                <div><i data-feather="check" style="color:green"></i></div>
            @else
                <div>-</div>
            @endif
        </div>
    </div>
    </td>
    <td>
        <div class="a">
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="2">
            @if($perkara['skor'] == 2)
                <div><i data-feather="check" style="color:green"></i></div>
            @else
                <div>-</div>
            @endif
        </div>
    </div>
    </td>
    <td>
        <div class="a">
        <div class="form-check form-check-inline perkara-check-col form-group" data-skor="tb">
            @if($perkara['skor'] == 'tb')
                <div><i data-feather="check" style="color:green"></i></div>
            @else
                <div>-</div>
            @endif
        </div>
    </div>
    </td>
</tr>
