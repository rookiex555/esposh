@extends('segment.layouts.main')

@section('customCSS')
    @include('segment.layouts.asset_include_links.datatable.css.datatable_css')
    @include('segment.layouts.asset_include_links.select2.css.select2_css')
    @include('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css')
    @include('segment.layouts.asset_include_links.date_time.css.datetime_css')
@endsection

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Urus Setia</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Projek</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">{{$projek_name}}</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">{{$tahun_name}}</a>
                                </li>
                                <li class="breadcrumb-item active">Senarai
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            @if($passYear == 1)
                                <table class="projek-penilaian-table-outdated table">
                            @else
                                <table class="projek-penilaian-table table">
                            @endif
                                    <thead>
                                    <tr>
                                        <th>Skop</th>
                                        <th>Penilai</th>
                                        <th>Tarikh Penilaian</th>
                                        <th>Status</th>
                                        {{--                                        <th>Aktif</th>--}}
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
    @include('segment.urussetia.projek_penilaian.modals.index')

    <input type="hidden" class="projek-id" value="{{$projek_id}}">
    <input type="hidden" class="tahun-id" value="{{$tahun_id}}">
@endsection

@section('customJS')
    {{--  Vendor Files  --}}
    @include('segment.layouts.asset_include_links.datatable.js.datatable_js')
    @include('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js')
    @include('segment.layouts.asset_include_links.common.js.common_js')
    @include('segment.layouts.asset_include_links.select2.js.select2_js')
    @include('segment.layouts.asset_include_links.date_time.js.datetime_js')

    {{--  Custom files  --}}
    <script src="{{ asset('js_helper/segment/urussetia/projek-penilaian/swal.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-penilaian/page_settings.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-penilaian/datatable.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-penilaian/ajax.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-penilaian/index.js') }}"></script>
@endsection
