d<div class="modal fade text-left modal-primary projek-penilaian-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">SPKKP</label>
                            <input type="text" class="form-control projek-penilaian-no-klausa" id="basicInput" placeholder="" value="KLAUSA ISO 45001:2018" readonly/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Pengarah Projek / Pegawai Penguasa</label>
                             <select class="projek-penilaian-pengarah-jawatan form-control select3">
                                 <option value="">Sila Pilih</option>
                                 @if(count($pengarah) > 0)
                                     @foreach($pengarah as $p)
                                         <option value="{{$p->id}}">{{$p->name}}</option>
                                     @endforeach
                                 @endif
                             </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Pejabat Pengarah Projek / Pegawai Penguasa</label>
                            <select class="projek-penilaian-pengarah-nama form-control select3">
                                <option value="">Sila Pilih</option>
                                @if(count($pPejabat) > 0)
                                    @foreach($pPejabat as $pp)
                                        <option value="{{$pp->id}}"> {{$pp->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Wakil Pengarah Projek / Wakil Pegawai Penguasa</label>
                             <select class="projek-penilaian-wakil-jawatan form-control select3">
                                 <option value="">Sila Pilih</option>
                                 @if(count($wakil) > 0)
                                     @foreach($wakil as $w)
                                         <option value="{{$w->id}}"> {{$w->name}}</option>
                                     @endforeach
                                 @endif
                             </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Pejabat Wakil Pengarah Projek / Wakil Pegawai Penguasa</label>
                            <select class="projek-penilaian-wakil-nama form-control select3">
                                <option value="">Sila Pilih</option>
                                @if(count($wPejabat) > 0)
                                    @foreach($wPejabat as $wp)
                                        <option value="{{$wp->id}}"> {{$wp->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Milik Tapak</label>
                            <input type="text" class="form-control projek-penilaian-tkh-tapak" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Siap Asal</label>
                            <input type="text" class="form-control projek-penilaian-tkh-asal" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Siap Semasa</label>
                            <input type="text" class="form-control projek-penilaian-tkh-semasa" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Tarikh Penilaian</label>
                            <input type="text" class="form-control projek-penilaian-tkh-penilaian" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Kemajuan Fizikal Semasa Penilaian (%)</label>
                            <input type="text" class="form-control projek-penilaian-kemajuan-fizikal" id="basicInput" placeholder=""/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Ketua Penilai Audit(KPA)</label>
                            <select class="projek-penilaian-penilai form-control" id="select25-ajax"></select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success post-add-projek-penilaian">Tambah</button>
                <button type="button" class="btn btn-warning post-update-projek-penilaian">Kemaskini</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="projek-penilaian-id" value="">

