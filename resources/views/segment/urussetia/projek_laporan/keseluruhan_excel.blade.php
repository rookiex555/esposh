<?php
$fileName = 'Keseluruhan-'.$data['projek']['nama'].'-'.$data['projek']['tahun'] . ".xls";

// Headers for download
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>

<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Document</title>
        <style>
            .centerCell {
                text-align: center;
                vertical-align: middle !important;
                font-weight: bold !important;
            }
            .center {
                text-align: center;
                vertical-align: middle !important;
                font-weight: solid;
            }

            div.a {
            text-align: center;
            }
            table, th, td {
                border: 1px solid black;
            }

            i {
                content: "\2713";
            }

            .page-break {
                page-break-before: always;
            }

            .report-table {
                padding-bottom: 50px;
            }
        </style>
    </head>
<body>
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="text-decoration: underline">Maklumat Audit Dalam SPKKP Projek</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        Nama Projek: {{$data['projek']['nama']}}
                                    </div>
                                    <div class="col-md-12">
                                        Tahun: {{$data['projek']['tahun']}}
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table" style="width: 100%;">
                                            <tr>
                                                <td>Tarikh Milik: <br>{{$data['projek_tahun']['tkh_milik']}}</td>
                                                <td>Tarikh Siap Asal: <br>{{$data['projek_tahun']['tkh_siap_asal']}}</td>
                                                <td>Tarikh Siap Semasa: <br>{{$data['projek_tahun']['tkh_siap_semasa']}}</td>
                                                <td>Tarikh Milik Tapak: <br>{{$data['projek_tahun']['tkh_milik_tpk']}}</td>
                                                <td>Tarikh Penilaian: <br>{{$data['projek_tahun']['tkh_penilaian']}}</td>
                                            <!--  <td>No Klausa: <br>{{$data['projek_tahun']['no_klausa']}}</td> -->
                                                <td>Kemajuan Fizikal: <br>{{$data['projek_tahun']['kemajuan_fizikal']}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    Pengarah Projek / Pegawai Penguasa: <br>{{$data['projek_tahun']['pengarah']}}
                                                </td>
                                                <td colspan="3">
                                                    Alamat Pengarah Projek/Pegawai Penguasa: <br>{{$data['projek_tahun']['pengarah_alamat']}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    Wakil Projek / Pegawai Penguasa: <br>{{$data['projek_tahun']['wakil']}}
                                                </td>
                                                <td colspan="3">
                                                    Alamat Wakil Pengarah Projek/Pegawai Penguasa: <br>{{$data['projek_tahun']['wakil_alamat']}}
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    Ketua Penilai Audit (KPA): {{$data['projek_tahun']['penilai']['nama']}} <br>
                                                    NOKP: ({{$data['projek_tahun']['penilai']['nokp']}})
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <div class="table-responsive">
                                    @if($data['projek_tahun']['status'] == 1)
                                        <table class="table" style="width: 100%">
                                            <thead>
                                            <tr style="text-align: center">
                                                <th class="centerCell" style="width: 50%;font-size: large">MARKAH KESELURUHAN</th>
                                                <th class="centerCell" style="width: 50%;font-size: large">GRED</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style="text-align: center;font-weight: bold" colspan="2">BELUM SIAP DIJAWAB</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @elseif($data['projek_tahun']['status'] == 2)
                                        <table class="table" style="width: 100%">
                                            <thead>
                                            <tr style="text-align: center">
                                                <th class="centerCell" style="width: 50%">MARKAH KESELURUHAN</th>
                                                <th class="centerCell" style="width: 50%">GRED</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style="text-align: center">{{ $data['projek_tahun']['overallSector'] }}%</td>
                                                <td style="text-align: center">{{ $data['projek_tahun']['grade'] }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                @foreach($data['soalan_jawapan'] as $penilaian)
                    <section id="basic-datatable main-penilaian-row">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title" style="text-decoration: underline">{{$penilaian['nama']}}</h4>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table report-table" style="width: 100%;page-break-after:auto;" >
{{--                                            <thead>--}}
                                            <tr style="text-align: center">
                                                <td class="centerCell" rowspan="2">BIL</td>
                                                <td class="centerCell" rowspan="2">KLAUSA <br>{{$data['projek_tahun']['no_klausa']}}</td>
                                                <td class="centerCell" rowspan="2" style="width:20%">PERKARA</td>
                                                <td class="centerCell" rowspan="2">PENEMUAN / BUKTI BERGAMBAR</td>
                                                <td class="centerCell" rowspan="2">CATATAN</td>
                                                <td colspan="4">SKOR</td>
                                            </tr>
                                            <tr style="">
                                                <td class="centerCell" style="background-color: red;color:black">0</td>
                                                <td class="centerCell" style="background-color: yellow;color:black">1</td>
                                                <td class="centerCell" style="background-color: green;color:black">2</td>
                                                <td class="centerCell" style="background-color: black;color:white">TB</td>
                                            </tr>
{{--                                            </thead>--}}
                                            <tbody>
                                            <tr style="background-color: lightyellow;font-weight: bold">
                                                <td>
                                                    <div class="a">{{ $penilaian['bil'] ? $penilaian['bil'] : '-' }}</div>
                                                </td>
                                                <td></td>
                                                <td colspan="7" style="text-decoration: underline;">{{$penilaian['nama']}} ({{$penilaian['peratus']}})</td>
                                            </tr>
                                            @foreach($penilaian['sub'] as $sub_pen)
                                                <tr style="background-color: lightgrey;font-weight: bold">
                                                    <td>
                                                        <div class="a">{{$sub_pen['bil']}}</div>
                                                    </td>
                                                    <td>
                                                        <div class="a">{{$sub_pen['no_klausa']}}</div>
                                                    </td>
                                                    <td colspan="7">{{$sub_pen['nama']}}</td>
                                                </tr>
                                                @foreach($sub_pen['jawapan_skor'] as $perkara)
                                                    @include('segment.urussetia.projek_laporan.review_jawapan_pdf')
                                                @endforeach
                                            @endforeach
{{--                                            <tr style="background-color: rgb(69, 250, 146);">--}}
{{--                                                <td colspan="5" style="text-align: right; font-weight: bold">Jumlah Soalan Dijawab</td>--}}
{{--                                                <td style="text-align: center;">{{ $penilaian['totalZero'] }}</td>--}}
{{--                                                <td style="text-align: center">{{ $penilaian['totalHalf'] }}</td>--}}
{{--                                                <td style="text-align: center">{{ $penilaian['totalFull'] }}</td>--}}
{{--                                                <td style="text-align: center">{{ $penilaian['totalTB'] }}</td>--}}
{{--                                            </tr>--}}
                                            <tr style="background-color: rgb(97, 255, 253)">
                                                <td colspan="5" style="text-align: right;font-weight: bold">Markah {{ ucwords(strtolower($penilaian['nama'])) }}:</td>
                                                <td colspan="5" style="text-align: center; font-weight: bold">({{ $penilaian['totalOverall'] }} / {{ $penilaian['peratus'] }}) % </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <hr>
                @endforeach
                <section id="basic-datatable main-penilaian-row">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table" style="width: 100%;">
                                    <tr style="background-color: rgb(125,240,115)">
                                        <td class="leftCell" colspan="1" style="text-align: right;width:804px">Markah Keseluruhan</td>
                                        <td class="rightCell" rowspan="2" style="text-align: center">({{ $data['projek_tahun']['overallSector'] }} / 100) %</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</body>
</html>
