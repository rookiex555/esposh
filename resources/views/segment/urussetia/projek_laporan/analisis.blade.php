@extends('segment.layouts.main')


@section('customCSS')
    @include('segment.layouts.asset_include_links.select2.css.select2_css')
    @include('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.0/chart.min.js"></script>
    <style>
        canvas{

        width:750px !important;
        height:750px !important;
        display:block;margin:0 auto;
        }
    </style>
@endsection

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Urus Setia</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Projek</a>
                                    </li>
                                    <li class="breadcrumb-item active">Laporan Analisis
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                        <div class="col-md-6 mb-1">
                                            <label>Filter Mengikut Tahun</label>
                                            <form method="POST" action="{{url('/urussetia/projek/laporan/analisis')}}">
                                                @csrf
                                                <select   name="tahun_select" class="select2 form-control form-control-lg select-penilaian-tahun">
                                                    <option value="">Pilih Tahun</option>
                                                    @foreach($tahun as $t)
                                                        <option value="{{$t->id}}">{{$t->tahun}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                        <div class="col-md-6 mb-1" style="padding-top: 20px">
                                            <button class="btn btn-success" type="submit">Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Analisis</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <canvas id="canvas" width="200" height="200"></canvas>
                                    </div>
                                    <div class="col-md-4 responsive-table">
                                        <table border="1" style="width:100%">
                                            <thead style="text-align:center; background-color:grey;font-weight:bolder;color:black">
                                                <th>MARKAH</th>
                                                <th>KATEGORI</th>
                                                <th>GRED</th>
                                            <thead>
                                            <tbody style="text-align: center">
                                                <tr style="background-color: green;font-weight:bolder;color:black">
                                                    <td>90-100</td>
                                                    <td>CEMERLANG</td>
                                                    <td>A</td>
                                                </tr>
                                                <tr style="background-color: blue;font-weight:bolder;color:black">
                                                    <td>70-89</td>
                                                    <td>BAIK</td>
                                                    <td>B</td>
                                                </tr>
                                                <tr style="background-color: orange;font-weight:bolder;color:black">
                                                    <td>50-69</td>
                                                    <td>MEMUASKAN</td>
                                                    <td>C</td>
                                                </tr>
                                                <tr style="background-color: yellow;font-weight:bolder;color:black">
                                                    <td>30-49</td>
                                                    <td>KURANG MEMUASKAN</td>
                                                    <td>D</td>
                                                </tr>
                                                <tr style="background-color: red;font-weight:bolder;color:black">
                                                    <td><29</td>
                                                    <td>TIDAK MEMUASKAN</td>
                                                    <td>E</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customJS')
    {{--  Vendor Files  --}}
    @include('segment.layouts.asset_include_links.select2.js.select2_js')
    @include('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js')
    @include('segment.layouts.asset_include_links.common.js.common_js');
    
    {{--  Custom files  --}}
    
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/swal.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/page_settings.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/datatable.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/ajax.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/index.js') }}"></script>

    <script>
        var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');
        
        var myChart = new Chart(ctx, {
            type: 'polarArea',
            data: {
                labels: @json($data["mainLabel"]),
                datasets: [{
                    label: 'My First Dataset',
                    data: @json($data["superSheet"]),
                    backgroundColor: [
                    'rgba(0, 128, 0, 0.9)',
                    'rgba(0, 0, 255, 0.9)',
                    'rgba(255,165,0, 0.9)',
                    'rgba(255, 255, 0, 0.9)',
                    'rgba(255, 0, 0, 0.9)',
                    ]
                }]
            },
            options: {
                // maintainAspectRatio: false,
                responsive: true,
                scales: {
                    r: {
                        pointLabels: {
                        display: true,
                        centerPointLabels: true,
                        font: {
                            size: 18
                        }
                        }
                    }
                },
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Jumlah Keseluruhan Projek: {{$data["totalProject"]}}'
                    }
                }
            }
        });
    </script>
@endsection
