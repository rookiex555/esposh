@extends('segment.layouts.main')


@section('customCSS')
    @include('segment.layouts.asset_include_links.select2.css.select2_css')
    @include('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css')
@endsection

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Urus Setia</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Projek</a>
                                    </li>
                                    <li class="breadcrumb-item active">Laporan Keseluruhan
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                        <div class="col-md-6 mb-1">
                                            <label>Filter Mengikut Tahun</label>
                                            <form method="POST" action="{{url('/urussetia/projek/laporan/keseluruhan')}}">
                                                @csrf
                                                <select   name="tahun_select" class="select2 form-control form-control-lg select-penilaian-tahun">
                                                    <option value="">Pilih Tahun</option>
                                                    @foreach($tahun as $t)
                                                        <option value="{{$t->id}}">{{$t->tahun}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                        <div class="col-md-6 mb-1" style="padding-top: 20px">
                                            <button class="btn btn-success" type="submit">Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Penilaian</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Bil</th>
                                                    <th>Projek</th>
                                                    <th>Markah Keseluruhan</th>
                                                    <th>Gred</th>
                                                    <th>Papar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($data) > 0)
                                                <?php $x = 1; ?>
                                                    @foreach($data as $projek => $keputusan)
                                                        @if(count($keputusan) > 0)
                                                            @foreach($keputusan as $k)
                                                                
                                                                @foreach($k as $latest)
                                                                    <tr>
                                                                        <td style="text-align: center">{{$x++}}.</td>
                                                                        <td style="background-color: rgb(252, 252, 175);">{{$projek}}</td>
                                                                        <td>{{$latest['markah_keseluruhan'].'%'}}</td>
                                                                        <td>{{$latest['gred']}}</td>
                                                                        <td>
                                                                            <a target="_blank" href="{{ route('urussetia/projek/laporan/keseluruhan/pdf', ['id' => $latest['projek_tahun_id']]) }}" class="btn btn-icon btn-outline-warning mr-1 mb-1 waves-effect waves-light pengguna-delete" data-toggle="tooltip" data-placement="top" title="Papar Keputusan (PDF)">
                                                                                <i data-feather="file" style="color: orange"></i>
                                                                            </a>
                                                                            <a download href="{{ route('urussetia/projek/laporan/keseluruhan/excel', ['id' => $latest['projek_tahun_id']]) }}" class="btn btn-icon btn-outline-success mr-1 mb-1 waves-effect waves-light pengguna-delete" data-toggle="tooltip" data-placement="top" title="Papar Keputusan (Excel)">
                                                                                <i data-feather="file-text" style="color: lightseagreen"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endforeach
                                                        @else
                                                            <tr>
                                                                <td colspan="5" style="background-color: rgb(252, 252, 175)">Tiada Penilaian</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <tr style="text-align: center">
                                                        <td colspan="5">Tiada Projek</td>
                                                    </tr>   
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('segment.urussetia.projek.modals.index')
@endsection

@section('customJS')
    {{--  Vendor Files  --}}
    @include('segment.layouts.asset_include_links.select2.js.select2_js')
    @include('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js')
    @include('segment.layouts.asset_include_links.common.js.common_js');

    {{--  Custom files  --}}
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/swal.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/page_settings.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/datatable.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/ajax.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/index.js') }}"></script>
@endsection
