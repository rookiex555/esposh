@extends('segment.layouts.main')


@section('customCSS')
    @include('segment.layouts.asset_include_links.select2.css.select2_css')
    @include('segment.layouts.asset_include_links.sweetAlert.css.sweet_alert_css')
@endsection

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Urus Setia</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Projek</a>
                                    </li>
                                    <li class="breadcrumb-item active">Laporan Mengikut Projek
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Penilaian</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 mb-1">
                                        <label>Tahun</label>
                                        <select class="select2 form-control form-control-lg select-penilaian-tahun">
                                            <option value="">Pilih Tahun</option>
                                            @foreach($tahun as $t)
                                                <option value="{{$t->id}}">{{$t->tahun}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 mb-1">
                                        <label>Penilaian</label>
                                        <select class="select2 form-control form-control-lg select-penilaian">
                                            <option value="">Pilih Penilaian</option>
                                            @foreach($projek as $p)
                                                <option value="{{$p->id}}">{{$p->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Bil</th>
                                                    <th>Perkara</th>
                                                    <th>Markah</th>
                                                </tr>
                                                </thead>
                                                <tbody class="penilaian_keputusan_result">
                                                <tr style="text-align: center">
                                                    <td colspan="3">Sila Pilih Penilaian</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('segment.urussetia.projek.modals.index')
@endsection

@section('customJS')
    {{--  Vendor Files  --}}
    @include('segment.layouts.asset_include_links.select2.js.select2_js')
    @include('segment.layouts.asset_include_links.sweetAlert.js.sweet_alert_js')
    @include('segment.layouts.asset_include_links.common.js.common_js');

    {{--  Custom files  --}}
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/swal.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/page_settings.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/datatable.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/ajax.js') }}"></script>
    <script src="{{ asset('js_helper/segment/urussetia/projek-laporan/index.js') }}"></script>
@endsection
