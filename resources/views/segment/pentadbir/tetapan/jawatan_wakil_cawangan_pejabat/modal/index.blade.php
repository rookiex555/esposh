<div class="modal fade text-left modal-primary jawatan-wakil-cawangan-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Nama</label>
                            <textarea class="form-control jawatan-wakil-cawangan-nama"></textarea>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="basicInput">Negeri</label>
                            <select class="form-control jawatan-wakil-cawangan-negeri">
                                <option value="">Sila Pilih</option>
                                @foreach($negeri as $n)
                                    <option value="{{$n->id}}">{{$n->nama}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success post-add-jawatan-wakil-cawangan">Tambah</button>
                <button type="button" class="btn btn-warning post-update-jawatan-wakil-cawangan">Kemaskini</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" class="jawatan-wakil-cawangan-id" value="">

<div class="modal fade text-left modal-primary jawatan-wakil-cawangan-pejabat-modal" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-12 mb-1">
                        <div class="form-group">
                            <label for="basicInput">Nama Pejabat</label>
                            <textarea class="form-control jawatan-wakil-cawangan-pejabat-nama"></textarea>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 add-new-cawangan-pejabat-button">
                        <button type="button" style="width: 100%" class="btn btn-info post-add-jawatan-wakil-cawangan-pejabat-baru">Tambah Baru</button>
                    </div>
                    <div class="col-md-12 add-cawangan-pejabat-button">
                        <button type="button" style="width: 100%" class="btn btn-success post-add-jawatan-wakil-cawangan-pejabat">Tambah</button>
                    </div>
                    <div class="col-md-6 update-cawangan-pejabat-button">
                        <button type="button" style="width: 100%" class="btn btn-warning post-update-jawatan-wakil-cawangan-pejabat">Kemaskini</button>
                    </div>
                </div>
                <hr>
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <table class="jawatan-wakil-cawangan-pejabat-table table">
                                    <thead>
                                    <tr>
                                        <th>Alamat</th>
                                        <th>Tindakan</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="jawatan-wakil-cawangan-pejabat-id" value="">