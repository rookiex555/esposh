<?php

Route::get('/login-sso', 'Auth\LoginController@login_sso')->name('/login-sso');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'Auth\LoginController@landing')->name('/');
Route::get('wrong-user', 'Auth\LoginController@redirectToerror')->name('wrong-user');

Route::get('/test-mail', 'Main\CommonController@mail_test');
Route::get('/delete-kpa-duplicate', 'Main\CommonController@delete_duplicate_kpa_soalan');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/auth/login', 'Auth\LoginController@authenticate')->name('auth/login');

Route::prefix('/dashboard')->group(function () {
    Route::get('/', 'Segment\Dashboard\DashboardController@index');
    Route::post('/get-chart-data-selesai', 'Segment\Dashboard\DashboardController@get_chart_data_selesai');
});

//All Disabled User
Route::get('/disabled', 'Auth\LoginController@disabledUser');

//Pentadbir Pengguna
Route::prefix('/admin/pentadbir')->group(function () {
    Route::prefix('/pengguna')->group(function () {
        Route::get('/', 'Segment\Pentadbir\Pengguna\PenggunaController@index');
        Route::get('/list', 'Segment\Pentadbir\Pengguna\PenggunaController@pengguna_list');
        Route::post('/tambah', 'Segment\Pentadbir\Pengguna\PenggunaController@pengguna_tambah');
        Route::post('/aktif', 'Segment\Pentadbir\Pengguna\PenggunaController@pengguna_aktif');
        Route::post('/delete', 'Segment\Pentadbir\Pengguna\PenggunaController@pengguna_delete');

        Route::prefix('/kpa')->group(function () {
            Route::get('/', 'Segment\Pentadbir\Pengguna\PenggunaKpaController@index');
            Route::get('/list', 'Segment\Pentadbir\Pengguna\PenggunaKpaController@pengguna_list');
            Route::post('/tambah', 'Segment\Pentadbir\Pengguna\PenggunaKpaController@pengguna_tambah');
            Route::post('/aktif', 'Segment\Pentadbir\Pengguna\PenggunaKpaController@pengguna_aktif');
            Route::post('/delete', 'Segment\Pentadbir\Pengguna\PenggunaKpaController@pengguna_delete');
        });
    });

    Route::prefix('/audit')->group(function () {
        Route::get('/', 'Segment\Pentadbir\Audit\AuditController@index');
        Route::get('/list', 'Segment\Pentadbir\Audit\AuditController@list');
    });

    Route::prefix('/tetapan')->group(function () {
        Route::prefix('/jawatan')->group(function () {
            Route::get('/', 'Segment\Pentadbir\Tetapan\JawatanController@index');
            Route::get('/list', 'Segment\Pentadbir\Tetapan\JawatanController@jawatan_list');
            Route::post('/tambah', 'Segment\Pentadbir\Tetapan\JawatanController@jawatan_tambah');
            Route::post('/get-jawatan', 'Segment\Pentadbir\Tetapan\JawatanController@jawatan_get_rekod');
            Route::post('/aktif', 'Segment\Pentadbir\Tetapan\JawatanController@jawatan_aktif');
            Route::post('/delete', 'Segment\Pentadbir\Tetapan\JawatanController@jawatan_delete');
        });

        Route::prefix('/jawatan/pejabat')->group(function () {
            Route::get('/', 'Segment\Pentadbir\Tetapan\JawatanPejabatController@index');
            Route::get('/list', 'Segment\Pentadbir\Tetapan\JawatanPejabatController@jawatan_pejabat_list');
            Route::post('/tambah', 'Segment\Pentadbir\Tetapan\JawatanPejabatController@jawatan_pejabat_tambah');
            Route::post('/get-jawatan', 'Segment\Pentadbir\Tetapan\JawatanPejabatController@jawatan_pejabat_get_rekod');
            Route::post('/aktif', 'Segment\Pentadbir\Tetapan\JawatanPejabatController@jawatan_pejabat_aktif');
            Route::post('/delete', 'Segment\Pentadbir\Tetapan\JawatanPejabatController@jawatan_pejabat_delete');
        });

        Route::prefix('/projek')->group(function () {
            Route::get('/', 'Segment\Pentadbir\Tetapan\TetapanProjekController@index');
            Route::get('/list', 'Segment\Pentadbir\Tetapan\TetapanProjekController@projek_list');
            Route::post('/tambah', 'Segment\Pentadbir\Tetapan\TetapanProjekController@projek_tambah');
            Route::post('/get-projek', 'Segment\Pentadbir\Tetapan\TetapanProjekController@projek_get_rekod');
            Route::post('/aktif', 'Segment\Pentadbir\Tetapan\TetapanProjekController@projek_aktif');
            Route::post('/delete', 'Segment\Pentadbir\Tetapan\TetapanProjekController@projek_delete');
        });

        Route::prefix('/jawatan-wakil')->group(function () {
            Route::get('/', 'Segment\Pentadbir\Tetapan\JawatanWakilController@index');
            Route::get('/list', 'Segment\Pentadbir\Tetapan\JawatanWakilController@jawatan_wakil_list');
            Route::post('/tambah', 'Segment\Pentadbir\Tetapan\JawatanWakilController@jawatan_wakil_tambah');
            Route::post('/get-jawatan-wakil', 'Segment\Pentadbir\Tetapan\JawatanWakilController@jawatan_wakil_get_rekod');
            Route::post('/aktif', 'Segment\Pentadbir\Tetapan\JawatanWakilController@jawatan_wakil_aktif');
            Route::post('/delete', 'Segment\Pentadbir\Tetapan\JawatanWakilController@jawatan_wakil_delete');

            Route::prefix('/cawangan')->group(function () {
                Route::get('/', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@index');
                Route::get('/list', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_list');
                Route::post('/tambah', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_tambah');
                Route::post('/get-cawangan', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_get_rekod');
                Route::post('/aktif', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_aktif');
                Route::post('/delete', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_delete');

                Route::prefix('/pejabat')->group(function () {
                    Route::post('/list', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_pejabat_list');
                    Route::post('/tambah', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_pejabat_tambah');
                    Route::post('/get-pejabat-wakil', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_pejabat_get_rekod');
                    Route::post('/delete', 'Segment\Pentadbir\Tetapan\JawatanCawanganPejabatController@jawatan_cawangan_pejabat_delete');
                });
            });
        });
    });
});

//Urussetia
Route::prefix('/urussetia')->group(function () {
    Route::prefix('/projek')->group(function () {
        Route::get('/', 'Segment\Urussetia\Projek\ProjekController@index');
        Route::get('/list', 'Segment\Urussetia\Projek\ProjekController@projek_list');
        Route::post('/tambah', 'Segment\Urussetia\Projek\ProjekController@projek_tambah');
        Route::post('/get-projek', 'Segment\Urussetia\Projek\ProjekController@get_projek');
        Route::post('/update', 'Segment\Urussetia\Projek\ProjekController@projek_tambah');
        Route::post('/aktif', 'Segment\Urussetia\Projek\ProjekController@projek_aktif');
        Route::post('/delete', 'Segment\Urussetia\Projek\ProjekController@projek_delete');
    });

    Route::prefix('/projek-tahunan')->group(function () {
        Route::get('/tahun/{projek_id}', 'Segment\Urussetia\Projek\ProjekTahunanController@index');
        Route::get('/list', 'Segment\Urussetia\Projek\ProjekTahunanController@projek_tahunan_list');
    });

    Route::get('/projek-penilaian/list/{projek_id}/{tahun_id}', 'Segment\Urussetia\Projek\ProjekPenilaianController@index');

    Route::prefix('/projek-penilaian')->group(function () {
        Route::get('/list/table/{projek_id}/{tahun_id}', 'Segment\Urussetia\Projek\ProjekPenilaianController@projek_penilaian_list');
        Route::post('/tambah', 'Segment\Urussetia\Projek\ProjekPenilaianController@projek_penilaian_tambah');
        Route::post('/get-projek-penilaian', 'Segment\Urussetia\Projek\ProjekPenilaianController@get_projek_penilaian');
        Route::post('/update', 'Segment\Urussetia\Projek\ProjekPenilaianController@projek_penilaian_tambah');
        Route::post('/aktif', 'Segment\Urussetia\Projek\ProjekPenilaianController@projek_penilaian_aktif');
        Route::post('/delete', 'Segment\Urussetia\Projek\ProjekPenilaianController@projek_penilaian_delete');
        Route::post('/hantar', 'Segment\Urussetia\Projek\ProjekPenilaianController@projek_penilaian_hantar');
        Route::get('/review/{projek_tahun_id}', 'Segment\Urussetia\Projek\ProjekPenilaianController@projek_penilaian_preview');
    });

    Route::get('/projek-penilaian-soalan/all/{projek_tahun_id}', 'Segment\Urussetia\Projek\ProjekSoalanController@index');

    Route::prefix('/projek-penilaian-soalan')->group(function () {
        Route::prefix('/main')->group(function () {
            Route::post('/tambah', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_tambah');
            Route::post('/get-soalan-main', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_get_soalan');
            Route::post('/update', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_tambah');
            Route::post('/delete', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_delete');

            Route::prefix('/sub')->group(function () {
                Route::post('/tambah', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_tambah');
                Route::post('/update', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_tambah');
                Route::post('/get-soalan-sub', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_get_soalan');
                Route::post('/delete', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_delete');

                Route::prefix('/perkara')->group(function () {
                    Route::post('/tambah', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_perkara_tambah');
                    Route::post('/update', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_perkara_tambah');
                    Route::post('/get-soalan', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_perkara_get_soalan');
                    Route::post('/delete', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_main_sub_perkara_delete');
                });
            });
        });
        Route::post('/preview/check-send', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_preview_check_send');
        Route::post('/duplicate/confirm', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_duplicate');
        Route::get('/preview/{projek_tahun_id}', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_preview');
        Route::post('/duplicate/confirm', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_duplicate');
    });
    Route::get('/projek-penilaian-soalan-sub/list/table/{projek_penilaian_soalan_main_id}', 'Segment\Urussetia\Projek\ProjekSoalanController@projek_penilaian_soalan_sub_list');

    Route::prefix('/projek/laporan')->group(function () {
        Route::get('/', 'Segment\Urussetia\Projek\ProjekLaporanController@index');
        Route::post('/get-penilaian', 'Segment\Urussetia\Projek\ProjekLaporanController@getPenilaianKeputusan');
        Route::get('/keseluruhan', 'Segment\Urussetia\Projek\ProjekLaporanController@keseluruhan');
        Route::post('/keseluruhan', 'Segment\Urussetia\Projek\ProjekLaporanController@keseluruhan');
        Route::get('/pejabat', 'Segment\Urussetia\Projek\ProjekLaporanController@pejabat');
        Route::post('/pejabat', 'Segment\Urussetia\Projek\ProjekLaporanController@pejabat');
        Route::get('/analisis', 'Segment\Urussetia\Projek\ProjekLaporanController@analisis');
        Route::post('/analisis', 'Segment\Urussetia\Projek\ProjekLaporanController@analisis');
        Route::get('/status-penilaian', 'Segment\Urussetia\Projek\ProjekLaporanController@status_penilaian');
        Route::post('/status-penilaian', 'Segment\Urussetia\Projek\ProjekLaporanController@status_penilaian');
    });
});
Route::get('urussetia/projek/laporan/keseluruhan/pdf/{id}', 'Segment\Urussetia\Projek\ProjekLaporanController@keseluruhan_pdf')->name('urussetia/projek/laporan/keseluruhan/pdf');
Route::get('urussetia/projek/laporan/keseluruhan/excel/{id}', 'Segment\Urussetia\Projek\ProjekLaporanController@keseluruhan_excel')->name('urussetia/projek/laporan/keseluruhan/excel');
 
//KPA
Route::prefix('/kpa')->group(function () {
    Route::prefix('/penilaian')->group(function () {
        Route::get('/', 'Segment\Kpa\Projek\ProjectKpaController@index');
        Route::get('/list', 'Segment\Kpa\Projek\ProjectKpaController@penilaian_list');
        Route::post('/jawab', 'Segment\Kpa\Projek\ProjectKpaController@penilaian_jawab');
        Route::post('/jawab-get', 'Segment\Kpa\Projek\ProjectKpaController@penilaian_jawab_get');
        Route::get('/print/{projeks_tahuns_id}', 'Segment\Kpa\Projek\ProjectKpaController@penilaian_print');
        Route::post('/padam-preview', 'Segment\Kpa\Projek\ProjectKpaController@penilaian_padam_preview');
        Route::post('/ubah-tkh', 'Segment\Kpa\Projek\ProjectKpaController@ubah_tkh');
    });
});

Route::get('/kpa/penilaian/kpa-get-all-soalan/{projek_tahun_id}/{penilai_id}', 'Segment\Kpa\Projek\ProjectKpaController@penilaian_get_soalan');

//Common Controller
Route::prefix('/common')->group(function () {
    Route::prefix('/pengguna')->group(function () {
        Route::get('/carian', 'Main\CommonController@pengguna_carian');
        Route::post('/maklumat', 'Main\CommonController@pengguna_maklumat');
    });
});
